#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import csv
import json
import classifier.classifier as CSO

def extractPapers(file):
	papers = {}
	with open(file) as csvfile:
		reader = csv.reader(csvfile, delimiter="\t")
		for row in reader:
			papers[row[0]] = {"title":row[2], "abstract":row[6], "keywords":row[12]}
	csvfile.close()
	return papers

def extractCSOConceptsFromPaper(papers):
		for key,val in papers.items():
			if (val != ''):
				concepts = CSO.run_cso_classifier(val)
				filename = os.getcwd() + "/concepts/" + key + ".json"
				print(filename)
				with open(filename, 'w') as outfile:
					json.dump(concepts, outfile, indent=4)

def extractCSOFromOneID(papers,id):
		for key,val in papers.items():
			if (val != '' and key == id):
				concepts = CSO.run_cso_classifier(val)
				filename = os.getcwd() + "/concepts/" + key + ".json"
				print(filename)
				with open(filename, 'w') as outfile:
					json.dump(concepts, outfile, indent=4)

if __name__ == '__main__':
	papers = extractPapers("data/acl.database.annot.tsv")
	extractCSOConceptsFromPaper(papers)
	#extractCSOFromOneID(papers,"W08-2136")
