#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import csv
from pcu_keyphrase.pcu_keyphrase import extractKeyphrases
from pcu_keyphrase.pcu_keyphrase import serializeKeyphrases

def extractAbstracts(file):
	abstracts = {}
	with open(file) as csvfile:
		reader = csv.reader(csvfile, delimiter="\t")
		for row in reader:
			abstracts[row[0]] = row[6]
	csvfile.close()
	return abstracts

def extractKeyphrasesFromAbstracts(abstracts):
	for key,val in abstracts.items():
		if (val != ''):
			keyphrases = extractKeyphrases(val.replace("\"",""))
			filename = os.getcwd() + "/keyphrases/" + key + ".json"
			print(filename)
			serializeKeyphrases(keyphrases,filename)


if __name__ == '__main__':
	abstracts = extractAbstracts("data/acl.database.annot.tsv")
	abstracts.pop("Id")
	extractKeyphrasesFromAbstracts(abstracts)
