<article xmlns:xlink="http://www.w3.org/1999/xlink">
  <front>
    <journal-meta />
    <article-meta>
      <title-group>
        <article-title>Integrating Terms Hierarchy into Dirichlet Language Model</article-title>
      </title-group>
      <contrib-group>
        <contrib contrib-type="author">
          <string-name>Mohannad ALMasri</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>KianLam Tan</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>Jean-Pierre Chevallet</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>Philippe Mulhem</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>Catherine Berrut</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <aff id="aff0">
          <label>0</label>
          <institution>Universit ́e de Grenoble</institution>
        </aff>
      </contrib-group>
      <pub-date>
        <year>2014</year>
      </pub-date>
      <fpage>61</fpage>
      <lpage>70</lpage>
      <abstract>
        <p>Most Information retrieval systems (IRSs) use the intersection between document and query in order to retrieve relevant documents to a given query. Term mismatch problem appears when users use different terms form terms used in the index to express their needs. Indexing term specificity is one face of term mismatch problem where a user query contains more general indexing terms from terms in the index. In this paper, we present an approach to capture the specificity of terms by incorporating the hierarchical information between indexing terms into a Language Model. Experiments on different CLEF corpora from the medical domain show an improvement in retrieval performance. We show that this improvement is independent to the length of documents and queries within the tested collection.</p>
      </abstract>
    </article-meta>
  </front>
  <body>
    <sec id="sec-1">
      <title>Introduction</title>
      <p>
        Specificity is a semantic property that can be applied to index terms: an
indexing term1 is more or less specific as its meaning is more or less detailed and
precise. For instance, in the medical domain, the terms “B-Cell ” and “T-Cell ”
are more specific than “Lymphocyte”, or in other words, we say that “B-Cell ”
and “T-Cell ” are types of “Lymphocyte” in the adaptive immune system.
Therefore, when a user query contains the term “Lymphocyte”, then, a document talks
about “B-Cell ” or “T-Cell ” is relevant to this query. Another example from the
same domain, documents talk about “Veins” or “Arteries” are relevant to the
query “Blood Vessel ”, where “Veins” and “Arteries” are types of “Blood
Vessel ”. A retrieval model that depends on the intersection between document and
query cannot capture this kind of relation between indexing terms. In order to
take these hierarchical relations between query and document terms, we should
incorporate them in the retrieval model. We need an external knowledge or
resource 2 further from query and document to identify these hierarchical relations
between terms.
1 Indexing terms differ from system to another, so it can be : word, noun phrase,
n-gram, or concept [
        <xref ref-type="bibr" rid="ref5">5</xref>
        ].
2 like thesaurus or ontology
Classical indexing techniques represent documents and queries as a bag of
words or phrases without taking into account the semantics, the meaning or the
correlation between these words. The main disadvantage of these techniques is
that they depend on the text signal, and not on the meaning [
        <xref ref-type="bibr" rid="ref12 ref5">5, 12</xref>
        ]. For example,
in the medical domain, the two phrases “Atrial Fibrillation” and “Auricular
Fibrillation” have the same meaning. However, when we use phrases to represent
a document and a query, if one phrase appears in a document and a different
one appears in a query, that leads to mismatch problem. So over the last 20
years, several approaches attempted to use available knowledge bases and natural
language processing techniques in order to overcome this problem and produce
more meaningful answers [
        <xref ref-type="bibr" rid="ref1">1</xref>
        ]. These approaches represent documents and queries
by means of concepts. This representation is obtained using conceptual indexing.
Conceptual indexing is the process of mapping text into the concepts of an
external resource. Therefore, it needs a resource out of documents and queries
which contains concepts and the information about them. In our study, we use
concepts as indexing terms.
      </p>
      <p>
        In this paper, we consider the problem of concepts specificity within in the
Language Model (LM). Language Model for IR has been proven to be a very
effective method for text retrieval [
        <xref ref-type="bibr" rid="ref15 ref20">15, 20</xref>
        ]. The extension that we propose in this
paper is to integrate concept hierarchy into the Dirichlet language model. This
extension is easily applied to other smoothing methods. Our proposed method
has the following advantages: a) it is easy and simple to generate concept
similarity based on the hierarchy of concepts from an external resource b) we propose
a light weight integration in the Dirichlet smoothing that improves the retrieval
performance. The rest of this paper is organized as follows. Firstly, we present
the problem of term mismatch in Section 2. Then, we discuss several approaches
to solve the problem of term intersection in Section 3 followed by our approach
in Sections 4. Finally, we conclude our results and present the future work in
Sections 5 and 6.
2
      </p>
    </sec>
    <sec id="sec-2">
      <title>Term</title>
    </sec>
    <sec id="sec-3">
      <title>Mismatch Problem</title>
      <p>
        Several techniques [
        <xref ref-type="bibr" rid="ref13">13</xref>
        ] have been proposed to tackle term mismatch problem.
Among these techniques: relevance feedback [
        <xref ref-type="bibr" rid="ref11 ref17">11, 17</xref>
        ], dimension reduction [
        <xref ref-type="bibr" rid="ref10 ref16 ref2 ref7 ref8">16,
10, 7, 2, 8</xref>
        ], and statistical translation model [
        <xref ref-type="bibr" rid="ref3 ref9">3, 9</xref>
        ].
      </p>
      <p>
        Relevance feedback involves the user in the IR process in order to improve
the final result. There are three types of relevance feedback: 1) explicit feedback,
2) implicit feedback and 3) pseudo or blind feedback [
        <xref ref-type="bibr" rid="ref13">13</xref>
        ]. Rocchio algorithm [
        <xref ref-type="bibr" rid="ref17">17</xref>
        ]
is the classic algorithm for implementing explicit feedback which enables the user
to select relevant documents in order to reformulate the original query. Query
Reformulation is made by adding terms extracted from the selected documents.
Implicit feedback incorporates user behavior like clicks, in order to predict
relevant documents to reformulate the query, while blind feedback provides a method
for automatic local analysis. Blind feedback automates the manual part of the
Rocchio algorithm without an extended interaction with the user. This method
performs normal retrieval and finds an initial set of relevant documents and
makes the assumption that the top k ranked documents are the most relevant.
Lavrenko and Croft [
        <xref ref-type="bibr" rid="ref11">11</xref>
        ] proposed an approach to estimate a relevance model
with no training data. The main problem for implicit and explicit relevance
feedback is that they should rely on accurate ways of finding term relation in
order to avoid the problem of query drift.
      </p>
      <p>
        Dimension reduction is the process of reducing the number of data dimensions
that represents a query and a document in cases where the query and the
document refer to the same concept but using different terms. This can be achieved
by using thesaurus [
        <xref ref-type="bibr" rid="ref8">8</xref>
        ], concept based approach [
        <xref ref-type="bibr" rid="ref2">2</xref>
        ], stemming [
        <xref ref-type="bibr" rid="ref10 ref16">16, 10</xref>
        ], and
latent semantic indexing [
        <xref ref-type="bibr" rid="ref7">7</xref>
        ]. All these techniques proposed different strategies to
reduce the chances that the query and document refer to the same concept but
using different terms. In later development, Peng et al.[
        <xref ref-type="bibr" rid="ref14">14</xref>
        ] performed stemming
according to the context of the query which helps to improve the accuracy and
the performance of retrieval compared to the query independent stemmers such
as Porter[
        <xref ref-type="bibr" rid="ref18">18</xref>
        ] and Krovetz[
        <xref ref-type="bibr" rid="ref10">10</xref>
        ]. Deerwester et al.[
        <xref ref-type="bibr" rid="ref7">7</xref>
        ] proposed to solve the
dimension reduction by representing the terms and the documents in a latent semantic
space where the terms that are similar in the space tend to be the terms that
not only co-occur in the documents, but also appear in similar contexts.
      </p>
      <p>
        Statistical Translation Model is a model where all of the translations are
generated on the basis of a statistical models. This idea is based on information
theory where a model estimates the probability of translate a document to a
user query according to the probability distribution P (u|v), which gives the
probability that a word, v can be semantically translated to a word, u [
        <xref ref-type="bibr" rid="ref19 ref3">19, 3</xref>
        ].
Unfortunately, Statistical Translation Model requires training data and some
relevant query-document pairs where the documents are relevant to the query.
3
      </p>
    </sec>
    <sec id="sec-4">
      <title>Exploiting term similarity</title>
      <p>Most of the approaches to solve the problem of term mismatch described in
Section 2 faced the same problem: how to select the best term and assign the
best weight to the corresponding term?. No single solution has been proved to
be the best.</p>
      <p>
        Some approaches have been proposed using LM such as the work of
Karimzadehgan and Zhai [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ], Berger and Lafferty[
        <xref ref-type="bibr" rid="ref3">3</xref>
        ] who use Statistical Translation Model.
The main difference between these two works is that Berger and Lafferty try
to identify the most important concepts apears in a verbose query [
        <xref ref-type="bibr" rid="ref3">3</xref>
        ] while
Karimzadehgan and Zhai used mutual information to generate term links3 [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ].
      </p>
      <p>
        In some ways, we can consider that the proposed approach [
        <xref ref-type="bibr" rid="ref3 ref9">9, 3</xref>
        ] are related
to the proposition from Crestani [
        <xref ref-type="bibr" rid="ref6">6</xref>
        ] where the idea is to consider the
similarity between each query term and all document terms. The results obtained by
Karimzadehgan and Zhai [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ] showed that integrating the term similarity and LM
is more efficient and more effective than the existing approaches in information
retrieval.
3 Term links refer to the relationship between two terms in a vocabulary
However, Karimzadehgan and Zhai [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ] noticed that the self-translation
probabilities lead to non-optimal retrieval performance because it is possible that the
value of P (w|u) is higher than P (w|w) for a term, w. In order to overcome this
problem, Karimzadehgan and Zhai [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ] defined a parameter to control the effect
of the self-translation.
      </p>
      <p>In a nutshell, we can remark that 1) the normalization of the mutual
information is rather artificial and requires a parameter to control the effect of the
self-translation, and 2) the regularization of the initial transition probabilities
may look uncertain.
4</p>
    </sec>
    <sec id="sec-5">
      <title>Proposed Approach</title>
      <p>
        Our model uses concepts as indexing terms. In other words, queries and
documents are represented by concepts. Then, we use hierarchical relations from an
external resource to build the Concept Similarity Matrix. This matrix contains
semantic similarities between each two concepts computed from an external
resource. Our goal is to integrate the Concept Similarity Matrix into LM in order
to overcome the mismatch problem. After the reviews of Crestani [
        <xref ref-type="bibr" rid="ref6">6</xref>
        ],
Karimzadehgan and Zhai [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ] and Zhai [
        <xref ref-type="bibr" rid="ref19">19</xref>
        ], we propose the approach as shown below:
– In the case that there is a query concept does not appear in the
document(Mismatch): we consider the most similar document concept to this
query concept in the matching process. We use concept similarity matrix to
find the most similar concept.
– We propose to exploit hierarchical relations between concepts which is
defined in the external knowledge to define the semantic link between concepts
rather than probability approaches in order to avoid the problem of
selftranslation Karimzadehgan and Zhai [
        <xref ref-type="bibr" rid="ref9">9</xref>
        ].
      </p>
      <p>In order to build the Concept Similarity Matrix, we find the links between
all the vocabulary V which is the set of all concepts. We make the assumption
that the two concepts are considered to be linked to each other if both concepts
belong to the same hierarchy in the external resource. Assume a query concept
c, and c0 refers to a document concept:
c, c0 ∈ V, 0 ≤ Sim(c, c0) ≤ 1
(1)
1. Sim(c, c0) = 0, there is no link between the concept c and c0
2. Sim(c, c0) &lt; 1, there is a link between the concept c and c0
3. Sim(c, c0) = 1, there is an exact match between the concept c and c0
4.1</p>
      <sec id="sec-5-1">
        <title>Extended Dirichlet Smoothing</title>
        <p>
          The LM approach in IR is proposed by Ponte and Croft [
          <xref ref-type="bibr" rid="ref15">15</xref>
          ]. The basic idea of
LM is to assume that a query q, which is generated by a probabilistic model
based on a document d, as shown below:
(2)
(3)
(5)
        </p>
        <p>P (d|q) ∝ P (q|d).P (d)
∝ means that the two sides give the same ranking. P (q|d) the query likelihood
for the given document d matches with the query q. If we consider that every
document is equally relevant to any other query, then we can discard P (d) and
we can rewrite the formula after adding the log function as:
logP (d|q) = X #(c; q).logP (c|d)</p>
        <p>c∈V
where #(c; q) is the count of concept c in the query q and V is a set of vocabulary.
Assuming a multinomial distribution, the simplest way to estimate P (c|d) is the
maximum likelihood estimator:
#(c; d)</p>
        <p>Pml(c|d) = |d| (4)
where |d| is the total length of the document d. Due to the data spareness
problem, the maximum likelihood estimator directly assign null to the unseen
concept in a document. Smoothing is a technique to assign extra probability
mass to the unseen concept in order to solve this problem.</p>
        <p>
          Basically, Dirichlet [
          <xref ref-type="bibr" rid="ref21">21</xref>
          ] is one of the smoothing technique based on the
principle of adding an extra pseudo concept frequency: μP (c|C). The Dirichlet
smoothing is obtained by taking into account the extra pseudo concept frequency
distribution:
        </p>
        <p>Pμ(c|d) =
#(c; d) + μP (c|C)</p>
        <p>Pc #(c; d) + μ
where C is the whole collection. The main idea of this proposal is to integrate
links between concepts which are represented by Concept Similarity Matrix into
the current Dirichlet formula. First, we assume that for a query concept c ∈ q, c ∈/
d, there is a document concept c0 ∈ d can play its role during the matching
process. More specifically, we consider that if c does not occur in the initial
document d but occurs in the document dext, which is the result of extending d
according to the query and some knowledge 4, the probability of the concept c0
is defined according to the extended document dext.</p>
        <p>
          The knowledge provides a similarity function Sim : V × V → [
          <xref ref-type="bibr" rid="ref1">0, 1</xref>
          ], that
denotes the strength of the similarity between the two concepts (the larger the
value, the higher the similarity between these two concepts). We propose that:
∀c, c0 ∈ V, Sim(c, c0) = 1 if exact matching between c with c0, and ∀c, c0 ∈
V, Sim(c, c0) = 0 if c is not at all semantically related to c0.
        </p>
        <p>In order to avoid any complex extension, we assume that a query concept c,
must only impact occurrences of one document concept, so:
1. If a query concept c occurs in a document d, then the concept will not change
the length of the document.
4 The knowledge refers to the Concept Similarity Matrix
2. If a query concept c does not occur in a document d but the concept c
contains a link with c0 (concept from document), then we define: :
c∗ = argmaxc0∈dSim(c, c0)
as the concept from the document will serve as the basic count of the pseudo
occurrences of c in d:</p>
        <p>#(c∗; d).Sim(c, c∗)
this pseudo occurrences of the concept c are then included into the size of
the extended document.
3. If a query concept c does not occur in the document and does not show any
link to the document concepts , then this concept will not change the length
of the document as the first case.</p>
        <p>According to the previous three cases, the expression of |dext| can be done:
|dext| = |d| + X #(c∗; d).Sim(c, c∗)</p>
        <p>c∈q</p>
        <p>Note that we propose to extend the document according to the query. We
extend the document by query concepts which are not in the document but
they are linked to at least on document concept. Now, the extended Dirichlet
Smoothing leads to the following probability for the concept c of the vocabulary
V in the extended document dext according to a query q, and note that pμ(c|dext)
is defined as:
1. if c ∈ d ∩ q :
2. c ∈/ d ∩ q and if ∃c∗ ∈ d \ q; Sim(c, c∗) 6= 0 :</p>
        <p>Pμ(c|dext) =
#(c; d) + μP (c|C)</p>
        <p>|dext| + μ
Pμ(c|dext) =
#(c∗; d).Sim(c, c∗) + μP (c∗|C)</p>
        <p>|dext| + μ
with c∗ = argmaxc0∈dSim(c, c0) .
3. c ∈/ d ∩ q and if \∃c∗ ∈ d \ q; Sim(c, c∗) 6= 0</p>
        <p>Pμ(c|dext) =
#(c; d) + μP (c|C)
|dext| + μ
(6)
(7)
(8)
(9)
with c∗ = argmaxc0∈dSim(c, c0) .</p>
        <p>In the specific case where all the query concepts occur in the document, we
have |dext| = |d|, and that leads to pμ(c|d) = pμ(c|dext).
4.2</p>
      </sec>
      <sec id="sec-5-2">
        <title>Concept Similarity Matrix</title>
        <p>
          We propose to use a lightweight way to build the Concept Similarity Matrix
using the concept hierarchy from an external resource. The similarity between
two concepts is the inverse of a distance between these two concepts in the
concept hierarchy [
          <xref ref-type="bibr" rid="ref18">18</xref>
          ]. We use the path length or the number of links in the
hierarchy between two concepts as distance.
        </p>
        <p>The similarity score is inversely proportional to the number of nodes along
the shortest path between the concepts. The shortest possible path occurs when
the two concepts are directly linked. Thus, the maximum similarity value is 1:
Sim(c, c0) =</p>
        <p>1
distance(c, c0)
, distance(c, c0) &gt; 0
(10)</p>
        <p>We also tried other similarity metrics like Leacock and Resnik but we
obtained best performance improvement using this path metric.
5
5.1</p>
      </sec>
    </sec>
    <sec id="sec-6">
      <title>Experimental Setup</title>
      <sec id="sec-6-1">
        <title>Indexing Terms</title>
        <p>Documents and queries is mapped to UMLS concepts using MetaMap. UMLS is a
multi-source knowledge base in the medical domain, whereas, MetaMap is a tool
for mapping text to UMLS concepts. Using concepts allows us to investigate
the semantic relations between concepts, so it allows to build our Concepts
Similarity Matrix. To build this matrix, we only consider, the ISA relations
between concepts from the different UMLS concept hierarchies. If we have two
concepts in multiple concept hierarchies we consider the shortest path.
5.2</p>
      </sec>
      <sec id="sec-6-2">
        <title>Corpora 5.3</title>
      </sec>
      <sec id="sec-6-3">
        <title>Results</title>
        <p>
          Five corpora from CLEF are used. Table 1 shows some statistics about them.
– Image10, Image11, Image12: contain short medical documents and queries.
– Case2011, Case2012: contain long medical documents and queries.
All the experiments are conducted using the XIOTA engine [
          <xref ref-type="bibr" rid="ref4">4</xref>
          ]. The performance
was measured by Mean Average Precision (MAP). The approaches used for
experiments are as follows:
– DIR-BL (baseline): language model with Dirichlet smoothing.
– DIR-CSM: Extended Dirichlet smoothing after integrating the Concepts
Similarity Matrix(CSM).
        </p>
        <p>Results of our Dirichlet smoothing extension are summarized in Table 2. We
first observe the consistent performance improvement achieved for our five
target collections, which confirms our belief that integrating hierarchical relations
from an external resource improves relevance model estimation. Second, the
improvement occurs in the studied collection is independent the length of these
collection. It seems to be similar for both types of collection: 1) short documents
and short queries, 2) long documents and long queries.
We present a model to exploit the indexing term hierarchy in order to capture
the specificity of indexing terms during the retrieval time. Our experimental
re</p>
        <p>Corpus #Shared #Linked #Not linked
Image2010 2,138,561 668,148 11,607,361
Image2011 2,492,692 1,275,058 82,285,162</p>
        <p>Case2011 6,725,714 932,321 21,049,109
Image2012 2,874,031 1,556,122 91,169,348</p>
        <p>Case2012 27,940,254 3,681,351 78,255,835
sults indicate that the proposed approach to extend Dirichlet smoothing using
Concept Similarity Matrix based on hierarchical information from an external
resource in the medical domain is more effective than the term intersection
approach. This extension is suitable for any situation where such a kind of this
mutual information between indexing terms is available. For future work, we
would like to validate our extension using mutual information between indexing
terms extracted from other external resource and maybe in different ways rather
than hierarchical relations. In addition, we think that with more mutual
information we will have a higher degree of knowledge to build the link between two
indexing terms.</p>
      </sec>
    </sec>
  </body>
  <back>
    <ref-list>
      <ref id="ref1">
        <mixed-citation>
          1.
          <string-name>
            <given-names>Mustapha</given-names>
            <surname>Baziz</surname>
          </string-name>
          , Mohand Boughanem, and
          <string-name>
            <surname>Nathalie</surname>
          </string-name>
          Aussenac-Gilles.
          <article-title>Conceptual indexing based on document content representation</article-title>
          .
          <source>CoLIS'05</source>
          ,
          <year>2005</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref2">
        <mixed-citation>
          2.
          <string-name>
            <given-names>Michael</given-names>
            <surname>Bendersky</surname>
          </string-name>
          and
          <string-name>
            <given-names>W. Bruce</given-names>
            <surname>Croft</surname>
          </string-name>
          .
          <article-title>Discovering key concepts in verbose queries</article-title>
          .
          <source>SIGIR '08</source>
          , pages
          <fpage>491</fpage>
          -
          <lpage>498</lpage>
          , New York, NY, USA,
          <year>2008</year>
          . ACM.
        </mixed-citation>
      </ref>
      <ref id="ref3">
        <mixed-citation>
          3.
          <string-name>
            <given-names>Adam</given-names>
            <surname>Berger</surname>
          </string-name>
          and John Lafferty.
          <article-title>Information retrieval as statistical translation</article-title>
          .
          <source>SIGIR '99</source>
          , pages
          <fpage>222</fpage>
          -
          <lpage>229</lpage>
          , New York, NY, USA,
          <year>1999</year>
          . ACM.
        </mixed-citation>
      </ref>
      <ref id="ref4">
        <mixed-citation>
          4.
          <string-name>
            <surname>Jean-Pierre Chevallet</surname>
          </string-name>
          .
          <article-title>X-iota: An open xml framework for ir experimentation</article-title>
          . volume
          <volume>3411</volume>
          of Lecture Notes in Computer Science, pages
          <fpage>263</fpage>
          -
          <lpage>280</lpage>
          . Springer Berlin Heidelberg,
          <year>2005</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref5">
        <mixed-citation>
          5.
          <string-name>
            <surname>Jean-Pierre</surname>
            <given-names>Chevallet</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Joo-Hwee Lim</surname>
          </string-name>
          , and Diem Thi Hoang Le.
          <article-title>Domain knowledge conceptual inter-media indexing: Application to multilingual multimedia medical reports</article-title>
          .
          <source>CIKM '07</source>
          , pages
          <fpage>495</fpage>
          -
          <lpage>504</lpage>
          . ACM,
          <year>2007</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref6">
        <mixed-citation>
          6.
          <string-name>
            <given-names>Fabio</given-names>
            <surname>Crestani</surname>
          </string-name>
          .
          <article-title>Exploiting the similarity of non-matching terms at retrieval time</article-title>
          .
          <volume>2</volume>
          :
          <fpage>25</fpage>
          -
          <lpage>45</lpage>
          ,
          <year>2000</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref7">
        <mixed-citation>
          7.
          <string-name>
            <given-names>Scott</given-names>
            <surname>Deerwester</surname>
          </string-name>
          , Susan T. Dumais, George W. Furnas,
          <string-name>
            <surname>Thomas</surname>
            <given-names>K.</given-names>
          </string-name>
          <string-name>
            <surname>Landauer</surname>
          </string-name>
          , and Richard Harshman.
          <article-title>Indexing by latent semantic analysis</article-title>
          .
          <source>JOURNAL OF THE AMERICAN SOCIETY FOR INFORMATION SCIENCE</source>
          ,
          <volume>41</volume>
          (
          <issue>6</issue>
          ):
          <fpage>391</fpage>
          -
          <lpage>407</lpage>
          ,
          <year>1990</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref8">
        <mixed-citation>
          8.
          <string-name>
            <given-names>Yufeng</given-names>
            <surname>Jing</surname>
          </string-name>
          and
          <string-name>
            <given-names>W. Bruce</given-names>
            <surname>Croft</surname>
          </string-name>
          .
          <article-title>An association thesaurus for information retrieval</article-title>
          . pages
          <fpage>146</fpage>
          -
          <lpage>160</lpage>
          ,
          <year>1994</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref9">
        <mixed-citation>
          9.
          <string-name>
            <given-names>Maryam</given-names>
            <surname>Karimzadehgan and ChengXiang Zhai</surname>
          </string-name>
          .
          <article-title>Estimation of statistical translation models based on mutual information for ad hoc information retrieval</article-title>
          . pages
          <fpage>323</fpage>
          -
          <lpage>330</lpage>
          . ACM,
          <year>2010</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref10">
        <mixed-citation>
          10.
          <string-name>
            <given-names>Robert</given-names>
            <surname>Krovetz</surname>
          </string-name>
          .
          <article-title>Viewing morphology as an inference process</article-title>
          . pages
          <fpage>191</fpage>
          -
          <lpage>202</lpage>
          . ACM Press,
          <year>1993</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref11">
        <mixed-citation>
          11.
          <string-name>
            <given-names>Victor</given-names>
            <surname>Lavrenko</surname>
          </string-name>
          and
          <string-name>
            <given-names>W. Bruce</given-names>
            <surname>Croft</surname>
          </string-name>
          .
          <article-title>Relevance based language models</article-title>
          .
          <source>SIGIR '01</source>
          , pages
          <fpage>120</fpage>
          -
          <lpage>127</lpage>
          , New York, NY, USA,
          <year>2001</year>
          . ACM.
        </mixed-citation>
      </ref>
      <ref id="ref12">
        <mixed-citation>
          12.
          <string-name>
            <given-names>Jimmy</given-names>
            <surname>Lin</surname>
          </string-name>
          and Dina
          <string-name>
            <surname>Demner-Fushman</surname>
          </string-name>
          .
          <article-title>The role of knowledge in conceptual retrieval: a study in the domain of clinical medicine</article-title>
          .
          <source>SIGIR '06</source>
          , pages
          <fpage>99</fpage>
          -
          <lpage>106</lpage>
          ,
          <year>2006</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref13">
        <mixed-citation>
          13.
          <string-name>
            <surname>Christopher D. Manning</surname>
          </string-name>
          , Prabhakar Raghavan, and Hinrich Schu¨tze. Introduction to Information Retrieval. Cambridge University Press, New York, NY, USA,
          <year>2008</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref14">
        <mixed-citation>
          14.
          <string-name>
            <surname>Fuchun</surname>
            <given-names>Peng</given-names>
          </string-name>
          , Nawaaz Ahmed,
          <string-name>
            <given-names>Xin</given-names>
            <surname>Li</surname>
          </string-name>
          ,
          <string-name>
            <given-names>and Yumao</given-names>
            <surname>Lu</surname>
          </string-name>
          .
          <article-title>Context sensitive stemming for web search</article-title>
          .
          <source>SIGIR '07</source>
          , pages
          <fpage>639</fpage>
          -
          <lpage>646</lpage>
          , New York, NY, USA,
          <year>2007</year>
          . ACM.
        </mixed-citation>
      </ref>
      <ref id="ref15">
        <mixed-citation>
          15.
          <string-name>
            <surname>Jay</surname>
            <given-names>M.</given-names>
          </string-name>
          <string-name>
            <surname>Ponte</surname>
            and
            <given-names>W. Bruce</given-names>
          </string-name>
          <string-name>
            <surname>Croft</surname>
          </string-name>
          .
          <article-title>A language modeling approach to information retrieval</article-title>
          .
          <source>SIGIR '98</source>
          , pages
          <fpage>275</fpage>
          -
          <lpage>281</lpage>
          . ACM,
          <year>1998</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref16">
        <mixed-citation>
          16.
          <string-name>
            <given-names>M. F.</given-names>
            <surname>Porter</surname>
          </string-name>
          .
          <article-title>Readings in information retrieval. chapter An algorithm for suffix stripping</article-title>
          , pages
          <fpage>313</fpage>
          -
          <lpage>316</lpage>
          . Morgan Kaufmann Publishers Inc.,
          <year>1997</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref17">
        <mixed-citation>
          17. Gerard Salton, editor.
          <source>The SMART Retrieval System - Experiments in Automatic Document Processing</source>
          . Prentice Hall, Englewood, Cliffs, New Jersey,
          <year>1971</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref18">
        <mixed-citation>
          18.
          <string-name>
            <given-names>Dominic</given-names>
            <surname>Widdows</surname>
          </string-name>
          .
          <article-title>Geometry and Meaning. Center for the Study of Language and Inf</article-title>
          , November
          <year>2004</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref19">
        <mixed-citation>
          19.
          <string-name>
            <given-names>ChengXiang</given-names>
            <surname>Zhai</surname>
          </string-name>
          .
          <article-title>Statistical Language Models for Information Retrieval</article-title>
          . Now Publishers Inc., Hanover, MA, USA,
          <year>2008</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref20">
        <mixed-citation>
          20.
          <string-name>
            <given-names>Chengxiang</given-names>
            <surname>Zhai</surname>
          </string-name>
          and
          <string-name>
            <given-names>John</given-names>
            <surname>Lafferty</surname>
          </string-name>
          .
          <article-title>A study of smoothing methods for language models applied to information retrieval</article-title>
          .
          <volume>22</volume>
          (
          <issue>2</issue>
          ):
          <fpage>179</fpage>
          -
          <lpage>214</lpage>
          ,
          <year>April 2004</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref21">
        <mixed-citation>
          21.
          <string-name>
            <given-names>Chengxiang</given-names>
            <surname>Zhai</surname>
          </string-name>
          and
          <string-name>
            <given-names>John</given-names>
            <surname>Lafferty</surname>
          </string-name>
          .
          <article-title>A study of smoothing methods for language models applied to information retrieval</article-title>
          .
          <volume>22</volume>
          (
          <issue>2</issue>
          ):
          <fpage>179</fpage>
          -
          <lpage>214</lpage>
          ,
          <year>April 2004</year>
          .
        </mixed-citation>
      </ref>
    </ref-list>
  </back>
</article>