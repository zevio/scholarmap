<article xmlns:xlink="http://www.w3.org/1999/xlink">
  <front>
    <journal-meta />
    <article-meta>
      <title-group>
        <article-title>Une mesure de Similarit´e S´emantique bas´ee sur la Recherche d'Information</article-title>
      </title-group>
      <contrib-group>
        <contrib contrib-type="author">
          <string-name>Davide Buscaldi</string-name>
        </contrib>
      </contrib-group>
      <pub-date>
        <year>2013</year>
      </pub-date>
      <fpage>81</fpage>
      <lpage>91</lpage>
      <abstract>
        <p>Re´sume´ : Dans cet article, nous de´crivons une mesure de similarite´ se´mantique base´e sur la recherche d'information qui a e´te´ utilise´e dans le “shared task” SemEval 2013. Habituellement, la similarite´ se´mantique est utilise´e pour ame´liorer la performance des syste`mes de recherche d'information. Nous avons essaye´ de faire le contraire : deux textes a` comparer sont traite´s comme des requeˆtes et on compare les listes des re´sultats de la recherche afin d'e´tablir la similarite´ se´mantique des textes. Les re´sultats pre´liminaires obtenus dans le “shared task” SemEval 2013 montrent que cette mesure a fonctionne´ mieux que les mesures de similarite´ se´mantique base´es sur WordNet et des mesures classiques comme le cosinus ou la distance d'e´dition. Mots-cle´s : Similarite´ Se´mantique. Recherche d'Information.</p>
      </abstract>
    </article-meta>
  </front>
  <body>
    <sec id="sec-1">
      <title>Introduction</title>
      <p>
        La de´termination du niveau de similarite´ se´mantique entre textes est
une taˆche tre`s importante pour diffe´rentes applications dans le contexte du
Traitement Automatique de la Langue (TAL). Une des premie`res
applications de similitude de texte est, peut-eˆtre, le mode`le d’espace vectoriel
utilise´ dans la Recherche d’Information (RI), o u` il faut classer des
documents dans une collection par ordre de pertinence par rapport a` une requeˆte
en entre´e (
        <xref ref-type="bibr" rid="ref14">Salton (1971</xref>
        )). La pertinence est calcule´e en fonction d’une
mesure de similarite´ entre la requeˆte et le document, dont le contenu est
repre´sente´ par un vecteur de mots. Les mesures de similarite´ de texte ont
e´te´ e´galement utilise´es pour la de´sambigu¨ısation lexicale (
        <xref ref-type="bibr" rid="ref10">Lesk (1986</xref>
        )) et
le re´sume´ automatique de texte (
        <xref ref-type="bibr" rid="ref11">Lin &amp; Hovy (2003</xref>
        )), entre autres.
      </p>
      <p>
        Tre`s re´cemment, une e´valuation comparative pour la taˆche de
similarite´ se´mantique a e´te´ propose´e en tant que “pilot ta
        <xref ref-type="bibr" rid="ref16">sk” au SemEval 2012</xref>
        (Semantic Textual Similarity, STS) par
        <xref ref-type="bibr" rid="ref1">Agirre et al. (2012</xref>
        ) et confirme´e
pour SemEval 2013, e´leve´e ainsi au rang de “shared task” du *SEM2013 1,
dans le but d’e´tablir un cadre d’e´valuation qui favorise le de´veloppement
de nouveaux syste`mes et mesures de similarite´ se´mantique. L’objectif
final des campagnes d’e´valuation SemEval est de promouvoir la recherche
dans tous les aspects de la se´mantique computationnelle et d’ame´liorer
les performances dans toutes les taˆches ou` il est ne´cessaire d’analyser
se´mantiquement un texte de fac¸on automatique. Il s’agit de d e´terminer, sur
une e´chelle de 0 (aucune similarite´) a` 5 (meˆme signification), la similari te´
se´mantique entre couples de phrases. Cette particularite´ distingue la taˆche
STS de la taˆche de de´tection de paraphrases, dont la re´ponse a` la question
si deux phrases sont similaires est une re´ponse binaire.
      </p>
      <p>
        Dans cet article, nous pre´sentons une mesure de similarite´ se´mantique
qui utilise la RI pour de´terminer la similarite´ entre textes ; cette mesure
est base´e sur l’hypothe`se que, si on utilise deux textes en tant que requeˆtes
d’entre´e dans un meˆme syste`me de RI, alors leur similarite´ de´pend de la
similarite´ des listes des documents re´cupe´re´s par le syste`me. Cette mesure a
e´te´ ve´rifie´e expe´rimentalement dans le cadre de la participation de l’e´quipe
RCLN du LIPN au STS du SemEval 2013 (
        <xref ref-type="bibr" rid="ref5">Buscaldi et al. (2013</xref>
        )), ou` elle
s’est re´ve´le´e la mesure la plus efficace, parmi celles mis en oeuvre par le
syste`me LIPN. La suite du papier est articule´e comme suit : dans la Section
2, nous pre´sentons des travaux connexes. Dans la Section 3, nous de´crivons
notre mesure de similarite´ base´e sur la RI. Dans la Section 4, nous rendons
compte de l’expe´rimentation et de l’e´valuation mene´es. Enfin, dans la
Section 5, nous concluons et nous proposons quelques perspectives.
2
      </p>
    </sec>
    <sec id="sec-2">
      <title>Travaux connexes</title>
      <p>
        Traditionnellement, les mesures de similarite´ se´mantique ont e´te´ de´finies
sur des mots ou des concepts, mais non sur des fragments de texte. La
de´rivation d’une mesure de similarite´ se´mantique compositionnelle a`
partir d’une mesure de similarite´ se´mantique lexicale (c’est a` dire, de´terminer
la similarite´ entre deux phrases a` partir des mots qui les composent) n’est
pas triviale. Par exemple, les phrases un chien mord un homme et un
homme mord un chien ont la meˆme similarite´ se´mantique mot-a`-mot
mais sont tre`s diffe´rentes globalement. L’accent mis sur les mesures de
1. http://clic2.cimec.unitn.it/starsem2013/
similarite´ mot-a`-mot est probablement d uˆ a` la disponibilite´ de ressources
se´mantiques telles que WordNet ou des ontologies qui codent des relations
concept-a` concept.
        <xref ref-type="bibr" rid="ref12">Pedersen et al. (2004</xref>
        ) a imple´mente´ et mis a` disposition
un package, WordNet :Similarity 2, qui code diffe´rentes mesures de
similarite´ mot-a`-mot, souvent utilise´es dans plusieurs applications de TAL, par
exemple la de´tection de l’implication textuelle (
        <xref ref-type="bibr" rid="ref8">Harabagiu &amp; Hickl (2006</xref>
        ))
ou la re´solution de core´fe´rences (
        <xref ref-type="bibr" rid="ref13">Ponzetto &amp; Strube (2006</xref>
        )). La plupart des
syste`mes qui participent a` SemEval STS ont utilise´ des mesures classiques,
mode`le vectoriel, des extensions du mode`le de n-grammes (
        <xref ref-type="bibr" rid="ref3 ref6">Buscaldi et al.
(2012</xref>
        )), la distance d’e´dition, ou des combinaisons de mesures de
similarite´ se´mantique lexicale (
        <xref ref-type="bibr" rid="ref2 ref3">Banea et al. (2012</xref>
        );
        <xref ref-type="bibr" rid="ref3">Ba¨r et al. (2012</xref>
        );
        <xref ref-type="bibr" rid="ref16">Sˇaric´ et al.
(2012</xref>
        )). Les meilleurs re´sultats ont e´te´ obtenus en inte´grant les diffe´rentes
mesures avec des me´thodes de re´gression line´aire (
        <xref ref-type="bibr" rid="ref15">Sch o¨lkopf et al. (1999</xref>
        )).
3
      </p>
    </sec>
    <sec id="sec-3">
      <title>Mesure de Similarite´ Base´e sur la Recherche d’Information</title>
      <p>E´tant donne´s deux textes p and q, un syste`me de RI S et une collection
de documents D indexe´ par S, cette mesure est base´e sur l’hypothe`se que p
et q sont se´mantiquement similaires si les documents re´cupe´re´s par S pour
les deux textes utilise´s en tant que requeˆtes d’entre´e, sont classe´s de fac¸on
similaire.</p>
      <p>Soient Lp = {dp1 , . . . , dpK } et Lq = {dq1 , . . . , dqK }, dxi ∈ D les
ensembles des premiers K documents re´cupe´re´s par S pour p et q,
respectivement. Soient sp(d) et sq(d) les scores assigne´s par S au document d pour
les requeˆtes p et q, respectivement. Le score de similarite´ est calcule´ de la
fac¸on suivante :</p>
      <p>P √(sp(d)−sq(d))2
simIR(p, q) = 1 − d∈Lp∩Lq max(sp(d),sq(d))
|Lp ∩ Lq|
(1)
si |Lp ∩ Lq| 6= ∅, 0 en cas contraire.</p>
      <p>
        La valeur optimale de K a e´te´ de´termine´e avec des expe´riences mene´es
sur le jeux de donne´e
        <xref ref-type="bibr" rid="ref16">s de test du SemEval-2012</xref>
        , compose´e par 3108 couples
de phrases en anglais, avec des jugements de similarite´ gradue´s entre 0 et
5. Le score de similarite´ calcule´ avec la formule (1) est toujours compris
entre 0 et 1, donc pour pouvoir calculer la corre´lation avec le gold standard
(jugements de similarite´), le score calcule´ pour chaque couple de phrases
2. http://wn-similarity.sourceforge.net/
a e´te´ multiplie´ par 5. Les re´sultats en Figure 1 montrent que la valeur
optimale se situe entre 70 et 100. Nous avons utilise´ une valeur K = 80
      </p>
    </sec>
    <sec id="sec-4">
      <title>4 Expe´rimentation</title>
      <p>
        La mesure de similarite´ a e´te´ teste´e sur le
        <xref ref-type="bibr" rid="ref16">s jeux de donne´es
SemEval2012</xref>
        et SemEval-2013, et compare´e avec des mesures de similarite´
habituellement utilise´es dans la taˆche STS, de´crites dans la Section 4.1. La
taˆche STS consiste a` comparer deux phrases et de´terminer leur similarite´
se´mantique sur une e´chelle de 0 a` 5. Chaque couple de phrases a e´te´ juge´
par 4 experts, le jugement de similarite´ du gold standard est la moyenne
de tous les 4 jugements. La Figure 2 pre´sente des exemples de phrases a`
comparer, avec le jugement de corre´lation donne´ par les experts.
      </p>
      <p>
        La collection de documents utilise´e pour les expe´riences est en anglais
et compose´e par la collection AQUAINT-2 3 et la collection NTCIR-8 4. Le
moteur de recherche utilise´ est Lucene 5, plus pre´cise´ment dans sa version
4.2, et la mesure de similarite´ utilise´e pour calculer la similarite´ entre les
requeˆtes et les documents est la mesure BM25 (
        <xref ref-type="bibr" rid="ref9">Jones et al. (2000</xref>
        )). La
3. http://www.nist.gov/tac/data/data_desc.html#AQUAINT-2
4. http://metadata.berkeley.edu/NTCIR-GeoTime/
ntcir-8-databases.php
5. http://lucene.apache.org/core
valeur K a e´te´ mis a` 20 pour les expe´riences du SemEval-2013, pour des
raisons de rapidite´ (il y a un temps limite pour produire les re´sultats).
O u` la valeur idf (inverse document frequency) a e´te´ calcule´e sur Google
Web 1T.
o u` Lev(p, q) est la distance de Levenshtein entre p et q, au niveau des
caracte`res.
      </p>
      <p>simcos(p, q) =
n
P wpi × wqi
i=1
s n</p>
      <p>P wpi 2 ×
i=1
s n</p>
      <p>P wqi 2
i=1
4.1.3</p>
    </sec>
    <sec id="sec-5">
      <title>Mesure de similarite´ base´e sur WordNet</title>
      <p>
        Cette mesure de distance a e´te´ introduite pour
        <xref ref-type="bibr" rid="ref3 ref6">Buscaldi et al. (2012</xref>
        ).
Soient Cp et Cq les ensembles de concepts contenus dans les phrases p et
q, avec |Cp| ≥ |Cq|, alors la similarite´ entre p et q est me re´sultat de :
simW N (p, q) =
      </p>
      <p>
        P max s(c1, c2)
c1∈Cp c2∈Cq
|Cp|
o u` s(c1, c2) est une mesure de similarite´ conceptuelle calcule´e sur la hie´rarchie
de WordNet, selon la formulation de
        <xref ref-type="bibr" rid="ref17">Wu &amp; Palmer (1994</xref>
        ).
4.2
      </p>
    </sec>
    <sec id="sec-6">
      <title>Mesure de similarite´ base´e sur N-grammes</title>
      <p>
        Cette mesure a e´te´ propose´e par
        <xref ref-type="bibr" rid="ref4">Buscaldi et al. (2009</xref>
        ) pour la taˆche de
recherche de passages dans le contexte des syste`mes question-re´ponse. La
similarite´ entre p et q est calcule´e de la fac¸on suivante :
simngrams(p, q) = ∀x∈Q
      </p>
      <p>X h(x, P )</p>
      <p>1
d(x, xmax)
Pin=1 wi
o u` wi est le poids idf du mot ti, P est l’ensemble des n-grammes
compose´s par les mots de p qui sont aussi dans q , Q est l’ensemble de tous
les n-grammes possibles dans q, et n le nombre de mots dans la phrase
la plus longue. d(x,x1max) est un facteur de distance qui re´duit le poids des
n-grammes en fonction de la distance du plus grand n-gramme. Le poids
pour un n-gramme est calcule´ comme la somme des poids des mots qui le
composent :
h(x, Pj ) =
( Pj</p>
      <p>k=1 wk
0
if x ∈ Pj
autrement
o u` wk est le poids du k-ie`me mot et j la taille du n-gramme x ;
4.3</p>
      <p>E´valuation</p>
      <p>
        Le coefficient de corre´lation de Pearson a e´te´ calcule´ pour chaque
mesure, sur les jeux de donne´e
        <xref ref-type="bibr" rid="ref16">s de test du SemEval-2012</xref>
        (Table 1) et du
SemEval-2013 (Table 2). Le jeu de donne´e
        <xref ref-type="bibr" rid="ref16">s de test SemEval-2012</xref>
        est
compose´ par diffe´rents sous-ensembles de phrases extraites de diffe´rents
(4)
(5)
(6)
corpus : MSRpar est compose´ par des couples de phrases extraites du
corpus de paraphrases de Microsoft 6 ; MSRvid est un corpus compose´
par des annotations de vide´os ; OnWN est compose´ par des de´finitions de
WordNet ; Europarl est compose´ par des transcriptions de traductions
automatiques des sessions du parlement europe´en ; News, Headlines sont des
corpus compose´s par des titres d’actualite´s. Le jeu de donne´es
SemEval2013 contient aussi des de´finitions de frames de FrameNet (FNWN) et des
phrases issues de l’e´valuation automatique de syste`mes de traduction
automatique (SMT). La Table 4 pre´sente la taille, en nombre moyen de mots
pour chaque phrase, dans chaque jeu de donne´es.
      </p>
      <p>simngrams
simW N
simED
simcos
simIR</p>
      <p>simngrams
simW N
simED
simcos
simIR</p>
      <p>La Table 3 pre´sente les re´sultats du test d’ablation mene´ sur le syste`me
utilise´ pour la participation au SemEval-2013, ou` chaque mesure a e´te´
utilise´e en tant que caracte´ristique pour entraˆıner un mod e`le de re´gression
line´aire.</p>
      <p>Dans tous les tests on peut observer que la mesure base´e sur la RI, en
moyenne, s’est montre´e meilleure, surtout pour les corpus base´s sur les
6. http://research.microsoft.com/en-us/downloads/
607d14d9-20cd-47e3-85bc-a2f65cd28042/</p>
      <p>Mesure enleve´e
aucune
simngrams
simW N
simED
simcos
simIR
actualite´s (c’est a` dire, dans le meˆme domaine du corpus indexe´ par le
moteur de recherche), meˆme si pour certains sous-ensembles de donne´es
elle a obtenu des re´sultats non satisfaisants, en particulier sur les donne´es
MSRpar et FNWN. On a suppose´ que la taille moyenne des phrases joue un
roˆle dans la qualite´ des re´sultats ; apparemment, c’est le cas pour MSRpar
et FNWN qui ont une taille en moyenne double que la plupart des autres
corpus (Table 4), mais ce n’est pas si e´vident dans le cas du corpus SMT,
ou` la perte de performances ne semble pas en corre´lation avec la taille des
phrases.
5</p>
    </sec>
    <sec id="sec-7">
      <title>Conclusion et perspectives</title>
      <p>
        Dans ce papier, nous avons pre´sente´ une mesure de similarite´ se´mantique
entre deux textes a` partir d’une collection de documents indexe´ avec un
syste`me de RI. L’e´valuation de cette mesure sur les jeux de donne´e
        <xref ref-type="bibr" rid="ref16">s de la
taˆche SemEval STS 2012</xref>
        et 2013 montre qu’elle obtient, en moyenne, des
meilleures valeurs de corre´lation par rapport a` d’autres mesures de
similarite´ entre textes, base´es soit sur des caracte´ristiques de surface du texte,
soit sur la similarite´ conceptuelle entre concepts. Cependant, cette mesure
montre des limites, surtout si la taille des textes a` comparer augmente, et
si le corpus de documents indexe´s n’est pas dans le meˆme domaine que les
textes a` comparer. Nous souhaitons, en perspective, ve´rifier si la mesure
est inde´pendante du mode`le de RI utilise´ par le moteur de recherche, et
imple´menter la mesure sur le web, pour re´soudre le proble`me lie´ au
domaine du corpus indexe´. Dans ce cas, un proble`me additionnel a` re´soudre
est constitue´ par le poids a` donner a` chaque document re´cupe´re´.
      </p>
    </sec>
    <sec id="sec-8">
      <title>Remerciements</title>
      <p>Le travail pre´sente´ a e´te´ en partie soutenu par le LabEx EFL (Empirical
Foundations of Linguistics) et par le projet OSEO Quaero.</p>
    </sec>
    <sec id="sec-9">
      <title>Re´fe´rences</title>
    </sec>
  </body>
  <back>
    <ref-list>
      <ref id="ref1">
        <mixed-citation>
          <string-name>
            <surname>AGIRRE E.</surname>
          </string-name>
          ,
          <string-name>
            <surname>CER D.</surname>
          </string-name>
          ,
          <string-name>
            <given-names>DIAB M.</given-names>
            &amp;
            <surname>GONZALEZ</surname>
          </string-name>
          <string-name>
            <surname>A.</surname>
          </string-name>
          (
          <year>2012</year>
          ).
          <article-title>A pilot on semantic textual similarity</article-title>
          .
          <source>In Proceedings of the 6th International Workshop on Semantic Evaluation (SemEval</source>
          <year>2012</year>
          ),
          <source>in conjunction with the First Joint Conference on Lexical and Computational Semantcis (*SEM</source>
          <year>2012</year>
          ), Montreal, Quebec, Canada.
        </mixed-citation>
      </ref>
      <ref id="ref2">
        <mixed-citation>
          <string-name>
            <surname>BANEA C.</surname>
          </string-name>
          , HASSAN S.,
          <string-name>
            <given-names>MOHLER M.</given-names>
            &amp;
            <surname>MIHALCEA R.</surname>
          </string-name>
          (
          <year>2012</year>
          ).
          <article-title>UNT : A Supervised Synergistic Approach to Semantic Text Similarity</article-title>
          .
          <source>In Proceedings of the 6th International Workshop on Semantic Evaluation (SemEval</source>
          <year>2012</year>
          ), p.
          <fpage>635</fpage>
          -
          <lpage>642</lpage>
          , Montreal, Canada.
        </mixed-citation>
      </ref>
      <ref id="ref3">
        <mixed-citation>
          <string-name>
            <surname>B A¨R D.</surname>
            ,
            <given-names>BIEMANN C.</given-names>
          </string-name>
          ,
          <string-name>
            <given-names>GUREVYCH I.</given-names>
            &amp;
            <surname>ZESCH T.</surname>
          </string-name>
          (
          <year>2012</year>
          ).
          <article-title>UKP : Computing Semantic Textual Similarity by Combining Multiple Content Similarity Measures</article-title>
          .
          <source>In Proceedings of the 6th International Workshop on Semantic Evaluation (SemEval</source>
          <year>2012</year>
          ), p.
          <fpage>435</fpage>
          -
          <lpage>440</lpage>
          , Montreal, Canada.
        </mixed-citation>
      </ref>
      <ref id="ref4">
        <mixed-citation>
          <string-name>
            <surname>BUSCALDI D.</surname>
          </string-name>
          , ROSSO P.,
          <string-name>
            <given-names>G O</given-names>
            <surname>´MEZ J. M. &amp; SANCHIS E.</surname>
          </string-name>
          (
          <year>2009</year>
          ).
          <article-title>Answering questions with an n-gram based passage retrieval engine</article-title>
          .
          <source>Journal of Intelligent Information Systems (JIIS)</source>
          ,
          <volume>34</volume>
          (
          <issue>2</issue>
          ),
          <fpage>113</fpage>
          -
          <lpage>134</lpage>
          .
        </mixed-citation>
      </ref>
      <ref id="ref5">
        <mixed-citation>
          <string-name>
            <surname>BUSCALDI D.</surname>
          </string-name>
          ,
          <string-name>
            <surname>ROUX J. L.</surname>
          </string-name>
          ,
          <string-name>
            <surname>FLORES J. G. G.</surname>
          </string-name>
          &amp;
          <article-title>POPESCU</article-title>
          <string-name>
            <surname>A.</surname>
          </string-name>
          (
          <year>2013</year>
          ).
          <article-title>LIPNCORE : Semantic Text Similarity using n-grams, WordNet, Syntactic Analysis, ESA and Information Retrieval based Features</article-title>
          .
          <source>In Proceedings of the 7th International Workshop on Semantic Evaluation (SemEval</source>
          <year>2013</year>
          ), Atlanta,
          <string-name>
            <surname>GA</surname>
          </string-name>
          , USA. to appear.
        </mixed-citation>
      </ref>
      <ref id="ref6">
        <mixed-citation>
          <string-name>
            <given-names>BUSCALDI D.</given-names>
            , TOURNIER R.,
            <surname>AUSSENAC-GILLES N</surname>
          </string-name>
          . &amp;
          <string-name>
            <surname>MOTHE J.</surname>
          </string-name>
          (
          <year>2012</year>
          ).
          <article-title>Irit : Textual similarity combining conceptual similarity with an n-gram comparison method</article-title>
          .
          <source>In Proceedings of the 6th International Workshop on Semantic Evaluation (SemEval</source>
          <year>2012</year>
          ), Montreal, Quebec, Canada.
        </mixed-citation>
      </ref>
      <ref id="ref7">
        <mixed-citation>
          <string-name>
            <surname>DUDOGNON D.</surname>
          </string-name>
          ,
          <string-name>
            <surname>HUBERT G.</surname>
          </string-name>
          &amp;
          <article-title>RALALASON B</article-title>
          . (
          <year>2010</year>
          ).
          <article-title>Proxige´ne´a : Une mesure de similarite´ conceptuelle</article-title>
          .
          <source>In Proceedings of the Colloque Veille Strate´gique Scientifique et Technologique (VSST</source>
          <year>2010</year>
          ).
        </mixed-citation>
      </ref>
      <ref id="ref8">
        <mixed-citation>
          <string-name>
            <surname>HARABAGIU S. &amp; HICKL A.</surname>
          </string-name>
          (
          <year>2006</year>
          ).
          <article-title>Methods for using textual entailment in open-domain question answering</article-title>
          .
          <source>In Proceedings of the 21st International Conference on Computational Linguistics</source>
          and
          <article-title>the 44th annual meeting of the Association for Computational Linguistics</article-title>
          , ACL-
          <volume>44</volume>
          , p.
          <fpage>905</fpage>
          -
          <lpage>912</lpage>
          , Stroudsburg, PA, USA : Association for Computational Linguistics.
        </mixed-citation>
      </ref>
      <ref id="ref9">
        <mixed-citation>
          <string-name>
            <surname>JONES K. S.</surname>
          </string-name>
          , WALKER S. &amp;
          <string-name>
            <surname>ROBERTSON S. E.</surname>
          </string-name>
          (
          <year>2000</year>
          ).
          <article-title>A probabilistic model of information retrieval : development and comparative experiments part 2</article-title>
          .
          <string-name>
            <surname>Inf</surname>
          </string-name>
          . Process. Manage.,
          <volume>36</volume>
          (
          <issue>6</issue>
          ),
          <fpage>809</fpage>
          -
          <lpage>840</lpage>
          .
        </mixed-citation>
      </ref>
      <ref id="ref10">
        <mixed-citation>
          <string-name>
            <surname>LESK M.</surname>
          </string-name>
          (
          <year>1986</year>
          ).
          <article-title>Automatic sense disambiguation using machine readable dictionaries : how to tell a pine cone from an ice cream cone</article-title>
          .
          <source>In Proceedings of the 5th annual international conference on Systems documentation, SIGDOC '86</source>
          , p.
          <fpage>24</fpage>
          -
          <lpage>26</lpage>
          , New York, NY, USA : ACM.
        </mixed-citation>
      </ref>
      <ref id="ref11">
        <mixed-citation>
          <string-name>
            <surname>LIN C.-Y</surname>
          </string-name>
          . &amp;
          <string-name>
            <surname>HOVY E.</surname>
          </string-name>
          (
          <year>2003</year>
          ).
          <article-title>Automatic evaluation of summaries using n-gram co-occurrence statistics</article-title>
          .
          <source>In Proceedings of the 2003 Conference of the North American Chapter of the Association for Computational Linguistics on Human Language Technology - Volume 1, NAACL '03</source>
          , p.
          <fpage>71</fpage>
          -
          <lpage>78</lpage>
          , Stroudsburg, PA, USA : Association for Computational Linguistics.
        </mixed-citation>
      </ref>
      <ref id="ref12">
        <mixed-citation>
          <string-name>
            <given-names>PEDERSEN T.</given-names>
            , PATWARDHAN S. &amp;
            <surname>MICHELIZZI J.</surname>
          </string-name>
          (
          <year>2004</year>
          ).
          <article-title>WordNet : :Similarity : measuring the relatedness of concepts</article-title>
          .
          <source>In Demonstration Papers at HLT-NAACL</source>
          <year>2004</year>
          ,
          <article-title>HLT-NAACL-</article-title>
          <string-name>
            <surname>Demonstrations</surname>
          </string-name>
          '
          <volume>04</volume>
          , p.
          <fpage>38</fpage>
          -
          <lpage>41</lpage>
          , Stroudsburg, PA, USA : Association for Computational Linguistics.
        </mixed-citation>
      </ref>
      <ref id="ref13">
        <mixed-citation>
          <string-name>
            <given-names>PONZETTO S. P.</given-names>
            &amp;
            <surname>STRUBE</surname>
          </string-name>
          <string-name>
            <surname>M.</surname>
          </string-name>
          (
          <year>2006</year>
          ).
          <article-title>Exploiting semantic role labeling, wordnet and wikipedia for coreference resolution</article-title>
          .
          <source>In Proceedings of the main conference on Human Language Technology Conference of the North American Chapter of the Association of Computational Linguistics, HLT-NAACL '06</source>
          , p.
          <fpage>192</fpage>
          -
          <lpage>199</lpage>
          , Stroudsburg, PA, USA : Association for Computational Linguistics.
        </mixed-citation>
      </ref>
      <ref id="ref14">
        <mixed-citation>
          <string-name>
            <surname>SALTON G.</surname>
          </string-name>
          (
          <year>1971</year>
          ).
          <source>The SMART Retrieval System&amp;#8212 ;Experiments in Automatic Document Processing. Upper Saddle River</source>
          , NJ, USA : Prentice-Hall, Inc.
        </mixed-citation>
      </ref>
      <ref id="ref15">
        <mixed-citation>
          <string-name>
            <surname>SCH O¨LKOPF</surname>
            <given-names>B.</given-names>
          </string-name>
          , BARTLETT P., SMOLA A. &amp;
          <string-name>
            <surname>WILLIAMSON R.</surname>
          </string-name>
          (
          <year>1999</year>
          ).
          <article-title>Shrinking the tube : a new support vector regression algorithm</article-title>
          .
          <source>In Proceedings of the 1998 conference on Advances in neural information processing systems II</source>
          , p.
          <fpage>330</fpage>
          -
          <lpage>336</lpage>
          , Cambridge, MA, USA : MIT Press.
        </mixed-citation>
      </ref>
      <ref id="ref16">
        <mixed-citation>
          <string-name>
            <surname>SˇARI C´</surname>
          </string-name>
          <article-title>F</article-title>
          ., GLAVA Sˇ G., KARAN M.,
          <string-name>
            <given-names>SˇNAJDER J</given-names>
            . &amp;
            <surname>BA SˇI C´ B. D.</surname>
          </string-name>
          (
          <year>2012</year>
          ).
          <article-title>TakeLab : Systems for Measuring Semantic Text Similarity</article-title>
          .
          <source>In Proceedings of the 6th International Workshop on Semantic Evaluation (SemEval</source>
          <year>2012</year>
          ), p.
          <fpage>441</fpage>
          -
          <lpage>448</lpage>
          , Montreal, Canada.
        </mixed-citation>
      </ref>
      <ref id="ref17">
        <mixed-citation>
          <string-name>
            <given-names>WU Z.</given-names>
            &amp;
            <surname>PALMER</surname>
          </string-name>
          <string-name>
            <surname>M.</surname>
          </string-name>
          (
          <year>1994</year>
          ).
          <article-title>Verbs semantics and lexical selection</article-title>
          .
          <source>In Proceedings of the 32nd annual meeting on Association for Computational Linguistics, ACL '94</source>
          , p.
          <fpage>133</fpage>
          -
          <lpage>138</lpage>
          , Stroudsburg, PA, USA : Association for Computational Linguistics.
        </mixed-citation>
      </ref>
    </ref-list>
  </back>
</article>