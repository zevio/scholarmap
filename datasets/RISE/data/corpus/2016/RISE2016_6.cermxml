<article xmlns:xlink="http://www.w3.org/1999/xlink">
  <front>
    <journal-meta />
    <article-meta>
      <title-group>
        <article-title>Exploring Deep Learning for Query Expansion</article-title>
      </title-group>
      <contrib-group>
        <contrib contrib-type="author">
          <string-name>Mohannad ALMasri</string-name>
          <email>mohannad.almasri@imag.fr</email>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>Catherine Berrut</string-name>
          <email>catherine.berrut@imag.fr</email>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>Jean-Pierre Chevallet</string-name>
          <email>jean-pierre.chevallet@imag.fr</email>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <aff id="aff0">
          <label>0</label>
          <institution>Universit ́e Grenoble Alpes LIG laboratory, MRIM group</institution>
          ,
          <addr-line>Grenoble</addr-line>
          ,
          <country country="FR">France</country>
        </aff>
      </contrib-group>
      <fpage>26</fpage>
      <lpage>28</lpage>
    </article-meta>
  </front>
  <body>
    <sec id="sec-1">
      <title>-</title>
      <p>
        User queries are usually too short to describe the information need accurately.
Important terms can be missing from the query, leading to a poor coverage
of the relevant documents. To solve this problem, automatic query expansion
techniques leveraging on several data sources and employ di↵erent methods for
finding expansion terms [
        <xref ref-type="bibr" rid="ref2">2</xref>
        ]. Selecting expansion terms is challenging and requires
a framework capable of adding interesting terms to the query.
      </p>
      <p>
        Di↵erent approaches have been proposed for selecting expansion terms.
Pseudorelevance feedback (PRF) assumes that the top-ranked documents returned for
the initial query are relevant, and uses a sub set of the terms extracted from
those documents for expansion. PRF has been proven to be e↵ective in
improving retrieval performance [
        <xref ref-type="bibr" rid="ref4">4</xref>
        ].
      </p>
      <p>
        Corpus-specific approaches analyze the content of the whole document
collection, and then generate correlation between each pair of terms by co-occurrence
[
        <xref ref-type="bibr" rid="ref6">6</xref>
        ], mutual information [
        <xref ref-type="bibr" rid="ref3">3</xref>
        ], etc. Mutual information (MI) is a good measure to
assess how much two terms are related, by analyzing the entire collection in order
to extract the association between terms. For each query term, every term that
has a high mutual information score with it is used to expand the user query.
      </p>
      <p>
        Other approaches like semantic vectors and neural probabilistic language
models, propose a rich term representation in order to capture the similarity
between terms. In these approaches, a term is represented by a mathematical
object in a high dimensional semantic space which is equipped with a metric.
The metric can naturally encode similarities between the corresponding terms.
A typical instantiation of these approaches is to represent each term by a vector
and use a cosine or distance between term vectors in order to measure term
similarity [
        <xref ref-type="bibr" rid="ref1">1</xref>
        ][
        <xref ref-type="bibr" rid="ref7">7</xref>
        ][
        <xref ref-type="bibr" rid="ref8">8</xref>
        ].
      </p>
      <p>
        Recently, several ecient Natural Language Processing methods, based on
Deep Learning, are proposed to learn high quality vector representations of terms
from a large amount of unstructured text data with billions of words [
        <xref ref-type="bibr" rid="ref5">5</xref>
        ]. This
high quality vector representation captures a large number of term relationships.
We propose to investigate these term vector representations in query expansion.
We then experimentally compare this approach with two other expansion
approaches: pseudo-relevance feedback and mutual information.
      </p>
      <p>
        First step, term vector representations are learned from a large amount of
unstructured text data using Deep Learning. Several syntactic and semantic
relationships are captured within these term vectors [
        <xref ref-type="bibr" rid="ref5">5</xref>
        ]. Each term is represented
by a vector of a predefined dimension, a real-valued vector of a predefined
dimension, 600 dimensions for example. Cosine or distance similarity could be used to
measure the strength of semantic similarity between two term vectors, and to
list the top similar terms for a given query term.
      </p>
      <p>Second step, we collect top similar terms for each query term using their
vector representations. Then, top similar terms for each query term are used to
expand the user query.</p>
      <p>Experiments are conducted on four CLEF medical collections:
– Image2010, Image2011, Image2012: contain short documents and queries.
– Case2011: contains long documents and queries.</p>
      <p>
        We use language modeling framework to evaluate expanded queries. Two
smoothing methods of language models are tested: Jelinek-Mercer and Dirichlet.
We use word2vec to generate deep learning vectors [
        <xref ref-type="bibr" rid="ref5">5</xref>
        ]. The word2vec tool takes
a text corpus as input and produces the term vectors as output.
      </p>
      <p>We compare three expansion methods: expansion using term vector
representations (VEXP), Pseudo-relevance feedback (PRF), and Expansion using
mutual information (MI). We use language models with no expansion as a baseline
(NEXP).</p>
      <p>Results are summarized in Figure 1. Experimental results show that the
retrieval e↵ectiveness can be significantly improved over the ordinary language
models and pseudo-relevance feedback. We find that term vector representations,
extracted using deep learning, are promising source for query expansion.</p>
      <p>Deep learning vectors are learned from hundreds of millions of words, in
contrast to PRF which is obtained from top retrieved document and MI which
is calculated on the collection itself. Deep learning vectors are not only useful
for collections that were used in the training phase, but also for other collections
which contain similar documents.</p>
    </sec>
  </body>
  <back>
    <ref-list>
      <ref id="ref1">
        <mixed-citation>
          1.
          <string-name>
            <given-names>Yoshua</given-names>
            <surname>Bengio</surname>
          </string-name>
          , Holger Schwenk,
          <string-name>
            <surname>Jean-Sebastien</surname>
            <given-names>Sencal</given-names>
          </string-name>
          , Frederic Morin, and
          <string-name>
            <given-names>JeanLuc</given-names>
            <surname>Gauvain</surname>
          </string-name>
          .
          <article-title>Neural probabilistic language models</article-title>
          . volume
          <volume>194</volume>
          <source>of Studies in Fuzziness and Soft Computing</source>
          , pages
          <fpage>137</fpage>
          -
          <lpage>186</lpage>
          . Springer Berlin Heidelberg,
          <year>2006</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref2">
        <mixed-citation>
          2.
          <string-name>
            <given-names>Claudio</given-names>
            <surname>Carpineto</surname>
          </string-name>
          and
          <string-name>
            <given-names>Giovanni</given-names>
            <surname>Romano</surname>
          </string-name>
          .
          <article-title>A survey of automatic query expansion in information retrieval</article-title>
          .
          <source>ACM Comput. Surv.</source>
          ,
          <volume>44</volume>
          (
          <issue>1</issue>
          ):1:
          <fpage>1</fpage>
          -
          <lpage>1</lpage>
          :
          <fpage>50</fpage>
          ,
          <year>January 2012</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref3">
        <mixed-citation>
          3.
          <string-name>
            <given-names>Jiani</given-names>
            <surname>Hu</surname>
          </string-name>
          , Weihong Deng, and
          <string-name>
            <given-names>Jun</given-names>
            <surname>Guo</surname>
          </string-name>
          .
          <article-title>Improving retrieval performance by global analysis</article-title>
          .
          <source>In ICPR</source>
          <year>2006</year>
          ., pages
          <fpage>703</fpage>
          -
          <lpage>706</lpage>
          ,
          <year>2006</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref4">
        <mixed-citation>
          4.
          <string-name>
            <given-names>Victor</given-names>
            <surname>Lavrenko</surname>
          </string-name>
          and
          <string-name>
            <given-names>W. Bruce</given-names>
            <surname>Croft</surname>
          </string-name>
          .
          <article-title>Relevance based language models</article-title>
          .
          <source>SIGIR '01</source>
          , pages
          <fpage>120</fpage>
          -
          <lpage>127</lpage>
          , New York, NY, USA,
          <year>2001</year>
          . ACM.
        </mixed-citation>
      </ref>
      <ref id="ref5">
        <mixed-citation>
          5.
          <string-name>
            <given-names>T.</given-names>
            <surname>Mikolov</surname>
          </string-name>
          , I. Sutskever,
          <string-name>
            <given-names>K.</given-names>
            <surname>Chen</surname>
          </string-name>
          , G. Corrado, and
          <string-name>
            <given-names>J.</given-names>
            <surname>Dean</surname>
          </string-name>
          .
          <article-title>Distributed representations of words and phrases and their compositionality</article-title>
          .
          <source>CoRR</source>
          ,
          <year>2013</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref6">
        <mixed-citation>
          6.
          <string-name>
            <surname>Helen</surname>
            <given-names>J.</given-names>
          </string-name>
          <string-name>
            <surname>Peat</surname>
            and
            <given-names>Peter</given-names>
          </string-name>
          <string-name>
            <surname>Willett</surname>
          </string-name>
          .
          <article-title>The limitations of term co-occurrence data for query expansion in document retrieval systems</article-title>
          .
          <source>J. Am. Soc. Inf. Sci</source>
          ,
          <year>1991</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref7">
        <mixed-citation>
          7.
          <string-name>
            <given-names>Midori</given-names>
            <surname>Serizawa</surname>
          </string-name>
          and
          <string-name>
            <given-names>Ichiro</given-names>
            <surname>Kobayashi</surname>
          </string-name>
          .
          <article-title>A study on query expansion based on topic distributions of retrieved documents</article-title>
          . In Alexander Gelbukh, editor,
          <source>Computational Linguistics and Intelligent Text Processing</source>
          , volume
          <volume>7817</volume>
          of Lecture Notes in Computer Science, pages
          <fpage>369</fpage>
          -
          <lpage>379</lpage>
          . Springer Berlin Heidelberg,
          <year>2013</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref8">
        <mixed-citation>
          8.
          <string-name>
            <given-names>D.</given-names>
            <surname>Widdows</surname>
          </string-name>
          and
          <string-name>
            <given-names>T.</given-names>
            <surname>Cohen</surname>
          </string-name>
          .
          <article-title>The semantic vectors package: New algorithms and public tools for distributional semantics</article-title>
          .
          <source>In ICSC</source>
          , pages
          <fpage>9</fpage>
          -
          <lpage>15</lpage>
          ,
          <year>2010</year>
          .
        </mixed-citation>
      </ref>
    </ref-list>
  </back>
</article>