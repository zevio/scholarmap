<article xmlns:xlink="http://www.w3.org/1999/xlink">
  <front>
    <journal-meta />
    <article-meta>
      <title-group>
        <article-title>Extraction des informations d'entreprises</article-title>
      </title-group>
      <contrib-group>
        <contrib contrib-type="author">
          <string-name>A. Fotsoh Tawofaing</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>- A. Le Parc Lacayrelle</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>- C. Sallaberry</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <contrib contrib-type="author">
          <string-name>- T. Moal</string-name>
          <xref ref-type="aff" rid="aff0">0</xref>
        </contrib>
        <aff id="aff0">
          <label>0</label>
          <institution>Laboratoire LUIPPA</institution>
          ,
          <addr-line>BP-1155, 64013 PAU Université Cedex</addr-line>
          ,
          <country country="FR">France</country>
        </aff>
      </contrib-group>
      <fpage>8</fpage>
      <lpage>16</lpage>
      <abstract>
        <p>Searching information about local businesses is not a trivial problem to address. Most of existing services are supplied with manually recorded data. Based on the observation that more and more businesses are presented on the web, we propose in this paper a new approach, which consists to extract companies targeted information (addresses, activities, jobs, products, emails, fax) from websites, to supply a local businesses search service. The retrieval information module combines thematic, spatial and full-text criteria. A prototype of this service is implemented to experiment our proposal. MOTS-CLÉS : Analyse du web, Extraction d'informations spatiales, Extraction d'informations thématiques.</p>
      </abstract>
      <kwd-group>
        <kwd>Web mining</kwd>
        <kwd>Spatial information extraction</kwd>
        <kwd>Thematic information extraction</kwd>
      </kwd-group>
    </article-meta>
  </front>
  <body>
    <sec id="sec-1">
      <title>1. Introduction</title>
      <p>De plus en plus d’entreprises sont présentes sur le web et publient de nombreuses
informations relatives à leurs activités, leurs métiers, leurs produits et leurs
coordonnées (adresses, numéros de téléphone, numéros de fax, adresses mail). Partant de ce
constat, le projet Cognisearch Business vise à exploiter ces données pour offrir un
service de recherche d’entreprises capable de répondre à des besoins d’information
du type « charpente en chêne au sud de Poyartin ». Ce type de recherche comporte
une dimension thématique (charpente en chêne) et une dimension spatiale (au sud de
Poyartin).</p>
      <p>
        Plusieurs solutions sur le web proposent des services permettant de répondre à ce
type de recherche. Nous pouvons les classer en trois catégories principales : (i) les
fournisseurs de données, comme Factual1 ou Axciom2, qui collectent et
commercialisent les informations d’entreprises. Les informations alimentant ces services
proviennent généralement des «données ouvertes», du crawl du web ou même de
plateformes partenaires ; (ii) les annuaires, qui sont des bases de données
d’informations d’entreprises consultables en ligne. Dans cette catégorie, nous pouvons citer
des services comme Google Maps, Google My Business3, Pages Jaunes, ou même
société.com (annuaire d’entreprises françaises déclarées au registre du commerce) ;
(iii) les réseaux sociaux, qui sont beaucoup plus orientés partage d’informations et
d’appréciations portant sur les commerces, les places et les événements dans une
région. C’est le cas des services comme Yelp4, Foursquare5, Facebook Places6. Dans les
catégories (ii) et (iii) les données proviennent, en général, de contributions (salariés et
utilisateurs) et de fournisseurs de données. Cependant, ces solutions (i) ne permettent
pas de prendre en compte toutes les relations topologiques dans la dimension spatiale ;
(ii) peuvent mal interpréter la dimension thématique dans certains cas ; (iii) s’appuient
en grande partie sur des données saisies manuellement.
        <xref ref-type="bibr" rid="ref1">(Ahlers, 2013)</xref>
        propose un
système d’analyse de pages web pour enrichir la base de données entreprises des Pages
Jaunes. L’approche proposée consiste à croiser les données des Pages jaunes, avec
celles de l’annuaire DMOZ7 pour identifier les pages web associées à une entreprise
et en extraire des informations (adresses, numéros de téléphone, adresses mail,
données commerciales, données fiscales). Cependant, la proportion d’entreprises existant
dans une région et qui sont référencées sur DMOZ reste faible pour le cas de la France
(par exemple, on a 2580 entrées DMOZ au total pour la région d’Aquitaine alors qu’il
y a plus de 250000 entreprises déclarées au registre du commerce), d’où
l’enrichissement limité de la base de données.
1. http ://www.factual.com/
2. http ://www.acxiom.fr/
3. http ://www.google.com/business/
4. http ://www.yelp.fr/
5. http ://fr.foursquare.com/
6. http ://fr-fr.facebook.com/places/
7. http ://www.dmoz.org/
      </p>
      <p>
        Notre objectif est de construire un service de recherche d’information
d’entreprises qui s’appuie sur des données publiées sur le web et qui combine des critères
spatiaux (prenant en compte différentes relations topologiques) et des critères
thématiques avec la recherche plein texte. Notre proposition se distingue des services
existants par son indépendance vis à vis des données enregistrées manuellement ; en
effet, notre service est alimenté uniquement par des données publiques et des
informations extraites du web. Ce service se veut également plus précis dans l’interprétation
de la zone spatiale couverte par les besoins d’informations en prenant en compte les
différentes relations topographiques
        <xref ref-type="bibr" rid="ref8">(Vaid et al., 2005)</xref>
        exprimées dans les requêtes.
A la différence de
        <xref ref-type="bibr" rid="ref1">(Ahlers, 2013)</xref>
        , les sites web d’entreprises à partir desquels se fait
l’extraction d’information sont identifiés par une heuristique à partir des données du
registre du commerce. Cette démarche permet d’avoir un corpus plus exhaustif que
celui construit à partir de l’annuaire DMOZ. Par ailleurs, au delà des données de
localisation (adresses), nous extrayons également les métiers, produits et activités
d’entreprises.
      </p>
      <p>La suite de l’article est structurée comme suit. La section 2 présente l’architecture
générale de notre service. La section 3 explique le processus de constitution de notre
corpus de sites web. La section 4 détaille l’annotation de ces sites, tandis que la section
5 présente l’indexation des données. Le prototype développé est décrit en section 6.
La section 7 montre une première évaluation. Enfin, la section 8 conclut l’article et
présente les perspectives envisagées.</p>
    </sec>
    <sec id="sec-2">
      <title>2. Architecture du service Cognisearch Businness</title>
      <p>Nous proposons un service de recherche d’informations d’entreprises qui
s’appuie uniquement sur des données publiées sur le Web. Nous avons défini un modèle
de représentation des entités entreprise qui est composé de deux parties. La première
correspond aux données d’immatriculation (de base) des entreprises (nom officiel,
numéro SIRET, . . .). La seconde partie est composée des données extraites des sites
web des entreprises qui sont relatives à ses activités, produits et métiers.
L’architecture de ce service est décrite figure 1. Une première étape consiste à identifier sur le
Web les pages publiées par les entreprises (figure 1.1). Une fois le corpus de pages
web constitué, une étape d’annotation (figure 1.3) permet d’en extraire
automatiquement les informations relatives aux activités, aux produits, aux métiers ainsi qu’aux
coordonnées de chaque entreprise (adresses, numéros de téléphones, emails, fax). Les
informations annotées viennent compléter les données d’immatriculation des
entreprises afin de constituer un premier index d’informations d’entreprises. En parallèle,
le texte des sites web est indexé afin de permettre la recherche plein-texte (figure 1.6).</p>
    </sec>
    <sec id="sec-3">
      <title>3. Filtrage de sites web</title>
      <p>Les données d’immatriculation, telles que renseignées au registre du commerce
sont issues automatiquement de la ressource societe.com, et constituent les données de
base d’une entité entreprise. Un traitement basé sur une heuristique combine ces
données pour filtrer le web et retrouver, s’il existe, le site web associé à chaque entreprise
(figure 1.1). Nous avons testé plusieurs combinaisons sur 300 compagnies françaises.
Les annuaires d’entreprises et les réseaux professionnels ont été répertoriés dans une
liste (appelée liste noire) afin d’être exclus lors de la recherche. L’algorithme retenu
est le suivant. Pour chaque entreprise du registre du commerce, nous commençons par
interroger le web en combinant son nom commercial (si il existe) avec sa localisation.
Nous conservons, parmi les trois sites les plus pertinents, le premier qui n’est pas dans
la liste noire. Si tous les sites sont dans la liste noire, nous recommençons le même
principe mais en combinant cette fois le nom officiel de l’entreprise avec sa
localisation. Si de nouveau, les trois premiers sites trouvés sont dans la liste noire, nous en
concluons que l’entreprise n’a pas de sites web. L’ensemble des sites web ainsi obtenu,
constitue le corpus d’entrée pour le processus d’annotation (figure 1.3).</p>
    </sec>
    <sec id="sec-4">
      <title>4. Annotation des sites web</title>
      <p>L’étape d’annotation (figure 1.3) permet d’extraire automatiquement des
informations thématiques et spatiales relatives aux différentes entreprises. En ce qui concerne
les informations spatiales, très peu de sites web utilisent les microformats ou des
balises particulières pour les définir. Les adresses peuvent être situées n’importe où dans
la page et les différentes informations qui les composent ne sont pas écrites forcément
dans le même ordre.</p>
      <sec id="sec-4-1">
        <title>4.1. Extraction d’informations thématiques</title>
        <p>Afin d’annoter automatiquement les métiers, les produits et les activités
contenues dans les sites web, nous avons construit, par transformation de modèles, trois
ressources de type connaissance (sous forme d’ontologies au format OWL)
décrivant, respectivement, les activités, les produits et les métiers d’entreprises. Ces
ressources ont été construites à partir des deux hiérarchies d’organisation des activités
et des produits définies par l’INSEE8 (NAF9 et CPF10) et de la hiérarchie définie par
Pôle Emploi qui organise les métiers et emplois en catégories socio-professionnelles
(ROME11). Le choix de ces ressources se justifie notamment par le niveau de finesse
dans la description des différentes catégories.</p>
        <p>
          L’ontologie relative aux activités ainsi construite est pauvre en vocabulaire. Nous
avons de ce fait, mis en oeuvre un processus pour son enrichissement de façon
semiautomatique (figure 1.2). Ce processus utilise l’apprentissage pour regrouper les
expressions des sites web qui sont communes à une catégorie précise d’activité. Pour
cela, chaque classe de la hiérarchie des activités est associée à un ensemble de sites
web d’entreprises. Un algorithme de clustering permet de mettre en évidence les
expressions communes à tous les sites web d’une classe. Ces expressions constituent un
vocabulaire potentiel pour enrichir la ressource. Une phase de validation par un expert
intervient par la suite pour sélectionner les expressions pertinentes. L’algorithme de
clustering que nous avons utilisé dans notre proposition est Latent Dirichlet Allocation
(LDA) illustré dans
          <xref ref-type="bibr" rid="ref3">(Blei et al., 2003)</xref>
          .
        </p>
        <p>L’extraction d’informations relatives aux métiers, aux produits et aux activités
d’entreprises est réalisée en utilisant les ontologies construites. Chaque syntagme
nominal, trouvé dans le texte de la page web, est annoté de l’identifiant de la classe
associée dans l’ontologie correspondante.</p>
        <p>En ce qui concerne les emails, les numéros de téléphone et numéros de fax, des
patrons sont utilisées pour leur extraction. Ces patrons ont été mis au point à partir de
l’observation d’un échantillon de sites web. Par exemple, le patron permettant
d’extraire les emails est le suivant :
Email ! Login("@"|"at")N omDomaine("."|"(dot)")ExtensionDomaine</p>
      </sec>
      <sec id="sec-4-2">
        <title>4.2. Extraction d’informations spatiales</title>
        <p>
          Deux approches principales peuvent être utilisées pour l’annotation automatique
des adresses : une première utilise des patrons d’extraction (
          <xref ref-type="bibr" rid="ref5">(Borges et al., 2007)</xref>
          ,
          <xref ref-type="bibr" rid="ref4">(Blohm, 2011)</xref>
          ,
          <xref ref-type="bibr" rid="ref2">(Ahlers et Boll, 2008)</xref>
          ) et une deuxième s’appuie sur les techniques
d’apprentissage (
          <xref ref-type="bibr" rid="ref6">(Loos et Biemann, 2008)</xref>
          ,
          <xref ref-type="bibr" rid="ref7">(Taghva et al., 2005)</xref>
          ). Nous avons choisi
8. http ://www.insee.fr/fr/
9. Nomenclature Française des Activités
10. Classification des Produits Française
11. Répertoire Opérationnel des Métiers et des Emplois
        </p>
        <p>Noms de champs
CA : Complément Adresse
INV : Introducteur Nom Voie
BP : Boite Postale
CS : Course Spéciale
NC : Numéro Courrier
NVo : Nom Voie</p>
        <p>Exemples
Résidence Rigaud
Avenue
BP 1167
CS 2587
CEDEX 01
Avenue de l’université</p>
        <p>Noms de champs
CP : Code Postal
C : Commune
NV : Numéro Voie
D : Département
P : Pays</p>
        <p>Exemples
64000
Pau
10 ter
Gers
France
une approche basée sur les patrons d’extraction. Ces approches exploitent en général
des gazetiers répertoriant les noms de toutes les voies, le noms des villes, . . .. Dans le
contexte français, une ressource complète contenant tous les noms de voie n’est pas
disponible en accès libre. Une des difficultés est donc d’identifier le nom de la voie
dans une adresse. En effet, il peut y avoir un ou plusieurs compléments qui peuvent
être positionnés avant et/ou après le nom de la voie. Le tableau 1 répertorie les
différentes informations que l’on peut trouver dans une adresse française.</p>
        <p>L’observation d’un échantillon de 160 sites web d’entreprises a permis de définir
des patrons, dont voici un extrait :</p>
        <p>Adresse ! CA? ((BP CS) | (CS BP ) | BP | CS)? NV ? NV o CA?
((BP CS) | (CS BP ) | BP | CS)? ((CP C) | (C CP )) NC? D? P ?</p>
        <p>NC? D? P ?
Adresse ! CA? ((BP CS) | (CS BP ) | BP | CS)? ((CP C) | (C CP ))
Adresse ! CA? ((BP CS) | (CS BP ) | BP | CS)? NV ? NV o CA?</p>
        <p>((BP CS) | (CS BP ) | BP | CS)? CP NC? D? P ?</p>
        <p>Dans le premier patron, le nom de voie, le code postal et la commune sont
obligatoires, et peuvent être complétés par d’autres informations («10 Rue du Maréchal
Foch, 49000 Angers», «Résidence des Aubiers, 3e Étage, 14 ter Rue de la République,
64000 Pau, France»). Cette forme est la plus fréquente dans l’échantillon (75% des
adresses). Pour le deuxième patron, le code postal et la commune sont obligatoires et
le nom de voie est absent («Résidence Rigaud 33350 Mouliets-et-Villemartin»). Le
patron 3 permet entre autre d’identifier le cas où on a un code postal et un nom de
voie sans commune («10 Place de la République, F-33600»). Cette dernière forme est
assez rare (moins de 4% des adresses de l’échantillon).</p>
      </sec>
    </sec>
    <sec id="sec-5">
      <title>5. Indexation</title>
      <p>Les annotations sont extraites des pages web pour la construction des entités finales
à indexer. Ces annotations sont rajoutées aux données d’immatriculation de chaque
entreprise ainsi que l’adresse du site web correspondant. Les coordonnées géolocalisées
correspondant à chaque adresse extraite sont calculées avant l’ajout. L’entité finale est
stockée dans un index (figure 1.6). De plus, une opération parallèle consiste à extraire
le contenu textuel des pages de chaque site web d’entreprise et à l’indexer.</p>
      <p>Les index ainsi construits sont utilisés pour répondre à des besoins d’information
qui supportent des critères d’interrogation multidimensionnels et exploitent les
caractéristiques spatiales, thématiques et plein texte contenues dans les index (figure 1.7).</p>
    </sec>
    <sec id="sec-6">
      <title>6. Prototype</title>
      <p>Un premier prototype mettant en oeuvre notre approche a été développé. Il traite
des données relatives aux entreprises de la région Aquitaine pour laquelle 254 000
entreprises ont été identifiées. Parmi ces entreprises, nous nous sommes intéressés
uniquement à celles traitant de 6 domaines d’activités : commerce, construction,
hébergement &amp; restauration, enseignement, information &amp; communication et activités
scientifiques &amp; techniques. Ceci a réduit la liste initiale à 115 000 entreprises. Le
module de filtrage de sites web a permis d’identifier un site pour 22 000 d’entre elles. Le
corpus d’analyse relatif à ces 22 000 entreprises est constitué de 550 000 pages web.
Il a été constitué en utilisant l’outil Nutch12 de la fondation Apache.</p>
      <p>Le module d’annotation des sites web utilise la plateforme GATE13 couplée avec le
framework HADOOP14 pour gérer la volumétrie et la scalabilité du processus. Cette
étape a permis d’annoter 30 000 adresses, 44 000 labels d’activités, 12 500 labels
de produits et 28 000 labels de métiers. Les entités entreprises, construites à partir
des annotations extraites, ont été indexées sous Elasticsearch15, ainsi que le texte des
pages web. Les deux index ainsi construits (index des entités entreprises et index plein
texte) ont une taille globale de l’ordre de 3 GB.</p>
    </sec>
    <sec id="sec-7">
      <title>7. Évaluation</title>
      <p>
        Nous avons fait une première évaluation du processus d’annotation pour les
adresses et les activités. Etant donné qu’il n’existe pas de campagnes d’évaluation pour
ce type d’entité, nous nous sommes inspirés du processus utilisé dans TREC
        <xref ref-type="bibr" rid="ref9">(Voorhees
et Harman, 2005)</xref>
        . Les résultats sont résumés dans le tableau 2. Le rappel et la
précision sont calculés pour chaque page, les valeurs globales de ces métriques étant
calculées en faisant la moyenne arithmétique. La F1-mesure est évaluée en faisant la
moyenne harmonique des métriques globales obtenues pour l’ensemble de
l’échantillon.
12. http ://nutch.apache.org/
13. http ://gate.ac.uk/
14. http ://hadoop.apache.org/
15. http ://www.elastic.co/
      </p>
      <p>Adresses
Activités</p>
      <p>Pertinentes
309
1131</p>
      <p>En ce qui concerne les adresses, notre échantillon est constitué de 240 pages web
contenant au moins une adresse. 309 adresses ont été annotées par un expert, tandis
que notre annotateur en a trouvé 286. Parmi ces 286, seules 251 sont jugées
pertinentes, 13 étant incomplétement annotées. Ces résultats s’expliquent, d’une part, par
le fait que nous avons considéré, dans nos patrons d’extraction, que toutes les adresses
avaient obligatoirement un code postal, ce qui n’est pas le cas dans notre échantillon.
D’autre part, certaines villes sont mal orthographiées.</p>
      <p>En ce qui concerne les activités, notre échantillon est constitué de 100 pages web
relatives à des entreprises travaillant dans le domaine de la toiture (charpente,
couverture, zinguerie, ...). 1131 activités ont été annotées par un expert tandis que notre
annotateur en a trouvé 1119. Parmi ces 1119, seules 631 sont pertinentes. Ces résultats
s’expliquent par un défaut de vocabulaire dans l’ontologie des activités en dépit de son
enrichissement. Par exemple, l’expert va annoter «charpente traditionnelle en chêne»
alors que l’ontologie contient seulement le label «charpente en chêne».</p>
    </sec>
    <sec id="sec-8">
      <title>8. Conclusion</title>
      <p>Nous avons présenté dans cet article la première étape de la construction du
service de recherche géo-localisé d’informations portant sur les entreprises. Cette étape
est centrée sur l’extraction des informations sur le web et leur restructuration dans des
index. Ce processus s’appuie sur plusieurs problématiques de recherche différentes,
qui deviennent complémentaires dans le processus de construction des entités
entreprises. Il s’agit notamment de l’apprentissage qui est utilisé pour l’enrichissement des
ressources de type connaissance. De plus, le processus d’annotation de texte
combine les approches basées sur les patrons d’extraction et celles exploitant les bases
de connaissances. Il permet d’extraire les adresses des sites malgré les différences de
formes que l’on peut trouver. Un premier prototype mettant en oeuvre notre approche
a été développé. Il montre la faisabilité et l’intérêt de l’approche.</p>
      <p>Une deuxième étape consistera à exploiter ces index pour répondre à des besoins
d’information combinant les critères spatiaux, thématiques et plein texte. Une
évaluation du service avec un jeu de requêtes représentatif sera également menée au terme
de cette dernière étape.</p>
    </sec>
    <sec id="sec-9">
      <title>9. Bibliographie</title>
    </sec>
  </body>
  <back>
    <ref-list>
      <ref id="ref1">
        <mixed-citation>
          <string-name>
            <surname>Ahlers D.</surname>
          </string-name>
          , «
          <article-title>Business entity retrieval and data provision for yellow pages by local search »</article-title>
          ,
          <source>Workshop of Integrating IR technologies for Professional Search</source>
          ,
          <year>2013</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref2">
        <mixed-citation>
          <string-name>
            <given-names>Ahlers D.</given-names>
            ,
            <surname>Boll</surname>
          </string-name>
          <string-name>
            <surname>S.</surname>
          </string-name>
          , «
          <article-title>Retrieving Address-based Locations from the Web »</article-title>
          ,
          <source>International Workshop on Geographic Information Retrieval (GIR)</source>
          , ACM, New York, NY, USA, p.
          <fpage>27</fpage>
          -
          <lpage>34</lpage>
          ,
          <year>2008</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref3">
        <mixed-citation>
          <string-name>
            <given-names>Blei D. M.</given-names>
            ,
            <surname>Ng</surname>
          </string-name>
          <string-name>
            <given-names>A. Y.</given-names>
            ,
            <surname>Jordan</surname>
          </string-name>
          <string-name>
            <surname>M. I.</surname>
          </string-name>
          , «
          <article-title>Latent dirichlet allocation »</article-title>
          ,
          <source>Journal of machine Learning research</source>
          , vol.
          <volume>3</volume>
          , p.
          <fpage>993</fpage>
          -
          <lpage>1022</lpage>
          ,
          <year>2003</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref4">
        <mixed-citation>
          <string-name>
            <surname>Blohm S.</surname>
          </string-name>
          ,
          <article-title>Large-scale pattern-based information extraction from the world wide web</article-title>
          ,
          <source>KIT Scientific Publishing</source>
          ,
          <year>2011</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref5">
        <mixed-citation>
          <string-name>
            <surname>Borges</surname>
            <given-names>K. A. V.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Laender</surname>
            <given-names>A. H. F.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Medeiros</surname>
            <given-names>C. B.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Davis Jr. C.</surname>
          </string-name>
          <article-title>A</article-title>
          ., «
          <article-title>Discovering Geographic Locations in Web Pages Using Urban Addresses (GIR) »</article-title>
          ,
          <source>International Workshop on Geographical Information Retrieval (GIR)</source>
          , ACM, p.
          <fpage>31</fpage>
          -
          <lpage>36</lpage>
          ,
          <year>2007</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref6">
        <mixed-citation>
          <string-name>
            <surname>Loos</surname>
            <given-names>B.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Biemann</surname>
            <given-names>C.</given-names>
          </string-name>
          , «
          <article-title>Supporting web-based address extraction with unsupervised tagging », Data Analysis</article-title>
          ,
          <source>Machine Learning and Applications</source>
          , p.
          <fpage>577</fpage>
          -
          <lpage>584</lpage>
          ,
          <year>2008</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref7">
        <mixed-citation>
          <string-name>
            <surname>Taghva</surname>
            <given-names>K.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Coombs</surname>
            <given-names>J. S.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Pereda</surname>
            <given-names>R.</given-names>
          </string-name>
          ,
          <string-name>
            <surname>Nartker</surname>
            <given-names>T. A.</given-names>
          </string-name>
          , «
          <article-title>Address extraction using hidden markov models »</article-title>
          ,
          <source>International Symposium on Electronic Imaging Science and Technology (IST/SPIE)</source>
          , p.
          <fpage>119</fpage>
          -
          <lpage>126</lpage>
          ,
          <year>2005</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref8">
        <mixed-citation>
          <string-name>
            <given-names>Vaid S.</given-names>
            ,
            <surname>Jones</surname>
          </string-name>
          <string-name>
            <given-names>C. B.</given-names>
            ,
            <surname>Joho</surname>
          </string-name>
          <string-name>
            <given-names>H.</given-names>
            ,
            <surname>Sanderson</surname>
          </string-name>
          <string-name>
            <surname>M.</surname>
          </string-name>
          , «
          <article-title>Spatio-textual Indexing for Geographical Search on the Web »</article-title>
          ,
          <source>International Conference on Advances in Spatial and Temporal Databases</source>
          ,
          <source>SSTD'05</source>
          , Berlin, Heidelberg, p.
          <fpage>218</fpage>
          -
          <lpage>235</lpage>
          ,
          <year>2005</year>
          .
        </mixed-citation>
      </ref>
      <ref id="ref9">
        <mixed-citation>
          <string-name>
            <given-names>Voorhees E. M.</given-names>
            ,
            <surname>Harman</surname>
          </string-name>
          <string-name>
            <surname>D. K.</surname>
          </string-name>
          ,
          <source>TREC : Experiment and Evaluation in Information Retrieval</source>
          , The MIT Press,
          <year>2005</year>
          .
        </mixed-citation>
      </ref>
    </ref-list>
  </back>
</article>