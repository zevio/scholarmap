
























Quel  cadre  d'évaluation  pour  la  RI 
sémantique ?

Haïfa Zargayouna

Laboratoire d’Informatique de l’université Paris-Nord (LIPN) - UMR 7030
Université Paris 13 - CNRS
haifa.zargayouna@lipn.univ-paris13.fr   

RÉSUMÉ.   La recherche d'information sémantique (RIS) a pour but de dépasser les limites  
d'une  recherche   classique  par  mots-clés.  De  plus  en  plus  de  travaux  s'intéressent  à  
l'exploitation  de  ressources  sémantiques  (ontologies,  terminologies,  thésaurii,  etc.)  pour  
améliorer l'accès à l'information.  Ces travaux sont à l'intersection de  deux communautés:  
recherche d'information et  Web sémantique.  Ils s'intéressent  entre autres  à des questions  
d'annotation et indexation sémantique, désambiguïsation, extraction d'information (termes,  
entités nommées), expansion de requêtes, etc.
Malgré  les  expériences accumulées,  il  est   encore difficile  de  dresser  un  bilan.  Ceci  est  
principalement dû à l'absence d'une base de test commune qui permettrait de comparer entre 
eux les différents algorithmes et méthodes. 
Cette présentation fait un état des lieux des différentes expériences d'évaluation qui peuvent  
être réutilisées et montre les spécificités de l'évaluation de l'apport d'une « sémantique »  à  
la recherche d'information. 
Une première expérience d'évaluation sur un corpus de recettes de cuisine est présentée. Elle  
permet de dresser un premier bilan.   
Un  effort  commun  est  nécessaire  pour  pouvoir  établir  un  cadre  ouvert  et partagé  pour  
l'évaluation  et  qui  aiderait  à  mesurer  les  avancées  mais  aussi  à  mettre en  commun  les 
algorithmes et les ressources. La prise en compte d'une sémantique est très liée à la qualité  
des ressources,  au domaine,  à  la  langue,  etc.  L'évaluation  de l'ensemble reste une tâche  
difficile  et  il  est  nécessaire  d'identifier les briques  de  base  d'un  moteur  de  recherche  
sémantique pour évaluer leurs apports respectifs.
A l'instar d'autres bancs de test d'évaluation, cette évaluation devrait suivre un cycle de vie 
itératif  (spécifications,  mise  en  place  et  recalibrage)  qui  permettrait  d'enrichir  
incrémentalement la base de tests .

MOTS-CLÉS  :   évaluation,  recherche  d'information  sémantique,  ressources,  mesures,  
campagnes

29

C
em

O
A

 : 
ar

ch
iv

e 
ou

ve
rte

 d
'Ir

st
ea

 / 
C

em
ag

re
f




