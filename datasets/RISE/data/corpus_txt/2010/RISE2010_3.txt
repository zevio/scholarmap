
























40     Recherche d’Information SEmantique 
 

Classification supervisée sémantique 
d’articles de presse en français 
 

 
Samuel Gesche1, Elöd Egyed-Zsigmond1, Sylvie Calabretto1, Guy 
Caplat2, Jean Beney2 

 
 

1 Université de Lyon, CNRS, INSA-Lyon, LIRIS, UMR5205, F-69621, France 
20, avenue Albert Einstein, 69621 Villeurbanne Cedex 
prenom.nom@insa-lyon.fr 
 
2 INSA-Lyon, LCI, F-69621, France 
20, avenue Albert Einstein 69621 Villeurbanne Cedex 
prenom.nom@insa-lyon.fr 
 
Résumé. La classification supervisée est un champ de recherche fertile, qui ne dispose pas 
encore d’une théorie figée. Elle se nourrit donc régulièrement d’avancées dans d’autres 
domaines. Nous présentons ici nos récentes expériences en classification supervisée de textes 
courts de presse francophone. Ces expériences tirent parti des bases sémantiques généralistes 
de grande taille qui ont été récemment mises en place dans le cadre du Web sémantique. 
Nous effectuons un enrichissement sémantique de nos documents afin de pallier à leur petite 
taille qui rend les approches classiques inefficaces. L’utilisation de ressources multilingues 
nous permet de tirer partie des ressources disponibles en anglais. 

Mots-clés: classification supervisée, enrichissement sémantique, textes courts 

Abstract. Supervised document classification is a research field where a unified theory has 
yet to be found. Therefore, advances in other fields can often be used in order to get better 
results in some case or another. In this context, we present our latest experiences with large-
scale semantic databases. We use these to semantically enrich small French texts for which 
statistical methods show a poor performance. Besides, the use of multilingual resources 
allows us to circumvent the fact that most resources are in English language.  

Keywords: supervised classification, semantic enrichment, small texts 



Recherche d’Information SEmantique  41 

 

1   Introduction 

La classification supervisée de documents est un champ de recherches ancien, 
mais toujours actif : il n’existe pas encore de théorie unifiée permettant de définir 
l’algorithme optimal pour ranger des documents dans les classes suivant une 
problématique donnée. Cependant, de nombreuses et diverses approches sont 
disponibles, de l’optimisation par interface du rangement manuel aux algorithmes 
par apprentissage et au traitement automatique de la langue. 

Les avancées récentes du Web Sémantique ont par ailleurs conduit à la 
compilation de grandes bases de données, souvent structurées sous formes 
d’ontologie. Or ces bases de données sont désormais multilingues, et peuvent être 
utilisées sur des matériaux en français là où la plupart des bases classiques sont 
limitées à l’anglais. Cela nous a amené à explorer l’utilisation de ces ressources 
comme catalyseurs d’une classification supervisée. Plus précisément, nous partons 
de documents de petite taille en français (titre et résumé de quelques dizaines de 
mots), à ranger dans des classes décrites de manière similaire (étiquette et descriptif) 
; nous utilisons donc ces ressources sémantiques pour enrichir le contenu de ces 
documents et exemples afin de pouvoir raisonnablement escompter des 
recoupements. 

Nous présenterons donc ici une approche de classification supervisée par 
enrichissement sémantique. Nous commencerons par présenter le contexte de notre 
recherche. Ensuite, nous présenterons le concept de nuage pondéré de lemmes, qui 
forme le centre de notre approche, ainsi que le processus d’enrichissement 
sémantique qui permet de les constituer. Enfin, nous présenterons nos premiers 
résultats, avant donner nos conclusions sur cette étude. 

2. Contexte 

2.1   Le projet IPRI 

Le projet IPRI15, auquel nous apportons notre contribution en tant que 
chercheurs en informatique, a pour objectif l’analyse du pluralisme et de la 
redondance dans la presse en ligne. 

En effet, il existe des initiatives nationales en France qui garantissent le 
pluralisme de la presse écrite (comme le contrôle du temps de parole par le CSA16), 
mais rien n'existe de comparable pour la presse en ligne. La position officielle est 
                             
15 Internet : Pluralisme et Redondance de l’Information, projet ANR jeunes chercheuses et 
jeunes chercheurs de 2009 à 2012, site web hébergé sur http://liris.cnrs.fr/IPRI 
16 Conseil Supérieur de l’Audiovisuel 



42     Recherche d’Information SEmantique 
 

que l’Internet est un lieu naturel de pluralisme ([1], [2]). Cependant, il s’agit aussi 
d’un lieu naturel de copie [3], et pas seulement en ce qui concerne les œuvres 
artistiques. Cette dernière thèse est renforcée par des études qualitatives qui ont 
pointé vers un risque de redondance du fait du métier de rédacteur Web [4]. Afin de 
comprendre le phénomène, nous menons actuellement une étude complète, et 
notamment avec un volet quantitatif pour donner du poids aux analyses plus 
qualitatives. 

 A cette fin, il nous est nécessaire, en tant que partenaires informaticiens, de 
fournir les outils d’analyse quantitative et qualitative nécessaires à cette étude. Nous 
avons donc mis en place un mécanisme de collecte d'un échantillon à peu près 
exhaustif de la production éditoriale d’actualités générales et politiques17 sur 
Internet, échantillon défini par nos partenaires en Communication18. Nous 
travaillons désormais à la structuration de cette collection, qui est une étape 
nécessaire. Cette structuration passe par une classification des articles au sein de 
thématiques. 

2.2   Positionnement 

L’étude de la presse en ligne est devenue ces derniers temps un enjeu de 
recherche phénoménal. En effet, les agrégateurs classiques laissent de côté beaucoup 
de problématiques, à commencer par la fouille de données. L’enjeu est réel, 
puisqu’il consiste à créer la consommation journalistique de demain. En effet, les 
agrégateurs ont déjà amené un effacement progressif du concept de média au profit 
de celui de sujet. Il est donc important pour les médias qui veulent conserver leur 
visibilité d’offrir un service supplémentaire à celui de l’actualité (et la visite des 
archives est un bon départ), et pour les futurs remplaçants des agrégateurs d’offrir ce 
que voudra l’internaute de demain. Ainsi, on peut voir aujourd’hui un certain 
nombre de projets basés sur l’utilisation de ressources sémantiques pour la fouille de 
corpus d’archives comme ceux de l’AFP [5], la BBC [6] ou le New York Times [7]. 
Parallèlement, des initiatives telles que MediaCloud19 ou des entreprises comme 
Linkfluence [8] proposent une approche transversale, multi-sites, mais centrée plus 
particulièrement sur l’actualité. L’INA ayant obtenu le dépôt légal du Web en 
France, leurs équipes de recherche travaillent elles aussi sur la question de la 
structuration d’un tel corpus. 

Notre approche est plus modeste : nous ne comptons pas offrir à l’internaute de 
demain l’actualité, mais offrir des réponses fiables à la question du pluralisme. Nous 
ne voulons pas explorer en profondeur un média donné, mais acquérir une vision 
exhaustive de l’offre –et de la consommation, mais c’est un autre sujet– 

                             
17 Cette catégorie, qui correspond à une labellisation officielle, est le principal champ 
d’investigation des études concernant le pluralisme. 
18 Notamment, les laboratoires ELICO de l’Université de Lyon et LERASS de Toulouse. 
19 http://www.mediacloud.org/ 



d’information. De ce fait, nous nous concentrons sur l’acquisition de la publication 
de l’ensemble des sites d’information générale et politique, mais nous ne pouvons 
nous offrir ni l’accès à 
partenariats. Nous travaillons donc à une troisième alternative, celle de tirer le 
maximum de ce qui est disponible directement du le Web 
Nous avons donc des textes courts is
HTML pour lesquels l’hétérogénéité structurelle est la règle. Nous exposons ici 
comment nous traitons l’information de

3   Approche 

L’enrichissement sémantique implique trois entités différentes : 

− Les articles sous forme d’entrées RSS ; 
− La taxonomie selon laquelle classer ces articles
− Les ressources sémantiques qui permettent d’effectuer la classification.
L’approche en elle

(ceux des articles et ceux de la taxonomie) et à les enrichir pour former ce que nous 
appelons des nuages pondérés de lemmes (
article à une thématique de la taxonomie est ensuite calculé à partir de l’intersection 
de ces nuages, par analyse vectorielle.

 

 

Figure 1. Schéma général du procédé de classification par enrichissement 
sémantique 

Recherche d’Information SEmantique  

d’information. De ce fait, nous nous concentrons sur l’acquisition de la publication 
de l’ensemble des sites d’information générale et politique, mais nous ne pouvons 
nous offrir ni l’accès à toutes les bases de données, ni un nombre inconsidéré de 
partenariats. Nous travaillons donc à une troisième alternative, celle de tirer le 
maximum de ce qui est disponible directement du le Web –et donc à l’internaute. 
Nous avons donc des textes courts issus des flux RSS, des articles sous forme 
HTML pour lesquels l’hétérogénéité structurelle est la règle. Nous exposons ici 
comment nous traitons l’information de ces flux RSS. 

L’enrichissement sémantique implique trois entités différentes :  

s articles sous forme d’entrées RSS ;  

La taxonomie selon laquelle classer ces articles ; 

Les ressources sémantiques qui permettent d’effectuer la classification.

L’approche en elle-même consiste à prendre les textes courts de part et d’autre 
icles et ceux de la taxonomie) et à les enrichir pour former ce que nous 

appelons des nuages pondérés de lemmes (figure 1). Le degré d’appartenance d’un 
article à une thématique de la taxonomie est ensuite calculé à partir de l’intersection 

par analyse vectorielle. 

Schéma général du procédé de classification par enrichissement 

Recherche d’Information SEmantique  43 

d’information. De ce fait, nous nous concentrons sur l’acquisition de la publication 
de l’ensemble des sites d’information générale et politique, mais nous ne pouvons 

toutes les bases de données, ni un nombre inconsidéré de 
partenariats. Nous travaillons donc à une troisième alternative, celle de tirer le 

et donc à l’internaute. 
sus des flux RSS, des articles sous forme 

HTML pour lesquels l’hétérogénéité structurelle est la règle. Nous exposons ici 

Les ressources sémantiques qui permettent d’effectuer la classification. 

même consiste à prendre les textes courts de part et d’autre 
icles et ceux de la taxonomie) et à les enrichir pour former ce que nous 

). Le degré d’appartenance d’un 
article à une thématique de la taxonomie est ensuite calculé à partir de l’intersection 

 



44     Recherche d’Information SEmantique 
 

3.1   Lemmatisation 

Puisque nous manipulons, dans les trois entités, des termes au sein de textes 
construits, nous avons pris le parti d’effectuer une lemmatisation et un étiquetage 
lexical de ces termes. Cela nous permet d’effectuer une comparaison des termes sans 
devoir recourir à une analyse de similarité coûteuse en termes de temps. La 
lemmatisation est effectuée à la collecte pour les articles, et une fois pour toutes en 
ce qui concerne la taxonomie et les ressources sémantiques. 

La lemmatisation diminue grandement le nombre de mots que nous avons à 
manipuler tout en n’introduisant que peu de bruit (les ambiguïtés, polysémies et 
homonymies). L’étiquetage lexical des termes nous permet de nous focaliser, en 
plus de cela, sur certaines natures de mots (par exemple les noms), ce qui réduit 
d’autant plus la charge. 

Puisque nous n’effectuons nos calculs de catégorisation que sur des formes 
lemmatisées et étiquetées, dans la suite de notre propos, nous parlerons de lemmes et 
non plus de  termes. 

Notre approche se fonde sur la provenance des lemmes et sur leur nature. Nous 
faisons abstraction de tout lien que des lemmes pourraient avoir entre eux (par 
exemple, faire partie d’un même texte, ou être en succession l’un de l’autre). Nous 
bâtissons donc, à mesure que l’enrichissement progresse, des ensembles, ou nuages, 
de lemmes. 

Un article ou une thématique est de ce fait décrite par un nuage contenant non 
seulement ses lemmes propres, mais également les lemmes obtenus par 
enrichissement. 

3.2   Enrichissement 

La présence de ressources sémantiques colossales est une donne relativement 
récente dans le monde de la recherche. En effet, depuis quelques années, nous 
assistons à la concrétisation du Web Sémantique par la constitution de gigantesques 
ressources. Cela fait longtemps qu’existent des ressources de petite taille, comme 
des ontologies de domaine ; en revanche, la récupération toujours plus exhaustive de 
l’encyclopédie Wikipedia [9] au sein de bases sémantiques généralistes comme 
DBPedia [10] et Yago ([11], [12]) forme une grande avancée. Ces ressources 
peuvent être interrogées en ligne (pour des usages modérés) ou téléchargées sous 
forme de bases de triplets (pour une utilisation intensive). 

L’intérêt majeur de ces bases, dans notre cas, est qu’elles indexent une 
encyclopédie multilingue et sont donc, par essence, multilingues. Elles nous 
permettent donc de nous affranchir de la problématique de la traduction. Par ailleurs, 



Recherche d’Information SEmantique  45 

la gratuité de ces sources, et leur disponibilité directe par téléchargement ou 
interrogation, nous permet de nous tenir à jour relativement facilement20. 

Nous avons testé quatre procédés d’enrichissement sémantique différents à partir  
de ces ressources sémantiques. 

Enrichissement par généralisation.  Les taxonomies, même quand elles ont un 
descriptif, utilisent des termes généraux, alors que les articles utilisent des termes 
spécifiques de la problématique traitée. Ainsi, l’actualité parlera de « Michaël 
Jackson » ou de « fuite de produit chimique toxique » tandis que la taxonomie 
disposera des étiquettes « Rock » et « Accident industriel ». 

L’idée est donc, puisque les ontologies commencent à fleurir sur Internet, et en 
particulier celles qui sont liées aux préoccupations des internautes, de remonter dans 
l’ontologie jusqu’à trouver le concept optimal pour le classement. 

DBPedia, par exemple, fournit non seulement un ensemble colossal de concepts 
tirés des pages Wikipedia, mais les organise en plus au sein d’une ontologie. 
Cependant, cette ontologie est encore embryonnaire et orientée principalement sur 
les personnes, les lieux et les œuvres artistiques, qui sont les préoccupations 
majeures du Web Sémantique. Cette approche est donc peu efficace avec la base 
DBpedia elle-même. 

Enrichissement par catégorisation. Une deuxième manière d’effectuer 
l’enrichissement de lemmes, si l’on trouve un concept dans une ressource 
sémantique tirée de Wikipedia, consiste à utiliser le réseau de catégories que fournit 
l’encyclopédie. En effet, dans un certain nombre de cas, les pages Wikipedia ont été 
regroupées au sein de listes thématiques. Par exemple, on pourra relier une œuvre 
artistique à la liste des œuvres de son auteur, de son année, de son mouvement 
artistique, et ce ne sont que quelques exemples. Toutes ces données peuvent être 
adjointes aux données initialement contenues dans le texte. 

Enrichissement par spécialisation. De manière symétrique, dès lors que les 
catégories thématiques de la taxonomie sont suffisamment étoffées, nous pouvons 
décider de rester aux thématiques générales (politique, économie, société, sport etc.) 
au lieu de chercher la thématique précise. Dans ce contexte, il est intéressant 
d’enrichir ces thématiques générales par le contenu sémantique additionnel des 
sous-thématiques. Ainsi, le champ du sport, par exemple, contiendra le nom de tous 
les sports médiatiques. 

Dans ce cas, il est important de tenir compte des disparités d’enrichissement. 
Supposons en effet, comme c’est le cas dans la taxonomie de l’IPTC, que le 
domaine du sport contienne quelques dizaines de sous-thématiques, alors qu’un 
autre domaine, comme les faits divers, n’en contiennent que quelques unes. 
Beaucoup d’articles relevant du fait divers risquent d’être catégorisés comme sport, 

                             
20 Cependant, le coût de la lemmatisation de ces bases limite quelque peu l’intérêt de mises à 
jour fréquentes. 



46     Recherche d’Information SEmantique 
 

à cause du seul nombre des termes sportifs qui peuvent s’y trouver. Cette 
préoccupation diminue fortement en cas de multi-classification. 

Enrichissement par champ sémantique. Cette dernière méthode consiste à 
utiliser la ressource sémantique comme un vecteur de champ sémantique. On 
retrouve ici une approche qui est déjà utilisée avec des réseaux de termes comme 
WordNet [13]. L’idée est, une fois que l’on a pu lier un terme du texte à un concept 
de la ressource, d’utiliser les termes propres de la ressource en renfort. 

En particulier, quand à la ressource est adjointe une définition, nous avons 
presque invariablement une généralisation et une différenciation. 

Dans le cas de ressources structurées (comme une base lexicale, un thésaurus 
voire une ontologie), il est possible de pondérer ce champ sémantique en fonction 
d’une distance sémantique. Cependant, dans notre cas, le champ sémantique est 
constitué d’un texte non structuré. 

3.3   Pondération 

Au final, en combinant ces méthodes d’enrichissement, nous allons nous 
retrouver avec des « nuages21 » de lemmes gravitant autour des articles et des 
thématiques. 

Tous ces lemmes n’ont pas le même poids. Certains sont présents directement 
dans le titre ; d’autres font partie du champ sémantique du chapeau ; d’autres encore 
sont le fruit d’une généralisation. Il y a des noms, des adjectifs, des verbes et des 
articles. Certains mots sont présents une fois, d’autres reviennent à plusieurs 
endroits. Toutes ces informations sont exprimées sous la forme de poids. 

Nous avons opté pour une pondération multiplicative : un lemme aura un certain 
nombre de caractéristiques (mode d’accès, nature et nombre d’occurrences). Ces 
caractéristiques seront traduites en coefficients. Le poids du lemme sera le produit 
de ces coefficients. 

Mode d’accès au lemme Coefficient 

Lemme du titre 1.0 

Lemme de la description 0.75 

Opération de généralisation 0.5 

Opération de recherche des catégories 0.5 

Opération de récupération du champ sémantique 0.33 

 
Tableau 1.  Exemple de coefficients attachés au mode d’accès 

                             
21 Sans relation aucune avec l’artefact de visualisation, le ‘nuage de mot’. 



Recherche d’Information SEmantique  47 

 
Nature du lemme Coefficient 

Nom commun 1.0 

Nom propre 1.0 

Verbe 0.5 

Adverbe 0.0 

Adjectif 0.5 

 
Tableau 2.  Exemple de coefficients attachés à la nature des lemmes 
 

Les tableaux 1 et 2 présentent des exemples de coefficients. Pour prendre un 
exemple, supposons que nous obtenions un lemme en généralisant un terme du 
champ sémantique de l’un des lemmes du titre d’un article : le coefficient attaché à 
son mode d’accès sera, selon le tableau 1, de 1.0*0.33*0.5, soit 0.17. Il aura donc un 
poids six fois moindre que le lemme du titre lui-même dans le nuage. 

4   Premières expériences 

4.1   Ressources 

Notre objectif est de classifier des articles de presse généralistes tirés de flux 
RSS (donc des textes courts comprenant un titre et une description) au sein de la 
taxonomie généraliste élaborée par l’IPTC (donc des classes comprenant une 
étiquette et une description). Pour effectuer l’enrichissement sémantique, nous 
utilisons la ressource multilingue DBPedia, proposant à la fois :  

− l’encapsulation des pages Wikipedia, dans toutes les langues disponibles, 
au sein de concepts translingues étiquetés par une URI ; 

− une ontologie minimale en OWL, comprenant environ 200 concepts, à 
laquelle sont liés ces concepts ; 

− l’ensemble des catégories de Wikipedia regroupant les pages par thème 
(ainsi, les concepts-pages de DBPedia sont liés à des concepts-catégories) ; 

− le titre et un court résumé en Français d’environ 500000 pages (contre plus 
de 1,2 millions en anglais). 

La mise en relation de ces différentes ressources se fait de la manière suivante :  

− Entre les nuages pondérés de lemmes des articles et des thématiques 
(comprenant les lemmes originaux auxquels ont été rajoutés les lemmes issus de 
l’enrichissement sémantique), la mise en relation se fait par identité : la 
lemmatisation est censée nous affranchir des contraintes de recherche de similarité ; 
deux lemmes représentés par la même chaîne de caractère, et ayant la même nature 



48     Recherche d’Information SEmantique 
 

lexicale, sont identiques ; deux lemmes présentant une différence d’un côté ou de 
l’autre sont différents. 

− Entre un lemme et un concept de DBPedia, la question est moins 
immédiate : le rapprochement est effectué par minimisation de la distance d’édition 
entre le lemme et le titre lié au concept. Plus précisément, nous utilisons 
l’algorithme suivant : pour un lemme L donné, nous cherchons tous les concepts 
contenant un lemme représenté par la même chaîne de caractère dans leur titre ; 
parmi ces concepts, nous choisissons celui dont la distance d’édition entre le titre et 
la chaîne de caractères représentant le lemme L est minimale. 

4.2   Résultats 

Nous avons mené des expériences sur un échantillon de 200 articles triés à la 
main, à catégoriser selon la taxonomie de l’IPTC. Nous n’avons pas enrichi la 
taxonomie à l’aide d’exemples (cela sera fait dans de prochaines expériences). 

Notre première expérience a consisté à tester le nombre de lemmes que pouvait 
nous fournir le champ sémantique des lemmes des thématiques de plus haut niveau 
de la taxonomie IPTC. Les résultats sont présentés dans la figure 2. 

Comme nous pouvons le voir, l’enrichissement est relativement conséquent, 
autour de 20 à 30 fois le nombre de lemmes initial, à l’exception d’une thématique 
qui n’obtient que 13 fois ce nombre. Les résultats sont similaires en ce qui concerne 
les articles. 

Par ailleurs, la distance de similarité (la distance d’édition) donne une excellente 
performance, d’autant meilleure bien sûr qu’il y a beaucoup de pages Wikipedia 
candidates pour un lemme. 

En revanche, l’étude de l’ontologie, comme nous l’avions pressenti, donne peu 
de résultats, et ces résultats sont généralement toujours les mêmes. Les catégories, 
de même, regroupent le même type d’informations que l’ontologie (lieux, personnes 
et œuvres) et donne des résultats similaires. La plupart des lemmes ne sont tout 
simplement pas enrichis. 

 



Recherche d’Information SEmantique  49 

 

Figure 2. Impact de l’enrichissement sémantique des thématiques de l’IPTC (en 
abscisse : les différentes thématiques IPTC ; en ordonnées : le facteur 
d’accroissement du nombre de lemmes 

Une tentative de catégorisation sans aucun enrichissement donne un résultat de 
l’ordre de 10% de pertinence (très peu de lemmes sont communs entre les articles et 
les classes) sans utiliser d’exemples. L’utilisation de l’ontologie et des catégories 
reste pauvre (aucun gain en performance notable n’est obtenu). En revanche, la 
spécialisation des thématiques par les sous-thématiques, ainsi que l’utilisation du 
champ sémantique, donnent des résultats meilleurs (bien que toujours limités en ce 
qui concerne le problème lui-même de classification). Le tableau 3 résume ces 
résultats. L’impact de la pondération n’est pas significative dans les expériences que 
nous avons faites jusqu’ici. 

 

Procédé employé Pertinence 

Aucun 10% 

Spécialisation 17% 

Généralisation 10% 

Catégorisation 10% 

Champ sémantique 17% 

 
Tableau 3.  Présentation des résultats suivant la méthode d’enrichissement 

0,0

5,0

10,0

15,0

20,0

25,0

30,0

35,0

Ar
ts,

 cu
ltu

re
, e

t s
pe

ct
ac

le
s

Dé
sa

str
es

 et
 ac

ci
de

nt
s

Ec
on

om
ie
 et

 fi
na

nc
es

Ed
uc

ati
on

En
vi

ro
nn

em
en

t

Ge
ns

 an
im

au
x 
in
so

lit
e

Gu
er
re
s e

t c
on

fli
ts

M
été

o

Po
lic

e e
t j
us

tic
e

Po
lit

iq
ue

Re
lig

io
n 
et 

cr
oy

an
ce

Sa
nt
é

Sc
ien

ce
 et

 te
ch

no
lo
gi
e

So
cia

l

So
cié

té

Sp
or

t

Vi
e q

uo
tid

ie
nn

e e
t l

oi
sir

s



50     Recherche d’Information SEmantique 
 

 

4.3   Discussion 

Contrairement à ce que nous avions espéré, l’enrichissement sémantique ne 
permet pas de se passer des exemples, et donc d’une classification préliminaire à la 
main d’une partie du corpus. 

Nous n’avons pas pu étudier la pondération de manière satisfaisante. Nos 
premières expériences n’ont pas montré d’influence mesurable, cependant il est 
probable que la médiocrité des résultats impacte ce paramètre. 

La question du temps réel n’était pas centrale dans nos expériences, mais elle 
l’est dans notre problématique. L’accès au champ sémantique d’un lemme se fait en 
10 secondes environ. Cela nous amène à des temps de calculs de plusieurs minutes 
par article ; le temps souhaitable est de l’ordre de la seconde, voire moins (nous 
avons 2500 articles par jour). 

Le caractère multilingue (surtout français/anglais) de notre approche est en 
revanche un réel apport. Nous sommes en mesure d’enrichir les textes français avec 
des lemmes indifféremment français et anglais en utilisant les concepts comme 
pivots, ce qui nous aidera à mesure que nous intégrerons d’autres ressources 
sémantiques. 

5   Conclusion 

Nous avons présenté notre méthode d’enrichissement sémantique, et nous 
l’avons appliqué au cas de textes courts, pour lesquels les méthodes classiques sont 
moins efficaces. Nous avons noté, en l’absence d’exemples, un apport dans deux 
cas : l’utilisation de ressources sémantiques dédiées au corpus (comme les sous-
thématiques de la taxonomie IPTC sont dédiées à leurs thématiques respectives), et 
l’utilisation du champ sémantique d’une ressource généraliste, entendu comme un 
ensemble textuel tenant lieu de définition. En revanche, nous n’avons pas noté 
d’amélioration lors de l’emploi de ressources sémantiques spécialisées dans peu de 
domaines (même lorsque ces domaines sont pertinents mais non exhaustifs, comme 
les lieux, personnes et œuvres dans notre cas). 

Le coût temporel de l’enrichissement sémantique, qui plus est, n’est pas 
négligeable dans le coût total de la classification. Cela n’est pas forcément 
rédhibitoire dans le cas d’expériences ponctuelles ; cependant, la classification en 
temps réel des articles collectés n’est pas envisageable de cette manière. 

Afin de parfaire notre analyse, il nous reste à combiner plusieurs de ces 
approches entre elles, et à les utiliser en présence d’exemples. 



Recherche d’Information SEmantique  51 

6   Bibliographie 

1. Lancelot A., Les problèmes de concentration dans le domaine des médias, Rapport pour le 
Premier ministre, 2005. 

2. Tessier M., Baffert M., La presse au défi du numérique, Rapport pour le Ministre de la 
culture et de la communication, 2007. 

3. Olivennes D., Le développement et la protection des œuvres culturelles sur les nouveaux 
réseaux, Rapport au ministre de la Culture, 2007. 

4. Rebillard F., « Du traitement de l’information à son retraitement. La publication de 
l’information journalistique sur l’internet », Réseaux, n°137, 2006, p. 29-68. 

5. Troncy R.,  « Explorer des actualités multimédia dans le Web de Données », Actes des 
10èmes journées Ingéniérie des Connaissances IC’2009, Hammamet, Tunisie, 25-29 mai 
2009, p. 181-192 

6. Kobilarov G., Scott T., Raimond Y., Oliver S., Sizemore C., Smethurst M., Bizer C., 
LeeMedia R.,  « Media meets Semantic Web - How the BBC uses DBpedia and Linked 
Data to make Connections », In European Semantic Web Conference, Heraklion, Grèce, 
31 mai-4 juin 2009. 

7. Bizer C., The Emerging Web of Linked Data, IEEE Intelligent Systems, vol. 24, n° 5, 
2009, p. 87-92. 

8. Fouetillou G., Jacomy M., Pfaender F., « Two visions of the Web : from globality to 
locality »,  In. 2nd IEEE International Conference on Information and Communication 
Technologies ICTTA ’06, Damas, Syrie , 24 – 28 avril 2006. 

9. Wu F., Weld D., « Autonomously Semantifying Wikipedia », In 16th Conference on 
Information and Knowledge Management (CIKM-07), Lisbonne, Portugal, 6-8 novembre 
2007, p. 41-50. 

10. Becker C., DBpedia – Extracting structured data from Wikipedia. Presentation à 
Wikimania, Buenos Aires, Argentine, août 2009. 

11. Suchanek F M., Automated Construction and Growth of a Large Ontology, Thèse de 
doctorat, Saarland University, 2008. 

12. Suchanek F M, Kasneci G., Weikum G., « YAGO: A Large Ontology from Wikipedia 
and WordNet », Elsevier Journal of Web Semantics, vol. 8, n°3, 2009, p. 203-217. 

13. Miller G A., WordNet: A Lexical Database for English. Communications of the ACM, 
vol. 38, n°11, 1995, p 39-41. 

 


