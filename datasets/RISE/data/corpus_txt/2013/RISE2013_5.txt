
























Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

Les Bulletins de Santé du Végétal : spécification 
d’une base d’annotations pour la recherche 

d’information sémantique en français. 

Catherine ROUSSEY
1
, Jean-Pierre CHANET

1
, Stéphan 

BERNARD
1
 

1 
UR TSCF Irstea,  

24 avenue des Landais CS 20085 63 178 Aubière 

prenom.nom@irstea.fr  

http://www.irstea.fr  

Résumé : Dans cet article nous décrivons les différents processus d’annotation 
envisagés pour annoter un corpus sur les bulletins d’alertes agricoles. Notre but 
est de publier ces annotations sur le web de données pour permettre à des 
applications environnementales d’enrichir au besoin nos annotations. 

Mots-clés : Annotations sémantiques, annotations spatio-temporelles, recherche 
d’information sémantique, benchmark, bulletins d'alerte agricole. 

1 Introduction 

Pour être plus respectueuse de l'environnement, l’agriculture doit 
modifier ses pratiques. Pour se faire, le plan Ecophyto s'appuie 
notamment sur le système de surveillance des pratiques agricoles, dont 
les Bulletins de Santé du Végétal (BSV) sont un des moyens de 
communication. Ce corpus du domaine agricole contient des 
informations sur les attaques des bio-agresseurs des cultures par région 
(par exemple : la DRAAF de la région PACA signale une explosion  des 
attaques de la rouille du blé sur les cultures de blé dur en vallée du 
Rhône, dans son bulletin du 23 mai 2011). 

Nous souhaitons mettre en place plusieurs processus d’annotations 
spatio-temporelles, notamment pour permettre à des acteurs du domaine 
agricole de retrouver les BSV répondant à leur besoin, ou à des fins 
d'étude des évolutions spatio-temporelles des attaques sur les cultures. 

Cet article présente un premier état des lieux de nos besoins en 
annotations. Tout d’abord nous présenterons nos motivations et le corpus 
des BSV. La section suivante définira notre processus d’annotation, 

52



RISE 2013 

 

partant de l'extraction des bulletins de santé du végétal et allant jusqu’aux 
annotations, chacune de ces étapes permettant la collecte d'informations. 
Nous terminerons par des explications sur la mise en place d’un système 
de recherche d’information sémantique dédié au monde agricole. Nous 
souhaitons mettre en place plusieurs expérimentations pour utiliser ce 
corpus comme benchmark pour la recherche d’informations en français. 

2 Motivations 

Au fil des dernières décennies, les pratiques agricoles ont fortement 
évolué sous le jeu de diverses contraintes : enjeux sociétaux et 
environnementaux, cadre réglementaire, changement climatique… 
Parallèlement, le rôle des données en agriculture a également fortement 
évolué : d’abord utilisées à des fins de traçabilité et de sécurité 
alimentaire, les données, qui sont de plus en plus nombreuses, 
contribuent maintenant directement au changement des pratiques 
agricoles par leur aide à une meilleure compréhension de celles-ci. La 
multiplication des équipements embarqués, des smart-phones, des 
capteurs aux champs, etc., permet à l’heure actuelle de disposer de grands 
volumes de données spatio-temporelles (Steinberger et al. 2009). Le 
prochain enjeu est de rendre ces données disponibles à l’ensemble des 
acteurs de la filière afin qu’ils puissent les mobiliser dans les outils 
d’aide à la décision et dans les outils d’analyse (Xie et al 2008; 
Goumopoulos et al. 2009). Le web de données est une opportunité pour 
accélérer cette mutualisation des données et, par voie de conséquence,  
pour faire évolutier les pratiques agricoles. 

Afin de pouvoir publier ces données sur le web de données, il convient 
de structurer les relations entre les différents concepts manipulés par la 
thématique agriculture : plantes, maladies, ravageurs, pesticides, 
rotation, … Un certain nombre de ressources sont disponibles et 
mobilisables : ontologies de taxons, thésaurus, bases de données, corpus 
de documents. Mais lorsqu’on s’intéresse à un thème particulier, comme 
par exemple la protection des cultures,  les ressources se font rares. Il faut 
en effet mobiliser un ensemble de ressources et les relier entre elles. Nous 
proposons de créer une méthode à même de construire une base de 
connaissances agrégeant un ensemble de ressources autour d’une 
thématique. Cela permettra ensuite de publier sur le web de données les 
données disponibles de manière pertinente, mais également d’annoter les 
nombreux documents mobilisables pour faire évoluer les pratiques. 

Nous avons à notre disposition un corpus de bulletins d’alertes 
agricoles francophones que nous souhaitons annoter à l’aide d’une base 
de connaissances sur la protection des cultures. Ainsi, les acteurs du 
monde agricole pourront rechercher dans ce corpus les bulletins qui les 
intéressent particulièrement. Dans un second temps nous aimerions 

53



Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

transformer ce corpus en un benchmark pour la recherche d’informations 
sémantiques francophones, en déployant plusieurs campagnes 
d’évaluation avec différents acteurs du monde agricole (agriculteurs, 
agronomes, jardiniers, conseillers agricoles). 

3 Les Bulletins de Santé du Végétal 

Dans nos travaux, nous nous focalisons sur la protection des cultures. 
Nous avons à disposition une collection de documents intitulés « 
Bulletins de Santé du Végétal » (BSV). Nous souhaitons constituer une 
base archivant les données des attaques des bio-agresseurs sur les 
cultures (par exemple : attaque de rouille sur une culture de blé dur). 

Le Grenelle de l’environnement et le plan Ecophyto ont renforcé les 
réseaux de surveillance sur les cultures et les pratiques agricoles. Les 
Bulletins de Santé du Végétal sont une des modalités mises en place par 
ces réseaux de surveillance. 

Le Bulletin de Santé du Végétal (BSV) est un document d’information 
technique et réglementaire, rédigé sous la responsabilité d’un 
représentant régional du ministère de l’agriculture. Ce représentant peut 
être par exemple la Chambre Régionale d’Agriculture ou bien la 
Direction Régionale de l'Alimentation, de l'Agriculture et de la Forêt 
(DRAAF). Ce représentant doit mettre à disposition ses bulletins sur son 
site internet afin d’en permettre un accès public. La conséquence est que 
les BSV sont répartis sur différents sites web (un par région). À notre 
connaissance, il n’existe pas encore de système donnant un accès 
uniforme à l’ensemble des BSV. 

Les BSV sont rédigés en collaboration avec de nombreux partenaires 
impliqués dans la protection des cultures. La liste des auteurs des BSV 
varie en fonction de la région et de la filière agricole. Par conséquent leur 
contenu et leur présentation ne sont pas uniformes et varient en fonction 
des auteurs. 

Les BSV diffusent des informations relatives à la situation sanitaire 
des principales productions végétales de la région et proposent une 
évaluation des risques encourus pour les cultures. Des données générales 
concernant les stratégies de lutte (notes nationales, …) ou sur la 
réglementation peuvent figurer également dans les BSV. 

Selon l’actualité sanitaire et/ou la culture, le rythme de parution des 
BSV est variable, allant d’une parution hebdomadaire à mensuelle. 

Les BSV sont une synthèse des observations effectuées sur les 
cultures. Il existe des bases de données d’observations mais la rédaction 
des BSV oblige leurs auteurs à décider si une observation est un 
phénomène unique non représentatif ou un phénomène important 
représentatif d’une réalité. Les BSV ne sont pas une agrégation 

54



RISE 2013 

 

automatique de données mesurées mais bien une synthèse humaine des 
jugements sur des observations. 

 
FIGURE 1 – Exemple de BSV de la région Midi-Pyrénées pour la filière 

grande culture 
 
Nous avons récupéré les BSV de l’année 2011 de 19 régions. En 

moyenne une région publie plus d’une centaine de BSV par an. Au total 

55



Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

nous avons 2825 BSV pour l’année 2011. Dans un premier temps nous 
limiterons notre analyse aux BSV de la région Bourgogne concernées par 
la filière « grande culture », c’est à dire 37 BSV. 

4 Processus d’annotation des BSV 

Ce processus d’annotation  de notre collection de bulletins est 
constitué de trois phases, chacune permettant l'extraction d'informations, 
de plus en plus fines. La première phase, qui consiste en la récupération 
et l'extraction des fichiers bruts, ne nécessitera pas d’analyse du contenu 
des BSV. Il permettra néanmoins d’identifier la région, la date et la filière 
agricole concernée. La seconde phase est un pré-traitement qui permettra 
de récupérer les noms des cultures et de leurs agresseurs dans les textes. 
Elle aura pour résultat une première série d’observations spatio-
temporelles des attaques des agresseurs sur les cultures. La dernière 
phase sera l'annotation en elle-même, et visera à  récupérer les niveaux de 
risques liés aux attaques des agresseurs dans les cultures. Ces différentes 
phases du processus sont décrites plus précisément dans les sections 
suivantes. 

4.1 Extraction brute 

4.1.1 Principe 

Les BSV sont disponibles sur les sites des chambres d’agricultures ou 
des DRAAFT. Les pages contenant les liens permettant le téléchargement 
des fichiers pdf contenant les BSV nous donnent déjà un certain nombre 
d’informations : 

 La région : elle est indiquée par le nom du site web 
 La filière agricole concernée : le site a généralement une page 

par filière, donc le nom de la page web nous permet de 
récupérer cette information 

 Le numéro du BSV : le nom du fichier téléchargé contient 
souvent le numéro du BSV. 

 La date de parution : le nom du fichier téléchargé contient 
souvent cette date. 

 
Ce premier niveau d’annotations a été réalisé en analysant les 

différentes pages web contenant les liens vers les fichiers pdf. 

4.1.2 Problèmes rencontrés 

Il est important de constater que les listes des filières agricoles 
disponibles ne sont pas normalisées d’une région à une autre. En effet, 
tout dépend des productions agricoles de la région. Certaines régions vont 

56



RISE 2013 

 

regrouper plusieurs filières. Par exemple, la Haute-Normandie a une 
filière « arboriculture et petits fruits ». D’autres régions ont au contraire 
une production très spécialisée et ne vont indiquer que la production qui 
les intéresse : la Bourgogne, par exemple, a une filière « cassis » sans 
avoir de filière « petit fruit ». Il est donc nécessaire de normaliser les 
noms des filières entre les régions, et de constituer une classification 
homogène des filières de productions agricoles en France. Une 
classification des filières agricoles françaises est déjà disponible sur le 
site français de Wikipedia

1
. Cette classification a été enrichie 

manuellement pour couvrir l’ensemble des noms de filières utilisées dans 
les 20 régions. 

Suivant les cas, il est possible qu’un BSV soit associé à plusieurs 
filières. 

4.2 Pré-traitement 

4.2.3 Principe  

Ce second niveau d’analyse, au contraire du précédent, a pour but de 
travailler sur le contenu textuel des BSV. Nous souhaitons récupérer dans 
le texte, le nom des plantes cultivées et le nom de leurs agresseurs. 

Dans l’exemple de la Fig 1, les noms des cultures sont présents dans 
les titres des sections (maïs), et les agresseurs parmi les sous-sections 
(sésamie, pyrale, …). La structure logique du document nous donne 
l’indication que la sésamie et la pyrale sont des agresseurs du maïs. Cette 
régularité éditoriale est vérifiée sur l’ensemble des BSV de toutes les 
régions. Ainsi la reconnaissance des noms des organismes vivants dans 
les titres des sections et leur organisation logique dans le texte nous 
permet de produire nos premières annotations : attaque de la culture maïs 
par l’agresseur pyrale dans la région Midi-Pyrénées à la date du 
15 juin 2011. 

La Fig 2 présente le processus de pré-traitement, puis le processus 
d’annotation : 

Dans un premier temps il est nécessaire de transformer les fichiers au 
format PDF en un format qui permet une lecture plus aisée de la structure 
du document. Le logiciel pdf2xml (Dejean Giguet 2012) nous offre une 
sortie XML contenant des balises représentant l’organisation physique du 
document et sa mise en page. Chaque mot est encadré par une balise 
ayant des attributs de mise en forme et de position dans la page. A partir 
de ces fichiers XML, nous récupérons l’ensemble des contenus textuels.  

Ensuite, nous reconstruisons automatiquement la structure logique des 
documents, en rassemblant les mots. Le but est d’identifier les sections, 
leur titre et leurs paragraphes, ainsi que leur inclusion logique (section et 
sous section). Cette structure logique sera décrite dans des fichiers XML, 

                                                           
1  http://fr.wikipedia.org/wiki/Classement_en_France_des_cultures_par_groupes_d’usage 

57



Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

et seront les entrées du processus d’annotation. On leur associera les 
informations spatio-temporelles recueillies lors de l'extraction des 

fichiers bruts. 
Cette phase de pré-traitement est en cours d'élaboration et a été testée 

sur les grandes cultures dans quatre régions. 
FIGURE 2 – Processus d'annotation. 

 

4.2.4 Problèmes rencontrés  

La mise en page des BSV n’est pas homogène : certains utilisent par 
exemple une mise en page en plusieurs colonnes, d’autres ont une mise 
en page sur une seule colonne. La détection des colonnes complexifie le 
processus de reconstruction de la structure logique,  surtout lorsque 
certains effets de mise en page, tels que des cadres chevauchant plusieurs 
colonnes, empêchent d’établir correctement la frontière entre colonnes. 

La mise en page des BSV ne varie pas seulement d'une région à 
l'autre, mais aussi d'une culture à l'autre, les auteurs des bulletins 
semblant peu contraints sur cet aspect. Cette diversité rend difficile la 
création d'une structure logique uniforme pour l'ensemble des BSV. 
Toutefois, l'association des tailles de caractères et de leurs attributs (gras, 
italique) et d'une description générale des structures des différents BSV 

58



RISE 2013 

 

permet d'obtenir, à quelques exceptions près, les informations nécessaires 
à la phase d'annotation. 

4.3 Annotations 

4.3.5 Principe 

L'annotation des BSV est composée de deux étapes : 
 La première étape, appelée simplement « annotation », consiste 

à reconnaître les noms des organismes vivants dans les titres 
des sections, à partir d'un lexique. 

 La seconde étape, nommée annotation fine, vise à estimer, à 
partir du contenu de la section, si l'agresseur en question est 
cité pour avoir été observé, ou au contraire parce qu'aucune 
attaque n'a été constatée, ou encore pour inciter à prévenir une 
attaque. 

 
Pour ces deux étapes, la plateforme GATE sera utilisée (Cunnigham et 

al. 2011). 

4.3.6 L'annotation « simple » 

4.3.6.1 Principe 

Cette première phase d'annotation utilisera en entrée un lexique 
précédemment défini. Le lexique sera composé des noms de plantes 
cultivées en France ainsi que les noms de leurs agresseurs (champignons, 
plantes, insectes, animaux, …). Ces lexiques seront dérivés d’une base de 
connaissances construite à partir de ressources disponible sur le LOD. 

La relation entre une plante cultivée et son agresseur sera dérivée 
automatiquement de la structure logique du document. L’inclusion 
logique entre sections implique la présence de l’agresseur sur la culture. 

4.3.6.2 Problèmes rencontrés 

Pour mettre en place ce processus d’annotations, nous avons besoin de 
construire des lexiques d’organismes vivants et de les classer en plantes 
cultivées (culture) et en agresseur des cultures (organisme vivant nuisant 
au développement d’une plante cultivée). Ces lexiques seront dérivés 
d’une ontologie des organismes vivants capable de classer 
automatiquement un organisme en plantes cultivées ou en agresseurs 
d’une culture. Contrairement à nos attentes, catégoriser une plante en 
plante cultivée (culture) ou en agresseur n’est pas évident : 

1 Une plante peut être associée à différentes filières agricoles. La 
filière agricole indique l’usage de sa production : huile, céréale 
panifiable, fourrage, ornementale etc… et donc une même plante 
peut être utilisée pour différentes cultures.  

59



Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

2 De plus, sa culture est contextuelle. En effet, certaines plantes 
sont plus adaptées à un climat qu’à un autre ; les usages 
alimentaires du pays peuvent aussi entrer en compte. Par exemple 
le riz est consommé partout en France mais il s’agit 
principalement de riz importé. Les principaux pays producteurs de 
riz sont la Thaïlande, le Pakistan, les USA et la Chine. En France 
métropolitaine seule la région PACA produit du riz. La définition 
d’une culture (d’une filière agricole) est dépendante du pays et 
donc elle ne peut pas être généralisée. 

3 Dernier point, une plante peut à la fois être une plante cultivée ou 
un agresseur d’une autre culture. Par exemple, sur une parcelle 
agricole cultivée pour du blé, il peut persister une ancienne 
culture qui va gêner le développement de la culture en cours. 
Ainsi d’une année à une autre, la même plante peut être 
catégorisée sous forme de culture ou d’agresseur. 

 
Pour identifier les plantes cultivées, nous allons nous baser sur la 

classification des filières agricoles précédemment définies dans le 
processus d’annotations brutes. Dans notre ontologie, les organismes 
vivants seront décrits par leurs caractéristiques agronomiques (plantes, 
champignons, animaux etc.). Cette description est générique, c’est à dire 
commune quel que soit le pays concerné ou le domaine étudié 
(agriculture, botanique, épidémiologie). Cette description sera ensuite 
enrichie par l’usage de la plante en agriculture en indiquant sa filière 
agricole.  Ainsi, un raisonneur pourra automatiquement découvrir les 
plantes cultivées en France. Dans un second temps, nous allons nous 
intéresser aux agresseurs des cultures les plus connus qui apparaissent 
dans des listes d’agresseurs publiées. La notion d’agresseur étant elle 
aussi contextuelle il est nécessaire de la dériver d’une observation 
d’agression. Nous allons, de la même façon que précédemment, enrichir 
la description d’un organisme par ses agressions connues, pour dériver 
les agresseurs des cultures. De la même manière nous pourrons dériver 
les auxiliaires des cultures, c'est-à-dire les agresseurs des agresseurs des 
cultures.  

Cette ontologie sera enrichie au cours du temps pour tenir compte des 
différents noms régionaux que peut avoir un organisme vivant et aussi  de 
l’apparition de nouveaux organismes dans différents pays. Par exemple 
les changements climatiques permettent à des organismes de se 
développer dans des lieux où préalablement ils n’avaient pas les moyens 
de subsister. Les nouveaux moyens de transports et routes commerciales 
permettent aussi des migrations d’organismes vivants. 

4.3.7 L'annotation fine 

4.3.7.3 Principe 

60



RISE 2013 

 

Dans l’annotation précédente des BSV nous avons fait l’hypothèse que 
toute apparition dans le texte d’un agresseur indique qu’il y a eu une 
observation de l’agression d’une culture par cet agresseur. Cette 
hypothèse est fausse dans certains cas. En effet, il arrive que la section 
sur l’agresseur indique qu’aucune observation de cet agresseur sur la 
culture n’a été constatée. Il est possible aussi que le texte indique que 
l’agresseur n’est pas assez présent sur la culture pour entraîner un risque.  

Nous avons donc besoin d’extraire des textes un ensemble 
d’informations précises comme : 

 Le stade de développement de la plante lors de l’observation. 
Cette information permet une estimation du risque. En effet, 
selon le stade de développement de la plante, l’apparition d’un 
organisme extérieur peut nuire à la culture ou avoir un impact 
limité voir inexistant sur le développement de la culture. Ce 
stade de développement est aussi nécessaire pour connaître le 
moyen de lutte adéquat. Les stades de développement des 
plantes sont publiés dans des listes et nous pourrons, à l’aide 
d’un lexique approprié, extraire ces informations. 

 Le niveau de risque de l’agression. Ce niveau est dépendant du 
stade de développement de la culture et du nombre 
d’agresseurs présents dans la culture. Les niveaux de risques 
sont aussi publiés dans des listes. Nous pourrons donc extraire 
ces informations à l’aide d’un lexique. 

 Le nombre de parcelles agricoles impliquées où une attaque a 
été constatée. Ce nombre peut aussi être exprimé en 
pourcentage. 

 
Pour extraire ce type d’information nous avons donc besoin d’une 

analyse des textes plus fine, avec des processus de traitement 
automatique du langage naturel. Nous allons mettre en place des règles 
linguistiques pour extraire des hypothèses qui devront être validées 
manuellement ensuite.  

La Fig 3 présente le processus d’annotations fines. Nous allons, 
comme précédemment, utiliser la plateforme Gate. Cette plateforme 
utilisera plusieurs lexiques dérivés automatiquement de notre ontologie. 
Le but des lexiques sera de découvrir les termes représentant les stades de 
développement des cultures et les niveaux de risques. Ensuite nous 
appliquerons des règles linguistiques pour extraire  l’observation d’une 
attaque d’une culture avec son niveau de risque et le stade de 
développement de la plante, à partir du contenu de la section. 

61



Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

FIGURE 3 – Processus d'annotations fines. 

4.3.7.4 Problèmes rencontrés 

Nous voulons atteindre plusieurs buts dans cette annotation. Dans un 
premier temps, nous voulons découvrir s’il y a eu effectivement une 
observation d’une attaque d’un agresseur sur une culture. Ensuite nous 
voulons enrichir cette observation en indiquant le niveau de risque et le 
stade de développement de la plante. Ces informations ne sont pas 
forcement indiquées dans le texte des BSV. Parfois le BSV rappelle juste 
dans un tableau les niveaux de risque en fonction des stades de 
développement, sans donner aucune indication sur l’observation réalisée. 
Donc nous devrons prendre en compte les absences d’observations. Les 
résultats du processus d’annotations fines devront être validés 
manuellement pour compléter au besoin les imperfections de ce 
processus automatique. 

5 Système d’interrogation des BSV 

Au fur et à mesure, l’ensemble de nos annotations sera mis à 
disposition sur le web par le biais d’un SPARQL end point. Pour faciliter 
l’interrogation SPARQL de ces annotations, nous intégrerons, au dessus 

62



RISE 2013 

 

de SPARQL end point, le système d’interrogation en langage naturel 
développé par l'IRIT intitulé SWIP (Pradel et al, 2012). Ce système 
permet aux utilisateurs de requêter un SPARQL end point à l’aide d’une 
requête sous forme d’une question en langage naturel, Il se base sur des 
patrons de requêtes représentatifs du domaine et sur des phrases en 
langage naturel pour proposer à l’utilisateur plusieurs requêtes SPARQL 
possibles. 

Il est à noter que nous ne sommes pas dépositaires des BSV. Nous 
avons connaissance d’un autre projet d’annotations des BSV dont l’INRA 
est l’un des membres partenaires : Le projet Vespa. Ce projet devrait 
mettre en ligne les versions électroniques des documents. Donc nous ne 
mettrons en ligne que nos annotations. Nous établirons un lien entre nos 
annotations et les versions des BSV mises en lignes dans le projet Vespa. 

6 Conclusion et perspectives 

Dans cet article nous décrivons les différents processus d’annotation 
envisagés pour annoter un corpus sur les bulletins d’alertes agricoles. 
Notre but est de publier ces annotations sur le web de données pour 
permettre à des applications environnementales d’enrichir au besoin nos 
annotations. Nous souhaitons utiliser ce corpus de bulletins pour mettre 
en place une première campagne d’évaluation de systèmes de recherche 
d’information sémantique en français. 

 
Dans le but de développer notre benchmark de recherche sémantique 

en français, nous souhaitons mettre en place plusieurs expérimentations 
de recherche d’information sur les BSV. Nous avons en tête d’utiliser 
plusieurs groupes d’utilisateurs : 

 des experts en agronomie, notamment de l’INRA,  
 des responsables d’exploitations issus des établissements 

d’enseignement agricole, 
 des internautes agriculteurs identifiés dans les réseaux sociaux 

agricoles.  
 
Ainsi il sera aussi possible d’effectuer des recherches en fonction du 

profil de l’utilisateur 

Références 

CUNNINGHAM H., MAYNARD D., BONTCHEVA K. (2011) Text Processing with 
GATE (Version 6) University of Sheffield Department of Computer 
Science. 15 April 2011. ISBN 0956599311 

DEJEAN H., GIGUET. (2012) pdf2xml, available at 
http://sourceforge.net/projects/pdf2xml/ last update octobre 2012. 

63



Bulletins de Santé du Végétal : Spécification d’une base d’annotations 

GOUMOPOULOS, C., KAMEAS, A.D., CASSELLS, A. (2009). An ontology-driven 
system architecture for precision agriculture applications. International 
Journal of Metadata, Semantics and Ontologies 4,  p 72–84. 

PRADEL, C. HAEMMERLE, O. HERNANDEZ, N. (2012) Des patrons modulaires 
de requêtes SPARQL dans le système SWIP. Dans : Journées Francophones 
d'Ingénierie des Connaissances (IC 2012), Paris, 27/06/2012-29/06/2012, 
juin 2012, p 385-400. 

STEINBERGER, G., ROTHMUND, M. & AUERNHAMMER, H. (2009) Mobile farm 
equipment as a data source in an agricultural service architecture. 
Computers and electronics in agriculture, 65(2), 2009, p.238-246. 

XIE, N., WANG, W., YANG, Y. (2008) Ontology-based agricultural knowledge 
acquisition and application, in: Computer And Computing Technologies In 
Agriculture, Volume I. Springer, 2008, p. 349–357. 

64


