# ScholarMap

Expert finding system for academia.

Written in Mathematica and Python. 

Based on [kleis][kleis], [CSO-Classifier][cso] and [MinerLC][minerlc].

[kleis]: https://github.com/sdhdez/kleis-keyphrase-extraction
[cso]: https://github.com/angelosalatino/cso-classifier 
[minerlc]: https://lipn.univ-paris13.fr/MinerLC/
