Abney, S. (2004). Understanding the Yarowsky algorithm. Computational Linguistics, 30(3):365–395.
Agirre, E. and P. Edmonds, eds. (2007). Word sense disambuguation: Algorithms and applications. Springer, New York, NY.
Agirre, E. and D. Martínez (2004). Unsupervised WSD based on automatically retrieved examples: The importance of bias. In Proceedings of EMNLP 2004, pp. 25–32, Barcelona, Spain.
Agirre, E. and G. Rigau (1996). Word sense disambiguation using conceptual density. In Proceedings of COLING 1996, pp. 16–22, Copenhagen, Denmark.
Agirre, E. and A. Soroa (2008). Using the multilingual central repository for graph-based word sense disambiguation. In Proceedings of LREC, pp. 1388–1392, Marrakesh, Morocco.
Agirre, E. and M. Stevenson (2007). Knowledge sources for word sense disambiguation. In E. Agirre and P. Edmonds (eds.), Word Sense Disambiguation: Algorithms and Applications, Springer, New York.
Agirre, E., O. Lopez de Lacalle, and D. Martínez (2005). Exploring feature spaces with SVD and unlabeled data for word sense disambiguation. In Proceedings of RANLP, pp. 32–38, Borovets, Bulgaria.
Bar-Hillel, Y. (1960). Automatic translation of languages. In D. Booth and R. E. Meagher (eds.), Advances in Computers, Academic Press, New York.
Basili, R., M. T. Pazienza, and P. Velardi (1992). Combining NLP and statistical techniques for lexical acquisition. In Proceedings of the AAAI Fall Symposium on Probabilistic Approaches to Natural Language, pp. 82–87, Cambridge, MA.
Basili, R., M. T. Pazienza, and P. Velardi (1994). The noisy channel and the braying donkey. In Proceedings of the ACL Balancing Act Workshop on Combining Symbolic and Statistical Approaches to Language, pp. 21–28, Las Cruces, NM.
Black, E. (1988). An experiment in computational discrimination of English word senses. IBM Journal of Research and Development, 232:185–194.
Brody, S., R. Navigli and M. Lapata (2006). Ensemble methods for unsupervised WSD. In Proceedings of COLING-ACL06, pp. 97–104, Sydney, Australia.
Brown, P., S. Della Pietra, V. Della Pietra, and R. Mercer (1991). Word sense disambiguation using statistical methods. In Proceedings of ACL 1991, pp. 264–270, Berkeley, CA.
Bruce, R. and L. Guthrie (1992). Genus disambiguation: A study in weighted preference. In Proceedings of COLING-92, pp. 1187–1191, Nantes, France.
Bruce, R. and J. Wiebe (1994). Word-sense disambiguation using decomposable models. In Proceedings of ACL 1994, pp. 139–146, Las Cruces, NM.
Carpuat, M. and D. Wu (2005). Word sense disambiguation vs. statistical machine translation. In Proceedings of ACL 2005, pp. 387–394, Morristown, NJ.
Carpuat, M. and D. Wu (2007). Improving statistical machine translation using word sense disambiguation. In Proceedings of EMNLP-CoNLL07, pp. 61–72, Prague, Czech Republic.
Chan, Y. S., H. T. Ng, and D. Chiang (2007a). Word sense disambiguation improves statistical machine translation. In Proceedings of ACL07, pp. 33–40, Prague, Czech Republic.
Chan, Y. S., H. T. Ng, and Z. Zhong (2007b). NUS-PT: Exploiting parallel texts for word sense disam- biguation in the English all-words tasks. In Proceedings of SemEval-2007, pp. 253–256, Prague, Czech Republic.
Chang, J., Y. Luo, and K. Su (1992). GPSM: A generalized probabilistic semantic model for ambiguity resolution. In Proceedings of ACL 1992, pp. 177–184, Newark, DE.
Chapman, R. (1977). Roget’s International Thesaurus (Fourth Edition), Harper and Row, New York. Cheeseman, P., M. Self, J. Kelly, W. Taylor, D. Freeman, and J. Stutz (1988). Bayesian classification. In Proceedings of AAAI88, pp. 607–611, St. Paul, MN.
Chen, J. and M. Palmer (2005). Towards robust high performance word sense disambiguation of English verbs using rich linguistic features. In Proceedings of the 2nd International Joint Conference on Natural Language Processing, pp. 933–944, Jeju Island, Korea.
Chklovski, T. and R. Mihalcea (2002). Building a sense tagged corpus with Open Mind Word Expert. In Proceedings of the ACL 2002 Workshop on Word Sense Disambiguation: Recent Successes and Future Directions, pp. 116–122, Philadelphia, PA.
Choueka, Y. and S. Lusignam (1985). Disambiguation by short contexts. Computers and the Humanities, 19:147–158. Computational Linguistics, 20:563–596.
Dagan, I., A. Itai, and U. Schwall (1991). Two languages are more informative than one. In Proceedings of ACL 1991, pp. 130–137, Berkeley, CA.
Dang, H. T., K. Kipper, M. Palmer, and J. Rosenzweig (1998). Investigating regular sense extensions based on intersective Levin classes. In Proceedings of Coling/ACL-98, pp. 293–300, Montreal, CA. Deerwester, S., S. Dumais, G. Furnas, T. Landauer, and R. Harshman (1990). Indexing by latent semantic analysis. Journal of the American Society for Information Science, 41(6):391–407.
Dempster, A., N. Laird, and D. Rubin (1977). Maximum likelihood from incomplete data via the EM algorithm. Journal of the Royal Statistical Society, 39:1–38.
Diab, M. (2003). Word sense disambiguation within a multilingual framework, PhD thesis, University of Maryland, College Park, MD.
Diab, M. and P. Resnik (2002). An unsupervised method for word sense tagging using parallel corpora. In Proceedings of ACL 2002, pp. 255–262, Philadelphia, PA.
Edmonds, P. and A. Kilgarriff (2002). Introduction to the special issue on evaluating word sense disambiguation systems. Journal of Natural Language Engineering, 8(4):279–291.
Eisner, J and D. Karakos (2005). Bootstrapping without the boot. In Proceedings of HLT-EMNLP05, pp. 395–402, Vancouver, BC.
Eizirik, L., V. Barbosa, and S. Mendes (1993). A Bayesian-network approach to lexical disambiguation. Cognitive Science, 17:257–283.
Escudero, G., L. Marquez, and G. Riagu (2000). An empirical study of the domain dependence of supervised word sense disambiguation systems. In Proceedings of EMNLP/VLC00, pp. 172–180, Hong Kong, China.
Firth, J. (1968). A synopsis of linguistic theory. In F.R. Palmer (ed.), Selected Papers of J.R. Firth 1952–59, Indiana University Press, Bloomington, IN.
Gale, W., K. Church, and D. Yarowsky (1992a). A method for disambiguating word senses in a large corpus. Computers and the Humanities, 26:415–439.
Gale, W., K. Church, and D. Yarowsky (1992b). One sense per discourse. In Proceedings of the 4th DARPA Speech and Natural Language Workshop, pp. 233–237, Harriman, NY.
Gale, W., K. Church, and D. Yarowsky (1992c). Using bilingual materials to develop word sense disambiguation methods. In Proceedings, 4th International Conference on Theoretical and Methodological Issues in Machine Translation, pp. 101–112, Montreal, CA.
Gale, W., K. Church, and D. Yarowsky (1992d). On evaluation of word-sense disambiguation systems. In Proceedings of ACL 1992, pp. 249–256, Columbus, OH.
Gale, W., K. Church, and D. Yarowsky (1994). Discrimination decisions for 100,000-dimensional spaces. In A. Zampoli, N. Calzolari, and M. Palmer (eds.), Current Issues in Computational Linguistics: In Honour of Don Walker, Kluwer Academic Publishers, pp. 429–450, Dordrecht, the Netherlands. Guthrie, J., L. Guthrie, Y. Wilks, and H. Aidinejad (1991). Subject dependent co-occurrence and word sense disambiguation. In Proceedings of ACL 1991, pp. 146–152, Berkeley, CA.
Hearst, M. (1991). Noun homograph disambiguation using local context in large text corpora. In Using Corpora, University of Waterloo, Waterloo, ON.
Hirst, G. (1987). Semantic Interpretation and the Resolution of Ambiguity, Cambridge University Press, Cambridge, U.K.
Hovy, E., M. Marcus, M. Palmer, L. Ramshaw, and R. Weischedel (2006). OntoNotes: The 90% solution. In Proceedings of HLT-NAACL06, pp. 57–60, New York.
Ide, N. (2000). Cross-lingual sense determination: Can it work? Computers and the Humanities, 34(1-2):223–234.
Jorgensen, J. (1990). The psychological reality of word senses. Journal of Psycholinguistic Research, 19:167–190.
Kelly, E. and P. Stone (1975). Computer Recognition of English Word Senses, North-Holland, Amsterdam, the Netherlands.
Kilgarriff, A. (1992). Dictionary word sense distinctions: An enquiry into their nature. Computers and the Humanities, 26:365–387.
Kilgarriff, A. (1997). I don’t believe in word senses. Computers and the Humanities, 31(2):91–113. Kilgarriff, A. (2001). English lexical sample task description. In Proceedings of the 2nd International Workshop on Evaluating Word Sense Disambiguation Systems, pp. 17–20, Toulouse, France. Kilgarriff, A. and M. Palmer (2000). Introduction to the special issue on Senseval. Computers and the Humanities, 34(1–2):1–13.
Klapaftis, I. and S. Manandhar (2008). Word sense induction using graphs of collocations. In Proceedings of ECAI08, pp. 298–302, Patras, Greece.
Kohomban, U. S. and W. S. Lee (2005). Learning semantic classes for word sense disambiguation. In Proceedings of ACL 2005, pp. 34–41, Ann Arbor, MI.
Krovetz, R. (1990). Lexical acquisition and information retrieval. In P.S. Jacobs (ed.), Text-Based Intelligent Systems: Current Research in Text Analysis, Information Extraction and Retrieval, GE Research and Development Center, Schenectady, NY, pp. 45–64.
Krovetz, R. (1997). Homonymy and polysemy in information retrieval. In Proceedings of ACL 1997, pp. 72–79, Madrid, Spain.
Krovetz, R. (1998). More than one sense per discourse. In Proceedings of Senseval-1, Sussex, U.K. Krovetz, R. and W. Croft (1989). Word sense disambiguation using machine-readable dictionaries. In Proceedings of the 12th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval, pp. 127–136, Cambridge, MA.
Krovetz, R. and B. Croft (1992). Lexical ambiguity and information retrieval. ACM Trans. Information Systems, 10(2):115–141.
Lapata, M. and F. Keller (2007). An information retrieval approach to sense ranking. In Proceedings of HLT-NAACL07, pp. 348–355, Rochester, NY.
Leacock, C., G. Towell, and E. Voorhees (1993). Corpus-based statistical sense resolution. In Proceedings, ARPA Human Language Technology Workshop, pp. 260–265, Plainsboro, NJ.
Lee, Y. K. and H. T. Ng (2002). An empirical evaluation of knowledge sources and learning algorithms for word sense disambiguation. In Proceedings of of EMNLP02, pp. 41–48, Philadelphia, PA.
Lesk, M. (1986). Automatic sense disambiguation: How to tell a pine cone from an ice cream cone. In Proceeding of the 1986 SIGDOC Conference, pp. 24–26, Toronto, ON. Association for Computing Machinery, New York.
Luk, A. (1995). Statistical sense disambiguation with relatively small corpora using dictionary definitions. In Proceedings of ACL 1995, pp. 181–188, Cambridge, MA.
Magnini, B. and G. Cavaglia (2000). Integrating Subject Field Codes into WordNet. In Proceedings of LREC00, pp. 1413–1418, Athens, Greece.
Márquez, G. Escudero, D. Martinez, and G. Rigau (2007). Supervised corpus-based methods for WSD. In E. Agirre and P. Edmonds (eds.), Word Sense Disambiguation: Algorithms and Applications, Springer, New York.
Martínez, D. and E. Agirre (2000). One sense per collocation and genre/topic variations. In Proceedings of EMNLP/VLC, pp. 207–215, Hong Kong, China.
Martínez, D., E. Agirre, and L. Màrquez (2002). Syntactic features for high precision word sense disambiguation. In Proceedings of COLING 2002, pp. 1–7, Taipei, Taiwan.
Martínez, D., E. Agirre, and O. Lopez de Lacalle (2008). On the use of automatically acquired examples for all-nouns WSD Journal of Artificial Intelligence Research, 33:79–107.
Martínez, D., E. Agirre, and X. Wang (2006). Word relatives in context for word sense disambiguation. In Proceedings of the Australasian Language Technology Workshop, pp. 42–50, Sydney, Australia.
McCarthy, D., J. Carroll, and J. Preiss (2002). Disambiguating noun and verb senses using automat- ically acquired selectional preferences. In Proceedings of SENSEVAL-2, pp. 119–122, Toulouse, France.
McCarthy, D., R. Koeling, J. Weeds, and J. Carroll (2004). Finding predominant word senses in untagged text. In Proceedings of ACL 2004, pp. 279–287, Barcelona, Spain.
McKeown, K. and V. Hatzivassiloglou (1993). Augmenting lexicons automatically: Clustering seman- tically related adjectives. In Proceedings, ARPA Workshop on Human Language Technology, pp. 272–277, Plainsboro, NJ.
McRoy, S. (1992). Using multiple knowledge sources for word sense disambiguation. Computational Linguistics, 18(1):1–30.
Mihalcea, R. (2002). Bootstrapping large sense tagged corpora. In Proceedings of LREC 2002, pp. 1407– 1411, Canary Islands, Spain.
Mihalcea, R. (2005). Unsupervised large-vocabulary word sense disambiguation with graph-based algorithms for sequence data labeling. In Proceedings of HLT05, pp. 411–418, Morristown, NJ.
Mihalcea, R. and D. Moldovan (2001). Pattern learning and active feature selection for word sense disambiguation. In Proceedings of SENSEVAL-2, pp. 127–130, Toulouse, France.
Mihalcea, R., T. Chklovski, and A. Killgariff (2004). The Senseval-3 English lexical sample task. In Proceedings of ACL/SIGLEX Senseval-3, pp. 25–28, Barcelona, Spain.
Mihalcea, R., P. Tarau, and E. Figa (2004). Pagerank on semantic networks with application to word sense disambiguation. In Proceedings of COLING04, pp. 1126–1132, Geneva, Switzerland.
Miller, G. (1990). WordNet: An on-line lexical database. International Journal of Lexicography, 3(4): 235–312.
Miller, G. and W. Charles (1991). Contextual correlates of semantic similarity. Language and Cognitive Processes, 6(1):1–28.
Miller, G., C. Leacock, R. Tengi, and R.Bunker (1993). A semantic concordance. In Proceedings of the ARPA Workshop on Human Language Technology, pp. 303–308, Plainsboro, NJ.
Mooney, R. (1996). Comparative experiments on disambiguating word senses: An illustration of the role of bias in machine learning. In Proceedings of EMNLP, pp. 82–91, Philadelphia, PA.
Navigli, R. (2006). Consistent validation of manual and automatic sense annotations with the aid of semantic graphs. Computatational Linguisitics, 32(2):273–281.
Navigli, R. and M. Lapata (2007). Graph connectivity measures for unsupervised word sense disambiguation. In Proceedings of IJCAI, pp. 1683–1688, Hyderabad, India.
Ng, H. T. and H. Lee (1996). Integrating multiple knowledge sources to disambiguate word sense: An exemplar-based approach. In Proceedings of ACL 1996, pp. 40–47, Santa Cruz, CA.
Ng, H. T., B. Wang and Y. Chan (2003). Exploiting parallel texts for word sense disambiguation: an empirical study. In Proceedings of ACL03, pp. 455–462, Sapporo, Japan.
Pakhomov, S. (2002). Semi-supervised maximum entropy based approach to acronym and abbreviation normalization in medical texts. In Proceedings of ACL 2002, pp. 160–167, Philadelphia, PA.
Palmer, M., H. Dang and C. Fellbaum (2007). Making fine-grained and coarse-grained sense distinctions, both manually and automatically. Journal of Natural Language Engineering, 13(2):137–163.
Pantel, P. and D. Lin (2002). Discovering word senses from text. In Proceedings of SIGKDD’02, pp. 613–619, Edmonton, Canada.
Pedersen, T. (2009). WordNet::SenseRelate::AllWords - A broad coverage word sense tagger that maximimizes semantic relatedness. In Proceedings of NAACL09, pp. 17–20, Boulder, CO.
Pedersen, T. and R. Bruce (1997). A new supervised learning algorithm for word sense disambiguation. In Proceedings of AAAI, pp. 604–609, Providence, RI.
Pereira, F., N. Tishby, and L. Lee (1993). Distributional clustering of English words. In Proceedings of ACL 1993, pp. 183–190, Columbus, OH.
Procter, P., ed. (1978). Longman Dictionary of Contemporary English, Longman Group Ltd., Harlow, U.K.
Pustejovsky, J. (1995). The Generative Lexicon, MIT Press, Cambridge, MA.
Resnik, P. (1993). Selection and information: A class-based approach to lexical relationships, PhD thesis, University of Pennsylvania, Philadelphia, PA.
Resnik, P. (1995). Using information context to evaluate sematic similarity in a taxonomy. In Proceedings of IJCAI95, pp 448–453, Montreal, Canada.
Resnik, P. (1997). Selectional preference and sense disambiguation. In Proceedings of the ACL Workshop on Tagging Text with Lexical Semantics, pp. 52–57, Washington.
Resnik, P. and D. Yarowsky (1999). Distinguishing systems and distinguishing senses: new evaluation methods for word sense disambiguation systems. Journal of Natural Language Engineering 5(2):113–133.
Sanderson, M. (1994). Word sense disambiguation and information retrieval. Proceedings of SIGIR 1994,pp. 142–151, Dublin, Ireland.
Schütze, H. (1992). Dimensions of meaning. In Proceedings of Supercomputing ’92, pp. 787–796, Minneapolis, MN.
Schütze, H. (1998). Automatic word sense discrimination. Computational Linguistics, 24(1):97–123. Schütze, H. and J. Pedersen (1995). Information retrieval based on word senses. In 4th Annual Symposium on Document Analysis and Information Retrieval, pp. 161–175, Las Vegas, NV.
Sinclair, J., ed. (1987). Collins COBUILD English Language Dictionary, Collins, London and Glasgow, U.K.
Sinha, R. and R. Mihalcea (2007). Unsupervised graphbased word sense disambiguation using measures of word semantic similarity. In Proceedings of the IEEE International Conference on Semantic Computing, pp. 363–369, Irvine, CA.
Small, S. and C. Rieger (1982). Parsing and comprehending with word experts (a theory and its realization). In W. Lehnert and M. Ringle (eds.), Strategies for Natural Language Processing, Lawrence Erlbaum Associates, Hillsdale, NJ.
Stevenson, M. (2003). Word Sense Disambiguation: The Case for Combinations of Knowledge Sources. CSLI Publications, Stanford, CA.
Stevenson, M. and Y. Wilks (2001). The interaction of knowledge sources in word sense disambiguation. Computational Linguistics, 27(3):321–349.
Veronis, J. and N. Ide (1990). Word sense disambiguation with very large neural networks extracted from machine readable dictionaries. In Proceedings, COLING-90, pp. 389–394, Helsinki, Finland. Voorhees, E. (1993). Using WordNet to disambiguate word senses for text retrieval. In Proceedings of SIGIR’93, pp. 171–180, Pittsburgh, PA.
Walker, D. and R. Amsler (1986). The use of machine-readable dictionaries in sublanguage analysis. In R. Grishman and R. Kittredge (eds.), Analyzing Language in Restricted Domains: Sublanguage Description and Processing, Lawrence Erlbaum, Hillsdale, NJ, pp. 69–84.
Wang, X. and J. Carroll (2005). Word sense disambiguation using sense examples automatically acquired from a second language. In Proceedings of HLT/EMNLP 2005, pp. 547–554, Vancouver, BC, Canada. Wilks, Y. (1975). A preferential, pattern-seeking semantics for natural language inference. Artificial Intelligence, 6:53–74.
Winograd, T. (1972). Understanding Natural Language. Academic Press, New York.
Woods, W., R. Kaplan, and B. Nash-Webber (1972). The lunar sciences natural language information system: Final report. BBN Technical Report No. 2378, Cambridge, MA.
Wu, Z. and M. Palmer (1994). Verb semantics and lexical selection. In Proceedings of ACL 1994, pp.133–138, Las Cruces, NM.
Yarowsky, D. (1992). Word-Sense disambiguation using statistical models of Roget’s categories trained on large corpora. In Proceedings, COLING-92, pp. 454–460, Nantes, France.
Yarowsky, D. (1993). One sense per collocation. In Proceedings, ARPA Human Language Technology Workshop, pp. 266–271, Princeton, NJ.
Yarowsky, D. (1994). Decision lists for lexical ambiguity resolution: Application to accent restoration in Spanish and French. In Proceedings of ACL 1994, pp. 88–97, Las Cruces, NM.
Yarowsky, D. (1995). Unsupervised word sense disambiguation rivaling supervised methods. In Proceedings of ACL 1995, pp. 189–196, Cambridge, MA.
Yarowsky, D. (2000). Hierarchical decision lists for word sense disambiguation. Computers and the Humanities, 34(2):179–186.
Yarowsky, D. and R. Florian (2002). Evaluating sense disambiguation performance across diverse parameter spaces. Natural Language Engineering, 8(4):293–310.
Yarowsky, D., R. Florian, S. Cucerzan, and C. Schafer (2001). The Johns Hopkins Senseval-2 system description. In Proceedings of the Senseval-2 Workshop, pp. 163–166, Toulouse, France.
Yngve, V. (1955). Syntax and the problem of multiple meaning. In W. Locke and D. Booth (eds.), Machine Translation of Languages, Wiley, New York.
Zernik, U. (1991). Train1 vs. train2: Tagging word senses in a corpus. In U. Zernik (ed.), Lexical Acquisition: Exploiting On-Line Resources to Build a Lexicon, Lawrence Erlbaum, Hillsdale, NJ, pp. 91–112.
Zhong, Z., H. T. Ng., and Y. S. Chan (2008). Word sense disambiguation using OntoNotes: An emprical study. In Proceedings of EMNLP 2008, pp. 1002–1010, Honolulu, HI.
Zhu, J. B. and E. Hovy (2007). Active learning for word sense disambiguation with methods for addressing the class imbalance problem. In Proceedings of EMNLP/CoNLL07, pp. 783–790, Prague, Czech Republic.
