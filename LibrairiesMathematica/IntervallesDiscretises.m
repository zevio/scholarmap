(* ::Package:: *)

(* ::Input:: *)
(*Quit*)


(* ::Title:: *)
(*G\[EAcute]n\[EAcute]ration de descripteurs sous forme d'intervalles discr\[EAcute]tis\[EAcute]s*)


(* ::Input::Initialization:: *)
BeginPackage["IntervallesDiscretises`"];


(* ::Text:: *)
(*Ce package a pour but de fournir un ensemble de fonctions permettant de manipuler des jeux d'\[EAcute]tiquettes ordonn\[EAcute]es par une relation d'ordre \[Precedes] (support\[EAcute]e par une fonction de type Sort[])*)


(* ::Text:: *)
(*Soit un ensemble d'\[EAcute]tiquettes ordonn\[EAcute]es E = {Subscript[e, 1], Subscript[e, 2], ..., Subscript[e, N]}, tel que pour tout i , Subscript[e, i]\[Precedes]Subscript[e, j+1](OrderedQ[{Subscript[e, i], Subscript[e, i+1]},\[Precedes]]==True)*)


(* ::Section:: *)
(*Partie publique du package*)


(* ::Input::Initialization:: *)
myOrderedSet::usage="Header d\[EAcute]crivant une liste ordonn\[EAcute]e";
myOrderedSet/:Normal[myOrderedSet[y___]]:=List[y];


(* ::Input::Initialization:: *)
myListPartitions::usage="myListPartitions[list] Retourne toutes les sous listes possibles compos\[EAcute]es d'\[EAcute]l\[EAcute]ments cons\[EAcute]cutifs de la liste initiales:\n
\"RemoveEmptyAndCompleteSet\"->False rajoute dans les sous listes, la liste vide et la liste compl\[EGrave]te.";


(* ::Input::Initialization:: *)
myOrderedSetToTag::usage="myOrderedSetToTag[ set, prefix, separator] Compose une \[EAcute]tiquette d'intervalle discr\[EAcute]tis\[EAcute] \[AGrave] partir de la liste des valeurs et d'un pr\[EAcute]fixe et d'un s\[EAcute]parateur.";


(* ::Input::Initialization:: *)
myTagToOrderedSet::usage="myTagToOrderedSet[ set, separator] Extrait l'objet myOrderedSet \[AGrave] partir d'une \[EAcute]tiquette produite par la fonction myOrderedSetToTag. Le premier champs s\[EAcute]par\[EAcute] par le s\[EAcute]parateur est consid\[EAcute]r\[EAcute] comme un pr\[EAcute]fixe.\n
\"ToExpression\"->True les \[EAcute]l\[EAcute]ments de l'objet myOrderedSet sont consid\[EAcute]r\[EAcute]s comme des expression (par d\[EAcute]faut ce sont des String).";


(* ::Input::Initialization:: *)
myOrderedSetsIntersection::usage="myOrderedSetsIntersection[ list] renvoie l'objet myOrderedSet r\[EAcute]sultat de l'intersection de plusieurs myOrderedSet."


(* ::Input::Initialization:: *)
myTagIntersection::usage="myTagIntersection[listOfTags, separator] renvoie le tag compress\[EAcute] r\[EAcute]sultat de l'intersection de plusieurs tags.";


(* ::Input::Initialization:: *)
myOrderedSetDescription::usage="myOrderedSetDescription[value, language] renvoie une description de la valeur val sous forme d'une liste d'intervalle de type myOrderedSet. Les objets myOrderedSet sont construit \[AGrave] partir du langage de description donn\[EAcute] en param\[EGrave]tre (dont la caleur est un \[EAcute]l\[EAcute]ment.";


(* ::Input::Initialization:: *)
hasAGeneralization::usage="hasAGeneralization[patt_,listOfPatt_] returns True there is at least one pattern in listOfPatt which is a generalization of patt.";


(* ::Input::Initialization:: *)
generators::usage="generators[listOfPatt_] returns the genrators of the pattern list listOfPatt.";


(* ::Input::Initialization:: *)
(*patternToHisto::usage="patternToHisto[patterns_,intervalRules_, infRules_,xLengths:{Rule[_String,{_,_}]..},size_] produit un graphique repr\[EAcute]sentatif d'une courbe d'influence discr\[EAcute]tis\[EAcute]e.";*)


(* ::Section:: *)
(*Private*)


(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*myOrderedSet*)


(* ::Input:: *)
(*InputForm@myOrderedSet[2,3,4]*)


(* ::Input:: *)
(*InputForm@Normal@myOrderedSet[2,3,4]*)


(* ::Subsection::Closed:: *)
(*myListPartitions*)


(* ::Text:: *)
(*Retourne toutes les sous listes possibles compos\[EAcute]es d'\[EAcute]l\[EAcute]ments cons\[EAcute]cutifs de la liste initiales*)


(* ::Input::Initialization:: *)
Clear[myListPartitions];
Options[myListPartitions]={"RemoveEmptyAndCompleteSet"->True};
myListPartitions[list_List,OptionsPattern[]]:=
Flatten[
Map[
{myOrderedSet@@Take[list,#],myOrderedSet@@Take[list,-Length[list]+#]}&,
Range[Length[list]-Boole[OptionValue@"RemoveEmptyAndCompleteSet"]]
],
1]


(* ::Subsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*myListPartitions[Range[3]]*)


(* ::Input:: *)
(*myListPartitions[Range[3],"RemoveEmptyAndCompleteSet"->False]*)


(* ::Input:: *)
(*myListPartitions[{"Jamais","Occasion","R\[EAcute]gulier","Permanent"}]*)


(* ::Input:: *)
(*myListPartitions[{"Jamais","Occasion","R\[EAcute]gulier","Permanent"},"RemoveEmptyAndCompleteSet"->False]*)


(* ::Input:: *)
(*Normal/@myListPartitions[{"Jamais","Occasion","R\[EAcute]gulier","Permanent"}]*)


(* ::Subsection::Closed:: *)
(*myOrderedSetToTag*)


(* ::Text:: *)
(*Compose une \[EAcute]tiquette d'intervalle discr\[EAcute]tis\[EAcute] \[AGrave] partir de la liste des valeurs et d'un pr\[EAcute]fixe*)


(* ::Input::Initialization:: *)
Clear[myOrderedSetToTag];

myOrderedSetToTag[set_myOrderedSet,separator_String]:=With[{list=Normal[set],prefix=First@First@StringSplit[Normal@set,separator]},StringJoin[Riffle[Prepend[Map[Last[StringSplit[#,separator]]&,list],prefix],separator]]]


(* ::Subsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*myOrderedSetToTag[myOrderedSet["T_2","T_3","T_4"],"_"]*)


(* ::Subsection:: *)
(*myTagToOrderedSet*)


(* ::Text:: *)
(*Extrait l'objet myOrderedSet \[AGrave] partir d'une \[EAcute]tiquette produite par la fonction myOrderedSetToTag*)


(* ::Input::Initialization:: *)
myTagToOrderedSet[tag_String,separator_String,OptionsPattern[]]:=
With[{splitted=StringSplit[tag,separator]},
With[{k=First@splitted,list=Rest@splitted},
Apply[myOrderedSet,Map[StringJoin[k,separator,#]&,list]]
]
];


(* ::Subsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*InputForm@myTagToOrderedSet["T_2_3_4","_"]*)


(* ::Subsection::Closed:: *)
(*myOrderedSetsIntersection*)


(* ::Text:: *)
(*Renvoie l'objet myOrderedSet r\[EAcute]sultat de l'intersection de plusieurs myOrderedSet.*)


(* ::Input::Initialization:: *)
Clear[myOrderedSetsIntersection];
Options[myOrderedSetsIntersection]={};

myOrderedSetsIntersection[list:{_myOrderedSet..},OptionsPattern[]]:=
Apply[myOrderedSet,Apply[Intersection,Normal/@list]];


(* ::Subsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*myOrderedSetsIntersection[{myOrderedSet["2", "3", "4"],myOrderedSet["3", "4"],myOrderedSet["4"]}]*)


(* ::Input:: *)
(*myOrderedSetsIntersection[{myOrderedSet["2", "3", "4"],myOrderedSet["3", "4","5"]}]*)


(* ::Subsection::Closed:: *)
(*myTagIntersection*)


(* ::Text:: *)
(*Renvoie le tag compress\[EAcute] r\[EAcute]sultat de l'intersection de plusieurs tags.*)


(* ::Input::Initialization:: *)
Clear[myTagIntersection];
Options[myTagIntersection]={};

myTagIntersection[{},_,OptionsPattern[]]:={};
myTagIntersection[list:{_String..},separator_String,OptionsPattern[]]:=
With[{partition=GatherBy[list,First[StringSplit[#,separator]]&]},
Map[
With[
{
prefix=First[StringSplit[First[#],separator]],
sets=Map[myTagToOrderedSet[#,separator]&,#]
},
myOrderedSetToTag[myOrderedSetsIntersection[sets],separator]
]&,
partition
]
];


(* ::Subsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*myTagIntersection[{"T_2_3_4","T_3_4_5"},"_"]*)


(* ::Input:: *)
(*myTagIntersection[{"T_2_3_4","T_3_4_5","D_1_2","D_2_3"},"_"]*)


(* ::Input:: *)
(*myTagIntersection[{"alcohol_onceAMonth_onceAWeek_moreThanOnceAWeek","cannabis_Non_triedOnce","alcohol_onceTwiceAYear_onceAMonth_onceAWeek_moreThanOnceAWeek","cannabis_Non_triedOnce_occasional"},"_"]*)


(* ::Subsection::Closed:: *)
(*myOrderedSetDescription*)


(* ::Input::Initialization:: *)
Clear[myOrderedSetDescription];
Options[myOrderedSetDescription]={};

myOrderedSetDescription[value_, language_,OptionsPattern[]]:=
Select[myListPartitions[language],MemberQ[#,value]&];


(* ::Subsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*myOrderedSetDescription[1,Range[4]]*)


(* ::Input:: *)
(*myOrderedSetDescription[3,Range[4]]*)


(* ::Input:: *)
(*With[{val=11},myOrderedSetsIntersection@myOrderedSetDescription[val,Range[100]]==myOrderedSet[val]]*)


(* ::Subsection::Closed:: *)
(*hasAGeneralization*)


(* ::Input::Initialization:: *)
ClearAll@hasAGeneralization;
(*hasAGeneralization[patt_myOrderedSet,listOfPatt:{_myOrderedSet...}]:=
Module[{i=1,p=Normal@patt,list=Normal/@listOfPatt},
While[i<=Length[list]&&Not[SubsetQ[p,list[[i]]]],i++];
i \[NotEqual](1+ Length[list])
];
*)hasAGeneralization[patt_,listOfPatt:{_List...}]:=
Module[{i=1,p=Normal@patt,list=listOfPatt},
While[i<=Length[list]&&Not[SubsetQ[p,list[[i]]]],i++];
i !=(1+ Length[list])
];
hasAGeneralization[patt_myOrderedSet,listOfPatt:{_myOrderedSet...}]:=hasAGeneralization[Normal@patt,Normal/@listOfPatt];


(* ::Subsubsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*hasAGeneralization[myOrderedSet["T_1","T_2","T_3"],{myOrderedSet["T_1","T_2"]}]*)


(* ::Input:: *)
(*hasAGeneralization[myOrderedSet["T_1","T_2","T_3"],{myOrderedSet["T_3","T_4"]}]*)


(* ::Subsection::Closed:: *)
(*generators*)


(* ::Input::Initialization:: *)
ClearAll@generators;
generators[listOfPatt:{(_myOrderedSet|_List)..}]:=generators[listOfPatt,{}];
generators[{},mostGenerals:{(_myOrderedSet|_List)..}]:=mostGenerals;
generators[listOfPatt:{(_myOrderedSet|_List)..},mostGenerals:{(_myOrderedSet|_List)...}]:=
With[
{patt=First@listOfPatt, rest=Rest@listOfPatt},
If[
hasAGeneralization[patt,mostGenerals]||hasAGeneralization[patt,rest],
(*it is not a most general pattern and it is not kept.*)
generators[rest,mostGenerals],
(* there is no more general pattern and it is kept ie added to most generals patterns.*)
generators[rest,Append[mostGenerals,patt]]]
]


(* ::Subsubsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*generators[{myOrderedSet["T_1","T_2","T_3"],myOrderedSet["T_1","T_2"]}]*)


(* ::Input:: *)
(*generators[{myOrderedSet["T_1"],myOrderedSet["T_1","T_2"],myOrderedSet["T_1","T_2","T_3"]}]*)


(* ::Input:: *)
(*generators[{myOrderedSet["T_2","T_3"],myOrderedSet["T_1","T_2"],myOrderedSet["T_1","T_2","T_3"]}]*)


(* ::Subsection:: *)
(*patternToHisto*)


(* ::Input:: *)
(*ClearAll@patternToHisto;*)


(* ::Input::Initialization:: *)
(*patternToHisto[patterns_,intervalRules_, infRules_,xLengths:{Rule[_String,{_,_}]..},size_]:=With[{
intervals=Normal@Map[
(
{Max@#,Min@#}&@Apply[
Union,
(Normal[myTagToOrderedSet[First[myTagIntersection[#,"_"]],"_"]]/.intervalRules)/.infRules
]
)&,
GroupBy[patterns,StringTake[#,1]&]
]//
(First/@xLengths)/.#/._String\[Rule]({-\[Infinity],\[Infinity]}/.infRules)&//
Map[Sort,#]&
},ListLinePlot[
{
Flatten[MapThread[Table[{i,#1},{i,Range@@#2}]&,{First/@intervals,Last/@xLengths}],1],
Flatten[MapThread[Table[{i,#1},{i,Range@@#2}]&,{Last/@intervals,Last/@xLengths}],1]
},PlotRange\[Rule]{All,.99999*({-\[Infinity],\[Infinity]}/.infRules)},Filling\[Rule]{1\[Rule]{2}},
Ticks\[Rule]{None,Automatic},
PlotStyle\[Rule]{Blue,Blue},
ImageSize\[Rule]size
]
]
*)


(* ::Subsubsection::Closed:: *)
(*Tests*)


(* ::Input:: *)
(*With[{intervalRules={"1"->{-\[Infinity],-25},"2"-> {-25,-15},"3"->{-15,-5},"4"->{-5,5},"5"->{5,15},"6"->{15,25},"7"->{25,\[Infinity]}},*)
(*infRules={-\[Infinity]->-40,\[Infinity]->40},*)
(*xLengths={"B"->{1,7},"S"->{8,11},"A"->{12,12}}*)
(*},*)
(*Row[{patternToHisto[{"A_1_2","B_3_4_5"},intervalRules,infRules,xLengths,200],*)
(*patternToHisto[{"A_1","S_4","B_7"},intervalRules,infRules,xLengths,200]}]*)
(*]*)
(**)


(* ::Subsection:: *)
(**)


(* ::Input::Initialization:: *)
End[];


(* ::Section:: *)
(*Fin*)


(* ::Input::Initialization:: *)
EndPackage[];
