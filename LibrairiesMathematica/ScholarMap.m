(* ::Package:: *)

(* ::Title:: *)
(*ScholarMap*)


(* ::Subtitle:: *)
(*Extraction de connaissances pour la recherche d'experts*)


(* ::Input::Initialization:: *)
BeginPackage["ScholarMap`",{"MinerLC`"}];


(* ::Section:: *)
(*Partie publique du package*)


(* ::Subsection:: *)
(*Param\[EGrave]tre -> r\[EAcute]sultats*)


(* ::Input::Initialization:: *)
parameterToResult::usage="parameterToResult[result_,parameter_] Associe les r\[EAcute]sultats d'une exp\[EAcute]rience \[AGrave] son param\[EGrave]tre";


(* ::Subsection:: *)
(*Pourcentage de motifs clos abstraits s\[EAcute]lectionn\[EAcute]s*)


(* ::Input::Initialization:: *)
percentageClosAbs::usage="percentageClosAbs[selectedClosAbs_, closabs_] Calcul le pourcentage de motifs clos abstraits s\[EAcute]lectionn\[EAcute]";


(* ::Subsection:: *)
(*Graphes*)


(* ::Subsubsection:: *)
(*Orientation*)


(* ::Input::Initialization:: *)
directedSelfLoopQ::usage="directedSelfLoopQ[DirectedEdge[v1_,v2_]] V\[EAcute]rifie si un arc dans un graphe orient\[EAcute] est une boucle";


(* ::Input::Initialization:: *)
undirectedSelfLoopQ::usage="undirectedSelfLoopQ[UndirectedEdge[v1_, v2_]] V\[EAcute]rifie si une ar\[EHat]te dans un graphe non orient\[EAcute] est une boucle";


(* ::Subsubsection:: *)
(*Relations dans un graphe*)


(* ::Input::Initialization:: *)
createOrientedEdges::usage="createOrientedEdges[source_, targets_] Cr\[EAcute]e des arcs orient\[EAcute]s dans un graphe, du sommet source_ vers les sommets targets_";


(* ::Input::Initialization:: *)
linkConceptsFromPublications::usage="linkConceptsFromPublications[conceptsPub1_, conceptsPub2_] Permet de cr\[EAcute]er des arcs dans un graphe orient\[EAcute] entre deux sommets \[EAcute]tant des concepts issus de publications distinctes. Ces arcs sont cr\[EAcute]\[EAcute]s entre tous les concepts ci, cj issus de deux publications i et j, ci appartenant \[AGrave] conceptsPub1, cj appartenant \[AGrave] conceptsPub2";


(* ::Input::Initialization:: *)
createBipartiteEdges::usage="createBipartiteEdges[source_, targets_] Cr\[EAcute]e les ar\[EHat]tes d'un graphe biparti";


(* ::Subsubsection:: *)
(*Modularit\[EAcute] locale*)


(* ::Input::Initialization:: *)
localModularityBipartite::usage="localModularityBipartite[graph_Graph, ext_] Calcule la modularit\[EAcute] locale des sommets dans un graphe biparti";


(* ::Subsection:: *)
(*G\[EAcute]n\[EAcute]ration des descripteurs*)


(* ::Subsubsection:: *)
(*Ann\[EAcute]es de publication*)


(* ::Input::Initialization:: *)
yearToLabels::usage="yearToLabels[year_,yearThresholds_] \[CapitalAGrave] partir du param\[EGrave]tre yearThresholds_ correspondant \[AGrave] des intervalles d'ann\[EAcute]e, l'ann\[EAcute]e year_ va \[EHat]tre repr\[EAcute]sent\[EAcute]e sous forme de labels. Ces labels situent l'ann\[EAcute]e repr\[EAcute]sent\[EAcute]e par rapport aux ann\[EAcute]es composant l'intervalle. Par exemple, si l'ann\[EAcute]e est 2017 et l'intervalle est {2013, 2018, 2020}, les labels repr\[EAcute]sentant 2017 seront >2013, <2018, <2020.";


(* ::Subsubsection:: *)
(*Auteurs*)


(* ::Input::Initialization:: *)
normalizeAuthor::usage="normalizeAuthor[author_] Normalise un auteur en rempla\[CCedilla]ant les espaces par _";


(* ::Subsubsection:: *)
(*Concepts s\[EAcute]mantiques*)


(* ::Input::Initialization:: *)
normalizeConcept::usage="normalizeConcept[concept_] Normalise un concept en rempla\[CCedilla]ant les espaces par _";


(* ::Subsection:: *)
(*R\[EAcute]partition des descripteurs composant les motifs clos*)


(* ::Subsubsection:: *)
(*R\[EAcute]partition des auteurs*)


(* ::Input::Initialization:: *)
countAuthors::usage="countAuthors[pattern_] Compte le nombre d'auteurs pr\[EAcute]sents dans les descripteurs qui composent un motif clos";


(* ::Subsubsection:: *)
(*R\[EAcute]partition des concepts s\[EAcute]mantiques*)


(* ::Input::Initialization:: *)
countConcepts::usage="countConcepts[pattern_] Compte le nombre de concepts s\[EAcute]mantiques pr\[EAcute]sents dans les descripteurs qui composent un motif clos";


(* ::Subsection:: *)
(*Matrices*)


(* ::Input::Initialization:: *)
subMatrix::usage="subMatrix[matrix_,line_,column_] R\[EAcute]cup\[EGrave]re la sous-matrice de matrix \[AGrave] partir de la ligne line et la colonne column";


(* ::Input::Initialization:: *)
sumMatrix::usage="sumMatrix[matrix] Calcul interm\[EAcute]diaire pour le calcul de la matrice cumul\[EAcute]e de matrix";


(* ::Input::Initialization:: *)
cumulativeMatrix::usage="cumulativeMatrix[matrix_] Calcule la matrice cumul\[EAcute]e de matrix";


(* ::Input::Initialization:: *)
printMatrix::usage="printMatrix[matrix_,parameter_, nbAuthors_, nbConcepts_] Affiche la matrice de nombre de motifs clos abstraits par couple {nombre d'auteurs (nbAuthors_), nombre de concepts s\[EAcute]mantiques (nbConcepts_)} apr\[EGrave]s abstraction de graphe avec le param\[EGrave]tre parameter_";


(* ::Subsection:: *)
(*Extension des motifs clos*)


(* ::Input::Initialization:: *)
getPatternExtension::usage="getPatternExtension[results_, closabs_] Permet d'obtenir l'extension d'un motif clos abstrait \[AGrave] partir du fichier r\[EAcute]sultat de MinerLC";


(* ::Subsection:: *)
(*Filtrage des motifs clos*)


(* ::Input::Initialization:: *)
filterYear::usage="filterYear[pattern_] Retire de la liste des descripteurs composant un motif clos tous les descripteurs correspondant \[AGrave] des ann\[EAcute]es de publication";


(* ::Subsection:: *)
(*S\[EAcute]lection de motifs clos*)


(* ::Subsubsection:: *)
(*S\[EAcute]lection de motifs clos en fonction des descripteurs qui les composent*)


(* ::Input::Initialization:: *)
selectPatterns::usage="selectPatterns[closabsParameters_, nbAuthors_, nbConcepts_] S\[EAcute]lectionne les motifs clos abstraits dont les descripteurs sont compos\[EAcute]s d'au moins nbAuthors_ auteurs et nbConcepts_ concepts s\[EAcute]mantiques (par exemple, 2 auteurs et 3 concepts) pour le param\[EGrave]tre d'abstraction closAbsParameters_";


(* ::Subsubsection:: *)
(*Affichage des motifs clos s\[EAcute]lectionn\[EAcute]s*)


(* ::Input::Initialization:: *)
printSelectedPatterns::usage="printSelectedPatterns[selection_, printYears_] Affiche les motifs clos s\[EAcute]lectionn\[EAcute]s (selection_) en filtrant les ann\[EAcute]es de publication des descripteurs du motif si printYears_ est \[EAcute]gal \[AGrave] False";


(* ::Subsection:: *)
(*Affichage des r\[EAcute]sultats*)


(* ::Subsubsection:: *)
(*Affichage partiel des r\[EAcute]sultats (statistiques uniquement)*)


(* ::Input::Initialization:: *)
printPartialResults::usage="printPartialResults[results_, selection_] Affiche les statistiques d'abstraction de graphe \[AGrave] partir des r\[EAcute]sultats d'exp\[EAcute]rience results_ obtenus sur les motifs clos abstraits selection_ s\[EAcute]lectionn\[EAcute]s";


(* ::Subsubsection:: *)
(*Affichage complet des r\[EAcute]sultats (statistiques, abstraction de graphe, sous-graphe abstrait induit)*)


(* ::Input::Initialization:: *)
printResults::usage="printResults[results_, selection_] Combine dans une interface l'ensemble des r\[EAcute]sultats d'exp\[EAcute]rience results_ obtenus sur les motifs clos abstraits selection_ s\[EAcute]lectionn\[EAcute]s";


(* ::Section:: *)
(*Partie priv\[EAcute]e du package*)


(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Subsection:: *)
(*Param\[EGrave]tre -> r\[EAcute]sultats*)


(* ::Input::Initialization:: *)
(*parameterToResult[result_,parameter_] :=
Join[{parameter},Map[#&,result]];*)


(* ::Input::Initialization:: *)
parameterToResult[result_,parameter_] :=
Join[{parameter,Map[#&,result]}];


(* ::Subsection:: *)
(*Pourcentage de motifs clos abstraits s\[EAcute]lectionn\[EAcute]s*)


(* ::Input::Initialization:: *)
percentageClosAbs[selectedClosAbs_, closabs_] :=Quiet@Map[N[(Length@selectedClosAbs[[#]]*100)/Length@closabs[[#]]]&,Range[Length@closabs]];


(* ::Subsection:: *)
(*Graphes*)


(* ::Subsubsection:: *)
(*Orientation*)


(* ::Input::Initialization:: *)
directedSelfLoopQ[DirectedEdge[v1_,v2_]] := v1 == v2;


(* ::Input::Initialization:: *)
undirectedSelfLoopQ[UndirectedEdge[v1_, v2_]] := v1 == v2;


(* ::Subsubsection:: *)
(*Relations dans un graphe*)


(* ::Input::Initialization:: *)
createOrientedEdges[source_, targets_] := 
Map[DirectedEdge[source,#]&,targets];


(* ::Input::Initialization:: *)
linkConceptsFromPublications[conceptsPub1_,conceptsPub2_]:=
Map[createOrientedEdges[#,conceptsPub2]&,conceptsPub1];


(* ::Input::Initialization:: *)
createBipartiteEdges[source_, targets_] :=
Map[UndirectedEdge[source,#]&,targets];


(* ::Subsubsection:: *)
(*Modularit\[EAcute] locale*)


(* ::Input::Initialization:: *)
localModularityBipartite[graph_Graph, ext_]:=Module[
{gt,lt,m,bigKt,bigDt},
gt=Subgraph[graph,ext];
lt=EdgeCount[gt];
m=EdgeCount[graph];
bigKt=Apply[Plus,Map[VertexOutDegree[graph,#]&,ext]];
bigDt=Apply[Plus,Map[VertexInDegree[graph,#]&,ext]];
(lt/m)-( (bigKt*bigDt)/m^2)
]


(* ::Subsection:: *)
(*G\[EAcute]n\[EAcute]ration des descripteurs*)


(* ::Subsubsection:: *)
(*Ann\[EAcute]es de publication*)


(* ::Input::Initialization:: *)
yearToLabels[year_,yearThresholds_]:=
Flatten[
Map[{
If[year<= #,"Year<="<>ToString[#],Nothing],
If[year>#,"Year>"<>ToString[#],Nothing]}&,
yearThresholds]];


(* ::Subsubsection:: *)
(*Auteurs*)


(* ::Input::Initialization:: *)
normalizeAuthor[author_] := 
StringJoin["AUTH_",StringReplace[author,{","->"", " "->"_"}]];


(* ::Subsubsection:: *)
(*Concepts s\[EAcute]mantiques*)


(* ::Input::Initialization:: *)
normalizeConcept[concept_] := 
StringJoin["CONC_",StringReplace[concept," "->"_"]];


(* ::Subsection:: *)
(*R\[EAcute]partition des descripteurs composant les motifs clos*)


(* ::Subsubsection:: *)
(*R\[EAcute]partition des auteurs*)


(* ::Input::Initialization:: *)
countAuthors[pattern_] :=
Count[Map[StringContainsQ[#,"AUTH_"]&,pattern],True];


(* ::Subsubsection:: *)
(*R\[EAcute]partition des concepts s\[EAcute]mantiques*)


(* ::Input::Initialization:: *)
countConcepts[pattern_] :=
Count[Map[StringContainsQ[#,"CONC_"]&,pattern],True];


(* ::Subsection:: *)
(*Matrices*)


(* ::Input::Initialization:: *)
subMatrix[matrix_,line_,column_] := Table[matrix[[i]][[j]],{i,line,Length[matrix]},{j,column,Length[matrix[[1]]]}]


(* ::Input::Initialization:: *)
sumMatrix[matrix_] :=
Module[{sum,i,j},
sum=0;
For[i=1,i<=Length[matrix],i++,
For[j=1,j<=Length[matrix[[1]]],j++,
sum = sum+matrix[[i]][[j]];
]
];
sum
];


(* ::Input::Initialization:: *)
cumulativeMatrix[matrix_] :=
Table[sumMatrix[subMatrix[matrix,i,j]],{i,1,Length[matrix]},{j,1,Length[matrix[[1]]]}]


(* ::Input::Initialization:: *)
printMatrix[matrix_,parameter_, nbAuthors_, nbConcepts_] :=Labeled[TableForm[Map[Last, GatherBy[matrix, First], {2}], TableHeadings -> {Map["#a = " <> ToString[#] &, Range[0, nbAuthors]], Map["#c = " <> ToString[#] &, Range[0, nbConcepts]]}],"Matrice de nombre de motifs clos abstraits par couple {nombre d'auteurs, nombre de concepts s\[EAcute]mantiques} : Abstraction "<>parameter];


(* ::Subsection:: *)
(*Extension des motifs clos*)


(* ::Input::Initialization:: *)
getPatternExtension[results_, closabs_]:=
Map[results["ExtAbs"][#]&,closabs];


(* ::Input::Initialization:: *)
getPatternExtensionHA[results_, closabs_]:=
Map[Union[results["ExtAbs"][#]["autorities"],results["ExtAbs"][#]["hubs"]]&,closabs];;


(* ::Subsection:: *)
(*Filtrage des motifs clos*)


(* ::Input::Initialization:: *)
filterYear[pattern_]:=
Select[Select[pattern,Not[StringMatchQ[#,"A_Year"~~___]]& ], Not[StringMatchQ[#,"P_Year"~~___]]& ];


(* ::Subsection:: *)
(*S\[EAcute]lection de motifs clos*)


(* ::Subsubsection:: *)
(*S\[EAcute]lection de motifs clos en fonction des descripteurs qui les composent*)


(* ::Input::Initialization:: *)
selectPatterns[closabsParameters_, nbAuthors_, nbConcepts_] :=
Select[closabsParameters, Function[{pattern}, With[
{
nbAuth=Plus@@StringCount[pattern,"AUTH_"~~___],
nbConc=Plus@@StringCount[pattern,"CONC_"~~___]
},
nbAuth>=nbAuthors && nbConc >=  nbConcepts]]];


(* ::Subsubsection:: *)
(*Affichage des motifs clos s\[EAcute]lectionn\[EAcute]s*)


(* ::Input::Initialization:: *)
printSelectedPatterns[selection_, printYears_] :=
With[
{
showYearsLabels=printYears
},
Grid[
Map[
With[{pattern=If[showYearsLabels,#,filterYear@#]},
Map[
Which[
StringMatchQ[#,"AUTH_"~~___],Style[#,Purple],
StringMatchQ[#,"CONC_"~~___],Style[#,Blue],
True,#
]&,
pattern
]
]&,
selection
],
Frame->All
]
];


(* ::Subsection:: *)
(*Affichage des r\[EAcute]sultats*)


(* ::Subsubsection:: *)
(*Affichage partiel des r\[EAcute]sultats (statistiques uniquement)*)


(* ::Input::Initialization:: *)
printPartialResults[results_, selection_] :=
Row[{
printGraphAbstractionStatistics[results,selection, "ExtraLines" -> Sequence[{"MeanExpectedSupAbs", results["MeanExpectedSupAbs"][selection], Null},{"SigmaExpectedSupAbs",results["SigmaExpectedSupAbs"][selection], Null}]]
}]


(* ::Subsubsection:: *)
(*Affichage complet des r\[EAcute]sultats (statistiques, abstraction de graphe, sous-graphe abstrait induit)*)


(* ::Input::Initialization:: *)
printResults[results_, selection_] :=
Manipulate[
Row[{
highlightGraphAbstraction[results,ImageSize->400],
printGraphAbstractionStatistics[results,pattern, "ExtraLines" -> Sequence[{"MeanExpectedSupAbs", results["MeanExpectedSupAbs"][pattern], Null},{"SigmaExpectedSupAbs",results["SigmaExpectedSupAbs"][pattern], Null}]],
CompleteGraphQ@Subgraph[results["Graphe"],results["ExtAbs"][pattern]]
}],
{pattern,selection},
SaveDefinitions->True
]


(* ::Input::Initialization:: *)
End[];


(* ::Input::Initialization:: *)
EndPackage[];


(* ::Subsection:: *)
(*Repr\[EAcute]sentation \[EAcute]tendue des motifs clos*)


(* ::Subsubsection:: *)
(*Version intentionnelle*)


(* ::Input::Initialization:: *)
distance[extQi_, extQj_] :=
1-(Length@Intersection[extQi, extQj] / Length@Union[extQi, extQj]);


(* ::Input::Initialization:: *)
representationInt[results_, pattern_, selectedPatterns_] :=
With[{patternsToModularity = Association[Map[Rule[#,N@localModularity[results["Graphe"], 
results["ExtAbs"][#]]]&,results["ClosAbs"]]], qi = pattern, extQi = results["ExtAbs"][pattern], q = selectedPatterns,L =results["Items"]},
distances = Association@Map[# -> N@distance[extQi,results["ExtAbs"][#]]&,q];
Nj = Keys@Select[distances,#<0.3&];
UextTj = Union@Flatten@Map[Union[results["ExtAbs"][#]]&,Nj];
intTj = Association@Map[# ->  calculApparitionNj[#, Nj]&,L];
ReverseSort@Select[intTj,#>0&]
];


(* ::Subsubsection:: *)
(*Version extensionnelle 1*)


(* ::Input::Initialization:: *)
apparitionFrequency[results_,vertex_,languageElement_]:=
N@Count[Map[MemberQ[results["Itemsets"][#], languageElement]&,vertex],True] / Length@vertex


(* ::Input::Initialization:: *)
representationExt1[results_, pattern_, selectedPatterns_]:=With[{patternsToModularity = Association[Map[Rule[#,N@localModularity[results["Graphe"], 
results["ExtAbs"][#]]]&,results["ClosAbs"]]], qi = pattern, extQi = results["ExtAbs"][pattern], q = selectedPatterns,L =results["Items"]},
distances = Association@Map[# -> N@distance[extQi,results["ExtAbs"][#]]&,q];
Nj = Keys@Select[distances,#<0.3&];
 extTj =extQi;
intTj = Association@Map[# -> frequenceApparitionSommets[extTj, #]&,L];
ReverseSort@Select[intTj, #>0.8&]
]


(* ::Subsubsection:: *)
(*Version extensionnelle 2*)


(* ::Input::Initialization:: *)
representationExt2[results_, pattern_, selectedPatterns_]:=With[{patternsToModularity = Association[Map[Rule[#,N@localModularity[results["Graphe"], 
results["ExtAbs"][#]]]&,results["ClosAbs"]]], qi = pattern, extQi = results["ExtAbs"][pattern], q = selectedPatterns,L =results["Items"]},
distances = Association@Map[# -> N@distance[extQi,results["ExtAbs"][#]]&,q];
Nj = Keys@Select[distances,#<0.3&];
extTj = Union@Flatten@Map[results["ExtAbs"][#]&,Nj];
intTj = Association@Map[# -> apparitionFrequency[extTj, #]&,L];
ReverseSort@Select[intTj, #>0.8&]
]


(* ::Input::Initialization:: *)
representationEtendue[in_, notin_, repEtendue_]:=
BarChart[{Association@in,Association@notin}, BarOrigin->Left, Axes->False, ChartLabels->Placed[Reverse@Map[StringJoin[StringJoin[ToString[First@#]," : "], ToString[Last@#]]&,Normal@repEtendue],After], ChartStyle->{Red, Blue}]


(* ::Subsection:: *)
(*Evaluation*)


(* ::Input::Initialization:: *)
precision[authorsGS_, authorsPattern_] :=
100*N[Length[Intersection[authorsGS,authorsPattern]] / Length[authorsPattern]];


(* ::Input::Initialization:: *)
recall[authorsGS_,authorsPattern_] :=
100*N[Length[Intersection[authorsGS, authorsPattern]] / Length[authorsGS]];


(* ::Input::Initialization:: *)
fMeasure[precision_,recall_] := 
If[precision+recall==0,0,2*((precision*recall)/ (precision+recall))];
