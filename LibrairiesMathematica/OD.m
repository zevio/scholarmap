(* ::Package:: *)

(* ::Input:: *)
(*Quit*)


(* ::Title:: *)
(*G\[EAcute]n\[EAcute]ration de descripteurs sous forme d'intervalles discr\[EAcute]tis\[EAcute]s*)


(* ::Input::Initialization:: *)
BeginPackage["OD`"];


(* ::Section:: *)
(*Partie publique du package*)


(* ::Input::Initialization:: *)
odLabVal::usage="header d'une expression codant une valeure \[EAcute]tiquet\[EAcute]e. Cette expression prend la forme suivante:
obLabVal[label,value]";


(* ::Input::Initialization:: *)
odLabInt::usage="header d'une expression codant un intervalle de valeurs \[EAcute]tiquet\[EAcute]es. Cette expression prend la forme suivante:
obLabInt[label,{val1, val2, ...}]";


(* ::Input::Initialization:: *)
$odSeparator::usage="s\[EAcute]parateur utilis\[EAcute] par d\[EAcute]faut dans la repr\[EAcute]sentation des valeurs ou intervalles \[EAcute]tiquet\[EAcute]s sous forme de cha\[IHat]ne de caract\[EGrave]res. ";


(* ::Input::Initialization:: *)
odLabel::usage="odLabel[val] renvoie l'\[EAcute]tiquette de l'expression expr (de type odLabVal ou odLabInt).";


(* ::Input::Initialization:: *)
odValue::usage="odValue[val] renvoie la valeur de l'expression expr (de type odLabVal ou odLabInt).";


(* ::Input::Initialization:: *)
odLabelSameQ::usage="odLabelSameQ[list] renvoie True si toutes les expressions de list (de type odLabVal ou odLabInt) ont la m\[EHat]me \[EAcute]tiquette.";


(* ::Input::Initialization:: *)
odGatherByLabels::usage="odGatherByLabels[list] regroupe les expressions de list (de type odLabVal ou odLabInt) d'apr\[EGrave]s leur \[EAcute]tiquette.";


(* ::Input::Initialization:: *)
odLabValRange::usage="odLabValRange[label,list] renvoie pour chaque \[EAcute]l\[EAcute]ment de list une valeur \[EAcute]tiquet\[EAcute]e de type odLabVal, ayant label pour \[EAcute]tiquettes, et pour valeur l'\[EAcute]l\[EAcute]ment list converti en cha\[IHat]ne de caract\[EGrave]res.";


(* ::Input::Initialization:: *)
odLabValJoin::usage="odLabValJoin[list] renvoie l'intervalle de valeurs \[EAcute]tiquet\[EAcute]es r\[EAcute]sultat de l'union des valeurs \[EAcute]tiquet\[EAcute]es de list (de type odLabVal).";


(* ::Input::Initialization:: *)
odLabIntRange::usage="odLabIntRange[list] renvoie la liste des intervalle \[EAcute]tiquet\[EAcute]s permettant d'encoder sous formes d'un ensemble d'intervalles inclusifs, toutes les valeurs \[EAcute]tiquet\[EAcute]es de list.";


(* ::Input::Initialization:: *)
odMemberQ::usage="odMemberQ[ int, val] renvoie True si la valeur \[EAcute]tiquet\[EAcute]e val est dnas l'intervalle de valeurs \[EAcute]tiquet\[EAcute]es int.";


(* ::Input::Initialization:: *)
odLabValToInt::usage="odLabValToInt[val, lang] renvoie la liste des intervalles de valeurs \[EAcute]tiquet\[EAcute]es d\[EAcute]crivant la valeur \[EAcute]tiquet\[EAcute]e val. Ces intervalles \[EAcute]tiquet\[EAcute]s sont pris dans la liste d'intervalles lang.";


(* ::Input::Initialization:: *)
odLabIntIntersection::usage="odLabIntIntersection[list] renvoie un intervalle de valeurs \[EAcute]tiquet\[EAcute]es r\[EAcute]sultat de l'intersection des valeurs des intervalle de valeurs \[EAcute]tiquet\[EAcute]es de list. Tous les intervalles doivent avoir la m\[EHat]me \[EAcute]tiquette.";


(* ::Input::Initialization:: *)
odToLabInts::usage="odToLabInts[p,separateur] renvoie un intervalle de valeur discr\[EAcute]tis\[EAcute]es (de type odLabInt) \[AGrave] partir de la cha\[IHat]ne de caract\[EGrave]re p. Cette cha\[IHat]ne de caract\[EGrave]re doit \[EHat]tre correctement format\[EAcute]e; ie de la forme:
label<>separateur<>val1<>separateur<>val2<>...";


(* ::Input::Initialization:: *)
odIsASpecilization::usage="odIsASpecilization[tested,reference] renvoie True si l'intervalle de valeur discr\[EAcute]tis\[EAcute] tested est une sp\[EAcute]cialisation de ref.";


(* ::Input::Initialization:: *)
odIsAGeneralization::usage="odIsAGeneralization[tested,reference] renvoie True si l'intervalle de valeur discr\[EAcute]tis\[EAcute] tested est une g\[EAcute]n\[EAcute]ralisation de ref.";


(* ::Input::Initialization:: *)
odContainAGeneralizationOf::usage="odContainAGeneralizationOf[list,p] renvoie True sir l'un des intervalles de valeurs discr\[EAcute]tis\[EAcute]es de list est une g\[EAcute]n\[EAcute]ralisation de l'intervalle de valeurs discr\[EAcute]tis\[EAcute]es p.";


(* ::Input::Initialization:: *)
odMinimaux::usage="odMinimaux[liste_List,fun_] renvoie les \[EAcute]l\[EAcute]ments minimaux de liste. Ces \[EAcute]l\[EAcute]ments minimaux sont calcul\[EAcute]s avec la relation d'ordre donn\[EAcute]e par la fonction fun. fun[x,y] doit renvoyer True si x\[SubsetEqual]y.";


(* ::Input::Initialization:: *)
odCompactLabInt::usage="odCompactLabInt[liste,separateur] renvoie une description condens\[EAcute]e de la liste des intervalles de valeurs \[EAcute]tiquett\[EAcute]es donn\[EAcute]es sous forme d'une liste de cha\[IHat]nes de caract\[EGrave]res.

odCompactLabInt[liste] renvoie une description condens\[EAcute]e de la liste des intervalles de valeurs \[EAcute]tiquett\[EAcute]es donn\[EAcute]es sous forme d'une liste d'expression de type odLabInt.";


(* ::Section:: *)
(*Private*)


(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*$odSeparator*)


(* ::Input::Initialization:: *)
$odSeparator="_";


(* ::Subsection::Closed:: *)
(*odLabel*)


(* ::Input::Initialization:: *)
odLabel[expr:(_odLabVal|_odLabInt)]:=First[expr];


(* ::Input:: *)
(*odLabel[odLabVal["D","1"]]*)
(*odLabel[odLabInt["D",{"1","2","3"}]]*)


(* ::Subsection::Closed:: *)
(*odValue*)


(* ::Input::Initialization:: *)
odValue[val:(_odLabVal|_odLabInt)]:=Last[val];


(* ::Input:: *)
(*odValue[odLabVal["D","1"]]*)
(*odValue[odLabInt["D",{"1","2","3"}]]*)


(* ::Subsection::Closed:: *)
(*ToString*)


(* ::Input::Initialization:: *)
odLabVal/:ToString[val_odLabVal]:=StringJoin[odLabel[val],$odSeparator,odValue[val]];
odLabInt/:ToString[int_odLabInt]:=Apply[StringJoin,Riffle[Join[{odLabel[int]},odValue[int]],$odSeparator]]


(* ::Input:: *)
(*ToString[odLabVal["D","3"]]//InputForm*)


(* ::Input:: *)
(*ToString[odLabInt["D",{"2","3","4"}]]//InputForm*)


(* ::Subsection::Closed:: *)
(*odLabelSameQ*)


(* ::Input::Initialization:: *)
odLabelSameQ[list:{(_odLabVal|_odLabInt)..}]:=Apply[SameQ,Map[odLabel,list]];


(* ::Input:: *)
(*odLabelSameQ[{odLabVal["D","1"],odLabVal["D","2"],odLabVal["D","3"]}]*)
(*odLabelSameQ[{odLabVal["D","1"],odLabVal["T","2"],odLabVal["D","3"]}]*)
(*odLabelSameQ[{odLabVal["D","1"],odLabInt["D",{"1","2"}]}]*)


(* ::Subsection::Closed:: *)
(*odGatherByLabels*)


(* ::Input::Initialization:: *)
odGatherByLabels[list:{(_odLabVal|_odLabInt)..}]:=GatherBy[list,odLabel]


(* ::Input:: *)
(*odGatherByLabels[{odLabVal["D","1"],odLabVal["D","2"],odLabVal["T","3"],odLabVal["T","4"],odLabVal["C","5"]}]*)


(* ::Subsection::Closed:: *)
(*odLabValRange*)


(* ::Input::Initialization:: *)
odLabValRange[label_String,list_List]:=Map[odLabVal[label,ToString[#]]&,list];


(* ::Input:: *)
(*odLabValRange["D",Range[5]]//InputForm*)


(* ::Subsection::Closed:: *)
(*odLabValJoin*)


(* ::Input::Initialization:: *)
odLabValJoin[list:{_odLabVal..}]/;odLabelSameQ[list]:=
odLabInt[odLabel[First[list]],Join[Map[odValue,list]]]


(* ::Input:: *)
(*odLabValJoin[odLabValRange["D",Range[5]]]*)


(* ::Input:: *)
(*odLabValRange["D",Range[5]]*)


(* ::Subsection::Closed:: *)
(*odLabIntRange*)


(* ::Input::Initialization:: *)
odLabIntRange[list:{_odLabVal..}]:=
Flatten[
Map[
Map[odLabValJoin,TakeDrop[list,#]]&,
Range[
Length[list]-1
]
],
1];


(* ::Input:: *)
(*odLabIntRange[odLabValRange["D",Range[4]]]*)


(* ::Input:: *)
(*odLabIntRange[odLabValRange["Anciennet\[EAcute]",{"]-inf;5]","]5;10]","]10;15]","]15;20]","]20;25]","]25;30]","]30;+inf]"}]]*)


(* ::Subsection::Closed:: *)
(*odMemberQ*)


(* ::Input::Initialization:: *)
odMemberQ[ int_odLabInt,val_odLabVal]/;odLabelSameQ[{val,int}]:=
Apply[MemberQ,Map[odValue,{int,val}]];


(* ::Input:: *)
(*odMemberQ[odLabInt["D",{"2","3","4"}],odLabVal["D","3"]]*)
(*odMemberQ[odLabInt["D",{"2","3","4"}],odLabVal["D","5"]]*)


(* ::Subsection::Closed:: *)
(*odLabValToInt*)


(* ::Input::Initialization:: *)
odLabValToInt[val_odLabVal,lang:{_odLabInt..}]/;odLabelSameQ[Append[lang,val]]:=Select[lang,odMemberQ[#,val]&]


(* ::Input:: *)
(*odLabValToInt[odLabVal["D","2"],odLabIntRange[odLabValRange["D",Range[5]]]]*)


(* ::Subsection:: *)
(*odLabIntIntersection*)


(* ::Input::Initialization:: *)
odLabIntIntersection[list:{_odLabInt..}]/;odLabelSameQ[list]:=
With[
{
l=odLabel[First[list]],
values=Map[odValue,list]
},
odLabInt[l,
Select[
First[MaximalBy[values,Length]],
MemberQ[Apply[Intersection,values],#]&
]
]
];



(* ::Input:: *)
(*odLabIntIntersection[{odLabInt["D",{"2","3","4","5"}],odLabInt["D",{"1","2"}],odLabInt["D",{"1","2","3"}],odLabInt["D",{"1","2","3","4"}]}]*)


(* ::Input:: *)
(*odLabIntIntersection[{odLabInt["alcohol",{"onceAMonth","onceAWeek","moreThanOnceAWeek"}],odLabInt["alcohol",{"onceAMonth","onceAWeek","moreThanOnceAWeek","onceTwiceAYear"}],odLabInt["alcohol",{"onceAWeek","moreThanOnceAWeek"}]}]*)


(* ::Subsection::Closed:: *)
(*odToLabInts*)


(* ::Input::Initialization:: *)
odToLabInts[p_String,separateur_String:$odSeparator]:=
With[{lab=First[#],int=Rest[#]},odLabInt[lab,int]]&@StringSplit[p,separateur]


(* ::Input:: *)
(*odToLabInts["D_1_2_3_4","_"]*)


(* ::Input:: *)
(*With[{i=odLabInt["D",{"1","2","3","4"}]},*)
(*odToLabInts[ToString[i],$odSeparator]==i*)
(*]*)
(**)
(*With[{i=odLabInt["D",{"1","2","3","4"}]},*)
(*odToLabInts[ToString[i]]==i*)
(*]*)


(* ::Subsection::Closed:: *)
(*odIsASpecilization*)


(* ::Input::Initialization:: *)
odIsASpecilization[tested_odLabInt,reference_odLabInt]/;odLabelSameQ[{tested,reference}]:=
ContainsAll[odValue[tested],odValue[reference]]


(* ::Input:: *)
(*odIsASpecilization[odLabInt["D",{"1","2","3"}],odLabInt["D",{"1","2"}]]*)


(* ::Input:: *)
(*odIsASpecilization[odLabInt["D",{"1","2","3"}],odLabInt["D",{}]]*)


(* ::Subsection::Closed:: *)
(*odIsAGeneralization*)


(* ::Input::Initialization:: *)
odIsAGeneralization[tested_odLabInt,reference_odLabInt]/;odLabelSameQ[{tested,reference}]:=
ContainsAll[odValue[reference],odValue[tested]]


(* ::Input:: *)
(*odIsAGeneralization[odLabInt["D",{"1","2"}],odLabInt["D",{"1","2","3"}]]*)
(*odIsAGeneralization[odLabInt["D",{"1","2","4"}],odLabInt["D",{"1","2","3"}]]*)


(* ::Input:: *)
(*odIsAGeneralization[tested:{_odLabInt..},reference:{_odLabInt..}]:=*)
(*With[*)
(*{*)
(*labels=Union[Map[odLabel,tested],Map[odLabel,reference]]*)
(*},*)
(*Map[*)
(*With[{lab=#},*)
(*With[*)
(*{*)
(*testedrep=Select[tested,SameQ[odLabel[#],lab]&],*)
(*referencerep=Select[reference,SameQ[odLabel[#],lab]&]*)
(*},*)
(*{testedrep,referencerep}*)
(*(*odIsAGeneralization[odLabIntIntersection[Select[tested,SameQ[odLabel[#],lab]&]],odLabIntIntersection[Select[reference,SameQ[odLabel[#],lab]&]]]*)*)
(*]*)
(*]&,*)
(*labels*)
(*]*)
(*];*)
(**)
(*odIsAGeneralization[{odLabInt["T",{"1"}],odLabInt["D",{"1"}]},{odLabInt["T",{"1","2","3"}],odLabInt["T",{"1","2"}],odLabInt["D",{"1","2"}],odLabInt["C",{"1"}]}]*)
(**)


(* ::Subsection::Closed:: *)
(*odContainAGeneralizationOf*)


(* ::Input::Initialization:: *)
odContainAGeneralizationOf[list:{_odLabInt...},p_odLabInt]/;odLabelSameQ[Join[{p},list]]:=SelectFirst[list,odIsAGeneralization[#,p]&]=!=Missing["NotFound"]


(* ::Input:: *)
(*odContainAGeneralizationOf[{odLabInt["D",{"1","2"}],odLabInt["D",{"1"}]},odLabInt["D",{"1","2","3"}]]*)
(*odContainAGeneralizationOf[{odLabInt["D",{"1","3"}],odLabInt["D",{"1","4"}]},odLabInt["D",{"1","2","3"}]]*)
(*odContainAGeneralizationOf[{odLabInt["D",{"1","3"}],odLabInt["D",{"1","4"}]},odLabInt["D",{"1","2"}]]*)
(*odContainAGeneralizationOf[{},odLabInt["D",{"1","2","3"}]]*)


(* ::Subsection::Closed:: *)
(*odMinimaux*)


(* ::Input::Initialization:: *)
odMinimaux[liste_List,fun_]:=odMinimaux[liste,{},fun];

odMinimaux[liste_,minimaux_,fun_]:=
If[
liste==={},
minimaux,
With[
{cur=First@liste, rest=Rest@liste},
If[
Or[
UnsameQ[SelectFirst[minimaux,fun[#,cur]&],Missing["NotFound"]],
UnsameQ[SelectFirst[rest,fun[#,cur]&],Missing["NotFound"]]
],
(* patt n'est pas minimal il est donc oubli\[EAcute].*)
odMinimaux[rest,minimaux,fun],
(* patt est minimal et est ajout\[EAcute] aux minimaux.*)
odMinimaux[rest,Append[minimaux,cur],fun]
]
]
]


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*odMinimaux[{2,4,6,1,9},Less]*)


(* ::Input:: *)
(*odMinimaux[{{1,2,3},{1,2}},SubsetQ[#2,#1]&]*)


(* ::Input:: *)
(*odMinimaux[{odLabInt["T",{1,2,3}],odLabInt["T",{1,2}]},SubsetQ[odValue[#2],odValue[#1]]&]*)


(* ::Subsection:: *)
(*odCompactLabInt*)


(* ::Input:: *)
(*ClearAll[odCompactLabInt]*)


(* ::Input::Initialization:: *)
odCompactLabInt[{},___]={};
odCompactLabInt[liste:{_odLabInt..}]:=Map[ToString[odLabIntIntersection[#]]&,GatherBy[liste,odLabel]];

odCompactLabInt[liste:{_String..},separateur_String]:=
odCompactLabInt[Map[odToLabInts[#,separateur]&,liste]];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*odCompactLabInt[{"alcohol_onceAMonth_onceAWeek_moreThanOnceAWeek","alcohol_onceAMonth_onceAWeek_moreThanOnceAWeek_onceTwiceAYear","alcohol_onceAWeek_moreThanOnceAWeek","cannabis_Non_triedOnce","sport_regular"},"_"]*)


(* ::Subsection:: *)
(**)


(* ::Input::Initialization:: *)
End[];


(* ::Section:: *)
(*Fin*)


(* ::Input::Initialization:: *)
EndPackage[];
