(* ::Package:: *)

(* ::Input:: *)
(*Button["Quit",Quit[]]*)


(* ::Input:: *)
(*<<MinerLC`*)


(* ::Title:: *)
(*Manipulation des fichiers de MinerLC*)


(* ::Input::Initialization:: *)
BeginPackage["MinerLC`",{"OD`"}];


(* ::Section:: *)
(*Partie publique du package*)


(* ::Subsection::Closed:: *)
(*NRI*)


(* ::Input::Initialization:: *)
booleToExplicit::usage="Convertie un vecteur binaire en un motif explicite exprim\[EAcute] dans le langage.";


(* ::Input::Initialization:: *)
importRI::usage="importNRI[fileName_] Importe les donn\[EAcute]es d'un fichier au format .ri (relational itemset). Le fichier est import\[EAcute] sous forme de graphe dont les sommets sont \[EAcute]tiquet\[EAcute]s avec un itemset.\n";


(* ::Input::Initialization:: *)
importNRI::usage="importNRI[fileName_] Importe les donn\[EAcute]es d'un fichier au format .nri (relational itemset). Le fichier est import\[EAcute] sous forme de graphe dont les sommets sont \[EAcute]tiquet\[EAcute]s avec un itemset.\n";


(* ::Input::Initialization:: *)
exportRI::usage="exportRI[fileName_] Exporte un graphe \[EAcute]tiquet\[EAcute] dans un fichier au format .ri (relational itemset).\n";


(* ::Input::Initialization:: *)
exportNRI::usage="exportNRI[Association<|\"Graphe\"\[Rule]graph,\"Objets\"\[Rule]listDesObjets,\"Items\"\[Rule]listeDesItems,\"Itemsets\"\[Rule]Association<|ob1->itset1, ...|> |>, fileName] exporte un graphe \[EAcute]tiquet\[EAcute] dans un fichier au format .nri (relational itemset).\n";


(* ::Input::Initialization:: *)
biPatternConvert::usage="biPatternConvert[assoc_] convertie la structure de graphe \[EAcute]tiquet\[EAcute] en un jeux de donn\[EAcute]es permettant de simuler le bi-pattern: d\[EAcute]doublement des sommets et des \[EAcute]tiquettes et couverture des sommets 'i' par toutes les \[EAcute]tiquettes 'o' et r\[EAcute]ciproquement.\n";


(* ::Subsection::Closed:: *)
(*minerLC*)


(* ::Input::Initialization:: *)
importMinerLCOut::usage="readMinerLCOut[fileName_] Importe les donn\[EAcute]es d'un fichier au format de sortie du programme minerLC. Le fichier peut contenir des donn\[EAcute]e globales ou locales.";


(* ::Subsection::Closed:: *)
(*Comodo*)


(* ::Subsubsection::Closed:: *)
(*importComodo*)


(* ::Input::Initialization:: *)
importComodo::usage="importComodo[filename_String] retourne une association contenant les motifs trouv\[EAcute]s par le programme Comodo ainsi que leur support et leur extension.";


(* ::Subsection::Closed:: *)
(*CORON (RCF FCIGE)*)


(* ::Input::Initialization:: *)
exportRcf::usage="exportRcf[{\"Objets\"\[Rule]listDesObjets,\"Items\"\[Rule]listeDesItems,\"Itemsets\"\[Rule]itemsets}, fileName_, message_String] export au format RCF (format d'entr\[EAcute]e de CORON) les donn\[EAcute]es transactionnelles de data.";


(* ::Input::Initialization:: *)
importFCIGE::usage="importFCIGE[fileName_] retourne les clos globaux lus dans le fichier de CORON, ainsi que leur support et leurs g\[EAcute]n\[EAcute]rateurs.
importFCIGE[fileName_, itemsets_] retourne les clos globaux lus dans le fichier de CORON, ainsi que leur support, leur extension et leurs g\[EAcute]n\[EAcute]rateurs.";


(* ::Subsection::Closed:: *)
(*supportsAttendus (GRA RES NRI)*)


(* ::Input::Initialization:: *)
(*exportGRA::usage="exportGRA[graph_,listDesObjets_, fileName_,opts:OptionsPattern[]] exporte un graphe  dans un fichier au format .gra (pour le programme supportsAttendus).";*)


(* ::Input::Initialization:: *)
(*exportRES::usage="exportRES[data_, fileName] exporte les donn\[EAcute]es d'extension dans un fichier au format .res (pour le programme supportsAttendus).";*)


(* ::Input::Initialization:: *)
importSupportAttendu::usage="importSupportAttendu[fileName_] import les donn\[EAcute]es du fichier de sortie du programme supportsAttendus point\[EAcute] par filename.";


(* ::Input::Initialization:: *)
importSupportAttenduBi::usage="importSupportAttenduBi[fileName_] import les donn\[EAcute]es du fichier de sortie du programme supportsAttendusBi point\[EAcute] par filename.";


(* ::Input::Initialization:: *)
resolveLocalSupportAttendu::usage="resolveLocalSupportAttendu[data_] met a jour les champs \"MeanExpectedSupLoc\" et \"SigmaExpectedSupLoc\". Le format de sortie du programme supportsAttendus associe \[AGrave] chaque support global de clos local un support local attendu. La fonction resolveLocalSupportAttendu permet d'associer les supports locaux attendus directement \[AGrave] chaque (ClosAbs_ExtLoc)";


(* ::Subsection::Closed:: *)
(*Fonctions d\[EAcute]finissant/utilisant une arborescence de nommage*)


(* ::Subsubsection::Closed:: *)
(*D\[EAcute]finition d'un ordre total sur les param\[EGrave]tres*)


(* ::Input::Initialization:: *)
$ParametersPatterns::usage="$ParametersPatterns variable globale d\[EAcute]finissant un ordre total sur les param\[EGrave]tre. Cette variable est utilis\[EAcute]e pour cr\[EAcute]er une cha\[IHat]ne de caract\[EGrave]res unique repr\[EAcute]sentative des param\[EGrave]tres pour le nommage des fichiers.";


(* ::Subsubsection::Closed:: *)
(*\[CapitalEAcute]x\[EAcute]cution des commandes dans une arborescence pr\[EAcute]d\[EAcute]finie*)


(* ::Input::Initialization:: *)
minerLCCommand::usage="minerLCCommand[ nriFileName, parameters] renvoie une cha\[IHat]ne de caract\[EGrave]re permettant d'invoquer une ex\[EAcute]cution du programme minerLC. Le nom du fichier de sortie est d\[EAcute]fini automatiquement de fa\[CCedilla]on unique.";


(* ::Input::Initialization:: *)
runMinerLC::usage"runMinerLC[ nriFileName, parameters] \[EAcute]x\[EAcute]cute le programme minerLC pour une exp\[EAcute]rience d\[EAcute]crite par le fichier d'entr\[EAcute]e nriFileName et les param\[EGrave]tres parameters. Pour plus d'informations sur les options voire Options[minerLCCommand].";


(* ::Input::Initialization:: *)
(*convertMinerLCOutToRES::usage="convertMinerLCOutToRES[nriFileName_String,parameters_List]  produit un fichier .res \[AGrave] partir d'un fichier de sortie de minerLC. Le fichier de sortie de minerLC est identifi\[EAcute] grace au couple de param\[EGrave]tres nriFileName et parameters.";*)


(* ::Input::Initialization:: *)
(*convertNRItoGRA::usage="convertNRItoGRA[ nriFileName] produit un fichier .gra \[AGrave] partir d'un fichier .nri.

Options:
 \"PathForNRI\" d\[EAcute]finit le r\[EAcute]pertoire dans lequel fichier d'input au format nri sera cherch\[EAcute]\"
 \"PathForGRA\" d\[EAcute]finit le r\[EAcute]pertoire dans lequel fichier d'output au format gra sera \[EAcute]crit";*)


(* ::Input::Initialization:: *)
supportsAttendusBiCommand::usage="supportsAttendusBiCommand[ nriFileName, parameters] renvoie une cha\[IHat]ne de caract\[EGrave]re permettant d'invoquer une ex\[EAcute]cution du programme supportsAttendusBi. Le nom du fichier de sortie est d\[EAcute]fini automatiquement de fa\[CCedilla]on unique.";


(* ::Input::Initialization:: *)
supportsAttendusCommand::usage="supportsAttendusCommand[ nriFileName, parameters] renvoie une cha\[IHat]ne de caract\[EGrave]re permettant d'invoquer une ex\[EAcute]cution du programme supportsAttendus. Le nom du fichier de sortie est d\[EAcute]fini automatiquement de fa\[CCedilla]on unique.";


(* ::Input::Initialization:: *)
runSupportsAttendus::usage"runSupportsAttendus[nriFileName,parameters] \[EAcute]x\[EAcute]cute le supportsAttendus  pour une exp\[EAcute]rience d\[EAcute]crite par les fichiers d'entr\[EAcute]e nriFileName et les param\[EGrave]tres parameters. Pour plus d'informations sur les options voire Options[supportsAttendusCommand].";


(* ::Input::Initialization:: *)
runSupportsAttendusBi::usage"runSupportsAttendusBi[nriFileName,parameters] \[EAcute]x\[EAcute]cute le supportsAttendusBi  pour une exp\[EAcute]rience d\[EAcute]crite par les fichiers d'entr\[EAcute]e nriFileName et les param\[EGrave]tres parameters. Pour plus d'informations sur les options voire Options[supportsAttendusBiCommand].";


(* ::Subsubsection::Closed:: *)
(*Lecture des donn\[EAcute]es dans une arborescence pr\[EAcute]d\[EAcute]finie*)


(* ::Input::Initialization:: *)
formatMinerLCOutFileName::usage="formatMinerLCOutFileName[nriFileName_String, parameters_List] retourne une cha\[IHat]ne de caract\[EGrave]re unique repr\[EAcute]sentant le nom du fichier de sortie d'un exp\[EAcute]rience avec minerLC. L'unicit\[EAcute] est assur\[EAcute]e par un ordre d\[EAcute]finit sur les param\[EGrave]tres possibles d\[EAcute]cris dans le variable globale $ParametersPatterns.";


(* ::Input::Initialization:: *)
readNRI::usage="readNRI[ nriFileName_String] importe les donn\[EAcute]es de d\[EAcute]part d'une exp\[EAcute]rience d\[EAcute]crite par le fichier d'entr\[EAcute]e nriFileName.";


(* ::Input::Initialization:: *)
readMinerLC::usage="readMinerLC[ nriFileName_String, parameters___String] importe les donn\[EAcute]es concernant les r\[EAcute]sultats d'une exp\[EAcute]rience men\[EAcute]e \[AGrave] partir du fichier d'entr\[EAcute]e nriFileName et pour les param\[EGrave]tres parameters.";


(* ::Input::Initialization:: *)
formatSUPFileName::usage="formatSUPFileName[nriFileName_String, parameters_List] retourne une cha\[IHat]ne de caract\[EGrave]re unique repr\[EAcute]sentant le nom du fichier de sortie d'un fichier .res extrait \[AGrave] partir du fichier .nri pass\[EAcute] en param\[EGrave]tre. L'unicit\[EAcute] est assur\[EAcute]e par un ordre d\[EAcute]finit sur les param\[EGrave]tres possibles d\[EAcute]cris dans le variable globale $ParametersPatterns.";


(* ::Input::Initialization:: *)
readSupportsAttendus::usage="readSupportsAttendus[ nriFileName_String, parameters___String] importe les donn\[EAcute]es concernant les r\[EAcute]sultats d'une exp\[EAcute]rience men\[EAcute]e \[AGrave] partir du fichier d'entr\[EAcute]e nriFileName et pour les param\[EGrave]tres parameters.";


(* ::Input::Initialization:: *)
formatBISUPFileName::usage="formatBISUPFileName[nriFileName_String, parameters_List] retourne une cha\[IHat]ne de caract\[EGrave]re unique repr\[EAcute]sentant le nom du fichier de sortie d'un fichier .res extrait \[AGrave] partir du fichier .nri pass\[EAcute] en param\[EGrave]tre. L'unicit\[EAcute] est assur\[EAcute]e par un ordre d\[EAcute]finit sur les param\[EGrave]tres possibles d\[EAcute]cris dans le variable globale $ParametersPatterns.";


(* ::Input::Initialization:: *)
readSupportsAttendusBi::usage="readSupportsAttendusBi[ nriFileName_String, parameters___String] importe les donn\[EAcute]es concernant les r\[EAcute]sultats d'une exp\[EAcute]rience men\[EAcute]e \[AGrave] partir du fichier d'entr\[EAcute]e nriFileName et pour les param\[EGrave]tres parameters.";


(* ::Subsubsection::Closed:: *)
(*R\[EAcute]glage des chemins*)


(* ::Input::Initialization:: *)
setPaths::usage="setPaths[paths] permets de r\[EAcute]gler les chemins par d\[EAcute]faut appeler par les diff\[EAcute]rentes fonctions d'ex\[EAcute]cution et de lecture. Les chemins sont donn\[EAcute]s comme des r\[EGrave]gles:
\"PathForResults\"\[Rule]path
\"PathForNRI\"\[Rule]path
\"PathForSUP\"\[Rule]path
\"PathForBinary\"\[Rule]path
\"Host\"\[Rule]host ou host est de la forme \"ssh login@host\"";


(* ::Subsection::Closed:: *)
(*Cores*)


(* ::Input::Initialization:: *)
subweightedgraph::usage="subweightedgraph[g_,X_] retourne le sous-graphe de g induit par X (un ensemble de sommets ou un ensemble d'ar\[EHat]tes) dans le cas ou le graphe est un graphe pond\[EAcute]r\[EAcute] (ce que ne fait pas la fonction de Mathematica).";


(* ::Input::Initialization:: *)
kCorePropertyQ::usage=
"kCorePropertyQ[k_][{g_,X_},v_] retourne True si le sommet v \[AGrave] un degr\[EAcute]  sup\[EAcute]rieur \[AGrave] k dans le sous-graphe de g induit par les ommets de l'ensemble X et False sinon.";


(* ::Input::Initialization:: *)
kStarPropertyQ::usage=
"kStarPropertyQ[k_][{g_,X_},v_] retourne True si le sommet v fait partie d'un k-star (un degr\[EAcute]  sup\[EAcute]rieur \[AGrave] k ou li\[EAcute] \[AGrave] un tel sommet)  dans le sous-graphe de g induit par les ommets de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
kCoreOutPropertyQ::usage=
"kCoreOutPropertyQ[k_][{g_,X_},v_] retourne True si le sommet v \[AGrave] un degr\[EAcute] sortant sup\[EAcute]rieur \[AGrave] k dans le sous-graphe de g induit par les ommets de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
kCoreInPropertyQ::usage=
"kCoreInPropertyQ[k_][{g_,X_},v_] retourne True si le sommet v \[AGrave] un degr\[EAcute] entrant sup\[EAcute]rieur \[AGrave] k dans le sous-graphe de g induit par les ommets de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
kStarOutPropertyQ::usage=
"kStarOutPropertyQ[k_][{g_,X_},v_] retourne True si le sommet v fait partie d'un k-outstar (un degr\[EAcute] sortant sup\[EAcute]rieur \[AGrave] k ou li\[EAcute] \[AGrave] un tel sommet) dans le sous-graphe de g induit par les ommets de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
kStarInPropertyQ::usage=
"kStarInPropertyQ[k_][{g_,X_},v_] retourne True si le sommet v fait partie d'un k-instar (un degr\[EAcute] entrant sup\[EAcute]rieur \[AGrave] k ou li\[EAcute] \[AGrave] un tel sommet) dans le sous-graphe de g induit par les ommets de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
trianglePropertyQ::usage=
"trianglePropertyQ[{g_,X_},v1_\[UndirectedEdge]v2_] retourne True si l'ar\[EHat]te v1\[UndirectedEdge]v2 fait partie d'un triangle dans le sous-graphe de g induit par les ar\[EHat]tes de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
kDensePropertyQ::usage=
"kDensePropertyQ[k_][{g_,X_},v1_\[UndirectedEdge]v2_] retourne True si l'ar\[EHat]te v1\[UndirectedEdge]v2 fait partie d'un k-dense dans le sous-graphe de g induit par les ar\[EHat]tes de l'ensemble X  et False sinon.";


(* ::Input::Initialization:: *)
hubOfHubAutPropertyQ::usage=
"hubOfHubAutPropertyQ[k_][{g_,hubList_,autList_},h_] retourne True si le sommet h \[AGrave] un degr\[EAcute] sortant sup\[EAcute]rieur ou \[EAcute]gal \[AGrave] k vers les sommets autList du graphe g et False sinon.";


(* ::Input::Initialization:: *)
autOfHubAutPropertyQ::usage=
"autOfHubAutPropertyQ[k_][{g_,hubList_,autList_},a_] retourne True si le sommet a \[AGrave] un degr\[EAcute] entrant sup\[EAcute]rieur ou \[EAcute]gal \[AGrave] k depuis les sommets hubList du graphe g et False sinon.";


(* ::Input::Initialization:: *)
biSetEdges::usage="biSetEdges[g_,{X1_,X2_}] retourne l'ensemble des ar\[EHat]tes de g telles qu'une extr\[EAcute]mit\[EAcute] de l'ar\[EHat]te est dans X1 et l'autre extr\[EAcute]mit\[EAcute] est dans X2.";


(* ::Input::Initialization:: *)
vertexCore::usage="vertexCore[{property_,X_},g_] retourne le sous-ensemble maximal de sommets du sous-graphe de g induits par X qui v\[EAcute]rifient la propri\[EAcute]t\[EAcute] property sur tous ses sommets. property est une fonction bool\[EAcute]enne de la forme property[{g_,X_}, v_] qui prend en param\[EGrave]tre le graphe, un sous-ensemble de d\[EAcute]part de sommets X et un sommet de ce graphe et renvoie True si ce sommet v\[EAcute]rifie la propri\[EAcute]t\[EAcute] voulue dans e graphe sous-graphe de g induits par X et False sinon.";


(* ::Input::Initialization:: *)
edgeCore::usage="edgeCore[{property_,X_},g_] retourne le sous-ensemble maximal des ar\[EHat]tes du sous-graphe de g induits par X qui v\[EAcute]rifient la propri\[EAcute]t\[EAcute] property sur toutes ses ar\[EHat]tes. property est une fonction bool\[EAcute]enne de la forme property[{g_,X_}, e_] qui prend en param\[EGrave]tre un graphe g, un sous-ensemble de d\[EAcute]part d'ar\[EHat]tes X de g et une ar\[EHat]te e de ce graphe et renvoie True si cette ar\[EHat]te v\[EAcute]rifie la propri\[EAcute]t\[EAcute] voulue dans le graphe sous-graphe de g induits par X et False sinon.";


(* ::Input::Initialization:: *)
vertexBiCore::usage="vertexBiCoreBiCore[{property1_,property2_,{X1_, X2_},g_] retourne les sous-ensembles maximaux de X1 et X2 des sommets du graphe g tels que toute ar\[EHat]te v1\[DirectedEdge]v2 de g liant un sommet de chaque ensemble X1 et X2 v\[EAcute]rifient les propri\[EAcute]t\[EAcute]s suivante: property1[{g, X1, X2},v1] est vrai et property2[{g, X1, X2},v2] est vrai.";


(* ::Subsection:: *)
(*Calcul sur les donn\[EAcute]es*)


(* ::Input::Initialization:: *)
densities::usage="densities[data_Association, extensionKey_String] renvoie une r\[EGrave]gle d'association entre chaque clos utilis\[EAcute] comme clef de la r\[EGrave]gle data[extensionKey] et sa densit\[EAcute] du graphe associ\[EAcute] \[AGrave] son extension. La densit\[EAcute] est d\[EAcute]finie comme le rapport 2|E|/(|V|*(|V|-1)";


(* ::Input::Initialization:: *)
specificities::usage="specificities[data_Association,keyListeClos_String,keySupportSpecifique_String, keySupportGeneral_String] renvoie une r\[EGrave]gle d'association entre chaque clos de la liste point\[EAcute]e par la clef keyListeClos et sa sp\[EAcute]cificit\[EAcute] calcul\[EAcute]e comme le rapport de (data[keySupportSpecifique][#])/(data[keySupportGeneral][#]) ou # est un des clos de la liste data[keyListeClos].
specificities[data] renvoie les sp\[EAcute]cificit\[EAcute]s par d\[EAcute]faut. Par d\[EAcute]faut dans le cas global: specificities[data,\"ClosAbs\",\"SupAbs\", \"SupGlob\"]; Par d\[EAcute]faut dans le cas local: specificities[data,\"ClosLoc\",\"SupLoc_ClosLoc\", \"SupGlob_ClosLoc\"]";


(* ::Input::Initialization:: *)
informativities::usage="informativities[data_] les informativit\[EAcute]s des r\[EGrave]gles ClosAbs\[Rule]ClosLoc.";


(* ::Input::Initialization:: *)
homogeneities::usage="homogeneities[data_,{vPref1_, vPref2_}] les homog\[EAcute]n\[EAcute]\[IDoubleDot]t\[EAcute]s des bipattern rep\[EAcute]r\[EAcute]s par les prefixes vPref1 et vPref2.";
inhomogeneities::usage="homogeneities[data_,{vPref1_, vPref2_}] les inhomog\[EAcute]n\[EAcute]\[IDoubleDot]t\[EAcute]s des bipattern rep\[EAcute]r\[EAcute]s par les prefixes vPref1 et vPref2.";


(* ::Input::Initialization:: *)
normalizedSpecificities::usage="normalizedSpecificities[data_Association,keyListeClos_String,keySupportObserve_String] renvoie une r\[EGrave]gle d'association entre chaque clos de la liste point\[EAcute]e par la clef keyListeClos et sa sp\[EAcute]cificit\[EAcute] normalis\[EAcute]e calcul\[EAcute]e comme le rapport de (data[keySupportObserve][#])/(data[\"MeanExpectedSupAbs\"][#]) ou # est un des clos de la liste data[keyListeClos].
normalizedSpecificities[data] renvoie les sp\[EAcute]cificit\[EAcute]s normalis\[EAcute]es par d\[EAcute]faut.Par d\[EAcute]faut dans le cas global: specificities[data,\"ClosAbs\",\"SupAbs\"]; Par d\[EAcute]faut dans le cas local: ToDo a faire";


(* ::Input::Initialization:: *)
deviationToNormalizedSpecificities::usage="deviationToNormalizedSpecificities[data_Association,keyListeClos_String,keySupportObserve_String] renvoie une r\[EGrave]gle d'association entre chaque clos de la liste point\[EAcute]e par la clef keyListeClos et la d\[EAcute]viation \[AGrave] la sp\[EAcute]cificit\[EAcute] normalis\[EAcute]e calcul\[EAcute]e comme le rapport de (data[keySupportObserve][#] -data[\"MeanExpectedSupAbs\"][#])/(data[\"SigmaExpectedSupAbs\"][#]) ou # est un des clos de la liste data[keyListeClos].
normalizedSpecificities[data] renvoie les sp\[EAcute]cificit\[EAcute]s normalis\[EAcute]es par d\[EAcute]faut. Par d\[EAcute]faut dans le cas global: specificities[data,\"ClosAbs\",\"SupAbs\"]; Par d\[EAcute]faut dans le cas local: ToDo a faire";


(* ::Input::Initialization:: *)
deviationToExpectedAbstractSupport::usage="deviationToExpectedAbstractSupport[data_Association] renvoie une r\[EGrave]gle d'association entre chaque clos de la liste point\[EAcute]e par la clef \"ClosAbs\" et la d\[EAcute]viation du support abstrait \[AGrave] l'attendu, selon la formule:
data[\"SupAbs\"][clos] - data[\"MeanExpectedSupAbs\"][clos] ) /data[\"SigmaExpectedSupAbs\"][clos]";


(* ::Input::Initialization:: *)
orderClosAbs::usage="orderClosAbs[data_Association] ordonne les clos des plus infr\[EAcute]quents aux moins infr\[EAcute]quents.";


(* ::Input::Initialization:: *)
localModularityEstimate::usage="localModularityEstimate[gAll_Graph, ext_] retourne l'estimation optimiste de la modularit\[EAcute] locale du sous graphe de gAll induit par les sommets ext.";


(* ::Input::Initialization:: *)
localModularity::usage="localModularity[gAll_Graph, ext_] retourne la modularit\[EAcute] locale du sous graphe de gAll induit par les sommets ext.";


(* ::Input::Initialization:: *)
distanceBasedPatternSelect::usage="selectPatterns[data_][listeOrdonnee_,\[Beta]_] s\[EAcute]l\[EAcute]ctionne dans la liste de patterns ordonn\[EAcute]s, un sous ensemble de patterns tels que toute paire de patterns de la s\[EAcute]lection est \[AGrave] une distance strictement sup\[EAcute]rieure \[AGrave] \[Beta] (qui prend ses valeurs entre 0 et 1). L'algo est glouton et commence par s\[EAcute]lectionner comme premier pattern, le premier de liste ordonn\[EAcute]e.";


(* ::Input::Initialization:: *)
fixSupportsAttendus::usage="fixSupportsAttendusBi[supData_,mlcData_] retourne une version corrig\[EAcute]e de supData dans le cas ou il y a des roles. Le support abstrait est corrig\[EAcute].";


(* ::Input::Initialization:: *)
selectPatternsByJaccardDistance::usage="[orderedListOfPatterns_,patternExtensions_,minimalDistance_] s\[EAcute]l\[EAcute]ctionne dans la liste de patterns ordonn\[EAcute]s orderedListOfPatterns, un sous ensemble de patterns tels que toute paire de patterns de la s\[EAcute]lection est \[AGrave] une distance strictement sup\[EAcute]rieure \[AGrave] minimalDistance (qui prend ses valeurs entre 0 et 1). L'algo est glouton et commence par s\[EAcute]lectionner comme premier pattern, le premier de liste ordonn\[EAcute]e. Les extensions des motifs de la liste orderedListOfPatterns sont donn\[EAcute]es par patternExtensions qui une Association dont les clefs sont les motifs et dont la valeur est soit une liste de sommets, soit une association qui donne les extensions associ\[EAcute]es \[AGrave] chaque role dans le cas d'un motifs avec roles.";


(* ::Input::Initialization:: *)
distanceBetweenSelections::usage="distanceBetweenSelections[aggFun_][e1List_,e2List_] calcule la distance entre deux ensembles d'ensemble de sommets.";


(* ::Subsection::Closed:: *)
(*Repr\[EAcute]sentation des donn\[EAcute]es*)


(* ::Input::Initialization:: *)
printNRI::usage="printNRI[data] fonction graphique d'affichage du contenu d'un fichier nri.";


(* ::Input::Initialization:: *)
highlightGraphAbstraction::usage="highlightGraphAbstractionGlobal[data, clos] propose une repr\[EAcute]sentation du clos sous forme de graphe. Si il s'agit d'une exp\[EAcute]rience globale le clos prend la forme d'une liste compos\[EAcute]e d'items. Si il s'agit d'une exp\[EAcute]rience locale le clos prend laforme d'une double liste compos\[EAcute]e d'un premier \[EAcute]l\[EAcute]ment qui est le clos local: une liste compos\[EAcute]e d'items et d'un second \[EAcute]l\[EAcute]ment qui est l'extension locale du clos.";


(* ::Input::Initialization:: *)
printGraphAbstractionStatistics::usage="printGraphAbstractionStatistics[data, clos] propose une tableau r\[EAcute]capitulant certaines mesures attach\[EAcute]e au clos. Si il s'agit d'une exp\[EAcute]rience globale le clos prend la forme d'une liste compos\[EAcute]e d'items. Si il s'agit d'une exp\[EAcute]rience locale le clos prend laforme d'une double liste compos\[EAcute]e d'un premier \[EAcute]l\[EAcute]ment qui est le clos local: une liste compos\[EAcute]e d'items d'un second \[EAcute]l\[EAcute]ment qui est l'extension locale du clos.";


(* ::Input::Initialization:: *)
plotObsVersusExpected::usage="plotObsVersusExpected[data_Association] produit un graphique comparant les supports observ\[EAcute]s aux supports attendus.";


(* ::Input::Initialization:: *)
hubAutLists::usage="hubAutLists[g_,dHub_,dAut_] retourne la liste des noeuds hub et la liste des noeuds autorit\[EAcute] du graphe g pour une abstraction Hub-Autorit\[EAcute] de degr\[EAcute]s (dHub, dAut)."; 


(* ::Input::Initialization:: *)
hubAutGraph::usage="hubAutGraph[g_Graph, dHub_Integer,dAut_Integer,opts:OptionsPattern[]] repr\[EAcute]sente le graphe g en lui appliquant un mode de repr\[EAcute]sentation propre \[AGrave] l'abstraction Hub-Autorit\[EAcute] de degr\[EAcute]s (dHub,dAut).";


(* ::Input::Initialization:: *)
makeCrdRules::usage="makeCrdRules[g_Graph] renvoie la liste des r\[EAcute]gle associant les sommets du graphe \[AGrave] leurs coordonn\[EAcute]es.";


(* ::Input:: *)
(**)


(* ::Section::Closed:: *)
(*Private*)


(* ::Input::Initialization:: *)
Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*NRI*)


(* ::Subsubsection::Closed:: *)
(*booleToExplicit (private)*)


(* ::Input::Initialization:: *)
booleToExplicit[booleList_List,canonicalOrder_List]:=
Select[booleList*canonicalOrder,#!=0&];


(* ::Subsubsection::Closed:: *)
(*(deprecated) importRI*)


(* ::Input::Initialization:: *)
Options[importRI]={Sequence@@FilterRules[Options@Graph,Except[Properties]],"Control"->True};

importRI[fileName_, opt:OptionsPattern[]]:=
Module[{itemset,objects,items,edges,objproperties},
(*------------------------------------------------*)
(* Contr\[OHat]le du format en 2 temps *)
With[{rawFile=Import[fileName,"Lines"]},
If[Not[rawFile[[1]]=="[Relational Context]"],
Print["importRI abort 1\n"];Abort[]];
If[Not[rawFile[[3]]=="[Binary Relation]"],
Print["importRI abort 2\n"];Abort[]];
objects=StringTrim/@StringSplit[rawFile[[5]],"|"];
items=StringTrim/@StringSplit[rawFile[[6]],"|"];
If[Not[Length[objects]>1],
Print["importRI abort 3\n"];Abort[]];
If[Not[Length[items]>1],
Print["importRI abort 4\n"];Abort[]];
If[Not[rawFile[[-1]]=="[END Graph Relation]"],
Print["importRI abort 5\n"];Abort[]];
If[Not[StringTrim[rawFile[[7+Length[objects]]]]=="[END Relational Context]"],
Print["importRI abort 6\n"];Abort[]];
If[Not[rawFile[[8+Length[objects]]]=="[Graph Relation]"],
Print["importRI abort 7\n"];Abort[]];
(*------------------------------------------------*)
(* Contr\[OHat]le du format se poursuit lors de la lecture des itemsets *)
itemset=
Table[
With[{attList=StringSplit[rawFile[[liNb]]],lenRef=Length@items},
If[OptionValue["Control"],If[Length[attList]!= Length@items,Print["importRI abort 8\n Attend ",lenRef, " descipteurs binaires et en trouve ",Length[attList]," a la ligne ", liNb];Abort[]]];
ToExpression[attList]
]
,
{liNb,7,6+Length[objects]}
];
(*------------------------------------------------*)
(* Contr\[OHat]le des noeuds du graphe se fait a la vol\[EAcute]e *)
edges=
Union[
Flatten[
Table[
With[{li=StringTrim/@StringSplit[rawFile[[liNb]]]},
If[Length[li]!=2,Print["importRI abort 9\n"];Abort[]];
With[{n=li[[1]],linked=StringTrim/@StringSplit[li[[2]],","]},
If[OptionValue["Control"],If[Not[And[MemberQ[objects,n],Apply[And,Map[MemberQ[objects,#]&,linked]]]],Print["importRI abort 10\n"];Abort[]
]];
Map[Sort[n\[UndirectedEdge]#]&,linked]
]
]
,
{liNb,9+Length[objects],Length[rawFile]-1}
]
]
];
objproperties=MapIndexed[
#1->booleToExplicit[itemset[[First@#2]],items]&,
objects
];
{
"Graphe"->Graph[
objects,
edges,
Evaluate[FilterRules[{opt}, Options[Graph]]]
],
"Objets"->objects,
"Items"->items,
"Itemsets"->objproperties
}
]
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*(*importRI[FileNameJoin[{NotebookDirectory[],"data","mougel.ri"}]]*)*)


(* ::Subsubsection::Closed:: *)
(*parseNriItemsetLine (private)*)


(* ::Input::Initialization:: *)
parseNriItemsetLine[nriLine_String,obj_List,it_List]:=
With[{li=StringSplit[nriLine]},
With[{o=obj[[1+ToExpression[StringTrim[First@li]]]]},
If[Length[li]>1,
With[{itemsets=Map[it[[1+ToExpression[StringTrim[#]]]]&,StringSplit[Last@li,","]]},
Rule[o,itemsets]
],
Rule[o,{}]
]
]
]


(* ::Subsubsection::Closed:: *)
(*parseNriAdjacencyLine (private)*)


(* ::Input::Initialization:: *)
parseNriAdjacencyLine[nriLine_String,obj_List,directed_]:=
With[
{
li=StringSplit[nriLine]
},
If[Length[li]>1,
With[
{
o=obj[[1+ToExpression[StringTrim[First@li]]]],adjacencies=Map[obj[[1+ToExpression[StringTrim[#]]]]&,StringSplit[Last@li,","]]
},
If[
directed,
Map[DirectedEdge[o,#]&,adjacencies],
Map[Sort[UndirectedEdge[o,#]]&,adjacencies]
]
],
{}]
]


(* ::Subsubsection::Closed:: *)
(*importNRI*)


(* ::Input::Initialization:: *)
Options[importNRI]={
Sequence@@FilterRules[Options@Graph,Except[Properties]],
"SelfLoops"->True,
"Multiple"->False,
"Directed"->False
};

importNRI[fileName_String,opt:OptionsPattern[]]:=
Module[
{rawData,obj,it,is,edges},
rawData=Import[fileName,"Lines"];
obj=Map[StringTrim,StringSplit[rawData[[2]],"|"]];
it=Map[StringTrim,StringSplit[rawData[[3]],"|"]];
is=Map[
parseNriItemsetLine[#,obj,it]&,
Take[rawData,{4,4+Length[obj]-1}]
];
edges=Flatten[
Map[
parseNriAdjacencyLine[#,obj,OptionValue["Directed"]]&,
Take[rawData,{4+Length[obj]+1,-1}]
],
1]//
If[Not@OptionValue["SelfLoops"],
Select[#,First[#]!=Last[#]&],
#
]&//
If[Not@OptionValue["Multiple"],
Union[#],
#
]&;

Association@{
"Objets"->obj,
"Items"->it,
"Itemsets"->Association@is,
"Graphe"->Graph[obj,edges,Evaluate[FilterRules[{opt}, Options[Graph]]]],
"ID"->Association[ MapIndexed[(#1-> ToString[-1+First[#2]])&,obj]]
}
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*fileName=FileNameJoin[{NotebookDirectory[],"data","pondere4.nri"}]*)


(* ::Input:: *)
(*rawData=Import[fileName,"Lines"]*)


(* ::Input:: *)
(*obj=Map[StringTrim,StringSplit[rawData[[2]],"|"]];*)
(*it=Map[StringTrim,StringSplit[rawData[[3]],"|"]];*)
(*is=Map[*)
(*parseNriItemsetLine[#,obj,it]&,*)
(*Take[rawData,{4,4+Length[obj]-1}]*)
(*]*)


(* ::Input:: *)
(*nriLine=First@Take[rawData,{4+Length[obj]+1,-1}]*)


(* ::Input:: *)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","oriente0.nri"}]]*)


(* ::Input:: *)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) exportRI*)


(* ::Input::Initialization:: *)
Options[exportRI]={"Itemsets"->"Binary"};
exportRI[{"Graphe"->graph_,"Objets"->listDesObjets_,"Items"->listeDesItems_,"Itemsets"->itemsets_}, fileName_, message_String, opts:OptionsPattern[]]:=
Export[
fileName,
"[Relational Context]"<>"\n"<>
message<>"\n"<>
Switch[
OptionValue["Itemsets"],
"Binary","[Binary Relation]",
_,"ToDo"
]<>"\n"<>
"genere par la fonction exportRI[] de Mathematica"<>"\n"<>
StringJoin[Riffle[ToString/@listDesObjets," | "]]<>" |"<>"\n"<>
StringJoin[Riffle[ToString/@listeDesItems," | "]]<>" |"<>"\n"<>
StringJoin[Riffle[Map[With[{itemset=#},Riffle[Map[If[MemberQ[itemset,#],"1","0"]&,listeDesItems]," "]]&,listDesObjets/.itemsets], "\n"]]<>"\n"<>
"[END Relational Context]"<>"\n"<>
"[Graph Relation]"<>"\n"<>
Map[With[{oth=AdjacencyList[graph, #]},If[{}==oth,"",StringJoin[ToString[#]," ",Riffle[Map[ToString,oth],","],"\n"]]]&,listDesObjets]<>
"[END Graph Relation]"<>"\n"
,
"Text"
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*(*exportRI[*)
(*importRI[FileNameJoin[{NotebookDirectory[],"data","mougel.ri"}]],*)
(*FileNameJoin[{HomeDirectory[],"Desktop","toto.ri"}],*)
(*"shuffleItemsetsRI de mougel.ri"*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*relationsString (private)*)


(* ::Input::Initialization:: *)
Options[relationsString]={"Verbose"->False,"AtIterations"->100};
relationsString[graph_,listDesObjets_, OptionsPattern[]]:=
With[{
objToId=Association[MapIndexed[#1->-1+First@#2&,listDesObjets]]
},
If[OptionValue@"Verbose",Print["----- \[CapitalEAcute]criture des relations"]];
StringJoin[
Riffle[
MapIndexed[
(
If[OptionValue["Verbose"]&&Mod[First@#2,OptionValue@"AtIterations"]==0,Print[First@#2]];
With[
{
oth=
If[
DirectedGraphQ[graph],
Cases[IncidenceList[graph,#1],#1\[DirectedEdge]o_:> o]
(*Cases[EdgeList[graph],#\[DirectedEdge]x_\[RuleDelayed]x]&[#1]*),
AdjacencyList[graph, #1]
],
obj=#1
},
If[
{}===oth,
ToString[objToId[obj]],
ToString[objToId[obj]]<>" "<>Riffle[Map[ToString[objToId[#]]&,oth],","]
]
]
)&,
listDesObjets
],
"\n"
]
]
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]//relationsString["Graphe"/.#,"Objets"/.#]&*)


(* ::Subsubsection::Closed:: *)
(*itemsetsString (private)*)


(* ::Input::Initialization:: *)
Options[itemsetsString]={"Verbose"->False,"AtIterations"->100};
itemsetsString[graph_,listDesObjets_,listeDesItems_,itemsets_, OptionsPattern[]]:=
With[{
objToId=Association[MapIndexed[#1->-1+First@#2&,listDesObjets]],
itemToId=Association[MapIndexed[#1->-1+First@#2&,listeDesItems]]
},
If[OptionValue@"Verbose",Print["------ \[CapitalEAcute]criture des itemsets"]];
StringJoin@Riffle[
MapIndexed[
(
If[OptionValue["Verbose"]&&Mod[First@#2,OptionValue@"AtIterations"]==0,Print[First@#2]];
ToString[objToId[#1]]<>" "<>
If[Not[KeyExistsQ[itemsets,#1]],
"",
StringJoin[Riffle[Map[ToString@itemToId@#&,itemsets[#1]],","]]
]
)&,
listDesObjets
],
"\n"]
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]//*)
(*itemsetsString["Graphe"/.#,"Objets"/.#,"Items"/.#,"Itemsets"/.#]&*)


(* ::Subsubsection::Closed:: *)
(*nriString (private)*)


(* ::Input::Initialization:: *)
Options[nriString]={"Verbose"->False,"AtIterations"->100};
nriString[data_Association, opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"Objets"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
("# "<>"genere par la fonction exportNRI[] de Mathematica"<>"\n"<>
If[Length[data["Objets"]]==1,ToString[data["Objets"][[1]]],StringJoin[Riffle[ToString/@data["Objets"]," | "]]]<>"\n"<>
If[Length[data["Items"]]==1,ToString[data["Items"][[1]]],StringJoin[Riffle[ToString/@data["Items"]," | "]]]<> "\n"<>
itemsetsString[data["Graphe"],data["Objets"],data["Items"],Association[data["Itemsets"]], opts]
<>"\n"<>
"#\n"<>
relationsString[data["Graphe"],data["Objets"], opts]
);


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*nriString[*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*exportNRI*)


(* ::Input::Initialization:: *)
Options[exportNRI]={"Verbose"->False,"AtIterations"->100};
exportNRI[data_Association, fileName_,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"Objets"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
Export[
fileName,
nriString[data]
,
"Text"
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*exportNRI[*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]],*)
(*FileNameJoin[{NotebookDirectory[],"data","mougel.nri.copy"}]*)
(*]*)


(* ::Input:: *)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]==*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.copy"}]]*)


(* ::Subsubsection::Closed:: *)
(*biPatternConvert*)


(* ::Input::Initialization:: *)
biPatternConvert[data_Association]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"Objets"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
Module[{graphe, objets, items,allO, allI, itemsets},

(* Construction du graphe *)
graphe=
Graph[
Apply[Union,Map[List[StringJoin["o",#],StringJoin["i",#]]&,VertexList[#]]],
Cases[EdgeList[#],DirectedEdge[o_,i_]:>DirectedEdge[StringJoin["o",o],StringJoin["i",i]]],
VertexLabels->"Name"
]&@DirectedGraph[data["Graphe"]];
(*Graph[
(* D\[EAcute]doublement des sommets *)
Map[
{
StringJoin["o",#],
StringJoin["i",#]
}&,
VertexList[data["Graphe"]]
]//Transpose//Flatten,
(* D\[EAcute]doublement des ar\[EHat]tes *)
Map[
{
DirectedEdge[StringJoin["o",First[#]],StringJoin["i",Last[#]]],
DirectedEdge[StringJoin["o",Last[#]],StringJoin["i",First[#]]]
}&,EdgeList[data["Graphe"]]
]//Transpose//Flatten
]*)

(* Construction de la liste des objets *)
objets =
Map[
{
StringJoin["o",#],
StringJoin["i",#]
}&,
data["Objets"]
]//Transpose//Flatten;

(* Construction de la liste des items *)
{allO, allI}=
Map[
{
StringJoin["o",#],
StringJoin["i",#]
}&,
data["Items"]
]//Transpose;
items=Flatten[{allO, allI}];

(* Construction de la liste des itemsets *)
itemsets=KeyValueMap[
{
StringJoin["o",#1]-> Join[Map[StringJoin["o",#]&,#2],allI],
StringJoin["i",#1]-> Join[Map[StringJoin["i",#]&,#2],allO]
}&,data["Itemsets"]
]//Transpose//Flatten//Association;
Association["Graphe"->graphe, "Objets"->objets, "Items"->items, "Itemsets"->itemsets]
];


(* ::Input:: *)
(*test=biPatternConvert[*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","brauer_elati_3_7.nri"}]],*)
(*{"i","o"}*)
(*]*)


(* ::Input:: *)
(*test["Graphe"]*)


(* ::Subsection::Closed:: *)
(*minerLC (NRI)*)


(* ::Subsubsection::Closed:: *)
(*rawImportMinerLCOut (private)*)


(* ::Input::Initialization:: *)
Options[rawImportMinerLCOut]={};
rawImportMinerLCOut[fileName_]:=
With[{rawdat=Import[fileName,"Lines"]},
Association[
Append[
Map[
Apply[Rule,
StringSplit[StringDelete[StringDrop[#,2]," "|"\t"],"="]
]&,
Select[rawdat,And[
StringLength[#]>3,
StringMatchQ[#,StartOfString~~"#"~~___],
Not[StringMatchQ[#,StartOfString~~"# ="~~___]]
]&
]
],
"rawdat"->Map[ToExpression["{"<>#<>"}"]&,Select[rawdat,Not[StringMatchQ[#,StartOfString~~"#"~~___]]&]]
]
]
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*rawImportMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.min_sup_1_t1"}]]*)


(* ::Subsubsection::Closed:: *)
(*importMinerLCOut*)


(* ::Input::Initialization:: *)
Options[importMinerLCOut]={"OmitExtensions"->False,"RemoveNoRole"-> False};

importMinerLCOut[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"rawdat"],
Length[data["rawdat"]]==0,
KeyExistsQ[data,"local"],
data["local"]=="0"
]:=
Association[
"ClosAbs"->{},
If[OptionValue["OmitExtensions"],Nothing,"ExtGlob"-> Association[]],
If[OptionValue["OmitExtensions"],Nothing,"ExtAbs"-> Association[]],
"SupGlob"->Association[],
"SupAbs"-> Association[],
"Roles"->{},
"ExperimentInformations"->Delete[data,"rawdat"]
];


importMinerLCOut[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"rawdat"],
Length[data["rawdat"]]==0,
KeyExistsQ[data,"local"],
data["local"]=="1"
]:=
Association[
"ClosAbs"->{},
If[OptionValue["OmitExtensions"],Nothing,"ExtGlob_ClosAbs"-> Association[]],
If[OptionValue["OmitExtensions"],Nothing,"ExtAbs_ClosAbs"-> Association[]],
If[OptionValue["OmitExtensions"],Nothing,"ExtGlob_ClosLoc"-> Association[]],
If[OptionValue["OmitExtensions"],Nothing,"ExtAbs_ClosLoc"-> Association[]],
If[OptionValue["OmitExtensions"],Nothing,"ExtLoc_ClosLoc"-> Association[]],
"ClosLoc"->Association[],
"SupGlob_ClosAbs"->Association[],
"SupAbs_ClosAbs"-> Association[],
"SupGlob_ClosLoc"-> Association[],
"SupAbs_ClosLoc"-> Association[],
"SupLoc_ClosLoc"-> Association[],
"(ClosAbs_ExtLoc)"-> {},
"ExperimentInformations"->Delete[data,"rawdat"]
];



importMinerLCOut[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"local"],
data["local"]=="1",
KeyExistsQ[data,"rawdat"],
Length[data["rawdat"]]>0
]:=
Association[Transpose[
Map[
With[{
closabs=Sort@#[[1]],
extglobClosabs=Sort@#[[2]],
extabsClosabs=Sort@#[[3]],
extglobClosloc=Sort@#[[4]],
extabsClosloc=Sort@#[[5]],
extlocClosloc=Sort@#[[6]],
closloc=Sort@#[[7]]
},
{
closabs,
{closabs,extlocClosloc}->extglobClosabs,
{closabs,extlocClosloc}->extabsClosabs,
{closabs,extlocClosloc}->extglobClosloc,
{closabs,extlocClosloc}->extabsClosloc,
{closabs,extlocClosloc}->extlocClosloc,
{closabs,extlocClosloc}->closloc
}
]&,
data["rawdat"]
]
]//
Association[
"ClosAbs"->#[[1]],
If[OptionValue["OmitExtensions"],Nothing,"ExtGlob_ClosAbs"-> Association[#[[2]]]],
If[OptionValue["OmitExtensions"],Nothing,"ExtAbs_ClosAbs"-> Association[#[[3]]]],
If[OptionValue["OmitExtensions"],Nothing,"ExtGlob_ClosLoc"-> Association[#[[4]]]],
If[OptionValue["OmitExtensions"],Nothing,"ExtAbs_ClosLoc"-> Association[#[[5]]]],
If[OptionValue["OmitExtensions"],Nothing,"ExtLoc_ClosLoc"-> Association[#[[6]]]],
"ClosLoc"->Association[#[[7]]],
"SupGlob_ClosAbs"->Map[Length, Association[#[[2]]]],
"SupAbs_ClosAbs"-> Map[Length, Association[#[[3]]]],
"SupGlob_ClosLoc"-> Map[Length, Association[#[[4]]]],
"SupAbs_ClosLoc"-> Map[Length, Association[#[[5]]]],
"SupLoc_ClosLoc"-> Map[Length, Association[#[[6]]]],
"(ClosAbs_ExtLoc)"-> Keys[#[[2]]],
"ExperimentInformations"->Delete[data,"rawdat"]
]&];

importMinerLCOut[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"local"],
data["local"]=="0",
KeyExistsQ[data,"rawdat"],
Length[data["rawdat"]]>0
]:=
Association[
Transpose[
Map[
With[{
pattern=Sort@#[[1]],
originalExtension=#[[2]],
abstractExtension=If[
MatchQ[#[[3]],{_Rule...}],
If[OptionValue["RemoveNoRole"]&&Keys[#]=={"no_role"},#["no_role"],#]&@Association[#[[3]]],
#[[3]]],
originalSupport=Length[#[[2]]],
abstractSupport=If[
MatchQ[#[[3]],{_Rule...}],
If[OptionValue["RemoveNoRole"]&&Keys[#]=={"no_role"},#["no_role"],#]&@Association[Cases[#[[3]],Rule[role_,ext_]:>Rule[role,Length@ext]]],
Length[#[[3]]]]
},
{
pattern,
pattern->originalExtension,
pattern->abstractExtension,
pattern->originalSupport,
pattern->abstractSupport
}
]&,
data["rawdat"]
]
]//(
Association[
"ClosAbs"-> #[[1]],
If[OptionValue["OmitExtensions"],Nothing,"ExtGlob"-> Association[#[[2]]]],
If[OptionValue["OmitExtensions"],Nothing,"ExtAbs"-> Association[#[[3]]]],
"SupGlob"-> Association[#[[4]]],
"SupAbs"-> Association[#[[5]]],
"Roles"-> If[OptionValue["RemoveNoRole"],{},If[AssociationQ[#],Keys[#],{}]&@Last[First[#[[3]]]]],
"ExperimentInformations"->Delete[data,"rawdat"]
]
)&
];


importMinerLCOut[filename_String,opts:OptionsPattern[]]:=importMinerLCOut[rawImportMinerLCOut[filename],"OmitExtensions"->OptionValue["OmitExtensions"],"RemoveNoRole"->OptionValue["RemoveNoRole"]];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*readMinerLC["lazega_advice2_alt.nri",{"-o","-hub_aut1 6 6","-n 100"},"PathForResults"-> "/Users/santini/LIPN/Work/AttributedGraphs/results"]*)


(* ::Input:: *)
(*runMinerLC["mougel.nri",{"-d1 1"},"PathForNRI"-> FileNameJoin[{NotebookDirectory[], "data"}],"PathForResults"->FileNameJoin[{NotebookDirectory[], "data"}],"Verbose"->False];*)
(**)
(*SetOptions[importMinerLCOut,"RemoveNoRole"-> False(* Default *)]*)
(*dataNoRole=importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.d1_1"}]];*)
(*dataNoRole["Roles"]*)
(*Map[dataNoRole["ExtAbs"][{"pop"}][#]&,dataNoRole["Roles"]]*)
(**)
(*dataNoRole=importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","lazega_advice2_alt.nri.o_hub_aut1_6_6"}]];*)
(*dataNoRole["Roles"]*)
(*Map[dataNoRole["ExtAbs"][{"Age_>25"}][#]&,dataNoRole["Roles"]]*)
(**)
(**)
(*SetOptions[importMinerLCOut,"RemoveNoRole"-> True]*)
(*dataNoNoRole=importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.d1_1"}]];*)
(*dataNoNoRole["Roles"]*)
(*dataNoNoRole["ExtAbs"][{"pop"}]*)
(**)


(* ::Input:: *)
(*importMinerLCOut[rawImportMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.min_sup_1_t1"}]]]*)


(* ::Input:: *)
(*importMinerLCOut[rawImportMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.local_min_sup_1_t1"}]]]*)


(* ::Input:: *)
(*data=importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.min_sup_1_t1"}]]*)


(* ::Input:: *)
(*data=importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.local_min_sup_1_t1"}]]*)


(* ::Input:: *)
(*data=importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.res"}]]//InputForm*)


(* ::Subsection::Closed:: *)
(*Comodo*)


(* ::Subsubsection::Closed:: *)
(*importComodo*)


(* ::Input::Initialization:: *)
ClearAll@importComodo;

Options[importComodo]={"OmitExtensions"->False};

importComodo[filename_String,opts:OptionsPattern[]]:=
importComodo[rawImportMinerLCOut[filename],"OmitExtensions"->OptionValue["OmitExtensions"]]

importComodo[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"local"],
data["local"]=="0",
KeyExistsQ[data,"rawdat"],
Length[data["rawdat"]]==0
]:=
Association[
"ComodoPatterns"-> {},
If[OptionValue["OmitExtensions"],Nothing,"ComodoExt"-> Association[]],
"ComodoSup"-> Association[],
"ComodoExperimentInformations"->Delete[data,"rawdat"]
];

importComodo[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"local"],
data["local"]=="0",
KeyExistsQ[data,"rawdat"],
Length[data["rawdat"]]>0
]:=
Association[
Transpose[
Map[
With[{
pattern=Sort[#[[1]]]
},
{
pattern,
If[OptionValue["OmitExtensions"],Nothing,pattern->#[[2]]],
pattern->Length[#[[2]]]
}
]&,
data["rawdat"]
]
]//
Association[
"ComodoPatterns"-> #[[1]],
If[OptionValue["OmitExtensions"],Nothing,"ComodoExt"-> Association[#[[2]]]],
If[OptionValue["OmitExtensions"],"ComodoSup"-> Association[#[[2]]],"ComodoSup"-> Association[#[[3]]]],
"ComodoExperimentInformations"->Delete[data,"rawdat"]
]&
]


(* ::Subsection::Closed:: *)
(*CORON (RCF FCIGE)*)


(* ::Subsubsection::Closed:: *)
(*rcfString (private)*)


(* ::Input::Initialization:: *)
rcfString[data_, message_String]/;And[
KeyExistsQ[data,"Objets"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
"[Relational Context]"<>"\n"<>
message<>"\n"<>
"[Binary Relation]\n"<>
"Produced with rcfString[] function of MinerLC Mathematica Package"<>"\n"<>
StringJoin[Riffle[ToString/@data["Objets"]," | "]]<>" |"<>"\n"<>
StringJoin[Riffle[ToString/@data["Items"]," | "]]<>" |"<>"\n"<>
StringJoin[Riffle[Map[With[{itemset=#},Riffle[Map[If[MemberQ[itemset,#],"1","0"]&,data["Items"]]," "]]&,data["Objets"]/.data["Itemsets"]], "\n"]]<>"\n"<>
"[END Relational Context]";


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*rcfString[*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri"}]],*)
(*"test"*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*exportRcf*)


(* ::Input::Initialization:: *)
exportRcf[data_, fileName_, message_String]/;And[
KeyExistsQ[data,"Objets"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
Export[
fileName,
rcfString[data,message],
"Text"
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*exportRcf[*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri"}]],*)
(*FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.rcf"}],*)
(*"test"*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*parseFCIGEline (private)*)


(* ::Input::Initialization:: *)
parseFCIGEline[fcigeLine_]:=
With[
{
rawCloExtGen= First@StringCases[fcigeLine,clos__~~" ("~~ext__~~") +; "~~gen__:>{clos,ext,gen}]
},
{
ToExpression[StringReplace[rawCloExtGen[[1]],{" "->"","{"->"{\"",","->"\",\"","}"->"\"}"}]]/.{{""}->{}},
ToExpression[If[StringMatchQ[rawCloExtGen[[2]],"["~~__~~"]"]||StringMatchQ[rawCloExtGen[[2]],"("~~__~~")"],StringReplace[rawCloExtGen[[2]],{"("->"{","["->"{","'"->"\"","]"->"}",")"->"}"}],If[rawCloExtGen[[2]]=="[]",{},rawCloExtGen[[2]]]]],
ToExpression[StringReplace[StringReplace[rawCloExtGen[[3]],{" "->"","["->"{","{"->"{\"",","->"\",\"","}"->"\"}","]"->"}"}],"}\",\"{"->"},{"]]/.{{""}->{}}
}
];


(* ::Subsubsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*parseFCIGEline["{folk, pop, rock} (5) +; [{pop}]"]//InputForm*)


(* ::Input:: *)
(*parseFCIGEline["{folk, pop, rock} (['N', 'O', 'P', 'Q', 'R']) +; [{pop} ,{folk, pop, rock}]"]//InputForm*)


(* ::Input:: *)
(*parseFCIGEline["{folk, pop, rock} ([]) +; [{pop} ,{folk, pop, rock}]"]//InputForm*)


(* ::Subsubsection::Closed:: *)
(*importFCIGE*)


(* ::Input::Initialization:: *)
importFCIGE[fileName_]:=
With[{rawFile=Import[fileName,"Lines"]},
Transpose[
Map[
With[{line=parseFCIGEline[#]},
With[{closGlob=line[[1]],supOrExtGlob=line[[2]],genGlob=line[[3]]},
{closGlob,closGlob-> supOrExtGlob,closGlob-> genGlob,Map[#->supOrExtGlob &,genGlob]}
]
]&,
Select[rawFile,#!="" && StringTake[#,1]!="#"&]
]
]//
Association[
"ClosGlob"->#[[1]],
"SupGlob_ClosGlob"->Association[#[[2]]],
"GenGlob_ClosGlob"-> Association[#[[3]]],
"SupGlob_GenGlob"->Association[Flatten[#[[4]]]]
]&
];

importFCIGE[fileName_,itemsets_]:=
With[{parsed=importFCIGE[fileName],objets=Keys@itemsets},
Map[
With[{patt=#},
patt-> Select[objets,SubsetQ[itemsets[#],patt]&]
]&,
parsed["ClosGlob"]
]//
Append[
parsed,
"ExtGlob_ClosGlob"->Association[#]
]&
]


(* ::Subsubsubsection:: *)
(*Tests*)


(* ::Input:: *)
(*dat=importFCIGE[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red_3.fcige"}]];*)
(*Keys@dat*)


(* ::Input:: *)
(*toto=importFCIGE[*)
(*FileNameJoin[{NotebookDirectory[],"data","s50_an1_red_3.fcige"}],*)
(*"Itemsets"/.importNRI[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri"}]]*)
(*]*)


(* ::Input:: *)
(*And@@Map[toto["SupGlob_ClosGlob"][#]==Length[toto["ExtGlob_ClosGlob"][#]]&,toto["ClosGlob"]]*)


(* ::Subsection::Closed:: *)
(*supportsAttendus (GRA RES NRI)*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) exportGRA*)


(* ::Input::Initialization:: *)
(*Options[exportGRA]={"Verbose"\[Rule]False,"AtIterations"\[Rule]100};
exportGRA[data_Association, fileName_String,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"Objets"],
KeyExistsQ[data,"Graphe"]
]:=
Export[
fileName,
StringJoin[Riffle[ToString/@data["Objets"]," | "]]<>"\n"<>
relationsString[data["Graphe"],data["Objets"], opts]
,
"Text"
];*)


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*(*exportGRA[*)
(*importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]], FileNameJoin[{NotebookDirectory[],"data","mougel.gra"}]*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) resString (private)*)


(* ::Input::Initialization:: *)
(*Clear@resString;
Options[resString]={"Verbose"\[Rule]False,"AtIterations"\[Rule]100};

resString[ data_Association,OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]\[Equal]"0",
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"ExtGlob"],
KeyExistsQ[data,"ExtAbs"]
]:=
StringJoin@MapIndexed[
(
If[OptionValue["Verbose"]&&Mod[First@#2,OptionValue@"AtIterations"]\[Equal]0,Print[First@#2]];
StringJoin[Riffle[ToString/@
{
#1,
data["ExtGlob"][#1],
data["ExtAbs"][#1]
},", "]]<>"\n"
)&,
data["ClosAbs"]
];

resString[ data_Association,OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]\[Equal]"1",
KeyExistsQ[data,"(ClosAbs_ExtLoc)"],
KeyExistsQ[data,"ExtGlob_ClosAbs"],
KeyExistsQ[data,"ExtAbs_ClosAbs"],
KeyExistsQ[data,"ExtGlob_ClosLoc"],
KeyExistsQ[data,"ExtAbs_ClosLoc"],
KeyExistsQ[data,"ExtLoc_ClosLoc"]
]:=
StringJoin@MapIndexed[
(
If[OptionValue["Verbose"]&&Mod[First@#2,OptionValue@"AtIterations"]\[Equal]0,Print[First@#2]];
StringJoin[Riffle[ToString/@
{
First@#1,
data["ExtGlob_ClosAbs"][#1],
data["ExtAbs_ClosAbs"][#1],
data["ExtGlob_ClosLoc"][#1],
data["ExtAbs_ClosLoc"][#1],
data["ExtLoc_ClosLoc"][#1],
data["ClosLoc"][#1]
},", "]]<>"\n"
)&,
data["(ClosAbs_ExtLoc)"]
];*)


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*(*resString[importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.min_sup_1_t1"}]]]*)*)


(* ::Input:: *)
(*(*resString[importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.local_min_sup_1_t1"}]]]*)*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) exportRES*)


(* ::Input::Initialization:: *)
(*Options[exportRES]={"Verbose"\[Rule]False,"AtIterations"\[Rule]100};
exportRES[data_Association, fileName_String,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"]
]:=
Export[
fileName,
resString[data, FilterRules[{opts},Options[resString]]]
,
"Text"
];*)


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*(*exportRES[*)
(*importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.min_sup_1_t1"}]],*)
(*FileNameJoin[{NotebookDirectory[],"data","mougel.nri.min_sup_1_t1"<>".res"}]*)
(*]*)*)


(* ::Input:: *)
(*(*exportRES[*)
(*importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.local_min_sup_1_t1"}]],*)
(*FileNameJoin[{NotebookDirectory[],"data","mougel.nri.local_min_sup_1_t1"<>".res"}]*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*rawImportSupportAttendu (private)*)


(* ::Input::Initialization:: *)
rawImportSupportAttendu[filename_String]:=
Association[
With[{rawdat=Import[filename,"Lines"]},
Append[
Map[
If[#=={},Nothing,Apply[Rule,#]]&,
Map[
StringSplit[StringDelete[StringDrop[#,2]," "],"="]&,
Select[rawdat,StringLength[#]>1&&StringTake[#,1]== "#"&]
]
],
"rawdat"->Map[
ToExpression["{"<>StringReplace[#,", -nan"~~EndOfString|", nan"~~EndOfString->"0"]<>"}"]&,
Select[rawdat,StringLength[#]>1&&StringTake[#,1]!= "#"&]
]
]
]
];


(* ::Input:: *)
(*rawImportSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.t1_n_200.sup"}]]*)


(* ::Input:: *)
(*rawImportSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.local_t1_n_100.sup"}]]*)


(* ::Input:: *)
(*Delete[rawImportSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.local_t1_n_100.sup"}]],"rawdat"]*)


(* ::Subsubsection::Closed:: *)
(*importSupportAttendu*)


(* ::Input::Initialization:: *)
importSupportAttendu[data_Association]/;And[
KeyExistsQ[data,"rawdat"] ,
Length[data["rawdat"]]==0
]:=Association[
"ClosAbs"-> {},
"SupGlob"-> Association[],
"SupAbs"-> Association[],
"MeanExpectedSupAbs"-> Association[],
"SigmaExpectedSupAbs"-> Association[],
"AttendusExperimentInformations"->Delete[data,"rawdat"]
];


importSupportAttendu[data_Association]/;And[
KeyExistsQ[data,"rawdat"] ,
Length[data["rawdat"]]>0 ,
Not[KeyExistsQ[data,"local"]]
]:=
Transpose[
Map[
With[{
pattern=Sort@#[[1]],
supportGlobalObserve=#[[2]],
supportAbstraitObserve=#[[3]],
moyenneSupportAbstraitAttendu=#[[4]],
sigmaSupportAbstraitAttendu=#[[5]]
},
{
pattern,
pattern->supportGlobalObserve,
pattern->supportAbstraitObserve,
pattern->moyenneSupportAbstraitAttendu,
pattern->sigmaSupportAbstraitAttendu
}
]&
,
data["rawdat"]
]
]//
Association[
"ClosAbs"-> #[[1]],
"SupGlob"-> Association[#[[2]]],
"SupAbs"-> Association[#[[3]]],
"MeanExpectedSupAbs"-> Association[#[[4]]],
"SigmaExpectedSupAbs"-> Association[#[[5]]],
"AttendusExperimentInformations"->Append[Delete[data,"rawdat"],"local"->"0"]
]&;


importSupportAttendu[data_Association]/;And[
KeyExistsQ[data,"rawdat"] ,
Length[data["rawdat"]]>0 ,
KeyExistsQ[data,"local"],
data["local"]=="1"
]:=Transpose[
Map[
With[{
supglobClosabs=#[[2]],
suplocCloslocExpected=#[[7]],
suplocCloslocSigma=#[[8]]
},
{
Rule[supglobClosabs,suplocCloslocExpected],
Rule[supglobClosabs,suplocCloslocSigma]
}
]&,
data["rawdat"]
]
]//
Association[
"MeanExpectedSupLoc"->Association[#[[1]]],
"SigmaExpectedSupLoc"-> Association[#[[2]]],
"AttendusExperimentInformations"->Delete[data,"rawdat"]
]&;

importSupportAttendu[filename_String]:=
importSupportAttendu[rawImportSupportAttendu[filename]];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*importSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","brauer_elati_3_7.nri.t1_n_200.sup"}]]*)


(* ::Input:: *)
(*importSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","mougel.nri.t1_n_200.sup"}]]*)


(* ::Input:: *)
(*importSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.t1_n_200.sup"}]]*)


(* ::Input:: *)
(*importSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.local_t1_n_100.sup"}]]*)


(* ::Subsubsection::Closed:: *)
(*importSupportAttenduBi*)


(* ::Input::Initialization:: *)
importSupportAttenduBi[data_Association]/;And[
KeyExistsQ[data,"rawdat"] ,
Length[data["rawdat"]]==0
]:=Association[
"ClosAbs"-> {},
"SupGlob"-> Association[],
"SupAbs"-> Association[],
"MeanExpectedSupAbs"-> Association[],
"SigmaExpectedSupAbs"-> Association[],
"AttendusExperimentInformations"->Delete[data,"rawdat"]
];


importSupportAttenduBi[data_Association]/;And[
KeyExistsQ[data,"rawdat"] ,
Length[data["rawdat"]]>0 ,
Not[KeyExistsQ[data,"local"]]
]:=
Transpose[
Map[
With[{
pattern=Sort@#[[1]],
supportGlobalObserveOut=#[[2]],
supportGlobalObserveIn=#[[3]],
supportAbstraitObserve=#[[4]],
moyenneSupportAbstraitAttendu=#[[5]],
sigmaSupportAbstraitAttendu=#[[6]]
},
{
pattern,
pattern->supportGlobalObserveOut,
pattern->supportGlobalObserveIn,
pattern->supportAbstraitObserve,
pattern->moyenneSupportAbstraitAttendu,
pattern->sigmaSupportAbstraitAttendu
}
]&
,
data["rawdat"]
]
]//
Association[
"ClosAbs"-> #[[1]],
"SupGlobOut"-> Association[#[[2]]],
"SupGlobIn"-> Association[#[[3]]],
"SupAbs"-> Association[#[[4]]],
"MeanExpectedSupAbs"-> Association[#[[5]]],
"SigmaExpectedSupAbs"-> Association[#[[6]]],
"AttendusExperimentInformations"->Append[Delete[data,"rawdat"],"local"->"0"]
]&;

importSupportAttenduBi[filename_String]:=
importSupportAttenduBi[rawImportSupportAttendu[filename]];


(* ::Subsubsection::Closed:: *)
(*resolveLocalSupportAttendu*)


(* ::Input::Initialization:: *)
SetAttributes[resolveLocalSupportAttendu,HoldFirst];
resolveLocalSupportAttendu[data_]:=
Transpose[
Map[
With[{supGlobClosLoc=data["SupGlob_ClosLoc"][#]},
{
Rule[#,data["MeanExpectedSupLoc"][supGlobClosLoc]],
Rule[#,data["SigmaExpectedSupLoc"][supGlobClosLoc]]
}
]&
,
data["(ClosAbs_ExtLoc)"]
]
]//
(
AppendTo[data,"MeanExpectedSupLoc"->Association[First[#]]];
AppendTo[data,"SigmaExpectedSupLoc"->Association[First[#]]];
)&;


(* ::Input:: *)
(*data=*)
(*Join[*)
(*importMinerLCOut[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.local_min_sup_1_t1"}]],*)
(*importSupportAttendu[FileNameJoin[{NotebookDirectory[],"data","s50_an1_red.nri.local_t1_n_100.sup"}]]*)
(*];*)


(* ::Input:: *)
(*data["MeanExpectedSupLoc"]*)


(* ::Input:: *)
(*resolveLocalSupportAttendu@data*)


(* ::Input:: *)
(*data["MeanExpectedSupLoc"]*)


(* ::Subsection::Closed:: *)
(*Fonctions d\[EAcute]finissant/utilisant une arborescence de nommage*)


(* ::Subsubsection::Closed:: *)
(*$ParametersPatterns*)


(* ::Input::Initialization:: *)
$ParametersPatterns={
StartOfString~~"-local"~~EndOfString,
StartOfString~~"-min_sup "~~__~~EndOfString,
StartOfString~~"-tg"~~EndOfString,
StartOfString~~"-eg"~~EndOfString,
StartOfString~~"-u "~~__~~EndOfString,

StartOfString~~"-d1 "~~__~~EndOfString,
StartOfString~~"-t1"~~EndOfString,
StartOfString~~"-c1 "~~__~~EndOfString,
StartOfString~~"-k1 "~~__~~EndOfString,
StartOfString~~"-top1 "~~__~~EndOfString,
StartOfString~~"-sd1 "~~__~~EndOfString,
StartOfString~~"-sk1 "~~__~~EndOfString,

StartOfString~~"-d2 "~~__~~EndOfString,
StartOfString~~"-t2"~~EndOfString,
StartOfString~~"-c2 "~~__~~EndOfString,
StartOfString~~"-k2 "~~__~~EndOfString,
StartOfString~~"-top2 "~~__~~EndOfString,
StartOfString~~"-sd2 "~~__~~EndOfString,
StartOfString~~"-sk2 "~~__~~EndOfString,

StartOfString~~"-o"~~EndOfString,
StartOfString~~"-d1_in "~~__~~EndOfString,
StartOfString~~"-d1_out "~~__~~EndOfString,
StartOfString~~"-d1_simple "~~__~~EndOfString,
StartOfString~~"-hub1 "~~__~~EndOfString,
StartOfString~~"-aut1 "~~__~~EndOfString,
StartOfString~~"-hub_aut1 "~~__~~EndOfString,
StartOfString~~"-d2_in "~~__~~EndOfString,
StartOfString~~"-d2_out "~~__~~EndOfString,
StartOfString~~"-d2_simple "~~__~~EndOfString,
StartOfString~~"-hub_aut2 "~~__~~EndOfString,
StartOfString~~"-hub2 "~~__~~EndOfString,
StartOfString~~"-aut2 "~~__~~EndOfString,

StartOfString~~"-lm "~~__~~EndOfString,

StartOfString~~"-n "~~__~~EndOfString
};


(* ::Subsubsection::Closed:: *)
(*filterParameters (private)*)


(* ::Input::Initialization:: *)
filterParameters[parameters_List, patt_List]:=
Select[
parameters,
Function[{param,exclusionList},
Apply[
And,
Map[Not[StringMatchQ[param,#]]&,exclusionList]
]
][#,patt]&
]


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*filterParameters[*)
(*{"-local" , "-q 333", "-d1 3", "-n 432"},*)
(* {StartOfString~~"-n "~~__~~EndOfString}*)
(*]*)


(* ::Input:: *)
(*filterParameters[*)
(*{"-local" , "-q 333", "-d1 3", "-n 432"},*)
(* {StartOfString~~"-n "~~__~~EndOfString,"-local"}*)
(*]*)


(* ::Input:: *)
(*filterParameters[*)
(*{"-local" , "-q 333", "-d1 3", "-n 432"},*)
(* {*)
(*StartOfString~~"-n "~~__~~EndOfString,*)
(*StartOfString~~"-q "~~__~~EndOfString*)
(*}*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*formatParametersExtension (private)*)


(* ::Input::Initialization:: *)
formatParametersExtension[parameters_List]:=
With[
{
rules=MapIndexed[Rule[#1,ToString@First@#2]&,$ParametersPatterns]
},
With[
{
sortedParameters=Sort[
parameters,
ToExpression[StringReplace[#1,rules]]<ToExpression[StringReplace[#2,rules]]&
]
},
StringReplace[StringJoin[Riffle[StringDelete[StringTrim[sortedParameters],StartOfString~~"-"],"_"]]," "->"_"]
]
]


(* ::Text:: *)
(*test*)


(* ::Input:: *)
(*formatParametersExtension[{"-local" , "-t1", "-d2 3", "-d1 3", "-k2 4"}]*)


(* ::Input:: *)
(*formatParametersExtension[{"-k1 7" , "-lm -0.01"}]*)


(* ::Input:: *)
(*formatParametersExtension[{"-k1 7" , "-lm 0"}]*)


(* ::Subsubsection::Closed:: *)
(*formatMinerLCOutFileName*)


(* ::Input::Initialization:: *)
Options[formatMinerLCOutFileName]={
"PathForResults":>NotebookDirectory[]
};

formatMinerLCOutFileName[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
params=filterParameters[parameters,{StartOfString~~"-n "~~__~~EndOfString}]
},
FileNameJoin[
{
OptionValue["PathForResults"],
nriFileName<>"."<>formatParametersExtension[params]
}
]
];


(* ::Text:: *)
(*test*)


(* ::Input:: *)
(*formatMinerLCOutFileName["mougel.nri",{"-k1 7" , "-lm -0.01"}]*)


(* ::Input:: *)
(*formatMinerLCOutFileName["mougel.nri",{"-local" , "-t1", "-d2 3", "-d1 3", "-k2 4"}]*)


(* ::Input:: *)
(*formatMinerLCOutFileName["mougel.nri",{"-local" , "-t1", "-d2 3", "-d1 3", "-k2 4"},*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*minerLCCommand*)


(* ::Input::Initialization:: *)
Options[minerLCCommand]={
"PathForBinary":>FileNameJoin[{HomeDirectory[],"bin"}],
"PathForResults":>NotebookDirectory[],
"PathForNRI":>NotebookDirectory[],
"Background"->False,
"Host"-> "Local"
};

minerLCCommand[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[
{
params=filterParameters[parameters,{StartOfString~~"-n "~~__~~EndOfString}]
},
StringJoin[
Riffle[
{
FileNameJoin[{OptionValue["PathForBinary"],"minerLC"}],
FileNameJoin[{OptionValue["PathForNRI"],nriFileName}],
Sequence@@params,">",
formatMinerLCOutFileName[nriFileName,params,FilterRules[{opts},Options[formatMinerLCOutFileName]]],
" 2> ",
formatMinerLCOutFileName[nriFileName,params,FilterRules[{opts},Options[formatMinerLCOutFileName]]]<>".err"
},
" "]
]//
If[
OptionValue["Background"],
StringJoin[#," &"],
#
]&//
If[
OptionValue["Host"]=="Local",
#,
StringJoin[OptionValue["Host"]," '",#,"'"]
]&
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*minerLCCommand["plantevit_dblp.nri",{"-k1 7", "-lm -0.01"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*minerLCCommand["mougel.nri",{"-t1","-min_sup 1"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*minerLCCommand["mougel.nri",{"-t1","-min_sup 1","-n 200"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*runMinerLC*)


(* ::Input::Initialization:: *)
Options[runMinerLC]={
"Verbose"->True,
Sequence@@Options[minerLCCommand]
};

runMinerLC[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[
{
cmd=minerLCCommand[nriFileName,parameters,FilterRules[{opts},Options[minerLCCommand]]]
},If[OptionValue["Verbose"],Print[cmd]];
Run[cmd]
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*runMinerLC[ "mougel.nri",{"-t1","-min_sup 1"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runMinerLC[ "s50_an1_red.nri",{"-t1","-min_sup 1"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runMinerLC[ "brauer_elati_3_7.nri",{"-t1","-min_sup 1"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runMinerLC[ "mougel.nri",{"-t1","-min_sup 1","-local"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runMinerLC[ "s50_an1_red.nri",{"-t1","-min_sup 1","-local"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runMinerLC[ "brauer_elati_3_7.nri",{"-t1","-min_sup 1","-local"},*)
(*"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) formatGRAFileName (private) *)


(* ::Input::Initialization:: *)
(*formatGRAFileName::usage="formatGRAFileName[nriFileName_String] retourne une cha\[IHat]ne de caract\[EGrave]re unique repr\[EAcute]sentant le nom du fichier de sortie d'un fichier .gra extrait \[AGrave] partir du fichier .nri pass\[EAcute] en param\[EGrave]tre.

Options:
 \"PathForGRA\" d\[EAcute]finit le r\[EAcute]pertoire dans lequel fichier de sortie du programme sera plac\[EAcute].";

Options[formatGRAFileName]={
"PathForGRA"\[RuleDelayed]FileNameJoin[{Directory[],"gra"}]
};

formatGRAFileName[nriFileName_String,opts:OptionsPattern[]]:=
FileNameJoin[{OptionValue["PathForGRA"],StringDelete[nriFileName,".nri"]<>".gra"}];*)


(* ::Text:: *)
(*test*)


(* ::Input:: *)
(*(*formatGRAFileName["mougel.nri"]*)*)


(* ::Input:: *)
(*(*formatGRAFileName["mougel.nri",*)
(*"PathForGRA"\[Rule]FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) convertNRItoGRA *)


(* ::Input::Initialization:: *)
(*Options[convertNRItoGRA]=
{
"PathForNRI"\[RuleDelayed]FileNameJoin[{Directory[],"nri"}],
"PathForGRA"\[RuleDelayed]FileNameJoin[{Directory[],"gra"}],
"Verbose"\[Rule]False,
"AtIterations"\[Rule]100
};

convertNRItoGRA[nriFileName_String,opts:OptionsPattern[]]:=importNRI[FileNameJoin[{OptionValue["PathForNRI"],nriFileName}]]//exportGRA[#1,formatGRAFileName[nriFileName,FilterRules[{opts},Options[formatGRAFileName]]],FilterRules[{opts},Options[exportGRA]]]&*)


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*(*convertNRItoGRA["mougel.nri",*)
(*"PathForNRI"\[Rule] FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForGRA"\[Rule] FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*(deprecated) formatRESFileName (private)*)


(* ::Input::Initialization:: *)
(*formatRESFileName::usage="formatRESFileName[nriFileName_String, parameters_List] retourne une cha\[IHat]ne de caract\[EGrave]re unique repr\[EAcute]sentant le nom du fichier de sortie d'un fichier .res extrait \[AGrave] partir du fichier .nri pass\[EAcute] en param\[EGrave]tre. L'unicit\[EAcute] est assur\[EAcute]e par un ordre d\[EAcute]finit sur les param\[EGrave]tres possibles d\[EAcute]cris dans le variable globale $ParametersPatterns.

Options:
 \"PathForRES\" d\[EAcute]finit le r\[EAcute]pertoire dans lequel fichier de sortie du programme sera plac\[EAcute].";

Options[formatRESFileName]={
"PathForRES"\[RuleDelayed]FileNameJoin[{Directory[],"res"}]
};

formatRESFileName[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
params=filterParameters[parameters,{StartOfString~~"-n "~~__~~EndOfString}]
},
FileNameJoin[
{
OptionValue["PathForRES"],
nriFileName<>"."<>formatParametersExtension[params]<>".res"
}
]
];*)


(* ::Text:: *)
(*test*)


(* ::Input:: *)
(*(*formatRESFileName["mougel.nri",{"-t1","-min_sup 1"}]*)*)


(* ::Input:: *)
(*(*formatRESFileName["mougel.nri",{"-t1","-min_sup 1"},*)
(*"PathForRES"\[Rule]FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*formatSUPFileName*)


(* ::Input::Initialization:: *)
Options[formatSUPFileName]={
"PathForSUP":>NotebookDirectory[],
"NoMinsup"->False
};

formatSUPFileName[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
params=If[
OptionValue["NoMinsup"],
filterParameters[parameters,{StartOfString~~"-min_sup "~~__}],
parameters
]
},
FileNameJoin[
{
OptionValue["PathForSUP"],nriFileName<>"."<>formatParametersExtension[params ]<>".sup"
}
]
];


(* ::Text:: *)
(*test*)


(* ::Input:: *)
(*formatSUPFileName["mougel.nri",{"-t1","-min_sup 1", "-n 200"},*)
(*"NoMinsup"->True]*)


(* ::Input:: *)
(*formatSUPFileName["mougel.nri",{"-t1","-min_sup 1", "-n 200"}]*)


(* ::Input:: *)
(*formatSUPFileName["mougel.nri",{"-t1","-min_sup 1", "-n 200"},*)
(*"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*formatBISUPFileName*)


(* ::Input::Initialization:: *)
Options[formatBISUPFileName]={
"PathForSUP":>NotebookDirectory[],
"NoMinsup"->False
};

formatBISUPFileName[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
params=If[
OptionValue["NoMinsup"],
filterParameters[parameters,{StartOfString~~"-min_sup "~~__}],
parameters
]
},
FileNameJoin[
{
OptionValue["PathForSUP"],nriFileName<>"."<>formatParametersExtension[params ]<>".bisup"
}
]
];


(* ::Subsubsection::Closed:: *)
(*(deprecated) convertMinerLCOutToRES*)


(* ::Input::Initialization:: *)
(*Options[convertMinerLCOutToRES]=
{
"PathForResults"\[RuleDelayed]FileNameJoin[{Directory[],"results"}],
"PathForRES"\[RuleDelayed]FileNameJoin[{Directory[],"res"}],
"Verbose"\[Rule]False,
"AtIterations"\[Rule]100
};

convertMinerLCOutToRES[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
params=filterParameters[parameters,{StartOfString~~"-n "~~__}]
},
readMinerLC[nriFileName,params,FilterRules[{opts},Options[readMinerLC]]]//
exportRES[#,formatRESFileName[nriFileName,params,FilterRules[{opts},Options[formatRESFileName]]],FilterRules[{opts},Options[exportRES]]]&
]*)


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*(*convertMinerLCOutToRES["mougel.nri",{"-t1","-min_sup 1"},*)
(*"PathForResults"\[Rule] FileNameJoin[{NotebookDirectory[],"data"}],"PathForRES"\[Rule]FileNameJoin[{NotebookDirectory[],"data"}]]*)*)


(* ::Input:: *)
(*(*convertMinerLCOutToRES["mougel.nri",{"-t1","-min_sup 1","-local"},*)
(*"PathForResults"\[Rule] FileNameJoin[{NotebookDirectory[],"data"}],"PathForRES"\[Rule]FileNameJoin[{NotebookDirectory[],"data"}]]*)*)


(* ::Subsubsection::Closed:: *)
(*supportsAttendusCommand*)


(* ::Input::Initialization:: *)
Options[supportsAttendusCommand]={
"PathForBinary":>FileNameJoin[{HomeDirectory[],"bin"}],
"PathForNRI":>NotebookDirectory[],
"PathForResults":>NotebookDirectory[],
"PathForSUP":>NotebookDirectory[],
"Background"->False,
"Host"->"Local"
};

supportsAttendusCommand[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[
{
params1=filterParameters[parameters,{StartOfString~~"-n "~~__}],
params2=filterParameters[parameters,{StartOfString~~"-min_sup "~~__,"-local",StartOfString~~"-lm "~~__}]
},
StringJoin[
Riffle[
{
If[
Not[MemberQ[parameters,"-local"]],
FileNameJoin[{OptionValue["PathForBinary"],"supportsAttendus"}],
FileNameJoin[{OptionValue["PathForBinary"],"supportsAttendusLocaux"}]
],
FileNameJoin[{OptionValue["PathForNRI"],nriFileName}],
formatMinerLCOutFileName[nriFileName,params1,FilterRules[{opts},Options[formatMinerLCOutFileName]]],
Sequence@@params2,
">",
formatSUPFileName[nriFileName,parameters,FilterRules[{opts},Options[formatSUPFileName]]],
" 2> ",
formatSUPFileName[nriFileName,parameters,FilterRules[{opts},Options[formatSUPFileName]]]<>".err"
},
" "]
]//
If[
OptionValue["Background"],
StringJoin[#," &"],
#
]&//
If[
OptionValue["Host"]=="Local",
#,
StringJoin[OptionValue["Host"]," '",#,"'"]
]&
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*supportsAttendusCommand["mougel.nri",{"-min_sup 1" , "-t1","-n 200"}]*)


(* ::Input:: *)
(*supportsAttendusCommand["mougel.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}],"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*runSupportsAttendus*)


(* ::Input::Initialization:: *)
Options[runSupportsAttendus]={"Verbose"->True,Sequence@@Options[supportsAttendusCommand]};
runSupportsAttendus[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=With[{cmd=supportsAttendusCommand[nriFileName,parameters,FilterRules[{opts},Options[supportsAttendusCommand]]]},If[OptionValue["Verbose"],Print[cmd]];
Run[cmd]];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*runSupportsAttendus["mougel.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}],"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runSupportsAttendus["s50_an1_red.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}],"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Input:: *)
(*runSupportsAttendus["brauer_elati_3_7.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}],*)
(*"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}],"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*supportsAttendusBiCommand*)


(* ::Input::Initialization:: *)
Options[supportsAttendusBiCommand]={
"PathForBinary":>FileNameJoin[{HomeDirectory[],"bin"}],
"PathForNRI":>NotebookDirectory[],
"PathForResults":>NotebookDirectory[],
"PathForSUP":>NotebookDirectory[],
"Background"->False,
"Host"->"Local"
};

supportsAttendusBiCommand[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[
{
params1=filterParameters[parameters,{StartOfString~~"-n "~~__}],
params2=filterParameters[parameters,{StartOfString~~"-min_sup "~~__,"-local",StartOfString~~"-lm "~~__}]
},
StringJoin[
Riffle[
{
FileNameJoin[{OptionValue["PathForBinary"],"supportsAttendusBi"}],
FileNameJoin[{OptionValue["PathForNRI"],nriFileName}],
formatMinerLCOutFileName[nriFileName,params1,FilterRules[{opts},Options[formatMinerLCOutFileName]]],
Sequence@@params2,
">",
formatBISUPFileName[nriFileName,parameters,FilterRules[{opts},Options[formatBISUPFileName]]],
" 2> ",
formatBISUPFileName[nriFileName,parameters,FilterRules[{opts},Options[formatBISUPFileName]]]<>".err"
},
" "]
]//
If[
OptionValue["Background"],
StringJoin[#," &"],
#
]&//
If[
OptionValue["Host"]=="Local",
#,
StringJoin[OptionValue["Host"]," '",#,"'"]
]&
];


(* ::Subsubsection::Closed:: *)
(*runSupportsAttendusBi*)


(* ::Input::Initialization:: *)
Options[runSupportsAttendusBi]={"Verbose"->True,Sequence@@Options[supportsAttendusBiCommand]};
runSupportsAttendusBi[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=With[{cmd=supportsAttendusBiCommand[nriFileName,parameters,FilterRules[{opts},Options[supportsAttendusBiCommand]]]},If[OptionValue["Verbose"],Print[cmd]];
Run[cmd]];


(* ::Subsubsection::Closed:: *)
(*readNRI*)


(* ::Input::Initialization:: *)
Options[readNRI]={
"PathForNRI":>NotebookDirectory[],
Sequence@@Options[importNRI]
};

readNRI[nriFileName_String,opts:OptionsPattern[]]:=
With[
{filename=FileNameJoin[{OptionValue["PathForNRI"],nriFileName}]},
If[
FileExistsQ[filename],
importNRI[filename,Evaluate[FilterRules[{opts},Options[importNRI]]]],
Missing["File not found: "<>filename]
]
]


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Input:: *)
(*readNRI["toto.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Subsubsection::Closed:: *)
(*readMinerLC*)


(* ::Input::Initialization:: *)
Options[readMinerLC]={
"PathForResults":>NotebookDirectory[],
"OmitExtensions"->False,
"RemoveNoRole"->False
};

readMinerLC[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[
{filename=formatMinerLCOutFileName[nriFileName,parameters,FilterRules[{opts},Options[formatMinerLCOutFileName]]]},
If[FileExistsQ[filename],
importMinerLCOut[filename,"OmitExtensions"->OptionValue["OmitExtensions"],"RemoveNoRole"->OptionValue["RemoveNoRole"]],
Missing["File not found: "<>filename]
]
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*readMinerLC["toto.nri",{"-t1","-min_sup 1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Input:: *)
(*readMinerLC["mougel.nri",{"-t1","-min_sup 1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Input:: *)
(*readMinerLC["mougel.nri",{"-t1","-min_sup 1","-local"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Subsubsection::Closed:: *)
(*readSupportsAttendus*)


(* ::Input::Initialization:: *)
Options[readSupportsAttendus]={"PathForSUP":>NotebookDirectory[]};

readSupportsAttendus[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
filenameNoMinsup=formatSUPFileName[nriFileName,parameters,"PathForSUP"->OptionValue["PathForSUP"],"NoMinsup"->True],
filenameWithMinsup=formatSUPFileName[nriFileName,parameters,"PathForSUP"->OptionValue["PathForSUP"],"NoMinsup"->False]
},
Which[
FileExistsQ[filenameWithMinsup],
importSupportAttendu[filenameWithMinsup],
FileExistsQ[filenameNoMinsup],
importSupportAttendu[filenameNoMinsup],
True,
Missing["File not found"]
]
];


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*readSupportsAttendus["toto.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Input:: *)
(*readSupportsAttendus["s50_an1_red.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}]]*)


(* ::Subsubsection::Closed:: *)
(*readSupportsAttendusBi*)


(* ::Input::Initialization:: *)
Options[readSupportsAttendusBi]={"PathForSUP":>NotebookDirectory[]};

readSupportsAttendusBi[nriFileName_String,parameters_List,opts:OptionsPattern[]]:=
With[{
filename=formatBISUPFileName[nriFileName,parameters,"PathForSUP"->OptionValue["PathForSUP"],"NoMinsup"->False]
},
If[
FileExistsQ[filename],
importSupportAttenduBi[filename],
Missing["File not found"]
]
];


(* ::Subsubsection::Closed:: *)
(*setPaths*)


(* ::Input::Initialization:: *)
setPaths[opts___]:=
Map[
Switch[
First[#],
"PathForResults",
(
SetOptions[minerLCCommand,#];
SetOptions[runMinerLC,#];
SetOptions[supportsAttendusCommand,#];
SetOptions[runSupportsAttendus,#];
SetOptions[supportsAttendusBiCommand,#];
SetOptions[runSupportsAttendusBi,#];
SetOptions[readMinerLC,#];
SetOptions[formatMinerLCOutFileName,#];
),
"PathForNRI",
(
SetOptions[minerLCCommand,#];
SetOptions[runMinerLC,#];
SetOptions[supportsAttendusCommand,#];
SetOptions[runSupportsAttendus,#];
SetOptions[supportsAttendusBiCommand,#];
SetOptions[runSupportsAttendusBi,#];
SetOptions[readNRI,#];
),
"PathForSUP",
(
SetOptions[supportsAttendusCommand,#];
SetOptions[runSupportsAttendus,#];
SetOptions[formatSUPFileName,#];
SetOptions[readSupportsAttendus,#];
SetOptions[supportsAttendusBiCommand,#];
SetOptions[runSupportsAttendusBi,#];
SetOptions[formatBISUPFileName,#];
SetOptions[readSupportsAttendusBi,#];
),
"PathForBinary",
(
SetOptions[minerLCCommand,#];
SetOptions[runMinerLC,#];
SetOptions[supportsAttendusCommand,#];
SetOptions[runSupportsAttendus,#];
SetOptions[supportsAttendusBiCommand,#];
SetOptions[runSupportsAttendusBi,#];
),
"Host",
(
SetOptions[minerLCCommand,#];
SetOptions[runMinerLC,#];
SetOptions[supportsAttendusCommand,#];
SetOptions[runSupportsAttendus,#];
SetOptions[supportsAttendusBiCommand,#];
SetOptions[runSupportsAttendusBi,#];
)
]&,
{opts}
];


(* ::Subsection::Closed:: *)
(*Cores*)


(* ::Subsubsection::Closed:: *)
(*subweightedgraph*)


(* ::Input::Initialization:: *)
subweightedgraph[g_,X_]:=
With[{gg=Subgraph[g,X]},
Graph[
gg,
EdgeWeight->Map[Rule[#,PropertyValue[{g,#},EdgeWeight]]&,EdgeList[gg]],
VertexWeight->Map[Rule[#,PropertyValue[{g,#},VertexWeight]]&,VertexList[gg]]
]]


(* ::Subsubsection::Closed:: *)
(*kCorePropertyQ*)


(* ::Input::Initialization:: *)
kCorePropertyQ[k_][{g_,X_},v_]/;UndirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
VertexDegree[g,v]>=k
];


(* ::Subsubsection::Closed:: *)
(*kStarPropertyQ*)


(* ::Input::Initialization:: *)
kStarPropertyQ[k_][{g_,X_},v_]/;UndirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
Or[
VertexDegree[gg,v]>=k,
Apply[Or,Map[VertexDegree[gg,#]>=k&,AdjacencyList[gg,v]]]
]
];


(* ::Subsubsection::Closed:: *)
(*kCoreOutPropertyQ*)


(* ::Input::Initialization:: *)
kCoreOutPropertyQ[k_][{g_,X_},v_]/;DirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
VertexOutDegree[gg,v]>=k
];


(* ::Subsubsection::Closed:: *)
(*kCoreInPropertyQ*)


(* ::Input::Initialization:: *)
kCoreInPropertyQ[k_][{g_,X_},v_]/;DirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
VertexInDegree[gg,v]>=k
];


(* ::Subsubsection::Closed:: *)
(*kStarInPropertyQ*)


(* ::Input::Initialization:: *)
kStarInPropertyQ[k_][{g_,X_},v_]/;DirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
Or[
VertexInDegree[gg,v]>=k,
Apply[Or,Map[VertexInDegree[gg,#]>=k&,AdjacencyList[gg,v]]]
]
];


(* ::Subsubsection::Closed:: *)
(*kStarOutPropertyQ*)


(* ::Input::Initialization:: *)
kStarOutPropertyQ[k_][{g_,X_},v_]/;DirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
Or[
VertexOutDegree[gg,v]>=k,
Apply[Or,Map[VertexOutDegree[gg,#]>=k&,AdjacencyList[g,v]]]
]
];



(* ::Subsubsection::Closed:: *)
(*kDensePropertyQ*)


(* ::Input::Initialization:: *)
kDensePropertyQ[k_][{g_,X_},v1_\[UndirectedEdge]v2_]/;UndirectedGraphQ[g]:=With[{gg=Subgraph[g,X]},
Length[Intersection[AdjacencyList[gg,v1],AdjacencyList[gg,v2]]]>=(k-2)
];


(* ::Subsubsection::Closed:: *)
(*trianglePropertyQ*)


(* ::Input::Initialization:: *)
trianglePropertyQ[{g_,X_},v1_\[UndirectedEdge]v2_]/;UndirectedGraphQ[g]:=
kDensePropertyQ[3][{g,X},v1\[UndirectedEdge]v2];


(* ::Subsubsection::Closed:: *)
(*hubOfHubAutPropertyQ*)


(* ::Input::Initialization:: *)
hubOfHubAutPropertyQ[k_][{g_,hubList_,autList_},h_]/;DirectedGraphQ[g]:=kCoreOutPropertyQ[k][{g,Join[{h},autList]},h];


(* ::Subsubsection::Closed:: *)
(*autOfHubAutPropertyQ*)


(* ::Input::Initialization:: *)
autOfHubAutPropertyQ[k_][{g_,hubList_,autList_},a_]/;DirectedGraphQ[g]:=kCoreInPropertyQ[k][{g,Join[{a},hubList]},a];


(* ::Subsubsection::Closed:: *)
(*deleteIsolatedVertices (Private)*)


(* ::Input::Initialization:: *)
deleteIsolatedVertices[g_,edges_]:=VertexDelete[g, Select[VertexList[Graph[edges]],(VertexDegree[g,#]==0&)]];
deleteIsolatedVertices[g_]:=VertexDelete[g, Select[VertexList[g],(VertexDegree[g,#]==0&)]];


(* ::Subsubsection::Closed:: *)
(*vertexCore*)


(* ::Input::Initialization:: *)
vertexCore[{property_,X_},g_]:=
With[{gg=If[WeightedGraphQ[g],subweightedgraph[g,X],Subgraph[g,X]]},
FixedPoint[
With[{curE=#},Select[curE,property[{gg,curE},#]&]]&,
X
]
];


(* ::Input:: *)
(*grapheU=Graph[readNRI["s50_an1_red.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]]["Graphe"],VertexLabels->"Name",ImageSize->Medium];*)
(*premuteEdge[x_\[DirectedEdge]y_]:=y\[DirectedEdge]x;*)
(*SeedRandom[1234];*)
(*permuteRandomEdge[edges:{_UndirectedEdge ...}]:=With[{e=Map[DirectedEdge@@#&,edges]},RandomSample[e,Floor[Length[e]/2]]//Join[Map[premuteEdge,#],RandomSample[e,Floor[Length[e]/4]],Complement[e,#]]&];*)
(*grapheD=Graph[VertexList[grapheU],permuteRandomEdge[EdgeList[grapheU]],VertexLabels->"Name",ImageSize->Medium];*)


(* ::Input:: *)
(*vertexCore[{kCorePropertyQ[3],VertexList[ grapheU]},grapheU]//*)
(*Subgraph[grapheU,#]&*)


(* ::Input:: *)
(*vertexCore[{kStarPropertyQ[7],VertexList[ grapheU]}, grapheU]//*)
(*Subgraph[grapheU,#]&*)


(* ::Input:: *)
(*vertexCore[{kCoreInPropertyQ[2],VertexList[ grapheU]}, grapheD]//*)
(*Subgraph[grapheD,#]&*)


(* ::Input:: *)
(*vertexCore[{kCoreOutPropertyQ[2],VertexList[ grapheU]}, grapheD]//*)
(*Subgraph[grapheD,#]&*)


(* ::Subsubsection::Closed:: *)
(*edgeCore*)


(* ::Input::Initialization:: *)
edgeCore[{property_,X_},g_]:=
With[{gg=If[WeightedGraphQ[g],subweightedgraph[g,X],Subgraph[g,X]]},
FixedPoint[
With[{curE=#},Sort[Select[curE,property[{Graph[curE],curE},#]&]]]&,
EdgeList[gg]
]
];


(* ::Input:: *)
(*grapheU=Graph[readNRI["s50_an1_red.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]]["Graphe"],VertexLabels->"Name",ImageSize->Medium];*)
(*premuteEdge[x_\[DirectedEdge]y_]:=y\[DirectedEdge]x;*)
(*SeedRandom[1234];*)
(*permuteRandomEdge[edges:{_UndirectedEdge ...}]:=With[{e=Map[DirectedEdge@@#&,edges]},RandomSample[e,Floor[Length[e]/2]]//Join[Map[premuteEdge,#],RandomSample[e,Floor[Length[e]/4]],Complement[e,#]]&];*)
(*grapheD=Graph[VertexList[grapheU],permuteRandomEdge[EdgeList[grapheU]],VertexLabels->"Name",ImageSize->Medium];*)


(* ::Input:: *)
(*edgeCore[{kDensePropertyQ[4],EdgeList[grapheU]}, grapheU]//*)
(*Subgraph[grapheU,#]&*)


(* ::Input:: *)
(*edgeCore[{trianglePropertyQ,EdgeList[grapheU]},grapheU]//*)
(*Subgraph[grapheU,#]&*)


(* ::Subsubsection:: *)
(*biSetEdges*)


(* ::Input::Initialization:: *)
biSetEdges[g_,{X1_,X2_}]/;DirectedGraphQ[g]:=
Cases[EdgeList[g],(v1_\[DirectedEdge]v2_)/;And[ MemberQ[X1,v1],MemberQ[X2,v2]]];

biSetEdges[g_,{X1_,X2_}]/;UndirectedGraphQ[g]:=
Cases[EdgeList[g],(v1_\[UndirectedEdge]v2_)/;Or[And[ MemberQ[X1,v1],MemberQ[X2,v2]],And[ MemberQ[X1,v2],MemberQ[X2,v1]]]];


(* ::Input:: *)
(*biSetEdges[*)
(*Graph[{1\[DirectedEdge]3,1\[DirectedEdge]4,2\[DirectedEdge]3,2\[DirectedEdge]4,1\[DirectedEdge]2,3\[DirectedEdge]4}],*)
(*{{1,2},{3,4}}*)
(*]*)


(* ::Input:: *)
(*biSetEdges[*)
(*Graph[{1\[UndirectedEdge]3,1\[UndirectedEdge]4,3\[UndirectedEdge]2,2\[UndirectedEdge]4,1\[UndirectedEdge]2,3\[UndirectedEdge]4}],*)
(*{{1,2},{3,4}}*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*vertexBiCore*)


(* ::Input::Initialization:: *)
vertexBiCore[{property1_,property2_},{X1_,X2_},g_]/;DirectedGraphQ[g]:=
FixedPoint[
With[
{
curE1=#[[1]],
curE2=#[[2]]
},
{
Select[curE1,property1[{g,curE1,curE2},#]&],
Select[curE2,property2[{g,curE1,curE2},#]&]
}
]&,
{X1, X2}
]


(* ::Input:: *)
(*vertexBiCore[{hubOfHubAutPropertyQ[3(*hub*)],autOfHubAutPropertyQ[3(*hub*)]},{VertexList[grapheD],VertexList[grapheD]},grapheD]//*)
(*biSetEdges[grapheD,#]&//*)
(*Subgraph[grapheD,#]&*)


(* ::Subsection:: *)
(*Calcul sur les donn\[EAcute]es*)


(* ::Subsubsection::Closed:: *)
(*densities*)


(* ::Input:: *)
(*ClearAll@densities;*)


(* ::Input::Initialization:: *)
densities[data_Association, extensionKey_String]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,extensionKey]
]:=
Map[
With[{ext=#},
With[{gSub=Subgraph[data["Graphe"],ext]},
With[{vCount=VertexCount@gSub,eCount=EdgeCount@gSub},
2*eCount/(vCount*(vCount))
]
]
]&,
data[extensionKey]
]


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*With[{data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]},*)
(*Association[*)
(*"Densit\[EAcute]Abs"->densities[data,"ExtAbs"],*)
(*"Densit\[EAcute]Glob"->densities[data,"ExtGlob"]*)
(*]*)
(*]*)


(* ::Input:: *)
(*specificities[*)
(*Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1","-local"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*specificities*)


(* ::Input:: *)
(*ClearAll@specificities;*)


(* ::Input::Initialization:: *)
specificities[data_Association,keyListeClos_String,keySupportSpecifique_String, keySupportGeneral_String]:=
Association[Map[Rule[#,(data[keySupportSpecifique][#])/(data[keySupportGeneral][#])]&,data[keyListeClos]]]


specificities[data_Association]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0",
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"SupAbs"],
KeyExistsQ[data,"SupGlob"]
]:=specificities[data,"ClosAbs","SupAbs", "SupGlob"];


specificities[data_Association]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="1",
KeyExistsQ[data,"(ClosAbs_ExtLoc)"],
KeyExistsQ[data,"SupLoc_ClosLoc"],
KeyExistsQ[data,"SupAbs_ClosLoc"]
]:=specificities[data,"(ClosAbs_ExtLoc)","SupLoc_ClosLoc", "SupAbs_ClosLoc"];



(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*specificities[*)
(*Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]*)
(*]*)


(* ::Input:: *)
(*specificities[*)
(*Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1","-local"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*informativities*)


(* ::Input::Initialization:: *)
informativities[data_Association]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0"
]:=Null;

Options[informativities]={"ListeDesClos"->"(ClosAbs_ExtLoc)" };

informativities[data_Association,OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="1",
KeyExistsQ[data,"SupAbs_ClosLoc"],
KeyExistsQ[data,"SupAbs_ClosAbs"],
KeyExistsQ[data,"(ClosAbs_ExtLoc)"]
]:=Association[
Map[Rule[#,1-(data["SupAbs_ClosLoc"][#]/data["SupAbs_ClosAbs"][#])]&,data[OptionValue["ListeDesClos"]]]
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*informativities[*)
(*Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1","-local"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*homogeneities and inhomogeneities*)


(* ::Input::Initialization:: *)
patternPart[patt:{_String..},pref_String]:=
Select[patt,StringMatchQ[#,StartOfString~~pref~~___]&];

homogeneities[data_Association,{vPref1_String,vPref2_String},OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="1"
]:=Null;

Options[homogeneities]=Options[inhomogeneities]={"ListeDesClos"->"ClosAbs" };

homogeneities[data_Association,{vPref1_String,vPref2_String},OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0",
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"],
KeyExistsQ[data,"Objets"]
]:=
Association[
Map[
With[{
clos=#,
extPref1=Map[StringDrop[#,1]&,patternPart[data["ExtGlob"][#],vPref1]],
extPref2=Map[StringDrop[#,1]&,patternPart[data["ExtGlob"][#],vPref2]]
},
Rule[clos,Length[Intersection[extPref1,extPref2]]/Length[Union[extPref1,extPref2]]]
]&,
data[OptionValue["ListeDesClos"]]
]
];

inhomogeneities[data_Association,{vPref1_String,vPref2_String},OptionsPattern[]]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0",
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"],
KeyExistsQ[data,"Objets"]
]:=
Association[
Map[
With[{
clos=#,
extPref1=Map[StringDrop[#,1]&,patternPart[data["ExtGlob"][#],vPref1]],
extPref2=Map[StringDrop[#,1]&,patternPart[data["ExtGlob"][#],vPref2]]
},
Rule[clos,1-(Length[Intersection[extPref1,extPref2]]/Length[Union[extPref1,extPref2]])]
]&,
data[OptionValue["ListeDesClos"]]
]
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*<<MinerLC`*)


(* ::Input:: *)
(*data=Join[*)
(*readNRI["bipat_brauer_elati_3_7.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}] ],*)
(*readMinerLC["bipat_brauer_elati_3_7.nri",{"-min_sup 3" , "-sd1 5", "-sk1 1","-n 200"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*];*)


(* ::Input:: *)
(*h=homogeneities[data,{"i","o"}]*)


(* ::Input:: *)
(*sorted=Sort[data["ClosAbs"],h[#1]<h[#2]&];*)


(* ::Input:: *)
(*Map[N@h@#&,sorted]*)


(* ::Subsubsection::Closed:: *)
(*normalizedSpecificities*)


(* ::Input::Initialization:: *)
normalizedSpecificities[data_Association,keyListeClos_String,keySupportObserve_String]:=
Association[Map[Rule[#,(data[keySupportObserve][#])/(data["MeanExpectedSupAbs"][#])]&,data[keyListeClos]]]


normalizedSpecificities[data_Association]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0",
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"SupAbs"],
KeyExistsQ[data,"MeanExpectedSupAbs"]
]:=normalizedSpecificities[data,"ClosAbs","SupAbs"];



(* ::Input:: *)
(*data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readSupportsAttendus["mougel.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*];*)
(*normalizedSpecificities@data*)


(* ::Input:: *)
(*data=Join[*)
(*readNRI["brauer_elati_3_7.nri","PathForNRI"->"/Users/santini/LIPN/Work/AttributedGraphs/nri/"],*)
(*readMinerLC["brauer_elati_3_7.nri",{"-min_sup 3" , "-t1","-n 200"},"PathForResults"->"/Users/santini/LIPN/Work/AttributedGraphs/results/"],*)
(*readSupportsAttendus["brauer_elati_3_7.nri",{"-min_sup 3" , "-t1","-n 200"},"PathForSUP"->"/Users/santini/LIPN/Work/AttributedGraphs/sup/"]*)
(*];*)
(*normalizedSpecificities@data*)


(* ::Subsubsection::Closed:: *)
(*deviationToNormalizedSpecificities (deprecated)*)


(* ::Input:: *)
(*ClearAll@deviationToNormalizedSpecificities*)


(* ::Input::Initialization:: *)
deviationToNormalizedSpecificities[data_Association,keyListeClos_String,keySupportObserve_String]:=
Association[Map[Rule[#,(data[keySupportObserve][#]-data["MeanExpectedSupAbs"][#])/(data["SigmaExpectedSupAbs"][#])]&,data[keyListeClos]]]


deviationToNormalizedSpecificities[data_Association]/;And[
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0",
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"SupAbs"],
KeyExistsQ[data,"MeanExpectedSupAbs"],
KeyExistsQ[data,"SigmaExpectedSupAbs"]
]:=deviationToNormalizedSpecificities[data,"ClosAbs","SupAbs"];



(* ::Input:: *)
(*data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readSupportsAttendus["mougel.nri",{"-min_sup 1" , "-t1","-n 200"},"PathForSUP"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*];*)
(*deviationToNormalizedSpecificities@data*)


(* ::Input:: *)
(*data=Join[*)
(*readNRI["brauer_elati_3_7.nri","PathForNRI"->"/Users/santini/LIPN/Work/AttributedGraphs/nri/"],*)
(*readMinerLC["brauer_elati_3_7.nri",{"-min_sup 3" , "-t1","-n 200"},"PathForResults"->"/Users/santini/LIPN/Work/AttributedGraphs/results/"],*)
(*readSupportsAttendus["brauer_elati_3_7.nri",{"-min_sup 3" , "-t1","-n 200"},"PathForSUP"->"/Users/santini/LIPN/Work/AttributedGraphs/sup/"]*)
(*];*)
(*deviationToNormalizedSpecificities@data*)


(* ::Subsubsection::Closed:: *)
(*deviationToExpectedAbstractSupport*)


(* ::Input::Initialization:: *)
deviationToExpectedAbstractSupport[data_Association]/;And[
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"SupAbs"],
KeyExistsQ[data,"MeanExpectedSupAbs"],
KeyExistsQ[data,"SigmaExpectedSupAbs"]
]:=
Association[
Map[
Function[
{clos},
With[
{
ecart=
If[ 
data["SigmaExpectedSupAbs"][clos]!=0, 
( data["SupAbs"][clos] - data["MeanExpectedSupAbs"][clos] ) /data["SigmaExpectedSupAbs"][clos]  ,
If [
( data["SupAbs"][clos] - data["MeanExpectedSupAbs"][clos] )==0,
0,
ComplexInfinity 
]
]
},
clos-> ecart
]
],
data["ClosAbs"]
]
];

deviationToExpectedAbstractSupport[data_Association]/;And[
KeyExistsQ[data,"(ClosAbs_ExtLoc)"],
KeyExistsQ[data,"SupGlob_ClosLoc"],
KeyExistsQ[data,"SupLoc_ClosLoc"],
KeyExistsQ[data,"MeanExpectedSupLoc"],
KeyExistsQ[data,"SigmaExpectedSupLoc"],
IntegerQ[First[Key[data["MeanExpectedSupLoc"]]]]
]:=
Association[
Map[
Function[
{closAbsExtLoc},
With[
{
supGlobClosLoc=data["SupGlob_ClosLoc"][closAbsExtLoc],
supLocClosLoc=data["SupLoc_ClosLoc"][closAbsExtLoc]
},
With[
{
ecart=
If[ 
data["SigmaExpectedSupLoc"][supGlobClosLoc]!=0, 
( supLocClosLoc - data["MeanExpectedSupLoc"][supGlobClosLoc] ) /data["SigmaExpectedSupLoc"][supGlobClosLoc]  ,
If [
( supLocClosLoc - data["MeanExpectedSupLoc"][supGlobClosLoc] )==0,
0,
ComplexInfinity 
]
]
},
closAbsExtLoc-> ecart
]
]
],
data["(ClosAbs_ExtLoc)"]
]
];

deviationToExpectedAbstractSupport[data_Association]/;And[
KeyExistsQ[data,"(ClosAbs_ExtLoc)"],
KeyExistsQ[data,"SupGlob_ClosLoc"],
KeyExistsQ[data,"SupLoc_ClosLoc"],
KeyExistsQ[data,"MeanExpectedSupLoc"],
KeyExistsQ[data,"SigmaExpectedSupLoc"],
Not[IntegerQ[First[Key[data["MeanExpectedSupLoc"]]]]]
]:=
Association[
Map[
Function[
{closAbsExtLoc},
With[
{
ecart=
If[ 
data["SigmaExpectedSupLoc"][closAbsExtLoc]!=0, 
( data["SupLoc_ClosLoc"][closAbsExtLoc] - data["MeanExpectedSupLoc"][closAbsExtLoc] ) /data["SigmaExpectedSupLoc"][closAbsExtLoc]  ,
If [
( data["SupLoc_ClosLoc"][closAbsExtLoc] - data["MeanExpectedSupLoc"][closAbsExtLoc] )==0,
0,
ComplexInfinity 
]
]
},
closAbsExtLoc-> ecart
]
],
data["(ClosAbs_ExtLoc)"]
]
];


(* ::Input:: *)
(*data//Keys*)


(* ::Input:: *)
(*data["SupGlob_ClosLoc"]//#[[50]]&*)


(* ::Subsubsection::Closed:: *)
(*closAbsOrderingFunction (private)*)


(* ::Input::Initialization:: *)
closAbsOrderingFunction::usage="closAbsOrderingFunction[data_,clos1_,clos2_] d\[EAcute]fini un ordre entre 2 clos, les clos les plus infr\[EAcute]quents sont plac\[EAcute]s avant.
Un clos c1 est plus grand qu'un clos c2 si la deviation au support abstrait attendu de c1 est plus \[EAcute]lev\[EAcute]e que celle de c2.
Si l'un des deux clos a une deviation au support abstrait attendu \[EAcute]gale a ComplexInfinity, c'est qu'il na pas \[EAcute]t\[EAcute] trouv\[EAcute] lors des tirages al\[EAcute]atoires (\[Sigma]=0 car le clos n'a pas \[EAcute]t\[EAcute] trouv\[EAcute]), il est donc consid\[EAcute]r\[EAcute] comme moins fr\[EAcute]quent.
Si les deux clos ont une deviation au support abstrait attendu \[EAcute]gale a ComplexInfinity, ont place devant ceux qui ont un support global le plus petit (les plus susceptibles d'\[EHat]tre infr\[EAcute]quents.";


(* ::Input::Initialization:: *)

closAbsOrderingFunction[data_Association,clos1_,clos2_, primaryKey_, secondaryKey_, primaryKeyFun_, secondaryKeyFun_]:=
Which[
data[primaryKey][clos1]===data[primaryKey][clos2],
secondaryKeyFun[data[secondaryKey][clos1], data[secondaryKey][clos2]],
data[primaryKey][clos1]===ComplexInfinity,
True,
data[primaryKey][clos2]===ComplexInfinity,
False,
primaryKeyFun[data[primaryKey][clos1],data[primaryKey][clos2]],
True,
True,
False
]


(* ::Subsubsection::Closed:: *)
(*orderClosAbs*)


(* ::Input::Initialization:: *)
Options[orderClosAbs]={"PrimaryKey"->"DeviationToExpectedAbstractSupport", "SecondaryKey"-> "SupGlob","PrimaryOrder"-> GreaterEqual,"SecondaryOrder"->LessEqual};

orderClosAbs[data_Association, opts:OptionsPattern[]]:=
If[
Not[And[KeyExistsQ[data,"ClosAbs"],KeyExistsQ[data,OptionValue["PrimaryKey"]],KeyExistsQ[data,OptionValue["SecondaryKey"]]]],
$Aborted,
Sort[
(* \[EAcute]l\[EAcute]ments a ordonner *)
data["ClosAbs"],

(* fonction bool\[EAcute]enne d'ordre sur les clos: #1 et #2 sont les clos *)
closAbsOrderingFunction[data,#1,#2,OptionValue["PrimaryKey"],OptionValue["SecondaryKey"],OptionValue["PrimaryOrder"],OptionValue["SecondaryOrder"]]&
]
]


(* ::Subsubsection::Closed:: *)
(*localModularityEstimate*)


(* ::Input::Initialization:: *)
localModularityEstimate[gAll_Graph, ext_]:=
With[{gSub=Subgraph[gAll,ext]},
With[{mc=EdgeCount[gSub],m=EdgeCount[gAll]},
mc/m-mc^2/m^2
]];


(* ::Subsubsection::Closed:: *)
(*localModularity*)


(* ::Input::Initialization:: *)
localModularity[gAll_Graph, ext_]:=
With[{gSub=Subgraph[gAll,ext]},
With[{mc=EdgeCount[gSub],m=EdgeCount[gAll]},
With[{ mo=Apply[Plus,Map[VertexDegree[gAll,#]&,ext]]-2*mc},
mc/m-((2*mc +mo)/(2*m))^2
]]];


(* ::Subsubsection::Closed:: *)
(*similarityG (private)*)


(* ::Input::Initialization:: *)
similarityG[data_][clos1_,clos2_]:=Module[{e1,e2,roles},
e1=data["ExtAbs"][clos1];
e2=data["ExtAbs"][clos2];
roles=Keys@data["ExtAbs"][clos1];
Min[
(*Length[e1["hubs"]~Intersection~e2["hubs"]]/Length[e1["hubs"]~Union~e2["hubs"]],
Length[e1["autorities"]~Intersection~e2["autorities"]]/Length[e1["autorities"]~Union~e2["autorities"]]*)
Map[
Length[e1[#]~Intersection~e2[#]]/Length[e1[#]~Union~e2[#]]&,
roles
]
]
];


(* ::Subsubsection::Closed:: *)
(*distanceG (private)*)


(* ::Input::Initialization:: *)
distanceG[data_][clos1_,clos2_]:=1-similarityG[data][clos1,clos2]


(* ::Subsubsection::Closed:: *)
(*compatibleAG (private)*)


(* ::Input::Initialization:: *)
compatibleAG[data_][clefATester_,listClefDejeSelectionnees_,\[Beta]_]:=
(* True si il n'y a aucun element de lCleSel de distance avec cle \[LessEqual] \[Beta] *)
MatchQ[
SelectFirst[
listClefDejeSelectionnees,
(distanceG[data][#,clefATester]<=\[Beta] )&
],
Missing[_]
];


(* ::Subsubsection::Closed:: *)
(*selectionGH (private)*)


(* ::Input::Initialization:: *)
selectionGH[data_,\[Beta]_][{lReste_,lSel_}]:=
With[
{c=SelectFirst[lReste,(compatibleAG[data][#,lSel,\[Beta]])&  ]},
If[
MatchQ[c,Missing[_]],
{lReste,lSel},
With[{posNext=First@FirstPosition[lReste,c]},
{Drop[lReste, posNext],Append[lSel,c]}
]
]
];(*
selectionGHPosition[data_,\[Beta]_][{lReste_,lSel_}]:=
With[
{posNext=FirstPosition[lReste,_?(compatibleAG[data][#,lSel,\[Beta]]&)  ]},
Print[posNext];
If[
MatchQ[posNext,Missing[_]],
{lReste,lSel},
Print[posNext];
{Drop[lReste, First[posNext]],Append[lSel,lReste[[First[posNext]]]]}
]
];*)


(* ::Subsubsection::Initialization::Closed:: *)
(*distanceBasedPatternSelect*)


(* ::Input::Initialization:: *)
distanceBasedPatternSelect[data_][listeOrdonnee_,\[Beta]_]:=Last@FixedPoint[selectionGH[data,\[Beta]], {listeOrdonnee,{}}];


(* ::Subsubsection::Initialization::Closed:: *)
(*fixSupportsAttendus*)


(* ::Input::Initialization:: *)
supAbsWithRoles[mlcData_, pattern_] :=Length[Apply[Union,Map[mlcData["ExtAbs"][pattern][#]&,mlcData["Roles"]]]];

fixSupportsAttendus[supData_,mlcData_]:=
Module[{supDataFixed=supData},
supDataFixed["SupAbs"]=Association[Map[Rule[#,supAbsWithRoles[mlcData, #]]&,Keys[supData["SupAbs"]]]];
supDataFixed
]


(* ::Input:: *)
(*runMinerLC["lazega_advice2_alt.nri",{"-o","-hub_aut1 6 6"}, "PathForResults"->"/Users/santini/Library/Mathematica/Applications/LibrairiesMathematica/data/",*)
(*"PathForNRI"->"/Users/santini/Library/Mathematica/Applications/LibrairiesMathematica/data/"]*)


(* ::Input:: *)
(*runSupportsAttendus["lazega_advice2_alt.nri",{"-o","-hub_aut1 6 6","-n 100"}, "PathForResults"->"/Users/santini/Library/Mathematica/Applications/LibrairiesMathematica/data/",*)
(*"PathForNRI"->"/Users/santini/Library/Mathematica/Applications/LibrairiesMathematica/data/",*)
(*"PathForSUP"->"/Users/santini/Library/Mathematica/Applications/LibrairiesMathematica/data/"*)
(*]*)


(* ::Input:: *)
(*fixSupportsAttendus[*)
(*readSupportsAttendus["lazega_advice2_alt.nri",{"-o","-hub_aut1 6 6","-n 100"},"PathForSUP"-> "/Users/santini/LIPN/Work/AttributedGraphs/sup"],*)
(*readMinerLC["lazega_advice2_alt.nri",{"-o","-hub_aut1 6 6","-n 100"},"PathForResults"-> "/Users/santini/LIPN/Work/AttributedGraphs/results"]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*jaccardSimilarity (private)*)


(* ::Input::Initialization:: *)
jaccardSimilarity[e1_List,e2_List]:=Length[e1~Intersection~e2]/Length[e1~Union~e2];
jaccardSimilarity[e1_Association,e2_Association]/;Keys[e1]==Keys[e2]:=
Min[
Map[
jaccardSimilarity[e1[#],e2[#]]&,
Keys[e1]
]
]


(* ::Subsubsection::Closed:: *)
(*jaccardDissimilarity (private)*)


(* ::Input::Initialization:: *)
jaccardDissimilarity[e1_,e2_]:=1-jaccardSimilarity[e1,e2];
jaccardDistance=jaccardDissimilarity;


(* ::Subsubsection::Closed:: *)
(*compatiblePatternQ (private)*)


(* ::Input::Initialization:: *)
compatiblePatternQ[patternExtensions_,minimalDistance_][testedPattern_,listOfPatterns_]:=
MatchQ[
SelectFirst[
listOfPatterns,
(jaccardDissimilarity[patternExtensions[#],patternExtensions[testedPattern]]<=minimalDistance&)
],
Missing[_]
];


(* ::Subsubsection::Closed:: *)
(*selectCompatiblePatterns (private)*)


(* ::Input::Initialization:: *)
selectCompatiblePatterns[patternExtensions_,minimalDistance_][{candidatePatterns_,selectedPatterns_}]:=
With[
{
foundPatternPos=FirstPosition[
candidatePatterns,
_?(compatiblePatternQ[patternExtensions,minimalDistance][#,selectedPatterns]&)  ,
Missing["NotFound"],
{1},
Heads->False
]
},
If[
MatchQ[foundPatternPos,Missing[_]],
{candidatePatterns,selectedPatterns},
{Drop[candidatePatterns, First[foundPatternPos]],Append[selectedPatterns,candidatePatterns[[First[foundPatternPos]]]]}
]
];


(* ::Subsubsection:: *)
(*selectPatternsByJaccardDistance*)


(* ::Input::Initialization:: *)
selectPatternsByJaccardDistance[orderedListOfPatterns_,patternExtensions_,minimalDistance_]:=
Last@FixedPoint[selectCompatiblePatterns[patternExtensions,minimalDistance][#]&,{orderedListOfPatterns,{}}];


(* ::Subsubsection::Closed:: *)
(*distanceBetweenSelections*)


(* ::Input::Initialization:: *)
distanceBetweenSelections[aggFun_][e1List_,e2List_]:=
(
Mean[
Map[
Function[{e1},
Apply[Min,Map[Function[{e2},jaccardDissimilarity[e1,e2]],e2List]]
],
e1List
]
]
~aggFun ~
Mean[
Map[
Function[{e2},
Apply[Min,Map[Function[{e1},jaccardDissimilarity[e1,e2]],e1List]]
],
e2List
]
]
) ;


(* ::Subsection::Closed:: *)
(*Repr\[EAcute]sentation des donn\[EAcute]es*)


(* ::Subsubsection::Closed:: *)
(*printNRIStatistics (private)*)


(* ::Input::Initialization:: *)
printNRIStatistics[data_Association]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
With[{g=data["Graphe"]},
Grid[
{
{"|V|",VertexCount[g]},
{"|E|",EdgeCount[g]},
{"|L|",Length[data["Items"]]},
{"\!\(\*OverscriptBox[\(deg \((v)\)\), \(_\)]\)",2.*EdgeCount[g]/VertexCount[g]},
{"\!\(\*OverscriptBox[\(\(|\)\(l \((v)\)\)\(|\)\), \(_\)]\)",N@Mean[Length/@data["Itemsets"]]}
},
Frame->All
]
];



(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*printNRIStatistics[importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]]*)


(* ::Subsubsection::Closed:: *)
(*printNRIGraph (private)*)


(* ::Input::Initialization:: *)
Options[printNRIGraph]={"LabelVertices"->True,Sequence@@Options@Graph};

printNRIGraph[data_Association,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"Itemsets"]
]:=
With[{g=data["Graphe"]},
With[
{
e=EdgeList@g,
v=If[
OptionValue@"LabelVertices",Map[Tooltip[#,#<>"\n"<>StringJoin[Riffle[data["Itemsets"][#]," "]]]&,VertexList[g]],
VertexList[g]
]},
Graph[v,e,FilterRules[{opts},Options[Graph]]]
]
]


(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*printNRIGraph[importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]]*)


(* ::Subsubsection::Closed:: *)
(*printNRI*)


(* ::Input::Initialization:: *)
Options[printNRI]={
"ShowGraphe"->True,
"ShowItemsets"->True,
"LabelVertices"->True,
Sequence@@Options@Graph
};

printNRI[data_,opts:OptionsPattern[]]/;And[
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"Items"],
KeyExistsQ[data,"Itemsets"]
]:=
With[{g=data["Graphe"]},
Grid[{
{
If[OptionValue@"ShowGraphe",printNRIGraph[data,FilterRules[{opts},Options[printNRIGraph]]],Null],
printNRIStatistics[data]
},
If[OptionValue["ShowItemsets"],
{ArrayPlot[
Map[With[{itemset=#},Map[Boole@MemberQ[itemset,#]&,data["Items"]]]&,Values@data["Itemsets"]],FilterRules[{opts},Options[ArrayPlot]]],SpanFromLeft},
Nothing]
},
Frame->All,Alignment->Top]
];


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*printNRI[importNRI[FileNameJoin[{NotebookDirectory[],"data","mougel.nri"}]]]*)


(* ::Subsubsection::Closed:: *)
(*printGraphAbstractionStatistics*)


(* ::Input:: *)
(*(**)
(**)
(*Options pour le Local et le Global*)
(* \"ClosAbs_State\"->True OpenerView ouvert pour la visualisation en d\[EAcute]tail de \"ClosAbs\"*)
(* \"ExtAbs_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail de \"ExtAbs\"*)
(* \"ExtraLines\"->Nothing, \[EAcute]l\[EAcute]ments \[AGrave] rajouter dans le tableau de description (doit prendre la forme d'une liste de 3 \[EAcute]l\[EAcute]ments*)
(* \"PatternFormatingFunction\"->Identity fonction qui s'applique aux motifs avant affichage. Par exemple:*)
(*(myTagIntersection[#,\"_\"]&) *)
(**)
(*Options le Global:*)
(* \"ExtGlob_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail de \"ExtGlob\"*)
(**)
(*Options le Local:*)
(* \"ClosLoc_State\"->True, OpenerView ouvert pour la visualisation en d\[EAcute]tail du \"ClosLoc\"*)
(* \"ExtAbs_ClosAbs_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail du \"ExtAbs_ClosAbs\"*)
(* \"ExtAbs_ClosLoc_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail du \"ExtAbs_ClosLoc\"*)
(* \"ExtGlob_ClosAbs_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail du \"ExtGlob_ClosAbs\"*)
(* \"ExtGlob_ClosLoc_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail du \"ExtGlob_ClosLoc\"*)
(* \"ExtLoc_ClosLoc_State\"->False, OpenerView ferm\[EAcute] pour la visualisation en d\[EAcute]tail du \"ExtLoc_ClosLoc\"*)
(*";*)*)


(* ::Input::Initialization:: *)
Options[printGraphAbstractionStatistics]={
"PatternFormatingFunction"->Identity,
"ExtraLines"->Nothing,
"ClosAbs_State"->True,

"ExtGlob_State"->False,
"ExtAbs_State"->False,

"ClosLoc_State"->True,
"ExtGlob_ClosAbs_State"->False,
"ExtAbs_ClosAbs_State"->False,
"ExtGlob_ClosLoc_State"->False,
"ExtAbs_ClosLoc_State"->False,
"ExtLoc_ClosLoc_State"->False

};



printGraphAbstractionStatistics[data_Association,clos_,opts:OptionsPattern[]]/;And[
MatchQ[clos,_List],
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"SupGlob"],
KeyExistsQ[data,"ExtGlob"],
KeyExistsQ[data,"ExtAbs"],
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0"
]:=
Grid[{
If[
OptionValue["PatternFormatingFunction"]===Identity,
{OpenerView[{"ClosAbs",OptionValue["PatternFormatingFunction"][clos]},OptionValue@"ClosAbs_State"],SpanFromLeft,SpanFromLeft},
{OpenerView[{"ClosAbs",Column[{OptionValue["PatternFormatingFunction"][clos],OpenerView[{"complete",clos},False]}]},OptionValue@"ClosAbs_State"],SpanFromLeft,SpanFromLeft}
],
{"SupGlob",data["SupGlob"][clos],OpenerView[{"ExtGlob",data["ExtGlob"][clos]},OptionValue@"ExtGlob_State"]},
{"SupAbs",data["SupAbs"][clos],OpenerView[{"ExtAbs",data["ExtAbs"][clos]},OptionValue@"ExtAbs_State"]},
If[KeyExistsQ[data,"Specificity"],{"Specificity",N@data["Specificity"][clos]},Nothing],
If[KeyExistsQ[data,"NormalizedSpecificity"]&&KeyExistsQ[data,"SigmaExpectedSupAbs"],{"NormSpecif",ToString[N@data["NormalizedSpecificity"][clos]]<>"\[PlusMinus]"<>ToString[N@data["SigmaExpectedSupAbs"][clos]]},Nothing],
If[KeyExistsQ[data,"DeviationToNormalizedSpecificity"],{"DevToNormSpecif",N@data["DeviationToNormalizedSpecificity"][clos]},Nothing],
If[KeyExistsQ[data,"DeviationToExpectedAbstractSupport"],{"DevToExpAbsSup",N@data["DeviationToExpectedAbstractSupport"][clos]},Nothing],
OptionValue@"ExtraLines"
},
Frame->All,
Alignment->Left,
ItemSize->Automatic
]



printGraphAbstractionStatistics[data_Association,clos_,opts:OptionsPattern[]]/;And[
MatchQ[clos,{_List,_List}],
KeyExistsQ[data,"ClosLoc"],
KeyExistsQ[data,"ClosAbs"],
KeyExistsQ[data,"SupGlob_ClosAbs"],
KeyExistsQ[data,"SupAbs_ClosAbs"],
KeyExistsQ[data,"SupGlob_ClosLoc"],
KeyExistsQ[data,"SupAbs_ClosLoc"],
KeyExistsQ[data,"SupLoc_ClosLoc"],
KeyExistsQ[data,"ExtGlob_ClosAbs"],
KeyExistsQ[data,"ExtAbs_ClosAbs"],
KeyExistsQ[data,"ExtGlob_ClosLoc"],
KeyExistsQ[data,"ExtAbs_ClosLoc"],
KeyExistsQ[data,"ExtLoc_ClosLoc"],
KeyExistsQ[data,"(ClosAbs_ExtLoc)"],
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="1"
]:=
Grid[{
{OpenerView[{"ClosLoc",OptionValue["PatternFormatingFunction"][data["ClosLoc"][clos]]},OptionValue@"ClosLoc_State"],SpanFromLeft,SpanFromLeft},
{OpenerView[{"ClosAbs",OptionValue["PatternFormatingFunction"][First@clos]},OptionValue@"ClosAbs_State"],SpanFromLeft,SpanFromLeft},
{"SupGlob_ClosAbs",data["SupGlob_ClosAbs"][clos],OpenerView[{"ExtGlob_ClosAbs",data["ExtGlob_ClosAbs"][clos]},OptionValue@"ExtGlob_ClosAbs_State"]},
{"SupAbs_ClosAbs",data["SupAbs_ClosAbs"][clos],OpenerView[{"ExtAbs_ClosAbs",data["ExtAbs_ClosAbs"][clos]},OptionValue@"ExtAbs_ClosAbs_State"]},
{"SupGlob_ClosLoc",data["SupGlob_ClosLoc"][clos],OpenerView[{"ExtGlob_ClosLoc",data["ExtGlob_ClosLoc"][clos]},OptionValue@"ExtGlob_ClosLoc_State"]},
{"SupAbs_ClosLoc",data["SupAbs_ClosLoc"][clos],OpenerView[{"ExtAbs_ClosLoc",data["ExtAbs_ClosLoc"][clos]},OptionValue@"ExtAbs_ClosLoc_State"]},
{"SupLoc_ClosLoc",data["SupLoc_ClosLoc"][clos],OpenerView[{"ExtLoc_ClosLoc",data["ExtLoc_ClosLoc"][clos]},OptionValue@"ExtLoc_ClosLoc_State"]},
If[KeyExistsQ[data,"Specificity"],{"Specificity",N@data["Specificity"][clos]},Nothing],
If[KeyExistsQ[data,"Informativity"],{"Informativity",N@data["Informativity"][clos]},Nothing],
If[KeyExistsQ[data,"NormalizedSpecificity"]&&KeyExistsQ[data,"SigmaExpectedSupAbs"],{"NormaSpecif",ToString[N@data["NormalizedSpecificity"][clos]]<>"\[PlusMinus]"<>ToString[N@data["SigmaExpectedSupAbs"][clos]]},Nothing],
If[KeyExistsQ[data,"DevToNormSpecif"],{"DeviationToNormalizedSpecificity",N@data["DeviationToNormalizedSpecificity"][clos]},Nothing],
OptionValue@"ExtraLines"
},
Frame->All,
Alignment->Left,
ItemSize->Automatic
]


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*With[{*)
(*data=*)
(*Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]*)
(*},*)
(*printGraphAbstractionStatistics[*)
(*data,*)
(*data["ClosAbs"][[3]],*)
(*"ClosAbs_State"->True(*,*)
(*"PatternFormatingFunction"\[Rule] (myTagIntersection[#,"_"]&)]*)*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{*)
(*data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1","-local"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]*)
(*},*)
(*printGraphAbstractionStatistics[*)
(*data,*)
(*data["(ClosAbs_ExtLoc)"][[3]],*)
(*"ClosAbs_State"->True(*,*)
(*"PatternFormatingFunction"\[Rule] (myTagIntersection[#,"_"]&)]*)*)
(*]*)
(*]*)


(* ::Input:: *)
(*With[{*)
(*data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1","-local"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]//*)
(*Append[#,"Specificity"->specificities[#]]&//*)
(*Append[#,"Informativity"->informativities[#]]&*)
(*},*)
(*printGraphAbstractionStatistics[*)
(*data,*)
(*data["(ClosAbs_ExtLoc)"][[3]],*)
(*"ClosAbs_State"->True(*,*)
(*"PatternFormatingFunction"\[Rule] (myTagIntersection[#,"_"]&)]*)*)
(*]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*highlightGraphAbstraction*)


(* ::Input::Initialization:: *)
Options[highlightGraphAbstraction]={
"VertexCountLimit"->100,
"Tooltips"->False,
"HighlightHubsVertices"->False,
"HubsVerticesSize"->1,
"FixVertexPosition"->False,
Sequence@@Options@Graph
};



highlightGraphAbstraction[data_Association,clos_,opts:OptionsPattern[]]/;And[
MatchQ[clos,_List],
KeyExistsQ[data,"Graphe"],
KeyExistsQ[data,"ExtGlob"],
KeyExistsQ[data,"ExtAbs"],
KeyExistsQ[data,"Itemsets"],
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="0"
]:=
With[
{
g=data["Graphe"],
extGlob=data["ExtGlob"][clos],
extAbs=data["ExtAbs"][clos]
},
Module[
{gCoor,gp,gGlobPat,gAbsPat,eAbsPat,vAbsPat, eGlobPat, vGlobPat},
gCoor=VertexCoordinates/.AbsoluteOptions[data["Graphe"],VertexCoordinates];
gGlobPat=Subgraph[g,extGlob,FilterRules[{opts},Options["Graphe"]]];
gAbsPat=Subgraph[g,extAbs,FilterRules[{opts},Options["Graphe"]]];
eAbsPat=EdgeList[gAbsPat];
vAbsPat=VertexList[gAbsPat];
eGlobPat=Complement[EdgeList[gGlobPat],eAbsPat];
vGlobPat=Complement[VertexList[gGlobPat],vAbsPat];

gp=
With[{graphe=If[VertexCount[g]<=OptionValue["VertexCountLimit"] ,g,gGlobPat]},
If[
OptionValue["Tooltips"],
With[{itemsets=data["Itemsets"]},
Graph[Map[Tooltip[#,{#,#/.itemsets}]&,VertexList@graphe],EdgeList@graphe,FilterRules[{opts},Options["Graphe"]]]
],
graphe
]
];

HighlightGraph[gp,
Union[
Style[#,Red,Thick]&/@eGlobPat,
Style[#,Red,Thick]&/@vGlobPat,
Style[#,Blue,Thick]&/@eAbsPat,
Style[#,Blue,Thick]&/@vAbsPat
],
Join[
If[
IntegerQ@OptionValue["HighlightHubsVertices"],
With[
{vHubs=Select[vAbsPat,(VertexDegree[gAbsPat,#]>=OptionValue["HighlightHubsVertices"]&)]},
{
VertexShapeFunction->Map[Rule[#,"Star"]&,vHubs],
VertexSize->Map[Rule[#,{"Nearest",OptionValue["HubsVerticesSize"]}]&,vHubs]
}
],
{Nothing}
],
FilterRules[{opts},Options[Graph]],
If[OptionValue@"FixVertexPosition",{VertexCoordinates->gCoor,PlotRange-> Map[{Min@#,Max@#}&,Transpose[gCoor]]},{}]
]
]

]
];



highlightGraphAbstraction[data_Association,closloc_,opts:OptionsPattern[]]/;And[
MatchQ[closloc,{_List,_List}],
KeyExistsQ[data,"ExtAbs_ClosAbs"],
KeyExistsQ[data,"ExtAbs_ClosLoc"],
KeyExistsQ[data,"ExtLoc_ClosLoc"],
KeyExistsQ[data,"Itemsets"],
KeyExistsQ[data,"ExperimentInformations"],
KeyExistsQ[data["ExperimentInformations"],"local"],
data["ExperimentInformations"]["local"]=="1"
]:=
With[
{
g=data["Graphe"],
extAbsClosAbs=data["ExtAbs_ClosAbs"][closloc],
extAbsClosLoc=data["ExtAbs_ClosLoc"][closloc],
extLocClosLoc=data["ExtLoc_ClosLoc"][closloc]
},
Module[
{gCoor,gp,gExtAbsClosAbs,gExtAbsClosLoc,gExtLocClosLoc,eExtAbsClosAbs,vExtAbsClosAbs,eExtAbsClosLoc,vExtAbsClosLoc,eExtLocClosLoc,vExtLocClosLoc},
gCoor=VertexCoordinates/.AbsoluteOptions[data["Graphe"],VertexCoordinates];
gExtAbsClosAbs=Subgraph[g,extAbsClosAbs];
gExtAbsClosLoc=Subgraph[g,extAbsClosLoc,FilterRules[{opts},Options["Graphe"]]];
gExtLocClosLoc=Subgraph[g,extLocClosLoc,FilterRules[{opts},Options["Graphe"]]];
eExtLocClosLoc=EdgeList[gExtLocClosLoc];
vExtLocClosLoc=VertexList[gExtLocClosLoc];
eExtAbsClosLoc=Complement[EdgeList[gExtAbsClosLoc],eExtLocClosLoc];
vExtAbsClosLoc=Complement[VertexList[gExtAbsClosLoc],vExtLocClosLoc];
eExtAbsClosAbs=Complement[EdgeList[gExtAbsClosAbs],eExtLocClosLoc,eExtAbsClosLoc];
vExtAbsClosAbs=Complement[VertexList[gExtAbsClosAbs],vExtLocClosLoc,eExtAbsClosLoc];
gp=With[
{
graphe=If[VertexCount[g]<=OptionValue["VertexCountLimit"],g,gExtAbsClosAbs]
},
If[
OptionValue["Tooltips"],
With[
{
itemsets=data["Itemsets"]
},
Graph[
Map[Tooltip[#,{#,#/.itemsets}]&,VertexList@graphe],
EdgeList@graphe,
FilterRules[{opts},Options["Graphe"]]
]
],
graphe
]
];
HighlightGraph[
gp,
Union[
Style[#,Blue,Thick]&/@eExtLocClosLoc,
Style[#,Blue,Thick]&/@vExtLocClosLoc,
Style[#,Green,Thick]&/@eExtAbsClosLoc,
Style[#,Green,Thick]&/@vExtAbsClosLoc,
Style[#,Red,Thick]&/@eExtAbsClosAbs,
Style[#,Red,Thick]&/@vExtAbsClosAbs
],
Join[
If[
IntegerQ@OptionValue["HighlightHubsVertices"],
With[
{vHubs=Select[vExtLocClosLoc,(VertexDegree[gExtLocClosLoc,#]>=OptionValue["HighlightHubsVertices"]&)]},{VertexShapeFunction->Map[Rule[#,"Star"]&,vHubs],VertexSize->Map[Rule[#,{"Nearest",OptionValue["HubsVerticesSize"]}]&,vHubs]}],
{Nothing}
],
FilterRules[{opts},Options[Graph]],
If[OptionValue@"FixVertexPosition",{VertexCoordinates->gCoor,PlotRange-> Map[{Min@#,Max@#}&,Transpose[gCoor]]},{}]
]
]
]
]


(* ::Text:: *)
(*Tests*)


(* ::Input:: *)
(*With[{data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]},*)
(*highlightGraphAbstraction[data,data["ClosAbs"][[5]]]*)
(*]*)


(* ::Input:: *)
(*With[{data=Join[*)
(*readNRI["mougel.nri","PathForNRI"->FileNameJoin[{NotebookDirectory[],"data"}]],*)
(*readMinerLC["mougel.nri",{"-min_sup 1" ,"-t1"},"PathForResults"->FileNameJoin[{NotebookDirectory[],"data"}]]*)
(*]},*)
(*highlightGraphAbstraction[data,data["ClosAbs"][[4]],*)
(*"Tooltips"->True,*)
(*VertexLabels->"Name",*)
(*"VertexCountLimit"->10,*)
(*"HighlightHubsVertices"->5*)
(*]*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*plotObsVersusExpected*)


(* ::Input:: *)
(*ClearAll[plotObsVersusExpected]*)


(* ::Input:: *)
(*Remove@plotObsVersusExpected*)


(* ::Input::Initialization:: *)
Options[plotObsVersusExpected]={
"KeyListeClos"-> Automatic,
"KeySupportOriginal"->Automatic,
"KeySupportObserve"-> Automatic,
"KeyMeanExpected"-> Automatic,
"KeySigmaExpected"->Automatic,
"Frame"->True,
"FrameLabel"->{"Reference\nsupport","Observed\nsupport"},
"PlotStyle"->{{Red,AbsoluteThickness[2]}, Black, Black, {Black,Dashed},  {Black,Dashed}, Lighter@Gray, Lighter@Gray},
"GridLines"->None,
"AxesOrigin"->{0,0},
"Filling"-> {1->{{2},Directive[Opacity[0.5],Lighter@Orange]},1->{{3},Directive[Opacity[0.5],Lighter@Orange]},1->{{4},Directive[Opacity[0.5],Yellow]},1->{{5},Directive[Opacity[0.5],Yellow]},1->{{6},Directive[Opacity[0.5],Lighter@Yellow]},1->{{7},Directive[Opacity[0.5],Lighter@Yellow]}},
"AspectRatio"->Automatic,
"PointColor"->Blue,
"PointSize"->.01,
"PatternTooltips"->False,
"PointActionFunction"->None,
"PatternFormatingFunction"->Identity,
"PointOriObsColorFunction"->Automatic,
"PlotLabel"->None,
"ImageSize"->Automatic,
"ShowLegend"-> True,
"SkipEmptyPattern"->False,
"PlotRange"->Automatic
};

plotObsVersusExpected[_Missing,___]:="Missing";
plotObsVersusExpected[data_Association,opts:OptionsPattern[]]:=
Module[{xmin,xmax,ymin,ymax,curvesData,interpolatedCurves,plot},
With[
{
localExp=(data["AttendusExperimentInformations"]["local"]=="1"),
skipEmpty=OptionValue["SkipEmptyPattern"]
},
With[
{
keyListClos=If[OptionValue["KeyListeClos"]===Automatic,If[localExp,"(ClosAbs_ExtLoc)","ClosAbs"],OptionValue["KeyListeClos"]],
keySupOri=If[OptionValue["KeySupportOriginal"]===Automatic,If[localExp,"SupGlob_ClosLoc","SupGlob"],OptionValue["KeySupportOriginal"]],
keySupObs=If[OptionValue["KeySupportObserve"]===Automatic,If[localExp,"SupLoc_ClosLoc","SupAbs"],OptionValue["KeySupportObserve"]],
keySupExpected=If[OptionValue["KeyMeanExpected"]===Automatic,If[localExp,"MeanExpectedSupLoc","MeanExpectedSupAbs"],OptionValue["KeyMeanExpected"]],
keySigmaExpected=If[OptionValue["KeySigmaExpected"]===Automatic,If[localExp,"SigmaExpectedSupLoc","SigmaExpectedSupAbs"],OptionValue["KeySigmaExpected"]]
},
With[
{
emptyPatternKey=
If[skipEmpty,
If[
localExp,
First@Cases[data[keyListClos],{{},_}],
{}
]
]
},
curvesData=Transpose[
Map[
With[
{
original=data[keySupOri][#],
expected=data[keySupExpected][#],
sigma=data[keySigmaExpected][#]
},
{
{original,expected},
{original,Max[expected-sigma,0]},
{original,expected+sigma},
{original,Max[expected-2*sigma,0]},
{original,expected+2*sigma},
{original,Max[expected-3*sigma,0]},
{original,expected+3*sigma}
}
]&
,
If[skipEmpty,DeleteCases[data[keyListClos],emptyPatternKey],data[keyListClos]]
]
];
{xmin,xmax}=MinMax[Values[If[skipEmpty,KeyDrop[data[keySupOri],{emptyPatternKey}],data[keySupOri]]]];
{ymin,ymax}=MinMax[Join[
Last/@Flatten[curvesData,1],
Values[If[skipEmpty,KeyDrop[data[keySupObs],{emptyPatternKey}],data[keySupObs]]]
]];

interpolatedCurves=
Map[
Interpolation[Union[#],InterpolationOrder->1]&,
curvesData
];
Show[
{
Plot[
Evaluate[Map[#[x]&,interpolatedCurves]],{x,xmin,xmax},
Frame->OptionValue["Frame"],
FrameLabel->OptionValue["FrameLabel"],
PlotStyle->OptionValue["PlotStyle"],
GridLines->OptionValue["GridLines"],
AxesOrigin->OptionValue["AxesOrigin"],
Filling->OptionValue["Filling"],
AspectRatio->If[OptionValue["AspectRatio"]===Automatic,ymax/xmax,OptionValue["AspectRatio"]],
PlotLabel->OptionValue["PlotLabel"],
ImageSize->OptionValue["ImageSize"],
PlotRange->If[OptionValue["PlotRange"]===All,{{xmin,xmax},{ymin,ymax}},OptionValue["PlotRange"]]
],
Graphics[
Join[
{OptionValue["PointColor"],PointSize[OptionValue["PointSize"]]},
MapIndexed[
With[
{
pointid=First@#2,
p=#1,
crdX=data[keySupOri][#1],
crdY=data[keySupObs][#1]
},
If[OptionValue["PointActionFunction"]===None,Point[{crdX,crdY}],OptionValue["PointActionFunction"][p,crdX,crdY,pointid]]//
If[OptionValue["PatternTooltips"],Tooltip[#,OptionValue["PatternFormatingFunction"][p]],#]&//
If[OptionValue["PointOriObsColorFunction"]=!=Automatic,List[OptionValue["PointOriObsColorFunction"][p],#],#]&
]&,
data[keyListClos]
]
],
PlotRange->If[OptionValue["PlotRange"]===All,{{xmin,xmax},{ymin,ymax}},OptionValue["PlotRange"]]
]
}
]//
If[OptionValue["ShowLegend"],
Legended[
#,
Placed[
Row[
{
LineLegend[{Directive[Red,AbsoluteThickness[2]],Black,Directive[Black,Dashed],Lighter@Gray},{keySupExpected,"\[PlusMinus]1\[Sigma]","\[PlusMinus]2\[Sigma]","\[PlusMinus]3\[Sigma]"},LegendFunction->(Framed[#,Background->Lighter@Lighter@Yellow]&)],
PointLegend[{OptionValue["PointColor"]},{keySupObs},LegendFunction->(Framed[#]&)]
}
],
Below]
],
#
]&
]
]
]
];



(* ::Text:: *)
(*Test*)


(* ::Input:: *)
(*$NumberOfRuns=200;*)
(*$ExperimentParameters={ "-t1","-n "<>ToString[$NumberOfRuns]};*)
(*$NRIFile="brauer_elati_3_7.nri";*)
(*data=readSupportsAttendus[$NRIFile,$ExperimentParameters,"PathForSUP"->"/Users/santini/Library/Mathematica/Applications/data/" ];*)
(*plotObsVersusExpected[data]*)


(* ::Input:: *)
(*$NumberOfRuns=100;*)
(*$ExperimentParameters={"-local", "-min_sup 1","-t1","-n "<>ToString[$NumberOfRuns]};*)
(*$NRIFile="s50_an1_red.nri";*)
(*data=Join[*)
(*readMinerLC[$NRIFile,$ExperimentParameters,"PathForResults"->"/Users/santini/Library/Mathematica/Applications/data/" ],*)
(*readSupportsAttendus[$NRIFile,$ExperimentParameters,"PathForSUP"->"/Users/santini/Library/Mathematica/Applications/data/" ]*)
(*];*)
(*resolveLocalSupportAttendu@data;*)


(* ::Input:: *)
(*plotObsVersusExpected[data]*)


(* ::Subsubsection::Closed:: *)
(*hubAutLists*)


(* ::Input::Initialization:: *)
iter[g_,dHub_,dAut_][{hublist_,autlist_}]:={
Select[hublist,VertexOutDegree[Graph[VertexList@#,EdgeList@#]&@Subgraph[g,Append[autlist,#]],#]>=dHub&],
Select[autlist,VertexInDegree[Graph[VertexList@#,EdgeList@#]&@Subgraph[g,Append[hublist,#]],#]>=dAut&]
};

hubAutLists[g_,dHub_,dAut_]:=
FixedPoint[
iter[g,dHub,dAut],
{VertexList[g],VertexList[g]}
]


(* ::Subsubsection::Closed:: *)
(*$hubAutVertex, $hubVertex, $autVertex (local)*)


(* ::Input::Initialization:: *)
$hubAutVertexRB::usage="$hubAutVertexRB motif graphique rouge et bleu utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub-Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$hubAutVertexRBfilled::usage="$hubAutVertexRBfilled motif graphique rouge et bleu utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub-Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$hubAutVertexCasinoNB::usage="$hubAutVertexCasinoNB motif graphique noir et blanc utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub-Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$hubAutVertexCasinoRB::usage="$hubAutVertexCasinoRB motif graphique couleur utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub-Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";


$autVertexRB::usage="$autVertexRB motif graphique rouge et blanc utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$autVertexRBfilled::usage="$autVertexRB motif graphique rouge utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$autVertexCasinoNB::usage="$autVertexCasinoNB motif graphique noir et blanc utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$autVertexCasinoRB::usage="$autVertexCasinoBW motif graphique couleur utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Autorit\[EAcute] dans un graphe d'abstraction Hub-Autorit\[EAcute].";


$hubVertexRB::usage="$hubVertexRB motif graphique bleu utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$hubVertexRBfilled::usage="$hubVertexRB motif graphique blanc et bleu utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$hubVertexCasinoNB::usage="$hubVertexCasinoNB motif graphique noir et blanc utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$hubVertexCasinoRB::usage="$hubAutVertexCasinoBW motif graphique couleur utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud Hub dans un graphe d'abstraction Hub-Autorit\[EAcute].";


$defaultVertexRB::usage="$defaultVertexRB motif graphique transparent utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud ni Hub ni Aut dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$defaultVertexRBfilled::usage="$defaultVertexRBfilled motif graphique transparent utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud ni Hub ni Aut dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$defaultVertexCasinoNB::usage="$defaultVertexCasinoNB motif graphique transparent utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud ni Hub ni Aut dans un graphe d'abstraction Hub-Autorit\[EAcute].";
$defaultVertexCasinoRB::usage="$defaultVertexCasinoRB motif graphique transparent utilis\[EAcute] pour repr\[EAcute]senter le sommet d'un noeud ni Hub ni Aut dans un graphe d'abstraction Hub-Autorit\[EAcute].";


(* ::Input::Initialization:: *)
$hubAutVertexCasinoNB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Black,
Rotate[#,0\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,90\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,180\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,270\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],

Black,
Rotate[#,45\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,135\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,225\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,315\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$hubAutVertexCasinoRB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Blue,
Rotate[#,0\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,90\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,180\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,270\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],

Red,
Rotate[#,45\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,135\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,225\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,315\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$hubAutVertexRB=$hubAutVertexRBfilled=Image[
Graphics[
{
EdgeForm[{ColorData["HTML"]["LightSalmon"],Thick}],
ColorData["HTML"]["LightSalmon"],
Disk[{0,0},1,{0,\[Pi]}],
EdgeForm[{ColorData["HTML"]["LightSkyBlue"],Thick}],
ColorData["HTML"]["LightSkyBlue"],
Disk[{0,0},1,{\[Pi],2\[Pi]}],
EdgeForm[{Thickness[.02],Black}],
FaceForm[{Opacity[0]}],
Disk[{0,0},1.07]
}
],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&


(* ::Input::Initialization:: *)
$autVertexCasinoNB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Black,
Rotate[#,45\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,135\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,225\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,315\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$autVertexCasinoRB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Red,
Rotate[#,45\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,135\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,225\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],
Rotate[#,315\[Degree],{0,0}]&@Rotate[#,180\[Degree]]&@Polygon[{{2,4.25},{-2,4.25},{0,7}}],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&


$autVertexRB=Image[
Graphics[
{
EdgeForm[{ColorData["HTML"]["LightSalmon"],Thick}],
ColorData["HTML"]["LightSalmon"],
Disk[{0,0},1,{0,\[Pi]}],
EdgeForm[{Gray,Thick}],
Gray,
Disk[{0,0},1,{\[Pi],2\[Pi]}],
EdgeForm[{Thickness[.02],Black}],
FaceForm[{Opacity[0]}],
Disk[{0,0},1.07]
}
],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$autVertexRBfilled=Image[
Graphics[
{
EdgeForm[{Thickness[.02],Black}],
ColorData["HTML"]["LightSalmon"],
EdgeForm[Black],
Disk[{0,0},1]
}
],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&


(* ::Input::Initialization:: *)
$hubVertexCasinoNB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Black,
Rotate[#,0\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,90\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,180\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,270\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$hubVertexCasinoRB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Blue,
Rotate[#,0\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,90\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,180\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],
Rotate[#,270\[Degree],{0,0}]&@Polygon[{{2,3.75},{-2,3.75},{0,7}}],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$hubVertexRB=Image[
Graphics[
{
EdgeForm[{Gray,Thick}],
Gray,
Disk[{0,0},1,{0,\[Pi]}],
EdgeForm[{ColorData["HTML"]["LightSkyBlue"],Thick}],
ColorData["HTML"]["LightSkyBlue"],
Disk[{0,0},1,{\[Pi],2\[Pi]}],
EdgeForm[{Thickness[.02],Black}],
FaceForm[{Opacity[0]}],
Disk[{0,0},1.07]
}
],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$hubVertexRBfilled=Image[
Graphics[
{
EdgeForm[{Thickness[.02],Black}],
ColorData["HTML"]["LightSkyBlue"],
EdgeForm[Black],
Disk[{0,0},1]
}
],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&


(* ::Input::Initialization:: *)
$defaultVertexCasinoNB=$defaultVertexCasinoRB=Image[
Graphics[{

Gray,
Disk[{0,0},7],

Black,
AbsoluteThickness[2],Circle[{0,0},4],
AbsoluteThickness[2],Circle[{0,0},7]
}],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&

$defaultVertexRB=$defaultVertexRBfilled=Image[
Graphics[
{
EdgeForm[{Thickness[.02],Black}],
Gray,
Disk[{0,0},1]
}
],
ImageSize->100
]//ColorReplace[#,White]&//ColorReplace[#,Gray->White]&


(* ::Input:: *)
(*g=Graph[{1\[DirectedEdge]2,2\[DirectedEdge]3,3\[DirectedEdge]4,4\[DirectedEdge]5,5\[DirectedEdge]6,6\[DirectedEdge]7,7\[DirectedEdge]8,8\[DirectedEdge]9,9-> 10,10->11,11->12,12->13,13->14, 14->15,15->16}];*)
(*Graph[*)
(*g,*)
(*VertexLabels->Map[Rule[#,Placed[#,Center]]&,VertexList[g]],*)
(*VertexShape->{*)
(**)
(*1-> $hubAutVertexCasinoRB,2-> $autVertexCasinoRB,3-> $hubVertexCasinoRB,4->$defaultVertexCasinoRB,*)
(*5->$hubAutVertexCasinoNB,6->$autVertexCasinoNB,7->$hubVertexCasinoNB,8->$defaultVertexCasinoNB,*)
(*9-> $hubAutVertexRB,10-> $autVertexRB,11-> $hubVertexRB,12->$defaultVertexRB,*)
(*13->$hubAutVertexRBfilled, 14->$autVertexRBfilled, 15->$hubVertexRBfilled, 16-> $defaultVertexRBfilled },*)
(*VertexSize->Large,*)
(*GraphLayout->"CircularEmbedding"*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*hubAutVertexStyle (deprecated)*)


(* ::Input:: *)
(*(*hubAutVertexStyle[g_,dHub_,dAut_]:=*)
(*Module[{ha,hubAndAutList,hubOnlyList,autOnlyList},*)
(*ha=hubAutLists[g,dHub,dAut];*)
(*hubAndAutList=Intersection[First[ha],Last[ha]];*)
(*hubOnlyList=Complement[First[ha],hubAndAutList];*)
(*autOnlyList=Complement[Last[ha],hubAndAutList];*)
(*Join[*)
(*Map[#\[Rule] Purple&,hubAndAutList],*)
(*Map[#\[Rule] Red&,hubOnlyList],*)
(*Map[#\[Rule] Blue&,autOnlyList]*)
(*]*)
(*]*)*)


(* ::Input:: *)
(*(*With[{*)
(*g=Graph[{1\[DirectedEdge]3,1\[DirectedEdge]4,2\[DirectedEdge]3,2\[DirectedEdge]4}],*)
(*dHub=2,*)
(*dAut=2*)
(*},*)
(*Graph[g,VertexLabels\[Rule]"Name",VertexStyle\[Rule]hubAutVertexStyle[g,dHub,dAut],ImageSize\[Rule]Tiny,VertexSize\[Rule].5]*)
(*]*)*)


(* ::Input:: *)
(*(*With[{*)
(*g=Graph[{1\[DirectedEdge]2,2\[DirectedEdge]2,2\[DirectedEdge]3,1\[DirectedEdge]3}],*)
(*dHub=2,*)
(*dAut=2*)
(*},*)
(*Graph[g,VertexLabels\[Rule]"Name",VertexStyle\[Rule]hubAutVertexStyle[g,dHub,dAut],ImageSize\[Rule]Tiny,VertexSize\[Rule].5]*)
(*]*)*)


(* ::Input:: *)
(*(*With[{*)
(*g=Graph[{1\[DirectedEdge]3,1\[DirectedEdge]4,2\[DirectedEdge]3,2\[DirectedEdge]4,3\[DirectedEdge]5,3\[DirectedEdge]6,4\[DirectedEdge]5,4\[DirectedEdge]6}],*)
(*dHub=2,*)
(*dAut=2*)
(*},*)
(*Graph[g,VertexLabels\[Rule]"Name",VertexStyle\[Rule]hubAutVertexStyle[g,dHub,dAut],ImageSize\[Rule]Tiny,VertexSize\[Rule].5]*)
(*]*)*)


(* ::Subsubsection::Closed:: *)
(*hubAutVertexShape(private)*)


(* ::Input::Initialization:: *)
(*hubAutVertexShape::usage="hubAutVertexShape[g_,dHub_,dAut_,shape_] renvoie une liste de r\[EGrave]gles associant chaque sommet du graphe g \[AGrave] un graphique de representation qui tient compte de la nature du noeuds dans une abstraction Hub-Autorit\[EAcute]s de degr\[EAcute]s (dHub,dAut). Le param\[EGrave]tre shape d\[EAcute]fini la forme du graphique de repr\[EAcute]sentation du noeud (RB ou CasinoRB ou CasinoNB).";*)
hubAutVertexShape::usage="hubAutVertexShape[g_,hubAutLists_,shape_] renvoie une liste de r\[EGrave]gles associant chaque sommet du graphe g \[AGrave] un graphique de representation qui tient compte de la nature du noeuds dans une abstraction Hub-Autorit\[EAcute]s. hubAutLists donne les noeuds qui ont la qualit\[EAcute] hub et les noeuds qui ont la qualit\[EAcute] autorit\[EAcute] (dans cet ordre). Le param\[EGrave]tre shape d\[EAcute]fini la forme du graphique de repr\[EAcute]sentation du noeud (RB ou CasinoRB ou CasinoNB).";


(* ::Input::Initialization:: *)
(*hubAutVertexShape[g_,dHub_,dAut_,shape_]:=
Module[{ha,haList,hList,aList,oList,haV,hV,aV,oV},
ha=hubAutLists[g,dHub,dAut];
haList=Intersection[First[ha],Last[ha]];
hList=Complement[First[ha],haList];
aList=Complement[Last[ha],haList];
oList=Complement[VertexList[g], Apply[Union,ha]];
{haV,hV,aV,oV}=
Switch[
shape,
"RB",
{$hubAutVertexRB,$hubVertexRB, $autVertexRB, $defaultVertexRB},
"RBfilled",
{$hubAutVertexRBfilled,$hubVertexRBfilled, $autVertexRBfilled, $defaultVertexRBfilled},
"CasinoRB",
{$hubAutVertexCasinoRB,$hubVertexCasinoRB, $autVertexCasinoRB,$defaultVertexCasinoRB},
"CasinoNB",
{$hubAutVertexCasinoNB,$hubVertexCasinoNB, $autVertexCasinoNB,$defaultVertexCasinoNB}
];
Join[
Map[#\[Rule] haV&,haList],
Map[#\[Rule] hV&,hList],
Map[#\[Rule] aV&,aList],
Map[#\[Rule] oV&,oList]
]
]*)
hubAutVertexShape[g_,ha_,shape_]:=
Module[{haList,hList,aList,oList,haV,hV,aV,oV},
haList=Intersection[First[ha],Last[ha]];
hList=Complement[First[ha],haList];
aList=Complement[Last[ha],haList];
oList=Complement[VertexList[g], Apply[Union,ha]];
{haV,hV,aV,oV}=
Switch[
shape,
"RB",
{$hubAutVertexRB,$hubVertexRB, $autVertexRB, $defaultVertexRB},
"RBfilled",
{$hubAutVertexRBfilled,$hubVertexRBfilled, $autVertexRBfilled, $defaultVertexRBfilled},
"CasinoRB",
{$hubAutVertexCasinoRB,$hubVertexCasinoRB, $autVertexCasinoRB,$defaultVertexCasinoRB},
"CasinoNB",
{$hubAutVertexCasinoNB,$hubVertexCasinoNB, $autVertexCasinoNB,$defaultVertexCasinoNB}
];
Join[
Map[#-> haV&,haList],
Map[#-> hV&,hList],
Map[#-> aV&,aList],
Map[#-> oV&,oList]
]
]


(* ::Input:: *)
(*Map[*)
(*With[{*)
(*g=Graph[{0->1,0->2,1\[DirectedEdge]3,1\[DirectedEdge]4,2\[DirectedEdge]3,2\[DirectedEdge]4,3\[DirectedEdge]5,3\[DirectedEdge]6,4\[DirectedEdge]5,4\[DirectedEdge]6,5->7,6->7}],*)
(*dHub=2,*)
(*dAut=2*)
(*},*)
(*Graph[*)
(*g,*)
(*VertexLabels->Map[Rule[#,Placed[#,Center]]&,VertexList[g]],*)
(*VertexShape->hubAutVertexShape[g,hubAutLists[g,dHub,dAut],#],*)
(*ImageSize->Small,*)
(*VertexSize->.5*)
(*]*)
(*]&,*)
(*{"RB","RBfilled","CasinoNB","CasinoRB"}*)
(*]*)


(* ::Subsubsection::Closed:: *)
(*hubAutGraph*)


(* ::Input::Initialization:: *)
ClearAll@hubAutGraph;
Options[hubAutGraph]={
"DirectedGraph"->True,
"VertexLabels"->"Names",
"VertexSize"->Medium,
"VertexLabelSize"->Medium,
"VertexShape"->"RB",
"VertexLabels"->"Names",
"AbstractEdgeStyle"-> Directive[Black,Thick],
"InducedEdgeStyle"-> Directive[Black,Dashed],
"DefaultEdgeStyle"-> Directive[Black,Dotted],
"ShowAllVertices"->True,
"ImageSize"->600,
"VertexCoordinates"->Automatic,
"GraphLayout"-> "SpringElectricalEmbedding",
"PlotRange"->Automatic,
"ImagePadding"->Automatic,
"ArrowheadSize"-> 0.05,
"PlotLabel"-> None
};

(*hubAutGraph[g_Graph, dHub_Integer,dAut_Integer,opts:OptionsPattern[]]:=
With[{hubVertices=First[#],autVertices=Last[#]},
With[{abstractEdges=Cases[EdgeList[g],out_\[DirectedEdge]in_/;And[MemberQ[hubVertices,out],MemberQ[autVertices,in]]]},
With[{otherEdges=Complement[EdgeList[g],abstractEdges]},
Graph[
g,
VertexLabels\[Rule]
Cases[
If["Names"===OptionValue["VertexLabels"],
Map[Rule[#,#]&,VertexList[g]],
OptionValue["VertexLabels"]
],
Rule[v_,label_]\[RuleDelayed] Rule[v,Placed[Style[label,OptionValue["VertexLabelSize"]],Center]]
],
VertexShape\[Rule]hubAutVertexShape[g,dHub,dAut,OptionValue["VertexShape"]],
VertexSize\[Rule]OptionValue["VertexSize"],
EdgeStyle\[Rule]Join[{Arrowheads[OptionValue["ArrowheadSize"]]},Map[Rule[#,OptionValue["DefaultEdgeStyle"]]&,otherEdges],Map[Rule[#,OptionValue["AbstractEdgeStyle"]]&,abstractEdges]],
ImageSize\[Rule]OptionValue["ImageSize"],
VertexCoordinates\[Rule]OptionValue["VertexCoordinates"],
GraphLayout\[Rule] OptionValue["GraphLayout"],
PlotRange\[Rule] OptionValue["PlotRange"],
ImagePadding\[Rule] OptionValue["ImagePadding"],
PlotLabel\[Rule] OptionValue["PlotLabel"]
]
]
]
]&@hubAutLists[g,dHub,dAut];*)


(*hubAutGraph[g_Graph, dHub_Integer,dAut_Integer,opts:OptionsPattern[]]:=
With[{hubVertices=First[#],autVertices=Last[#]},
With[
{
abstractEdges=Cases[EdgeList[g],head_\[DirectedEdge]tail_/;And[MemberQ[hubVertices,head],MemberQ[autVertices,tail]]],
allVertices=Union[hubVertices,autVertices]
},
With[
{
inducedEdges=Cases[EdgeList[g],head_\[DirectedEdge]tail_/;And[MemberQ[allVertices,head],MemberQ[allVertices,tail],Not[MemberQ[abstractEdges,head\[DirectedEdge]tail]]]],
notInducedEdges=Cases[EdgeList[g],head_\[DirectedEdge]tail_/;Or[Not[MemberQ[allVertices,head]],Not[MemberQ[allVertices,tail]]]]
},
With[{otherEdges=Complement[EdgeList[g],abstractEdges]},
Graph[
g,
VertexLabels\[Rule]
Cases[
If["Names"===OptionValue["VertexLabels"],
Map[Rule[#,#]&,VertexList[g]],
OptionValue["VertexLabels"]
],
Rule[v_,label_]\[RuleDelayed] Rule[v,Placed[Style[label,OptionValue["VertexLabelSize"]],Center]]
],
VertexShape\[Rule]hubAutVertexShape[g,dHub,dAut,OptionValue["VertexShape"]],
VertexSize\[Rule]OptionValue["VertexSize"],
EdgeStyle\[Rule]Join[
{Arrowheads[OptionValue["ArrowheadSize"]]},
Map[Rule[#,OptionValue["DefaultEdgeStyle"]]&,notInducedEdges],Map[Rule[#,OptionValue["InducedEdgeStyle"]]&,inducedEdges],Map[Rule[#,OptionValue["AbstractEdgeStyle"]]&,abstractEdges]
],
ImageSize\[Rule]OptionValue["ImageSize"],
VertexCoordinates\[Rule]OptionValue["VertexCoordinates"],
GraphLayout\[Rule] OptionValue["GraphLayout"],
PlotRange\[Rule] OptionValue["PlotRange"],
ImagePadding\[Rule] OptionValue["ImagePadding"],
PlotLabel\[Rule] OptionValue["PlotLabel"]
]
]
]
]
]&@hubAutLists[g,dHub,dAut]*)

hubAutGraph[g_Graph, dHub_Integer,dAut_Integer,opts:OptionsPattern[]]:=
hubAutGraph[g, hubAutLists[g,dHub,dAut],opts];

hubAutGraph[g_Graph, haLists_,opts:OptionsPattern[]]:=
With[{hubVertices=First[haLists],autVertices=Last[haLists]},
With[
{
abstractEdges=Cases[EdgeList[g],head_\[DirectedEdge]tail_/;And[MemberQ[hubVertices,head],MemberQ[autVertices,tail]]],
allVertices=Union[hubVertices,autVertices]
},
With[
{
inducedEdges=Cases[EdgeList[g],head_\[DirectedEdge]tail_/;And[MemberQ[allVertices,head],MemberQ[allVertices,tail],Not[MemberQ[abstractEdges,head\[DirectedEdge]tail]]]],
notInducedEdges=Cases[EdgeList[g],head_\[DirectedEdge]tail_/;Or[Not[MemberQ[allVertices,head]],Not[MemberQ[allVertices,tail]]]]
},
With[{otherEdges=Complement[EdgeList[g],abstractEdges]},
Graph[
g//
If[OptionValue["ShowAllVertices"],#,Subgraph[#,Union@@haLists]]&//
If[OptionValue["DirectedGraph"],#,UndirectedGraph@#]&,
VertexLabels->
Cases[
If["Names"===OptionValue["VertexLabels"],
Map[Rule[#,#]&,VertexList[g]],
OptionValue["VertexLabels"]
],
Rule[v_,label_]:> Rule[v,Placed[Style[label,OptionValue["VertexLabelSize"]],Center]]
],
VertexShape->hubAutVertexShape[g,haLists,OptionValue["VertexShape"]],
VertexSize->OptionValue["VertexSize"],
EdgeStyle->Join[
If[OptionValue["DirectedGraph"],{Arrowheads[OptionValue["ArrowheadSize"]]},{Nothing}],
Map[Rule[#,OptionValue["DefaultEdgeStyle"]]&,If[OptionValue["DirectedGraph"],notInducedEdges,Map[Apply[UndirectedEdge,#]&,notInducedEdges]]],
Map[Rule[#,OptionValue["InducedEdgeStyle"]]&,If[OptionValue["DirectedGraph"],inducedEdges,Map[Apply[UndirectedEdge,#]&,inducedEdges]]],
Map[Rule[#,OptionValue["AbstractEdgeStyle"]]&,If[OptionValue["DirectedGraph"],abstractEdges,Map[Apply[UndirectedEdge,#]&,abstractEdges]]]
],
ImageSize->OptionValue["ImageSize"],
VertexCoordinates->OptionValue["VertexCoordinates"],
GraphLayout-> OptionValue["GraphLayout"],
PlotRange-> OptionValue["PlotRange"],
ImagePadding-> OptionValue["ImagePadding"],
PlotLabel-> OptionValue["PlotLabel"]
]
]
]
]
];


(* ::Input:: *)
(*hubAutGraph[*)
(*Graph[{1\[DirectedEdge]1,1\[DirectedEdge]2,2\[DirectedEdge]2,2\[DirectedEdge]3,1\[DirectedEdge]3}],2,2,*)
(*"VertexLabelSize"->Medium,*)
(*"VertexSize"->Medium,*)
(*"VertexShape"->"RB",*)
(*"AbstractEdgeStyle"-> Directive[Black,Thick],*)
(*"DefaultEdgeStyle"-> Directive[Black,Dashed],*)
(*"ImageSize"->200,*)
(*"ArrowheadSize"->.2*)
(*]*)
(**)


(* ::Input:: *)
(*hubAutGraph[*)
(*Subgraph[Graph[{"1","2", "3"}, {DirectedEdge["1", "2"], DirectedEdge["2", "2"], DirectedEdge["2", "3"], DirectedEdge["1", "3"]}],{"1","2","3"}],2,2,*)
(*"VertexLabelSize"->Medium,*)
(*"VertexSize"->Medium,*)
(*"VertexShape"->"RB",*)
(*"AbstractEdgeStyle"-> Directive[Black,Thick],*)
(*"DefaultEdgeStyle"-> Directive[Black,Dashed],*)
(*"ImageSize"->200(*,*)
(*"VertexCoordinates"\[Rule] {"1"\[Rule]{0,0},"2"\[Rule]{0,1},"3"\[Rule]{0,2}}*)*)
(*]*)


(* ::Input:: *)
(*gPrime=hubAutGraph[gSimple,{{"0","1","2","3","4"},{"3","4","5","6","7"}},"DirectedGraph"->False,"InducedEdgeStyle"->Directive[Opacity[0]],"DefaultEdgeStyle"->Directive[Opacity[0]],"ShowAllVertices"->True,"VertexSize"->0.5,"VertexLabelSize"->Large,"ArrowheadSize"->0.02,"VertexCoordinates"->Map[Rule[#,PropertyValue[{g,#},VertexCoordinates]]&,VertexList@gSimple],(*pour le cadre non transform\[EAcute]*)PlotRange->Map[{Min[#],Max[#]}&,Transpose[Map[Last,Map[Rule[#,PropertyValue[{g,#},VertexCoordinates]]&,VertexList@gSimple]]]],ImagePadding->20,"VertexShape"->"RBfilled"]*)


(* ::Subsubsection::Closed:: *)
(*makeCrdRules*)


(* ::Input::Initialization:: *)
makeCrdRules[g_]:=Map[
 Rule[#,PropertyValue[
{g,#},VertexCoordinates
]
]&,
VertexList@g];


(* ::Section:: *)
(*Fin*)


(* ::Input::Initialization:: *)
End[];


(* ::Input::Initialization:: *)
EndPackage[];
