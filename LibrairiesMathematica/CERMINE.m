(* ::Package:: *)

(* ::Title:: *)
(*CERMINE*)


(* ::Subtitle:: *)
(*Extraction de connaissances \[AGrave] partir de publications scientifiques*)


(* ::Subsubtitle:: *)
(*Extraction des titres, auteurs, mots-clefs et dates de publication \[AGrave] partir d'un corpus de publications scientifiques au format PDF*)


(* ::Input::Initialization:: *)
BeginPackage["CERMINE`"];


(* ::Section:: *)
(*Partie publique du package*)


(* ::Subsection:: *)
(*Manipulation des fichiers de sortie de CERMINE*)


(* ::Input::Initialization:: *)
readCERMINEOutputs::usage="readCERMINEOutputs[pathCorpusPDF_] Chargement des fichiers de sortie de CERMINE \[AGrave] partir du chemin o\[UGrave] CERMINE a \[EAcute]t\[EAcute] ex\[EAcute]cut\[EAcute]. Ce chemin contient les publications scientifiques au format PDF";


(* ::Input::Initialization:: *)
checkCERMINEOutputsXMLIncompatible::usage="checkCERMINEOutputsXMLIncompatible[cermineOutputs_] V\[EAcute]rification de la compatibilit\[EAcute] des fichiers de sortie de CERMINE avec l'import au format XML dans Mathematica";


(* ::Input::Initialization:: *)
makeCERMINEOutputsXMLCompatible::usage="makeCERMINEOutputsXMLCompatible[cermineOutputs_] Si les fichiers de sortie de CERMINE ne sont pas compatibles avec l'import au format XML dans Mathematica, la premi\[EGrave]re ligne de chaque fichier de sortie de CERMINE est supprim\[EAcute]e. En effet, les fichiers de sortie de CERMINE comportent une premi\[EGrave]re ligne non compatible avec le format XML importable sous Mathematica. Cette op\[EAcute]ration est ex\[EAcute]cut\[EAcute]e une seule fois";


(* ::Input::Initialization:: *)
readCERMINEOutputsXML::usage="readCERMINEOutputsXML[cermineOutputs_] Chargement des fichiers de sortie de CERMINE au format XML";


(* ::Subsection:: *)
(*Extraction de connaissances \[AGrave] partir des fichiers de sortie de CERMINE*)


(* ::Input::Initialization:: *)
extractTitles::usage="extractTitles[cermineOutputsXML_] Extraction des titres de publications scientifiques \[AGrave] partir des fichiers de sortie de CERMINE au format .cermxml import\[EAcute]s en tant que XML";


(* ::Input::Initialization:: *)
extractAuthors::usage="extractAuthors[cermineOutputsXML_] Extraction des auteurs de publications scientifiques \[AGrave] partir des fichiers de sortie de CERMINE au format .cermxml import\[EAcute]s en tant que XML";


(* ::Input::Initialization:: *)
extractKeywords::usage"extractKeywords[cermineOutputsXML_] Extraction de la liste de mots-clefs associ\[EAcute]s \[AGrave] des publications scientifiques \[AGrave] partir des fichiers de sortie de CERMINE au format .cermxml import\[EAcute]s en tant que XML";


(* ::Input::Initialization:: *)
extractDates::usage"extractDates[cermineOutputsXML_] Extraction des dates associ\[EAcute]s \[AGrave] des publications scientifiques \[AGrave] partir des fichiers de sortie de CERMINE au format .cermxml import\[EAcute]s en tant que XML";


(* ::Section:: *)
(*Partie priv\[EAcute]e du package*)


(* ::Input::Initialization:: *)
Begin["Private`"];


(* ::Subsection:: *)
(*Manipulation des fichiers de sortie de CERMINE*)


(* ::Input::Initialization:: *)
readCERMINEOutputs[pathCorpusPDF_]:=
FileNames["*.cermxml",FileNameJoin[{pathCorpusPDF,"*"}]];


(* ::Input::Initialization:: *)
checkCERMINEOutputsXMLIncompatible[cermineOutputs_] :=
StringTake[
First[
Import[cermineOutputs[[1]], "Lines"]
],9
]== "<!DOCTYPE";


(* ::Input::Initialization:: *)
makeCERMINEOutputsXMLCompatible[cermineOutputs_]:=
If[checkCERMINEOutputsXMLIncompatible[cermineOutputs],
Map[
With[{output=#},
Export[output,Rest[Import[output,"Lines"]],"Text"]
]
&, cermineOutputs
],
"CERMINE outputs already XML-compatible"
];


(* ::Input::Initialization:: *)
readCERMINEOutputsXML[cermineOutputs_] :=
Map[Import[#,"XML"]&, cermineOutputs];


(* ::Subsection:: *)
(*Extraction de connaissances \[AGrave] partir des fichiers de sortie de CERMINE*)


(* ::Subsubsection:: *)
(*Extraction des titres de publications*)


(* ::Input::Initialization:: *)
extractTitles[cermineOutputsXML_] :=
With[
{
titles=
Map[
Cases[
Cases[#,XMLElement["title-group",_, t_] :> First@t,Infinity],
XMLElement["article-title",_, t_] :> First@t,Infinity]&,
cermineOutputsXML
]
},
Map[
Rule["Title", First[Flatten[#],""]]&,
titles
]
];


(* ::Subsubsection:: *)
(*Extraction des auteurs de publications*)


(* ::Input::Initialization:: *)
extractAuthors[cermineOutputsXML_] :=
With[{authors =Map[
Cases[
Cases[#, XMLElement["contrib-group",_,t_] :> t,Infinity],
XMLElement["string-name",_,t_]:> t,Infinity
]&,cermineOutputsXML]},
Map[
Rule["Authors",Flatten[#]]
&,authors]
];


(* ::Subsubsection:: *)
(*Extraction des mots-clefs associ\[EAcute]s aux publications*)


(* ::Input::Initialization:: *)
extractKeywords[cermineOutputsXML_] :=
With[{keywords =Map[
Cases[
Cases[#, XMLElement["kwd-group",_,t_] :> t,Infinity],
XMLElement["kwd",_,t_]:> t,Infinity
]&,cermineOutputsXML]},
Map[
Rule["Keywords",Flatten[#]]
&,keywords]
];


(* ::Subsubsection:: *)
(*Extraction des dates de publication*)


(* ::Input::Initialization:: *)
extractDates[cermineOutputsXML_] :=
Map[
Cases[
Cases[#, XMLElement["pub-date", _, t_] :> t,Infinity],
XMLElement["year", _, t_] :> t,Infinity]
&,cermineOutputsXML
];


(* ::Input::Initialization:: *)
End[];


(* ::Input::Initialization:: *)
EndPackage[];
